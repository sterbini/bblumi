# %%
from pyzotero import zotero

from datetime import datetime
def print_list(my_list):
    bib = ''
    for data in my_list:
        try:
            if len(data['creators']) == 0:
                author = None 
            elif  len(data['creators']) == 1:
                if 'firstName' not in data['creators'][0]:
                    author = data['creators'][0]['name']
                else:
                    author = data['creators'][0]['firstName'] + ' ' + data['creators'][0]['lastName']
            elif len(data['creators']) == 2:
                if 'firstName' not in data['creators'][0]:
                    author = data['creators'][0]['name'] + ' and ' + data['creators'][1]['name']
                else:
                    author = data['creators'][0]['firstName'] + ' ' + data['creators'][0]['lastName'] + ' and ' + data['creators'][1]['firstName'] + ' ' + data['creators'][1]['lastName']
            elif len(data['creators']) == 3:
                if 'firstName' not in data['creators'][0]:
                    author = data['creators'][0]['name'] + ', ' + data['creators'][1]['name'] + ' and ' + data['creators'][2]['name']
                else:
                    author = data['creators'][0]['firstName'] + ' ' + data['creators'][0]['lastName'] + ', ' + data['creators'][1]['firstName'] + ' ' + data['creators'][1]['lastName'] + ' and ' + data['creators'][2]['firstName'] + ' ' + data['creators'][2]['lastName']
            else:
                if 'firstName' not in data['creators'][0]:
                    author = data['creators'][0]['name'] + ' et al.'
                else:
                    author = data['creators'][0]['firstName'] + ' ' + data['creators'][0]['lastName'] + ' et al.'
            if author is None:
                bib += f"- [**\'{data['title']}\'**]({data['url']}), {data['date']}.\n"
            else:
                bib += f"- [**\'{data['title']}\'**]({data['url']}), *{author}*, {data['date']}.\n"
        except:
            print(f'Data: {data} has a problem')
    return bib

def convert_time_string(x):
    # if my_string contains two '-'
    if x.count('-') == 2:
        return datetime.strptime(x, '%Y-%m-%d')
    # if my_string contains one '-'
    elif x.count('-') == 1:
        return datetime.strptime(x, '%Y-%m')
    # if my_string contains no '-'
    elif x.count('-') == 0:
        return datetime.strptime(x, '%Y') 

def make_zotero_bib(zot):
    items = zot.items()
    my_list = [item['data'] for item in items if 'date' in item['data']]
    # my_list is a list of dictionaries. Can you sort the list by the key "date" of the dictionary? Please note that the date format is YYYY-M-D.
    for x in my_list:
        if 'date' not in x:
            print(x)
    my_list = sorted(my_list, key=lambda x: convert_time_string(x['date']))
    bib = f"From the corresponding [*Zotero* group library]({items[0]['library']['links']['alternate']['href']}).\n\n"
    bib += print_list(my_list)
    return bib

# %%
bib = '### References\n'

zot = zotero.Zotero(5576027, 'group')
bib += '\n#### Beam-beam effects\n\n'
bib += make_zotero_bib(zot)

zot = zotero.Zotero(5576798, 'group')
bib += '\n#### DA studies\n\n'
bib += make_zotero_bib(zot)

zot = zotero.Zotero(5581867, 'group')
bib += '\n#### Beam-Beam wire compensation\n\n'
bib += make_zotero_bib(zot)

zot = zotero.Zotero(5583046, 'group')
bib += '\n#### Crab cavities\n\n'
bib += make_zotero_bib(zot)

with open(f'./docs/dl2/bib.md', 'w') as file:
    file.write(bib)