# %%
import markdown
# add ../fill-tagger to the python path
import sys
import os
import glob
import pandas as pd
import ruamel.yaml
yaml = ruamel.yaml.YAML()
from datetime import datetime
import pytz

path_eos = '/eos/project/l/lhc-lumimod/web_site/'


def load_yaml_files(inpdir):
    _existing_yaml = glob.glob(f'{inpdir}/*.yaml')
    if len(_existing_yaml):
        print(f'>>> {len(_existing_yaml)} annotated fill yaml files found and will be loaded! ')
        _dlist = []
        for fyml in _existing_yaml:
            with open(fyml, 'r') as fin:
                aux = yaml.load(fin)
                aux['yaml_file'] = fyml
                _dlist.append(aux)
        _dfy = pd.DataFrame(_dlist)
        _dfy =  _dfy.sort_values(by='HX:FILLN', ascending=True).reindex()
        _dfy.dropna(subset=['HX:FILLN'], inplace=True)
        _dfy['fill'] = _dfy['HX:FILLN'].astype(int)
        _dfy.set_index('fill', inplace=True)
    else:
        _dfy = pd.DataFrame()
        print(f'>>> No yaml files found - should exit!')
    return _dfy# please note the there is a fill_tegger here

df = load_yaml_files("../fill-tagger/weekly_follow_up")
df = df[['HX:FILLN', 'tags', 'comment', 'yaml_file', 'start','end','duration']].sort_index(ascending=False)

#df['HX:FILLN'].apply(lambda x: f'[{x}](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/{x}.md) <br> <br> [:writing_hand:]() [:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9812.md)')

# %% make htlm table
html_table = df.to_html(classes='dataframe', index=False)

# HTML structure with DataTables
html = f'''
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>DataTable Example</title>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>
    <script>
        $(document).ready(function() {{
            $('.dataframe').DataTable();
        }});
    </script>
</head>
<body>
    {html_table}
</body>
</html>
'''

# Write the HTML to a file
with open(f'{path_eos}/overview.html', 'w') as file:
    file.write(html)

print(f'HTML file with sortable and filterable table has been created: {path_eos}/overview.html')

# get the friday of Week 10 of 2024
# %%

dl2_weekly = '### Weekly summaries 2024\n\n'

current_time_now_CET = pd.Timestamp(datetime.now(), tz='CET')

a = pd.Timestamp('2024-04-26 00:00:00', tz='CET')
b = a + pd.Timedelta('7D') - pd.Timedelta('0.000001s')
my_list = []
my_list_date = []
while a < current_time_now_CET:
    #my_list.append('[- From '+a.strftime('%d %B')+ ' to ' + b.strftime('%d %B')+f'](https://dltwo.web.cern.ch/{a.strftime("%Y_%m_%d.htlm")})\n')
    my_list_date.append([a,b])
    a = b + pd.Timedelta('0.000001s')
    b = a + pd.Timedelta('7D') - pd.Timedelta('0.000001s')

# invert list order
#my_list = my_list[::-1]

for ii in my_list_date[::-1]:
    a = ii[0]
    b = ii[1]
    dl2_weekly += '#### [From '+a.strftime('%d %B')+ ' to ' + b.strftime('%d %B')+f'](https://dltwo.web.cern.ch/{a.strftime("%Y_%m_%d.htlm")})\n' + '\n'
    dl2_weekly += f'<iframe seamless src="https://dltwo.web.cern.ch/{a.strftime("%Y_%m_%d.htlm")}" width="1200" height="350"></iframe>\n\n'

import datetime
from backend_plot import load_fills_filtered, get_dict_fills_data, plot_fill_data
from bokeh.io import output_notebook
from bokeh.models import Model
from IPython.display import IFrame

# Ensure Bokeh Loaded properly
output_notebook()

for ii in my_list_date:
    a = ii[0]
    b = ii[1]
    html_filepath = path_eos + a.strftime("%Y_%m_%d.htlm")
    #if html_filepath exists, skip
    if os.path.exists(html_filepath) and not (ii == my_list_date[-1] or ii == my_list_date[-2]):
        print(f'{html_filepath} exists')
    else:
        # Load fills between requested times
        path_parquet = "/eos/project/l/lhc-lumimod/LuminosityFollowUp/2024/fills-info/fills_and_bmodes_2024.parquet"
        start_time = a
        end_time = b
        fills_filtered = load_fills_filtered(path_parquet, start_time, end_time)

        # Get variables of interest for the current fill
        # Energy is always assumed to be present, as the first variable
        variables = [
            "LHC.BCCM.B1.A:BEAM_ENERGY",
            "LHC.BCTDC.A6R4.B1:BEAM_INTENSITY",
            "LHC.BCTDC.A6R4.B2:BEAM_INTENSITY",
        ]
        dict_fills = get_dict_fills_data(
            fills_filtered,
            path_raw_data="/eos/project/l/lhc-lumimod/LuminosityFollowUp/2024/rawdata/",
            path_tag_files="../fill-tagger/weekly_follow_up/",
            variables=variables,
            verbose = True,
        )
        print(dict_fills)

        # Build a dictionnary of variables to plot
        dict_var = {
            'intensity_b1': {
                'ax': "intensity",
                "full_name": "LHC.BCTDC.A6R4.B1:BEAM_INTENSITY",
                "color": "skyblue",
                'start': 0, 
                'end': 5e14,
            },
            'intensity_b2': {
                'ax': "intensity",
                "full_name": "LHC.BCTDC.A6R4.B2:BEAM_INTENSITY",
                "color": "salmon",
                'start': 0, 
                'end': 5e14,
            }
        }
        plot_output = plot_fill_data(
            dict_fills = dict_fills,
            start_time = start_time,
            end_time = end_time,
            dict_var = dict_var,
            color_energy_even="yellowgreen",
            color_energy_odd="peru",
            clickable_link=True,
            save_html = True,
            html_filepath = html_filepath,
            verbose = False,
        )
        print('The plot has been saved to', html_filepath)
        # Need to clean bokeh document before displaying
        #for model in plot_output.select({'type': Model}):
        #    prev_doc = model.document
        #    model._document = None
        #    if prev_doc:
        #        prev_doc.remove_root(model)

        # Display final plot      
    #    show(plot_output)

with open(f'./docs/dl2/dl2_weekly.md', 'w') as file:
    file.write(dl2_weekly)
# %% DL2 file

# add all the fill of df to the dl2 string
df = df.reset_index()[['fill','tags', 'comment', 'yaml_file', 'start','end','duration']]

aux_1  = df['fill'].apply(lambda x: f'[**{x}**](../fills/2024/{x}/index.md) [:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/{x}.md)')
aux_2 = df['yaml_file'] = df['yaml_file'].apply(lambda x: f'[:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/{x.split("/")[-1]})')

# apply a strftime to convert for example in 01 Mar (not March) 10:00

def duration(x):
    try:
        return f'**{x.days}d {x.seconds//3600}h {(x.seconds//60)%60}**'
    except:
        return ''
    
# making the folders
for ii in df['fill']:
    if int(ii) > 9850:
        print(ii)
        # make a folder with the fill number in /eos/project/l/lhc-lumimod/web_site/ if it does not exist
        if not os.path.exists(f'./docs/fills/2024/{ii}'):
            os.system(f'mkdir ./docs/fills/2024/{ii}')
            md_string=f'### FILL {ii}\n'
            with open(f'./docs/fills/2024/{ii}/index.md', 'w') as file:
                file.write(md_string)

aux3 = df['start'].apply(lambda x: pd.Timestamp(x).tz_convert('CET').strftime('%d.%m %H:%M'))
aux4 = df['end'].apply(lambda x: pd.Timestamp(x).tz_convert('CET').strftime('%d.%m %H:%M'))
aux5 = df['duration'].apply(lambda x: duration(pd.Timedelta(x)))
df['fill'] = aux_1 + ' ' + aux_2 + '<br>' + aux3 + '<br>' + aux4 + '<br>' + aux5
df  = df[['fill','tags', 'comment']]




def _format_tags(x):
    try:
        return '<br>'.join(x)
    except:
        return ''
    
#make capital the first letter of my_string
def _capitalize_first_letter(my_string):
    return my_string[0].upper() + my_string[1:]
    
def _format_comment(x):
    try:
        aux = '<br>'.join(['- ' + y[0].upper() + y[1:] for y in x])
        if len(aux) > 2:
            return aux
        else:
            return ''
    except:
        return ''
df['tags'] = df['tags'].apply(lambda x: _format_tags(x))
df['comment'] = df['comment'].apply(lambda x: _format_comment(x))
#    
# %% make bokeh plots
dl2 = '''
### 2024
 
#### Weekly summaries
The weekly summaries bokeh plots [here](dl2_weekly.md).

#### 2024 Fills

'''


dl2 += df.to_markdown(index=False)

dl2 += '''\n

#### DL2 INDICO link
You can find the DL2 INDICO page [here](https://indico.cern.ch/category/9172/).

#### HTML table
You can find an html table of the 2024 fills [here](https://dltwo.web.cern.ch/overview.html) 

#### LHC Page 1
[Here](https://op-webtools.web.cern.ch/vistar/?usr=LHC1) you can find the LHC Page 1.

#### How to edit the fill tags/comments

Please refer to the following gist:

<script src="https://gist.github.com/sterbini/b9c48fb02346c45c0a825e6e490b66af.js"></script>

'''



# write the dl2 string to a file
with open(f'./docs/dl2/index.md', 'w') as file:
    file.write(dl2)

# run bip_script.py
print('>>> Running bib_script.py')
os.system('python bib_script.py')
# %%
# run bip_script.py
print('>>> Running make_fill_page.py')
os.system('python make_fill_page.py')
