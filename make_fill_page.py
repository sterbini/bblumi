# %%
import glob
import pandas as pd
import os
my_year = 2024
# make a list of fills, my_fills, from 9894 to 9900

# read the csv file in ../fill-tagger/2024.csv
my_df = pd.read_csv('../fill-tagger/2024.csv')
my_fills = [ii for ii in range(9323, int(my_df['HX:FILLN'].max())+1)]


# %%
import numpy as np
from datetime import datetime
from datetime import datetime

def convert_to_readable_format(timestamp_str):
    """
    Convert a timestamp string to a more readable format.
    
    Args:
    timestamp_str (str): The original timestamp string in the format 'YYYY-MM-DD HH:MM:SS.ssssss+00:00'.
    
    Returns:
    str: The formatted, human-readable timestamp string.
    """
    # Parse the timestamp string into a datetime object
    # Truncate the extra precision
    parsed_datetime = datetime.fromisoformat(timestamp_str[:26] + timestamp_str[-6:])
    
    # Format the datetime object into a human-readable string
    readable_str = parsed_datetime.strftime('%B %d, %Y %I:%M:%S %p %Z')
    
    return readable_str

def list_to_markdown(input_list, comment=True):
    """
    Transforms a list of strings into a markdown-formatted list.
    
    Args:
    input_list (list of str): The list of strings to transform.

    Returns:
    str: The markdown-formatted list as a string.
    """
    if not input_list:
        return ""
    if str(input_list) == 'nan':
        return ""   
    if len(input_list) == 0:
        return ""
    print(input_list)
    if comment:
        markdown_list = [f"- {item}\n\n" for item in eval(input_list)]
    else:
        markdown_list = [f"- {item[1:]}\n\n" for item in eval(input_list)]
    # concatanate the list of strings
    return "".join(['\n\n']+markdown_list)

def add_plot(my_string, file_name, my_description):
    my_web_file = file_name.replace('/eos/project/l/lhc-lumimod/web_site/','https://dltwo.web.cern.ch/')
    # check if the file exists
    if os.path.exists(file_name):
        my_string += my_description

        if file_name.endswith('.pdf'):
            aux = f'''<object data="{my_web_file}" width="700px" height="700px">
            <embed src="{my_web_file}">
                <p>This browser does not support PDFs. Please download the PDF to view it: <a href="{my_web_file}">Download PDF</a>.</p>
            </embed>
        </object>'''
            my_string += f'{aux}\n\n'
        elif file_name.endswith('.png'):
            my_string += f'![]({my_web_file})\n\n'
        else:
            print(f'\tFile {file_name} has an unknown extension')
            return my_string
    
    # in ../docs/dl2 make a fill for my_fill.md
    else:
        print(f'\tFile {file_name} does not exist')
    return my_string

def write_fill_page(my_fill):
    # search for the file ending with '9900.yaml' in ../fill-tagger/weekly_follow_up
    my_file_name = glob.glob(f'../fill-tagger/weekly_follow_up/*{my_fill}.yaml')[0].split('/')[-1]
    fill_info = my_df[my_df["HX:FILLN"] == my_fill].iloc[0]

    my_string = (f'''# FILL {my_fill}\n\n 
### Summary\n\n
[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/{my_fill}.md) '''+
f'''[:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/{my_file_name}) ''' +
f'''[:arrow_left:](../{my_fill-1}/index.md) '''+
f'''[:arrow_right:](../{my_fill+1}/index.md)\n\n''')
    
    #my_string += f'**next fill**: [{my_fill+1}](../{my_fill+1}/index.md)\n\n'
    #my_string += f'**previous fill**: [{my_fill-1}](../{my_fill-1}/index.md)\n\n'

    my_string += f'**start**: \t{convert_to_readable_format(fill_info["start"])}\n\n'
    my_string += f'**end**: \t\t{convert_to_readable_format(fill_info["end"])}\n\n'
    my_string += f'**duration**: {fill_info["duration"]}\n\n'
    my_string += f'**tags**:\t {list_to_markdown(fill_info["tags"], comment=False)}\n\n'
    print(fill_info["comment"])
    my_string += f'**comments**:\t {list_to_markdown(fill_info["comment"], comment=True)}\n\n'

   

    
    my_path = '/eos/project/l/lhc-lumimod/web_site/followup_plots/2024/'

    ###### Property of the machine
    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Cycle_duration.png',
            '\n#### Cycle duration\n\n')
    

    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Fill_betastar.png',
            '\n#### Beta-functions at the IPs\n\n')
    

    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Fill_xing.png',
            '#### Crossing angles\n\n\n')
    
   
    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Fill_octupoles.png',
            '#### Octupoles\n\n\n')
    
    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Fill_trims.png',
            '#### Trims\n\n\n##### Tune trims\n\n')
    
    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Fill_chromaticity.png',
            '##### Chromaticity Trims\n\n')
    
    ###### Intensity

    my_string += '#### Intensity\n\n\n'
    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Cycle_intensity.png',
            '##### Cycle intensity\n\n')
    
    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/bbb_SB_intensity.png',
            '##### BBB SB intensity\n\n')
    
    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Cycle_violin_intensity_bl.png',
            '##### Violin BBB intensity\n\n')

    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Fill_adjust_stable_losses_summary.png',
            '##### SB bunch intensity \n\n')
    
    ###### Bunch length
    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Cycle_bunchlength.png',
            '#### Bunch Length\n\n\n')

    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Fill_bunch_length.png',
            '##### Bunch Length Fill\n\n\n')


    ###### Emittance
    my_string = my_string + '#### Emittance\n\n\n'
    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Cycle_emit_b1.png',
            '##### B1 along the cycle\n\n')
    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Cycle_emit_b2.png',
            '##### B2 along the cycle\n\n')
    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Fill_injection_emit_blowup.png',
            '##### Emittance growth at injection\n\n')   
    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Cycle_violin_emit.png',
            '##### Summary violin plot\n\n')
    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/bbb_SB_B1_emit.png',
            '##### BBB in SB for B1 \n\n')
    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/bbb_SB_B2_emit.png',
            '##### BBB in SB for B2 \n\n')
    
    ###### Bunch brightness
    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Cycle_brightness.png',
            '#### Bunch brightness\n\n\n')

    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Cycle_summary.png',
            '#### Beam properties summary \n\n')




    ###### Luminosity
    my_string = my_string + '#### Luminosity\n\n\n'

    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Fill_luminosity.png',
            '##### Overview\n\n')

    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Fill_luminous_regions.png',
            '##### Luminous Regions\n\n')

    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/bbb_SB_luminosity.png',
            '##### BBB luminosity in SB\n\n')
    
    ###### Cross-sections

    my_string = my_string + '#### Cross-sections\n\n\n'
    my_string += '##### ADJUST\n\n'

    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Xsection_ADJUST_heatmap.png',
            '')
    
    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Xsection_ADJUST_mean.png',
            '')
    
    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Xsection_ADJUST_bbb_ip15lr.png',
            '')
    
    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Xsection_ADJUST_bbb_ip2ho.png',
            '')    

    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Xsection_ADJUST_bbb_lhcbho.png',
            '')
    
    my_string += '##### STABLE BEAM\n\n'
    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Xsection_STABLE_heatmap_DBLM.png',
            '')
    
    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Xsection_SB_mean_DBLM.png',
            '')
    
    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Xsection_SB_mean_DBLM_separation.png',
            '')
        
    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Xsection_SB_bbb_DBLM_ip15lr.png',
            '')
    
    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Xsection_SB_bbb_DBLM_ip2ho.png',
            '')
    
    my_string = add_plot(my_string, 
            f'{my_path}{my_fill}/Xsection_SB_bbb_DBLM_lhcbho.png',
            '')

    
    #my_string = add_plot(my_string, 
    #         f'{my_path}{my_fill}/Fill{my_fill}_summary.pdf',
    #         '#### PDF summary\n\n\n')
    

    #if the folder does not exist, create it
    if not os.path.exists(f'./docs/fills/{my_year}/{my_fill}'):
        os.system(f'mkdir ./docs/fills/{my_year}/{my_fill}')

    with open(f'./docs/fills/{my_year}/{my_fill}/index.md', 'w') as file:
        file.write(my_string)
    
for my_fill in my_fills[-1::-1]:
    print('Writing fill ', my_fill)
    write_fill_page(my_fill)
# %%
