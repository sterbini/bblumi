In this repository we will collect info, tools, hints, tricks, snippet of codes and links useful for the **beam-beam and luminosity follow-up and studies** (DL2). 
It is *python* oriented.

We would like to share with you our working flow and to hear from you about yours.

We organized the site the following sections:

- **DL2**: for the fill by fill follow-up
- **How-tos**: examples on how to use the tools. This is the key section of the site, where we will describe simple use case to share the user experience.
- **Utilies and tricks**: suggestions and utilities to improve your working flow and productivity.
- **Tools**: a list of tools and links with a short description. An extended list of codes can be found at the [ABP-CWG web site](https://twiki.cern.ch/twiki/bin/view/ABPComputing/Software).
- **Links**: a collection of web-sites where the bblumi information are circulated.
- **On-the-spot**: we will collect there, on weekly basis, article of general interest in the Accelerator Physics domain.
- **Monitoring**: a section from Philippe Bélanger.

We suggest to use the site **Search** functionalities to look for a specific keyword.


!!! info "Share your tools, tricks and how-tos with us."

The platform is based on [mkdocs](https://gitlab.cern.ch/authoring/documentation/mkdocs) + [gitlab](https://gitlab.cern.ch/). 
To edit this site just click the pencil symbol at the top-right.

