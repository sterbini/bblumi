
## Burn-off efficiency
---
A common figure of merit used to quantify the condition of the LHC is the lifetime of the beam. It provides important information for the LHC as a storage ring, but does not inform about the luminosity production of the collider. Instead, it is more appropriate to define the instantaneous burn-off efficiency $\eta$, following:

$$ \eta \equiv \left[\frac{dN}{dt}\right]_\text{bo}\bigg/ \left[\frac{dN}{dt}\right]_\text{total} = \frac{\sigma_\text{pp} \mathcal{L}}{\sigma_\text{pp} \mathcal{L} + R_\ell N} $$

where $\left[\tfrac{dN}{dt}\right]_\text{total}$ and $\left[\tfrac{dN}{dt}\right]_\text{bo}$ are the total and burn-off losses, $N$ is the beam intensity, $\mathcal{L} = \sum_{\text{IPs}}\mathcal{L}_\text{IP}$ is the total luminosity of the collider, $R_\ell$  is the loss rate from non-burn-off losses and $\sigma_\text{pp} = 80\ \text{mb}$ is the accepted value for the cross section of proton-proton collisions at 7 TeV. The efficiency is similar to the effective cross section $\sigma_\text{eff}= \frac{1}{\mathcal{L}}\left[\sigma_\text{pp} \mathcal{L} +  R_\ell N\right]$ used in previous work. One can note that $\eta = \sigma_\text{pp}/\sigma_\text{eff}$. To optimise luminosity production, a well-behaved machine needs to lose most of its protons to collisions, i.e.  $\eta \to 1$. Assuming small $R_\ell$ over a short period of time compared to the length, $T$, of luminosity production for a fill, one can show that the relative gain in integrated luminosity, $G$, scales with the integral of $\eta(t)$, or more precisely:
$$	G \sim  -r_0\left[\frac{r_0 T + 2}{r_0 T + 1}\right] \cdot \int_0^T\left(\frac{1}{\eta} - 1\right)\ dt $$

where $r_0 = \sigma_\text{pp} \mathcal{L}_0/N_0$ is a constant which depends on the initial conditions (with units of \unit{s^{-1}}). Alternatively, one can see that $\left(\frac{1}{\eta} - 1\right) = \frac{R_\ell N}{\sigma_\text{pp} \mathcal{L}}$ corresponds to the luminosity-normalized losses of the machine.
For example, if $\eta = 0.5$ for 30 minutes in typical LHC conditions ($N_0 = 2400\cdot 1.3\times 10^{11}\ \text{p}^+$ and $\mathcal{L}_0 = 2\times (2.2\times10^{34}\ \text{Hz/cm}^2)$), the change in integrated luminosity is $G \sim -3\ \%$ for $T=10\ \text{h}$. Hence, the experimental efficiency is to be discussed on the basis of its deviation from $\eta=1$ over a certain period of time.

---
    
    
### Special fills

|Fill|Comments|Efficiency|B-by-B signature|
| :---: | :---: | :---: |  :---: | 
|**8348**| MD8043, Nov 2022 |  [**DBLM**](https://phbelang.web.cern.ch/Monitoring_efficiency/DBLM/FILL8348.html){target=_blank}  \| [**BCTF**](https://phbelang.web.cern.ch/Monitoring_efficiency/BCTF/FILL8348.html){target=_blank} |[**DBLM**](https://phbelang.web.cern.ch/Monitoring_bbbsignature/DBLM/FILL8348.html){target=_blank}  \| [**BCTF**](https://phbelang.web.cern.ch/Monitoring_bbbsignature/BCTF/FILL8348.html){target=_blank} |
|**8462**| MD7003, Nov 2022  |  [**DBLM**](https://phbelang.web.cern.ch/Monitoring_efficiency/DBLM/FILL8462.html){target=_blank}  \| [**BCTF**](https://phbelang.web.cern.ch/Monitoring_efficiency/BCTF/FILL8462.html){target=_blank} |[**DBLM**](https://phbelang.web.cern.ch/Monitoring_bbbsignature/DBLM/FILL8462.html){target=_blank}  \| [**BCTF**](https://phbelang.web.cern.ch/Monitoring_bbbsignature/BCTF/FILL8462.html){target=_blank} |
|**8469**| MD7003, Nov 2022  |  [**DBLM**](https://phbelang.web.cern.ch/Monitoring_efficiency/DBLM/FILL8469.html){target=_blank}  \| [**BCTF**](https://phbelang.web.cern.ch/Monitoring_efficiency/BCTF/FILL8469.html){target=_blank} |[**DBLM**](https://phbelang.web.cern.ch/Monitoring_bbbsignature/DBLM/FILL8469.html){target=_blank}  \| [**BCTF**](https://phbelang.web.cern.ch/Monitoring_bbbsignature/BCTF/FILL8469.html){target=_blank} |
|**8470**| MD7003, Nov 2022  |  [**DBLM**](https://phbelang.web.cern.ch/Monitoring_efficiency/DBLM/FILL8470.html){target=_blank}  \| [**BCTF**](https://phbelang.web.cern.ch/Monitoring_efficiency/BCTF/FILL8470.html){target=_blank} |[**DBLM**](https://phbelang.web.cern.ch/Monitoring_bbbsignature/DBLM/FILL8470.html){target=_blank}  \| [**BCTF**](https://phbelang.web.cern.ch/Monitoring_bbbsignature/BCTF/FILL8470.html){target=_blank} |





### Fill-by-fill monitoring
Extra fill-by-fill summary data can be found on the [BPT Dashboard](https://bpt.web.cern.ch/lhc/supertable/2023/){target=_blank}



