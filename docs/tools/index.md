On this page wee collect the tools we are using in our working flow.

### PyComplete
(Py)thon (Co)llective (M)acro-(P)article Simulation (L)ibrary with (E)xtendible (T)racking (E)lements
https://github.com/PyCOMPLETE

### pytimber
This is a python package used as wrapper for the JAVA API of the CERN Accelerator Logging System.

https://github.com/rdemaria/pytimber

??? info "Please note that the [NXCALS interface](https://codimd.web.cern.ch/p/SyHOT7mwI#){target = _blank} is being prepared." 

### cl2pd
This is a python package used as wrapper for [pytimber](https://github.com/rdemaria/pytimber) and exporting data in [pandas](https://pandas.pydata.org/) dataframes.

The master version is compatible with Py2 but we suggest to use the branch ["py3"](https://github.com/sterbini/cl2pd/tree/py3) (compatible with Py3).

https://github.com/sterbini/cl2pd

### cpymad
cpymad is a [Cython](https://cython.org/) binding to [MAD-X](https://cern.ch/mad) for giving full control and access to a
MAD-X interpreter within the python environment.

This version of cpymad should be built against MAD-X. It can be
installed on linux, Windows and Mac.
Python 3 is recommended but not (yet) required.

https://github.com/hibtc/cpymad

### pyjapc
PyJapc is a Python to FESA/LSA/INCA interface via JAPC.

https://gitlab.cern.ch/scripting-tools/pyjapc

### pjlsa
A Python wrapping of LSA API.

https://gitlab.cern.ch/scripting-tools/pjlsa

### pyNAFF
A Python module that implements the Numerical Analysis of Fundamental Frequencies method of J. Laskar. 

https://pypi.org/project/PyNAFF/

### pyTrain
A first attempt to make the TRAIN program scriptable from Python. Needs TRAIN from
https://gitlab.cern.ch/agorzaws/train to run.

https://gitlab.cern.ch/mihostet/pytrain

### pyHTC
<a href="https://github.com/apoyet/pyHTC" target="_blank">pyHTC</a> is a python package provides a more convenient environment for the submission of  multiple jobs to the HTCondor cluster system used at CERN. 






