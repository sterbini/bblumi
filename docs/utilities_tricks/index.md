This page presents a collections of useful utilities and tricks to improve the daily workflow. 

### O'Reilly
CERN provides access to O'Reilly resources at this

[link](https://home.cern/news/announcement/cern/cern-community-can-access-e-books-videos-and-audiobooks-oreilly){target=_blank} 
or, directly,

[here](https://learning.oreilly.com/home/){target=_blank}.

---

### VS Studio
We suggest to use as text editor (integrated with Git/GitLab/GitHub) [VS Studio](https://code.visualstudio.com/){target=_blank}.

---

### Maxima

As an alternative of Mathematica or sympy from python you can test [Maxima](https://wxmaxima-developers.github.io/wxmaxima/index.html){target=_blank}.

---

### Data from a plot

If you want import data from a plot that you have in png/gif/... format you can try [WebPlotDigitizer](https://automeris.io/WebPlotDigitizer/){target=_blank} 

---

### Blender for 3D graphics

Just for fun, you can do nice animation using [Blender](https://www.blender.org/){target=_blank}  (quite a complex code). 
Perhaps there is a potential to visualize your problem.

---

### Git and Overleaf

You can *git clone/push/pull* a Overleaf repository. You find the git address of your project on the  Menu > Git. 
Pay attention to respect the Overleaf naming guidelines (no empty spaces in file names).

---

### High quality plots

In order to improve the editorial quality of your plot in $\LaTeX$ documents you can use the [tikzplotlib](https://pypi.org/project/tikzplotlib/){target=_blank} package.

--- 

### Trello for your work planning
An interesting utility for planning your work is [Trello](https://trello.com/){target=_blank}.
This is not integrated with the CERN IT services but we are not aware of an alternatice CERN IT service. 

---

### Mattermost at CERN

Mattermost is an open-source, self-hostable online chat service.
It is designed as an internal chat for organisations and companies, and mostly markets itself as an alternative to Slack.
[https://mattermost.web.cern.ch/](https://mattermost.web.cern.ch/){target=_blank}

---

### Markdown Cheatsheet

To improve your editing skills in Markdown (and therefore, for this site), you can find useful tips in the cheatsheet available by clicking
[https://3os.org/markdownCheatSheet](https://3os.org/markdownCheatSheet/welcome/){target=_blank}.

---

### CERN or commercial provider?
CERN IT offers many services which are comparable to ones offered by commercial providers. 
[Here](http://information-technology.web.cern.ch/services/cern-commercial){target=_blank} is the complete comparison list.

---

### SWAN
SWAN (Service for Web based ANalysis) is a platform to perform interactive data analysis in the cloud.

[https://swan.web.cern.ch/](https://swan.web.cern.ch/){target=_blank}

In particular have a look to the [Example Galleries](https://swan.web.cern.ch/content/accelerator-complex){target=_blank}.

---

### CodiMD
For sharing and sketching your idea in a fast a practical way.

[https://codimd.web.cern.ch/](https://codimd.web.cern.ch/){target=_blank}

---

### Mendeley
Mendeley is a desktop and web program produced by Elsevier for managing and sharing research papers,discovering research data and collaborating online.

[https://www.mendeley.com/](https://www.mendeley.com/){target=_blank}

See also a [CERN Bulletin article on mendeley](https://cds.cern.ch/journal/CERNBulletin/2012/30/News%20Articles/1462800?ln=en){target=_blank}.

---

### Overleaf
Use [Overleaf](https://www.overleaf.com/org/cern){target=_blank} for latex concurrent editing. It features also [review capabilities](https://www.overleaf.com/track-changes-and-comments-in-latex){target=_blank}.

---

### Mathpix
Take a screenshot of math and paste the $\LaTeX$ into your editor, all with a single keyboard shortcut.

[https://mathpix.com/](https://mathpix.com/){target=_blank}

---

### UDEMY for CERN
[UDEMY](https://www.udemy.com/){target=_blank} is an elearning platform offering on-line courses (they are not for free, see [wiki](https://en.wikipedia.org/wiki/Udemy){target=_blank}).
There is a UDEMY/CERN agreement described [here](https://lms.cern.ch/ekp/servlet/ekp?PX=N&TEACHREVIEW=N&CID=EKP000043153&TX=FORMAT1&LANGUAGE_TAG=en&DECORATEPAGE=N){target=_blank}.

---

### *git* and terminal commands

A synthetic list of the most frequently used git command can be found [here](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html){target=_blank}.

---

### 10 ways to filter a *pandas* dataframe

Filtering (sub-setting) a collection of information is a typical task in data crunching. 
This [article](https://www.listendata.com/2019/07/how-to-filter-pandas-dataframe.html?m=1){target=_blank} presents 10 ways to do it usign *pandas* dataframes.

---

### Link Mendeley to Overleaf

It is possible to import directly the bibliography of [Mendeley](https://www.mendeley.com/){target=_blank} on [Overleaf](https://www.overleaf.com/){target=_blank}
(see [here](https://www.overleaf.com/blog/639-tip-of-the-week-overleaf-and-reference-managers){target=_blank}).
Therefore is very convenient to share your bibliography's presentation using a Mendeley group (see an example [here](https://www.mendeley.com/community/bb-compensation-with-a-dc-wire/){target=_blank}).
