# FILL 10328

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10328.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_06_05_47_14_F10328.yaml) [:arrow_left:](../10327/index.md) [:arrow_right:](../10329/index.md)

**start**: 	November 06, 2024 05:47:14 AM UTC

**end**: 		November 06, 2024 07:53:32 AM UTC

**duration**: 0 days 02:06:18.444125

**tags**:	 

- ION_PHYSICS



**comments**:	 

- trip of FMCM_RQ5.LR3




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10328/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10328/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10328/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10328/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10328/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10328/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

