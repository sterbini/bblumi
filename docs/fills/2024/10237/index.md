# FILL 10237

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10237.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_17_14_09_20_F10237.yaml) [:arrow_left:](../10236/index.md) [:arrow_right:](../10238/index.md)

**start**: 	October 17, 2024 02:09:20 PM UTC

**end**: 		October 17, 2024 02:21:42 PM UTC

**duration**: 0 days 00:12:21.746000

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

