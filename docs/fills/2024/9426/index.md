# FILL 9426

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9426.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_26_17_18_58_F9426.yaml) [:arrow_left:](../9425/index.md) [:arrow_right:](../9427/index.md)

**start**: 	March 26, 2024 05:18:58 PM UTC

**end**: 		March 26, 2024 05:28:08 PM UTC

**duration**: 0 days 00:09:09.276375

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

