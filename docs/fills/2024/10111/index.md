# FILL 10111

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10111.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_13_18_43_41_F10111.yaml) [:arrow_left:](../10110/index.md) [:arrow_right:](../10112/index.md)

**start**: 	September 13, 2024 06:43:41 PM UTC

**end**: 		September 13, 2024 08:10:20 PM UTC

**duration**: 0 days 01:26:39.149875

**tags**:	 

- PHYSICS

- QPS_DUMP



**comments**:	 

- QPS trigger a dump during the injection plateau. The RQ10.L8 quenched (looks QPS induced)

- Before the dump, last injection in B2 (L8) was ~30s before and did not show any unusual losses or trajectory




#### Cycle duration

![](https://dltwo.web.cern.ch/followup_plots/2024/10111/Cycle_duration.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10111/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10111/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10111/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10111/Fill_chromaticity.png)

#### Intensity


##### Cycle intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/10111/Cycle_intensity.png)

##### Violin BBB intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/10111/Cycle_violin_intensity_bl.png)

#### Bunch Length


![](https://dltwo.web.cern.ch/followup_plots/2024/10111/Cycle_bunchlength.png)

#### Emittance


##### B1 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/10111/Cycle_emit_b1.png)

##### B2 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/10111/Cycle_emit_b2.png)

##### Emittance growth at injection

![](https://dltwo.web.cern.ch/followup_plots/2024/10111/Fill_injection_emit_blowup.png)

##### Summary violin plot

![](https://dltwo.web.cern.ch/followup_plots/2024/10111/Cycle_violin_emit.png)

#### Bunch brightness


![](https://dltwo.web.cern.ch/followup_plots/2024/10111/Cycle_brightness.png)

#### Beam properties summary 

![](https://dltwo.web.cern.ch/followup_plots/2024/10111/Cycle_summary.png)

#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

