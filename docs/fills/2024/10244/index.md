# FILL 10244

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10244.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_18_20_28_07_F10244.yaml) [:arrow_left:](../10243/index.md) [:arrow_right:](../10245/index.md)

**start**: 	October 18, 2024 08:28:07 PM UTC

**end**: 		October 18, 2024 10:04:06 PM UTC

**duration**: 0 days 01:35:58.545625

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

