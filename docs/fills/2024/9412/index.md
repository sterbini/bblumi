# FILL 9412

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9412.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_23_11_33_49_F9412.yaml) [:arrow_left:](../9411/index.md) [:arrow_right:](../9413/index.md)

**start**: 	March 23, 2024 11:33:49 AM UTC

**end**: 		March 23, 2024 12:55:49 PM UTC

**duration**: 0 days 01:21:59.276000

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- nominal bunch ramps for BI tests



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

