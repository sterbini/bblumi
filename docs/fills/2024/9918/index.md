# FILL 9918

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9918.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_20_14_24_54_F9918.yaml) [:arrow_left:](../9917/index.md) [:arrow_right:](../9919/index.md)

**start**: 	July 20, 2024 02:24:54 PM UTC

**end**: 		July 20, 2024 03:45:00 PM UTC

**duration**: 0 days 01:20:06.174625

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

