# FILL 9381

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9381.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_18_12_57_45_F9381.yaml) [:arrow_left:](../9380/index.md) [:arrow_right:](../9382/index.md)

**start**: 	March 18, 2024 12:57:45 PM UTC

**end**: 		March 18, 2024 02:29:28 PM UTC

**duration**: 0 days 01:31:42.531500

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

