# FILL 10280

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10280.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_25_03_54_08_F10280.yaml) [:arrow_left:](../10279/index.md) [:arrow_right:](../10281/index.md)

**start**: 	October 25, 2024 03:54:08 AM UTC

**end**: 		October 25, 2024 11:40:35 AM UTC

**duration**: 0 days 07:46:27.136500

**tags**:	 

- BEAM_COMMISSIONING

- ION_OPTICS



**comments**:	 

- Ion cycle setup with ions (ramping and testing the collapse, steering with DOROS)



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

