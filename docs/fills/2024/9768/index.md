# FILL 9768

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9768.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_09_16_42_35_F9768.yaml) [:arrow_left:](../9767/index.md) [:arrow_right:](../9769/index.md)

**start**: 	June 09, 2024 04:42:35 PM UTC

**end**: 		June 09, 2024 04:52:56 PM UTC

**duration**: 0 days 00:10:20.519375

**tags**:	 

- MD



**comments**:	 

- MD11703



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

