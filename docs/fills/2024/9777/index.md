# FILL 9777

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9777.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_14_20_28_11_F9777.yaml) [:arrow_left:](../9776/index.md) [:arrow_right:](../9778/index.md)

**start**: 	June 14, 2024 08:28:11 PM UTC

**end**: 		June 15, 2024 02:13:27 AM UTC

**duration**: 0 days 05:45:15.769000

**tags**:	 

- DUCK



**comments**:	 

- Difficult recovery from the TS1 (MKI, RB.A56, ADT...)

- Access was needed



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

