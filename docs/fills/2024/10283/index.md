# FILL 10283

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10283.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_26_04_22_02_F10283.yaml) [:arrow_left:](../10282/index.md) [:arrow_right:](../10284/index.md)

**start**: 	October 26, 2024 04:22:02 AM UTC

**end**: 		October 26, 2024 05:30:45 AM UTC

**duration**: 0 days 01:08:43.382250

**tags**:	 

- BEAM_COMMISSIONING

- ION_OPTICS



**comments**:	 

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10283/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10283/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10283/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10283/Fill_chromaticity.png)

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

