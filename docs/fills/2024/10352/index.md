# FILL 10352

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10352.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_11_15_09_08_F10352.yaml) [:arrow_left:](../10351/index.md) [:arrow_right:](../10353/index.md)

**start**: 	November 11, 2024 03:09:08 PM UTC

**end**: 		November 11, 2024 03:29:07 PM UTC

**duration**: 0 days 00:19:58.430500

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

