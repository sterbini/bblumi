# FILL 9732

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9732.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_07_23_00_09_F9732.yaml) [:arrow_left:](../9731/index.md) [:arrow_right:](../9733/index.md)

**start**: 	June 07, 2024 11:00:09 PM UTC

**end**: 		June 08, 2024 12:09:08 AM UTC

**duration**: 0 days 01:08:59.192625

**tags**:	 

- MD



**comments**:	 

- MD10483, LHC injection SIS frozen




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9732/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9732/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9732/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9732/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9732/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9732/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

