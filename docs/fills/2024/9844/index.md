# FILL 9844

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9844.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_01_00_16_12_F9844.yaml) [:arrow_left:](../9843/index.md) [:arrow_right:](../9845/index.md)

**start**: 	July 01, 2024 12:16:12 AM UTC

**end**: 		July 01, 2024 04:47:18 AM UTC

**duration**: 0 days 04:31:05.925000

**tags**:	 

- DUCK



**comments**:	 

- Access for QPS piquet



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

