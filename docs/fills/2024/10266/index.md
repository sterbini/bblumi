# FILL 10266

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10266.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_20_14_23_23_F10266.yaml) [:arrow_left:](../10265/index.md) [:arrow_right:](../10267/index.md)

**start**: 	October 20, 2024 02:23:23 PM UTC

**end**: 		October 20, 2024 02:24:39 PM UTC

**duration**: 0 days 00:01:16.438000

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

