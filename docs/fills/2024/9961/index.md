# FILL 9961

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9961.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_31_23_03_51_F9961.yaml) [:arrow_left:](../9960/index.md) [:arrow_right:](../9962/index.md)

**start**: 	July 31, 2024 11:03:51 PM UTC

**end**: 		July 31, 2024 11:27:50 PM UTC

**duration**: 0 days 00:23:58.096375

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

