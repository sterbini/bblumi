# FILL 9429

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9429.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_26_22_07_45_F9429.yaml) [:arrow_left:](../9428/index.md) [:arrow_right:](../9430/index.md)

**start**: 	March 26, 2024 10:07:45 PM UTC

**end**: 		March 27, 2024 01:20:25 AM UTC

**duration**: 0 days 03:12:40.184500

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- cycle to 30cm



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

