# FILL 10063

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10063.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_28_14_58_14_F10063.yaml) [:arrow_left:](../10062/index.md) [:arrow_right:](../10064/index.md)

**start**: 	August 28, 2024 02:58:14 PM UTC

**end**: 		August 28, 2024 03:06:15 PM UTC

**duration**: 0 days 00:08:00.803250

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

