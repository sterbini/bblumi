# FILL 9632

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9632.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_15_06_40_23_F9632.yaml) [:arrow_left:](../9631/index.md) [:arrow_right:](../9633/index.md)

**start**: 	May 15, 2024 06:40:23 AM UTC

**end**: 		May 15, 2024 09:09:57 AM UTC

**duration**: 0 days 02:29:34.132500

**tags**:	 

- MD



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

