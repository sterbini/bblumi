# FILL 9572

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9572.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_29_06_56_26_F9572.yaml) [:arrow_left:](../9571/index.md) [:arrow_right:](../9573/index.md)

**start**: 	April 29, 2024 06:56:26 AM UTC

**end**: 		April 29, 2024 07:34:53 AM UTC

**duration**: 0 days 00:38:26.468875

**tags**:	 

- DUCK



**comments**:	 

- BPM timing test



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

