# FILL 9428

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9428.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_26_21_37_33_F9428.yaml) [:arrow_left:](../9427/index.md) [:arrow_right:](../9429/index.md)

**start**: 	March 26, 2024 09:37:33 PM UTC

**end**: 		March 26, 2024 10:07:45 PM UTC

**duration**: 0 days 00:30:11.957125

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

