# FILL 10290

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10290.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_27_04_22_57_F10290.yaml) [:arrow_left:](../10289/index.md) [:arrow_right:](../10291/index.md)

**start**: 	October 27, 2024 04:22:57 AM UTC

**end**: 		October 27, 2024 06:10:46 AM UTC

**duration**: 0 days 01:47:48.988875

**tags**:	 

- BEAM_COMMISSIONING

- PPREF_OPTICS



**comments**:	 

- Loss maps in collision



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

