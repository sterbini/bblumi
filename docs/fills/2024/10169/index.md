# FILL 10169

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10169.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_28_07_09_05_F10169.yaml) [:arrow_left:](../10168/index.md) [:arrow_right:](../10170/index.md)

**start**: 	September 28, 2024 07:09:05 AM UTC

**end**: 		September 28, 2024 11:46:25 AM UTC

**duration**: 0 days 04:37:20.021500

**tags**:	 

- MD



**comments**:	 

- 12663Wirecompensationduringthebeta*-leveling



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

