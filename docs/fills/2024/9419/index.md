# FILL 9419

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9419.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_24_08_21_17_F9419.yaml) [:arrow_left:](../9418/index.md) [:arrow_right:](../9420/index.md)

**start**: 	March 24, 2024 08:21:17 AM UTC

**end**: 		March 24, 2024 08:32:23 PM UTC

**duration**: 0 days 12:11:06.115250

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- optics corrections squeeze down to 30cm



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

