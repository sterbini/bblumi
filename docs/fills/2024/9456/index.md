# FILL 9456

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9456.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_03_01_47_03_F9456.yaml) [:arrow_left:](../9455/index.md) [:arrow_right:](../9457/index.md)

**start**: 	April 03, 2024 01:47:03 AM UTC

**end**: 		April 03, 2024 04:05:25 AM UTC

**duration**: 0 days 02:18:21.415875

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- MP tests



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

