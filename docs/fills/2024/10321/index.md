# FILL 10321

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10321.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_05_17_18_44_F10321.yaml) [:arrow_left:](../10320/index.md) [:arrow_right:](../10322/index.md)

**start**: 	November 05, 2024 05:18:44 PM UTC

**end**: 		November 06, 2024 12:20:26 AM UTC

**duration**: 0 days 07:01:41.636125

**tags**:	 

- ION_PHYSICS

- OP_DUMP



**comments**:	 

- Loss maps

- Asynchronous beam dump



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

