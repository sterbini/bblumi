# FILL 9692

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9692.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_31_04_15_47_F9692.yaml) [:arrow_left:](../9691/index.md) [:arrow_right:](../9693/index.md)

**start**: 	May 31, 2024 04:15:47 AM UTC

**end**: 		May 31, 2024 09:26:49 AM UTC

**duration**: 0 days 05:11:01.751125

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

