# FILL 9956

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9956.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_31_10_20_19_F9956.yaml) [:arrow_left:](../9955/index.md) [:arrow_right:](../9957/index.md)

**start**: 	July 31, 2024 10:20:19 AM UTC

**end**: 		July 31, 2024 10:22:32 AM UTC

**duration**: 0 days 00:02:12.965500

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

