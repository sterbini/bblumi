# FILL 9640

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9640.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_17_23_44_58_F9640.yaml) [:arrow_left:](../9639/index.md) [:arrow_right:](../9641/index.md)

**start**: 	May 17, 2024 11:44:58 PM UTC

**end**: 		May 18, 2024 01:17:36 AM UTC

**duration**: 0 days 01:32:37.967125

**tags**:	 

- VDM

- DUCK



**comments**:	 

- Dumped at injection on QPS crate reset with closed permit loop

- After the dump, some pilot injection was tried and after we changed the fill number.




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9640/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9640/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9640/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9640/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9640/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9640/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

