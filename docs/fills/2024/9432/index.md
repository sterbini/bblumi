# FILL 9432

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9432.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_27_05_35_05_F9432.yaml) [:arrow_left:](../9431/index.md) [:arrow_right:](../9433/index.md)

**start**: 	March 27, 2024 05:35:05 AM UTC

**end**: 		March 27, 2024 12:53:11 PM UTC

**duration**: 0 days 07:18:05.894125

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- TCDQ alignment at FT

- vertical apertrue at 1.2m



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

