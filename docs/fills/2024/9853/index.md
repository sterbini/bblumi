# FILL 9853

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9853.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_03_12_43_26_F9853.yaml) [:arrow_left:](../9852/index.md) [:arrow_right:](../9854/index.md)

**start**: 	July 03, 2024 12:43:26 PM UTC

**end**: 		July 03, 2024 03:26:23 PM UTC

**duration**: 0 days 02:42:57.220000

**tags**:	 

- DUCK



**comments**:	 

- MKI8 access and opportunistic access to check RQT12.L1B1



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

