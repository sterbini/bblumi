# FILL 9551

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9551.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_22_21_25_34_F9551.yaml) [:arrow_left:](../9550/index.md) [:arrow_right:](../9552/index.md)

**start**: 	April 22, 2024 09:25:34 PM UTC

**end**: 		April 22, 2024 10:41:01 PM UTC

**duration**: 0 days 01:15:26.736250

**tags**:	 

- DUCK



**comments**:	 

- quench during precycle in S56, MQ29L6, 1st time



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

