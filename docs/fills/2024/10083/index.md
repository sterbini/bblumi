# FILL 10083

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10083.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_03_16_15_28_F10083.yaml) [:arrow_left:](../10082/index.md) [:arrow_right:](../10084/index.md)

**start**: 	September 03, 2024 04:15:28 PM UTC

**end**: 		September 03, 2024 04:41:22 PM UTC

**duration**: 0 days 00:25:53.349375

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

