# FILL 9668

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9668.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_25_09_14_32_F9668.yaml) [:arrow_left:](../9667/index.md) [:arrow_right:](../9669/index.md)

**start**: 	May 25, 2024 09:14:32 AM UTC

**end**: 		May 25, 2024 10:40:20 AM UTC

**duration**: 0 days 01:25:47.596000

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

