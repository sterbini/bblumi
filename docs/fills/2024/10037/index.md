# FILL 10037

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10037.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_21_10_35_29_F10037.yaml) [:arrow_left:](../10036/index.md) [:arrow_right:](../10038/index.md)

**start**: 	August 21, 2024 10:35:29 AM UTC

**end**: 		August 21, 2024 12:45:45 PM UTC

**duration**: 0 days 02:10:16.412500

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

