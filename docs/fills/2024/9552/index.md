# FILL 9552

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9552.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_22_22_41_01_F9552.yaml) [:arrow_left:](../9551/index.md) [:arrow_right:](../9553/index.md)

**start**: 	April 22, 2024 10:41:01 PM UTC

**end**: 		April 23, 2024 03:27:35 AM UTC

**duration**: 0 days 04:46:33.837000

**tags**:	 

- DUCK



**comments**:	 

- precycle

- MQ29L6 quench during precycle, 2nd time



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

