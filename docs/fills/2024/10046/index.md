# FILL 10046

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10046.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_22_18_58_01_F10046.yaml) [:arrow_left:](../10045/index.md) [:arrow_right:](../10047/index.md)

**start**: 	August 22, 2024 06:58:01 PM UTC

**end**: 		August 23, 2024 04:22:51 AM UTC

**duration**: 0 days 09:24:50.150750

**tags**:	 

- MD



**comments**:	 

- MD6943 60 degree arc FODO cell phase advance LHC optics



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

