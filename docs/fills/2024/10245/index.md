# FILL 10245

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10245.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_18_22_04_06_F10245.yaml) [:arrow_left:](../10244/index.md) [:arrow_right:](../10246/index.md)

**start**: 	October 18, 2024 10:04:06 PM UTC

**end**: 		October 18, 2024 10:51:01 PM UTC

**duration**: 0 days 00:46:55.346125

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

