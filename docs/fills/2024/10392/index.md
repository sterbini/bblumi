# FILL 10392

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10392.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_19_21_18_11_F10392.yaml) [:arrow_left:](../10391/index.md) [:arrow_right:](../10393/index.md)

**start**: 	November 19, 2024 09:18:11 PM UTC

**end**: 		November 20, 2024 07:40:46 AM UTC

**duration**: 0 days 10:22:34.948000

**tags**:	 

- ION_PHYSICS



**comments**:	 

- Dumped due to quench S23




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10392/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10392/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10392/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10392/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10392/Fill_chromaticity.png)

#### Intensity


##### BBB SB intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/10392/bbb_SB_intensity.png)

##### SB bunch intensity 

![](https://dltwo.web.cern.ch/followup_plots/2024/10392/Fill_adjust_stable_losses_summary.png)

##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10392/Fill_bunch_length.png)

#### Emittance


##### BBB in SB for B1 

![](https://dltwo.web.cern.ch/followup_plots/2024/10392/bbb_SB_B1_emit.png)

##### BBB in SB for B2 

![](https://dltwo.web.cern.ch/followup_plots/2024/10392/bbb_SB_B2_emit.png)

#### Luminosity


##### Overview

![](https://dltwo.web.cern.ch/followup_plots/2024/10392/Fill_luminosity.png)

##### Luminous Regions

![](https://dltwo.web.cern.ch/followup_plots/2024/10392/Fill_luminous_regions.png)

##### BBB luminosity in SB

![](https://dltwo.web.cern.ch/followup_plots/2024/10392/bbb_SB_luminosity.png)

#### Cross-sections


##### ADJUST

![](https://dltwo.web.cern.ch/followup_plots/2024/10392/Xsection_ADJUST_heatmap.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10392/Xsection_ADJUST_mean.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10392/Xsection_ADJUST_bbb_ip15lr.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10392/Xsection_ADJUST_bbb_ip2ho.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10392/Xsection_ADJUST_bbb_lhcbho.png)

##### STABLE BEAM

![](https://dltwo.web.cern.ch/followup_plots/2024/10392/Xsection_STABLE_heatmap_DBLM.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10392/Xsection_SB_mean_DBLM.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10392/Xsection_SB_mean_DBLM_separation.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10392/Xsection_SB_bbb_DBLM_ip15lr.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10392/Xsection_SB_bbb_DBLM_ip2ho.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10392/Xsection_SB_bbb_DBLM_lhcbho.png)

