# FILL 9420

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9420.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_24_20_32_23_F9420.yaml) [:arrow_left:](../9419/index.md) [:arrow_right:](../9421/index.md)

**start**: 	March 24, 2024 08:32:23 PM UTC

**end**: 		March 24, 2024 10:52:56 PM UTC

**duration**: 0 days 02:20:32.869125

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- beta* levelling test



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

