# FILL 10365

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10365.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_14_09_07_09_F10365.yaml) [:arrow_left:](../10364/index.md) [:arrow_right:](../10366/index.md)

**start**: 	November 14, 2024 09:07:09 AM UTC

**end**: 		November 14, 2024 09:49:07 AM UTC

**duration**: 0 days 00:41:58.198500

**tags**:	 

- MD



**comments**:	 

- MD14364 RF flat bottom optimization for ion debunching

- 10 MV

- bunch length lower than 1.4 ns at the end

- lifetime to 7 hours and debunched beam at 1.6%




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10365/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10365/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10365/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10365/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10365/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10365/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

