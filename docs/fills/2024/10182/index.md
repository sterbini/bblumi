# FILL 10182

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10182.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_01_14_27_26_F10182.yaml) [:arrow_left:](../10181/index.md) [:arrow_right:](../10183/index.md)

**start**: 	October 01, 2024 02:27:26 PM UTC

**end**: 		October 01, 2024 06:27:14 PM UTC

**duration**: 0 days 03:59:48.088375

**tags**:	 

- DUCK



**comments**:	 

- nan



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

