# FILL 10369

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10369.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_14_11_53_47_F10369.yaml) [:arrow_left:](../10368/index.md) [:arrow_right:](../10370/index.md)

**start**: 	November 14, 2024 11:53:47 AM UTC

**end**: 		November 14, 2024 01:44:22 PM UTC

**duration**: 0 days 01:50:35.449000

**tags**:	 

- MD



**comments**:	 

- MD14364 RF flat bottom optimization for ion debunching

- injected with a voltage dip, 8 MV at injection and keeping 12 MV between injections. unbunched beam was similar to constant 8 MV but lifetime was better and slightly better than 12 MV.




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10369/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10369/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10369/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10369/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10369/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10369/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

