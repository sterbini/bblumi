# FILL 9587

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9587.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_03_10_06_54_F9587.yaml) [:arrow_left:](../9586/index.md) [:arrow_right:](../9588/index.md)

**start**: 	May 03, 2024 10:06:54 AM UTC

**end**: 		May 03, 2024 10:34:07 AM UTC

**duration**: 0 days 00:27:13.151000

**tags**:	 

- DUCK



**comments**:	 

- BPM offset setup for B2




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9587/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9587/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9587/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9587/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9587/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9587/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

