# FILL 10251

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10251.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_19_06_15_23_F10251.yaml) [:arrow_left:](../10250/index.md) [:arrow_right:](../10252/index.md)

**start**: 	October 19, 2024 06:15:23 AM UTC

**end**: 		October 19, 2024 06:17:09 AM UTC

**duration**: 0 days 00:01:46.794500

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

