# FILL 9827

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9827.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_25_21_11_24_F9827.yaml) [:arrow_left:](../9826/index.md) [:arrow_right:](../9828/index.md)

**start**: 	June 25, 2024 09:11:24 PM UTC

**end**: 		June 26, 2024 01:09:19 PM UTC

**duration**: 0 days 15:57:54.965625

**tags**:	 

- DUCK



**comments**:	 

- electrical glich

- sps cooling issue



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

