# FILL 9406

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9406.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_22_23_38_43_F9406.yaml) [:arrow_left:](../9405/index.md) [:arrow_right:](../9407/index.md)

**start**: 	March 22, 2024 11:38:43 PM UTC

**end**: 		March 23, 2024 12:31:51 AM UTC

**duration**: 0 days 00:53:08.454875

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- cycle test

- new squeeze adn bunch length



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

