# FILL 9404

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9404.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_22_14_22_38_F9404.yaml) [:arrow_left:](../9403/index.md) [:arrow_right:](../9405/index.md)

**start**: 	March 22, 2024 02:22:38 PM UTC

**end**: 		March 22, 2024 04:10:43 PM UTC

**duration**: 0 days 01:48:05.103250

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

