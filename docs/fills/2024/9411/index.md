# FILL 9411

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9411.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_23_09_04_19_F9411.yaml) [:arrow_left:](../9410/index.md) [:arrow_right:](../9412/index.md)

**start**: 	March 23, 2024 09:04:19 AM UTC

**end**: 		March 23, 2024 11:33:49 AM UTC

**duration**: 0 days 02:29:30.347000

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- nominal bunch ramps for BI tests



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

