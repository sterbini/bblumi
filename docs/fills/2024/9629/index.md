# FILL 9629

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9629.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_14_19_52_08_F9629.yaml) [:arrow_left:](../9628/index.md) [:arrow_right:](../9630/index.md)

**start**: 	May 14, 2024 07:52:08 PM UTC

**end**: 		May 15, 2024 01:57:16 AM UTC

**duration**: 0 days 06:05:07.760000

**tags**:	 

- MD



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

