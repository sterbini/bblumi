# FILL 9451

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9451.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_01_21_29_04_F9451.yaml) [:arrow_left:](../9450/index.md) [:arrow_right:](../9452/index.md)

**start**: 	April 01, 2024 09:29:04 PM UTC

**end**: 		April 02, 2024 01:21:40 AM UTC

**duration**: 0 days 03:52:36.189250

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- ramp in steps



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

