# FILL 9954

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9954.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_31_10_08_49_F9954.yaml) [:arrow_left:](../9953/index.md) [:arrow_right:](../9955/index.md)

**start**: 	July 31, 2024 10:08:49 AM UTC

**end**: 		July 31, 2024 10:17:05 AM UTC

**duration**: 0 days 00:08:15.068875

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

