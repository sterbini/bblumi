# FILL 9330

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9330.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_08_22_42_46_F9330.yaml) [:arrow_left:](../9329/index.md) [:arrow_right:](../9331/index.md)

**start**: 	March 08, 2024 10:42:46 PM UTC

**end**: 		March 09, 2024 07:53:18 AM UTC

**duration**: 0 days 09:10:31.294000

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

