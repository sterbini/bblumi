# FILL 9489

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9489.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_09_14_05_34_F9489.yaml) [:arrow_left:](../9488/index.md) [:arrow_right:](../9490/index.md)

**start**: 	April 09, 2024 02:05:34 PM UTC

**end**: 		April 09, 2024 02:05:46 PM UTC

**duration**: 0 days 00:00:11.934750

**tags**:	 

- INTENSITY_RAMPUP



**comments**:	 

- None

- empty fills



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

