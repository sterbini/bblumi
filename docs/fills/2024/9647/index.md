# FILL 9647

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9647.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_19_09_11_37_F9647.yaml) [:arrow_left:](../9646/index.md) [:arrow_right:](../9648/index.md)

**start**: 	May 19, 2024 09:11:37 AM UTC

**end**: 		May 19, 2024 11:12:20 AM UTC

**duration**: 0 days 02:00:43.856625

**tags**:	 

- VDM

- OP_DUMP



**comments**:	 

- Programmed dump after injection check for ATLAS calibration fill




#### Cycle duration

![](https://dltwo.web.cern.ch/followup_plots/2024/9647/Cycle_duration.png)


#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9647/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9647/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9647/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9647/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9647/Fill_chromaticity.png)

#### Intensity


##### Cycle intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/9647/Cycle_intensity.png)

##### Violin BBB intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/9647/Cycle_violin_intensity_bl.png)

#### Bunch Length


![](https://dltwo.web.cern.ch/followup_plots/2024/9647/Cycle_bunchlength.png)

##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9647/Fill_bunch_length.png)

#### Emittance


##### B1 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/9647/Cycle_emit_b1.png)

##### B2 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/9647/Cycle_emit_b2.png)

##### Summary violin plot

![](https://dltwo.web.cern.ch/followup_plots/2024/9647/Cycle_violin_emit.png)

#### Bunch brightness


![](https://dltwo.web.cern.ch/followup_plots/2024/9647/Cycle_brightness.png)

#### Beam properties summary 

![](https://dltwo.web.cern.ch/followup_plots/2024/9647/Cycle_summary.png)

#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

