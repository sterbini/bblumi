# FILL 9544

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9544.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_20_07_09_20_F9544.yaml) [:arrow_left:](../9543/index.md) [:arrow_right:](../9545/index.md)

**start**: 	April 20, 2024 07:09:20 AM UTC

**end**: 		April 20, 2024 09:34:34 AM UTC

**duration**: 0 days 02:25:13.851625

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

