# FILL 10308

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10308.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_01_08_28_04_F10308.yaml) [:arrow_left:](../10307/index.md) [:arrow_right:](../10309/index.md)

**start**: 	November 01, 2024 08:28:04 AM UTC

**end**: 		November 01, 2024 10:22:37 AM UTC

**duration**: 0 days 01:54:32.889250

**tags**:	 

- UCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

