# FILL 10353

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10353.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_11_15_29_07_F10353.yaml) [:arrow_left:](../10352/index.md) [:arrow_right:](../10354/index.md)

**start**: 	November 11, 2024 03:29:07 PM UTC

**end**: 		November 11, 2024 04:22:52 PM UTC

**duration**: 0 days 00:53:45.350500

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

