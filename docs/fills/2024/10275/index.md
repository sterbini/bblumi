# FILL 10275

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10275.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_24_12_21_41_F10275.yaml) [:arrow_left:](../10274/index.md) [:arrow_right:](../10276/index.md)

**start**: 	October 24, 2024 12:21:41 PM UTC

**end**: 		October 24, 2024 02:50:15 PM UTC

**duration**: 0 days 02:28:33.667750

**tags**:	 

- DUCK



**comments**:	 

- A lot of settings to prepare the ion cycle



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

