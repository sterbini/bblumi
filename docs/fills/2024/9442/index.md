# FILL 9442

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9442.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_29_05_25_43_F9442.yaml) [:arrow_left:](../9441/index.md) [:arrow_right:](../9443/index.md)

**start**: 	March 29, 2024 05:25:43 AM UTC

**end**: 		March 30, 2024 04:03:50 PM UTC

**duration**: 1 days 10:38:07.070875

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

