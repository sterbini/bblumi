# FILL 9339

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9339.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_10_12_16_17_F9339.yaml) [:arrow_left:](../9338/index.md) [:arrow_right:](../9340/index.md)

**start**: 	March 10, 2024 12:16:17 PM UTC

**end**: 		March 11, 2024 03:57:46 AM UTC

**duration**: 0 days 15:41:29.253000

**tags**:	 

- BEAM_COMMISSIONING

- OPTICS_MEASUREMENTS



**comments**:	 

- optics measuremeents at injection



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

