# FILL 9761

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9761.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_09_11_54_39_F9761.yaml) [:arrow_left:](../9760/index.md) [:arrow_right:](../9762/index.md)

**start**: 	June 09, 2024 11:54:39 AM UTC

**end**: 		June 09, 2024 12:14:13 PM UTC

**duration**: 0 days 00:19:33.744000

**tags**:	 

- MD



**comments**:	 

- MD11703




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9761/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9761/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9761/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9761/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9761/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9761/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

