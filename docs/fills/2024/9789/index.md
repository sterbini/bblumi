# FILL 9789

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9789.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_17_03_29_38_F9789.yaml) [:arrow_left:](../9788/index.md) [:arrow_right:](../9790/index.md)

**start**: 	June 17, 2024 03:29:38 AM UTC

**end**: 		June 18, 2024 08:57:37 AM UTC

**duration**: 1 days 05:27:58.597000

**tags**:	 

- BEAM_COMMISSIONING

- OP_DUMP



**comments**:	 

- Aperture and optics measurement at 30 cm

- Extraction of an INDIV at top energy via the ADT excitation




#### Cycle duration

![](https://dltwo.web.cern.ch/followup_plots/2024/9789/Cycle_duration.png)


#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9789/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9789/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9789/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9789/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9789/Fill_chromaticity.png)

#### Intensity


##### Cycle intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/9789/Cycle_intensity.png)

##### Violin BBB intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/9789/Cycle_violin_intensity_bl.png)

#### Bunch Length


![](https://dltwo.web.cern.ch/followup_plots/2024/9789/Cycle_bunchlength.png)

##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9789/Fill_bunch_length.png)

#### Emittance


##### B1 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/9789/Cycle_emit_b1.png)

##### B2 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/9789/Cycle_emit_b2.png)

##### Summary violin plot

![](https://dltwo.web.cern.ch/followup_plots/2024/9789/Cycle_violin_emit.png)

#### Bunch brightness


![](https://dltwo.web.cern.ch/followup_plots/2024/9789/Cycle_brightness.png)

#### Beam properties summary 

![](https://dltwo.web.cern.ch/followup_plots/2024/9789/Cycle_summary.png)

#### Luminosity


##### Overview

![](https://dltwo.web.cern.ch/followup_plots/2024/9789/Fill_luminosity.png)

#### Cross-sections


##### ADJUST

![](https://dltwo.web.cern.ch/followup_plots/2024/9789/Xsection_ADJUST_heatmap.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9789/Xsection_ADJUST_mean.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9789/Xsection_ADJUST_bbb_ip15lr.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9789/Xsection_ADJUST_bbb_ip2ho.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9789/Xsection_ADJUST_bbb_lhcbho.png)

##### STABLE BEAM

