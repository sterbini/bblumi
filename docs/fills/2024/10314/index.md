# FILL 10314

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10314.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_04_16_50_27_F10314.yaml) [:arrow_left:](../10313/index.md) [:arrow_right:](../10315/index.md)

**start**: 	November 04, 2024 04:50:27 PM UTC

**end**: 		November 05, 2024 01:07:19 AM UTC

**duration**: 0 days 08:16:52.129500

**tags**:	 

- ION_PHYSICS

- DUCK



**comments**:	 

- Calibration for ion physics



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

