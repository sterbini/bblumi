# FILL 9588

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9588.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_03_10_34_07_F9588.yaml) [:arrow_left:](../9587/index.md) [:arrow_right:](../9589/index.md)

**start**: 	May 03, 2024 10:34:07 AM UTC

**end**: 		May 03, 2024 10:47:37 AM UTC

**duration**: 0 days 00:13:29.108625

**tags**:	 

- DUCK



**comments**:	 


#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9588/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9588/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9588/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9588/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9588/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9588/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

