# FILL 10372

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10372.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_14_15_32_14_F10372.yaml) [:arrow_left:](../10371/index.md) [:arrow_right:](../10373/index.md)

**start**: 	November 14, 2024 03:32:14 PM UTC

**end**: 		November 14, 2024 06:27:26 PM UTC

**duration**: 0 days 02:55:11.741375

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

