# FILL 10294

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10294.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_27_09_20_33_F10294.yaml) [:arrow_left:](../10293/index.md) [:arrow_right:](../10295/index.md)

**start**: 	October 27, 2024 09:20:33 AM UTC

**end**: 		October 27, 2024 09:43:16 PM UTC

**duration**: 0 days 12:22:43.195625

**tags**:	 

- BEAM_COMMISSIONING

- IONS_OPTICS



**comments**:	 

- ALICE background fill. The the situation wrt 2023 seems to have improved even without additional mitigation measures in place

- The collimator team did an aligment check of TCLA and loss maps



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

