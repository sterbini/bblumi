# FILL 9737

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9737.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_08_15_10_30_F9737.yaml) [:arrow_left:](../9736/index.md) [:arrow_right:](../9738/index.md)

**start**: 	June 08, 2024 03:10:30 PM UTC

**end**: 		June 08, 2024 04:56:06 PM UTC

**duration**: 0 days 01:45:36.129250

**tags**:	 

- MD

- OP_DUMP



**comments**:	 

- MD11243




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9737/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9737/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9737/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9737/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9737/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9737/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

