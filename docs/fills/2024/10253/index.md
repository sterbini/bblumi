# FILL 10253

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10253.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_19_11_01_18_F10253.yaml) [:arrow_left:](../10252/index.md) [:arrow_right:](../10254/index.md)

**start**: 	October 19, 2024 11:01:18 AM UTC

**end**: 		October 19, 2024 06:35:29 PM UTC

**duration**: 0 days 07:34:10.259875

**tags**:	 

- MD



**comments**:	 

- MD13883 Optics with HV crossing plane



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

