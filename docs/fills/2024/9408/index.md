# FILL 9408

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9408.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_23_02_35_40_F9408.yaml) [:arrow_left:](../9407/index.md) [:arrow_right:](../9409/index.md)

**start**: 	March 23, 2024 02:35:40 AM UTC

**end**: 		March 23, 2024 04:03:59 AM UTC

**duration**: 0 days 01:28:19.075875

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- cycle test

- new squeeze adn bunch length



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

