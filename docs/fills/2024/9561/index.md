# FILL 9561

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9561.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_25_09_15_34_F9561.yaml) [:arrow_left:](../9560/index.md) [:arrow_right:](../9562/index.md)

**start**: 	April 25, 2024 09:15:34 AM UTC

**end**: 		April 25, 2024 09:31:35 AM UTC

**duration**: 0 days 00:16:00.888250

**tags**:	 

- DUCK



**comments**:	 

- cryo issues



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

