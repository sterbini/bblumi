# FILL 9613

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9613.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_11_16_29_04_F9613.yaml) [:arrow_left:](../9612/index.md) [:arrow_right:](../9614/index.md)

**start**: 	May 11, 2024 04:29:04 PM UTC

**end**: 		May 11, 2024 06:35:15 PM UTC

**duration**: 0 days 02:06:10.748625

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

