# FILL 9968

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9968.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_02_09_14_37_F9968.yaml) [:arrow_left:](../9967/index.md) [:arrow_right:](../9969/index.md)

**start**: 	August 02, 2024 09:14:37 AM UTC

**end**: 		August 02, 2024 12:13:57 PM UTC

**duration**: 0 days 02:59:19.770625

**tags**:	 

- DUCK



**comments**:	 

- Intervention for the QPS of RQT13.R5B1/2



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

