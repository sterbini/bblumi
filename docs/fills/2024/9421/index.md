# FILL 9421

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9421.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_24_22_52_56_F9421.yaml) [:arrow_left:](../9420/index.md) [:arrow_right:](../9422/index.md)

**start**: 	March 24, 2024 10:52:56 PM UTC

**end**: 		March 25, 2024 12:40:39 AM UTC

**duration**: 0 days 01:47:42.854000

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

