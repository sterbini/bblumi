# FILL 9360

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9360.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_15_02_00_21_F9360.yaml) [:arrow_left:](../9359/index.md) [:arrow_right:](../9361/index.md)

**start**: 	March 15, 2024 02:00:21 AM UTC

**end**: 		March 15, 2024 03:31:56 AM UTC

**duration**: 0 days 01:31:35.038875

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- cycle to 30cm with bumps



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

