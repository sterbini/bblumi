# FILL 10047

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10047.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_23_04_22_51_F10047.yaml) [:arrow_left:](../10046/index.md) [:arrow_right:](../10048/index.md)

**start**: 	August 23, 2024 04:22:51 AM UTC

**end**: 		August 23, 2024 08:23:06 AM UTC

**duration**: 0 days 04:00:14.663000

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

