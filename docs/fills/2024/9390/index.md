# FILL 9390

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9390.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_19_11_32_26_F9390.yaml) [:arrow_left:](../9389/index.md) [:arrow_right:](../9391/index.md)

**start**: 	March 19, 2024 11:32:26 AM UTC

**end**: 		March 19, 2024 11:34:07 AM UTC

**duration**: 0 days 00:01:41.128000

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

