# FILL 10282

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10282.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_25_20_15_22_F10282.yaml) [:arrow_left:](../10281/index.md) [:arrow_right:](../10283/index.md)

**start**: 	October 25, 2024 08:15:22 PM UTC

**end**: 		October 26, 2024 04:22:02 AM UTC

**duration**: 0 days 08:06:39.333750

**tags**:	 

- BEAM_COMMISSIONING

- ION_OPTICS



**comments**:	 

- SVC (TCR part) reconnected

- 1st fill: Setting up crystals at injection, and updated the functions.

- 2nd fill: Setting up crystals at flat top, loss maps at flat top for channelling, amorphous and volume reflection, linear scans. Channelling optimisation tests before dump.



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

