# FILL 9963

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9963.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_01_08_46_48_F9963.yaml) [:arrow_left:](../9962/index.md) [:arrow_right:](../9964/index.md)

**start**: 	August 01, 2024 08:46:48 AM UTC

**end**: 		August 01, 2024 10:52:41 AM UTC

**duration**: 0 days 02:05:53.312000

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

