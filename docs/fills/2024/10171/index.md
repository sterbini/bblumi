# FILL 10171

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10171.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_28_15_14_57_F10171.yaml) [:arrow_left:](../10170/index.md) [:arrow_right:](../10172/index.md)

**start**: 	September 28, 2024 03:14:57 PM UTC

**end**: 		September 28, 2024 09:01:52 PM UTC

**duration**: 0 days 05:46:55.526125

**tags**:	 

- MD



**comments**:	 

- 13463ChrimaticitymeasurementinphysicsconditionswithBTFandADT-ACdipole



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

