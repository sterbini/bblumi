# FILL 9720

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9720.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_06_11_56_41_F9720.yaml) [:arrow_left:](../9719/index.md) [:arrow_right:](../9721/index.md)

**start**: 	June 06, 2024 11:56:41 AM UTC

**end**: 		June 06, 2024 05:07:39 PM UTC

**duration**: 0 days 05:10:57.446500

**tags**:	 

- MD



**comments**:	 

- MD11243, MCBX circuit tripped because it exceeded its maximum allowed current during the ramp



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

