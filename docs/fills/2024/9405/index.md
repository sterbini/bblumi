# FILL 9405

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9405.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_22_16_10_43_F9405.yaml) [:arrow_left:](../9404/index.md) [:arrow_right:](../9406/index.md)

**start**: 	March 22, 2024 04:10:43 PM UTC

**end**: 		March 22, 2024 11:38:43 PM UTC

**duration**: 0 days 07:27:59.558375

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

