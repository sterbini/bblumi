# FILL 10038

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10038.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_21_12_45_45_F10038.yaml) [:arrow_left:](../10037/index.md) [:arrow_right:](../10039/index.md)

**start**: 	August 21, 2024 12:45:45 PM UTC

**end**: 		August 21, 2024 04:58:05 PM UTC

**duration**: 0 days 04:12:19.971625

**tags**:	 

- MD



**comments**:	 

- MD12723 HL-LHC optics cycle



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

