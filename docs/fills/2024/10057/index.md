# FILL 10057

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10057.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_26_05_41_17_F10057.yaml) [:arrow_left:](../10056/index.md) [:arrow_right:](../10058/index.md)

**start**: 	August 26, 2024 05:41:17 AM UTC

**end**: 		August 26, 2024 10:15:58 AM UTC

**duration**: 0 days 04:34:40.450250

**tags**:	 

- DUCK



**comments**:	 

- Access for the vacuum equipment communication problem and P2 compensator

- Ramp down of the ALICE solenoid and dipole

- Access needed  to reset RBA23 and RBA12 to reset a breaker following the restart of the active filter



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

