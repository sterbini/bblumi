# FILL 9346

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9346.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_11_18_49_25_F9346.yaml) [:arrow_left:](../9345/index.md) [:arrow_right:](../9347/index.md)

**start**: 	March 11, 2024 06:49:25 PM UTC

**end**: 		March 12, 2024 02:16:23 AM UTC

**duration**: 0 days 07:26:58.198250

**tags**:	 

- BEAM_COMMISSIONING

- VDM



**comments**:	 

- B1 TCP splashes

- ADT check

- VDM optics



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

