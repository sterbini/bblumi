# FILL 9444

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9444.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_31_05_03_02_F9444.yaml) [:arrow_left:](../9443/index.md) [:arrow_right:](../9445/index.md)

**start**: 	March 31, 2024 05:03:02 AM UTC

**end**: 		March 31, 2024 09:27:27 AM UTC

**duration**: 0 days 04:24:25.629750

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- FASER background test TCL@1.8mm half gap



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

