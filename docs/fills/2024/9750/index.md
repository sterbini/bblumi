# FILL 9750

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9750.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_09_05_40_03_F9750.yaml) [:arrow_left:](../9749/index.md) [:arrow_right:](../9751/index.md)

**start**: 	June 09, 2024 05:40:03 AM UTC

**end**: 		June 09, 2024 05:55:43 AM UTC

**duration**: 0 days 00:15:39.985000

**tags**:	 

- DUCK



**comments**:	 


#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9750/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9750/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9750/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9750/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9750/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9750/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

