# FILL 10358

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10358.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_12_13_47_43_F10358.yaml) [:arrow_left:](../10357/index.md) [:arrow_right:](../10359/index.md)

**start**: 	November 12, 2024 01:47:43 PM UTC

**end**: 		November 12, 2024 08:02:00 PM UTC

**duration**: 0 days 06:14:17.467000

**tags**:	 

- ION_PHYSICS



**comments**:	 

- 1240 bunches with higher intensity

- 45% injection losses with 2.08e10 intensity for B1 and 32% for B2

- 55% losses at the start of ramp

- Dumped due to quench of 5 MBs




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10358/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10358/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10358/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10358/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10358/Fill_chromaticity.png)

#### Intensity


##### BBB SB intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/10358/bbb_SB_intensity.png)

##### SB bunch intensity 

![](https://dltwo.web.cern.ch/followup_plots/2024/10358/Fill_adjust_stable_losses_summary.png)

##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10358/Fill_bunch_length.png)

#### Emittance


##### BBB in SB for B1 

![](https://dltwo.web.cern.ch/followup_plots/2024/10358/bbb_SB_B1_emit.png)

##### BBB in SB for B2 

![](https://dltwo.web.cern.ch/followup_plots/2024/10358/bbb_SB_B2_emit.png)

#### Luminosity


##### Overview

![](https://dltwo.web.cern.ch/followup_plots/2024/10358/Fill_luminosity.png)

##### Luminous Regions

![](https://dltwo.web.cern.ch/followup_plots/2024/10358/Fill_luminous_regions.png)

##### BBB luminosity in SB

![](https://dltwo.web.cern.ch/followup_plots/2024/10358/bbb_SB_luminosity.png)

#### Cross-sections


##### ADJUST

![](https://dltwo.web.cern.ch/followup_plots/2024/10358/Xsection_ADJUST_heatmap.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10358/Xsection_ADJUST_mean.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10358/Xsection_ADJUST_bbb_ip15lr.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10358/Xsection_ADJUST_bbb_ip2ho.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10358/Xsection_ADJUST_bbb_lhcbho.png)

##### STABLE BEAM

![](https://dltwo.web.cern.ch/followup_plots/2024/10358/Xsection_STABLE_heatmap_DBLM.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10358/Xsection_SB_mean_DBLM.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10358/Xsection_SB_mean_DBLM_separation.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10358/Xsection_SB_bbb_DBLM_ip15lr.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10358/Xsection_SB_bbb_DBLM_ip2ho.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10358/Xsection_SB_bbb_DBLM_lhcbho.png)

