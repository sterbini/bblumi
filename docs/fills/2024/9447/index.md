# FILL 9447

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9447.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_01_05_08_15_F9447.yaml) [:arrow_left:](../9446/index.md) [:arrow_right:](../9448/index.md)

**start**: 	April 01, 2024 05:08:15 AM UTC

**end**: 		April 01, 2024 05:58:47 AM UTC

**duration**: 0 days 00:50:31.727125

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

