# FILL 9327

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9327.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_08_06_08_54_F9327.yaml) [:arrow_left:](../9326/index.md) [:arrow_right:](../9328/index.md)

**start**: 	March 08, 2024 06:08:54 AM UTC

**end**: 		March 08, 2024 08:00:23 AM UTC

**duration**: 0 days 01:51:28.558500

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

