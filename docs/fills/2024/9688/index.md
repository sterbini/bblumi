# FILL 9688

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9688.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_30_01_34_53_F9688.yaml) [:arrow_left:](../9687/index.md) [:arrow_right:](../9689/index.md)

**start**: 	May 30, 2024 01:34:53 AM UTC

**end**: 		May 30, 2024 03:13:45 AM UTC

**duration**: 0 days 01:38:51.328875

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

