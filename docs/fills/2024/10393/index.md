# FILL 10393

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10393.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_20_07_40_46_F10393.yaml) [:arrow_left:](../10392/index.md) [:arrow_right:](../10394/index.md)

**start**: 	November 20, 2024 07:40:46 AM UTC

**end**: 		November 20, 2024 03:08:00 PM UTC

**duration**: 0 days 07:27:13.617750

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

