# FILL 10033

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10033.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_20_04_11_18_F10033.yaml) [:arrow_left:](../10032/index.md) [:arrow_right:](../10034/index.md)

**start**: 	August 20, 2024 04:11:18 AM UTC

**end**: 		August 20, 2024 05:57:58 AM UTC

**duration**: 0 days 01:46:39.088875

**tags**:	 

- DUCK



**comments**:	 


#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10033/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10033/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10033/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10033/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10033/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10033/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

