# FILL 9981

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9981.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_06_12_45_48_F9981.yaml) [:arrow_left:](../9980/index.md) [:arrow_right:](../9982/index.md)

**start**: 	August 06, 2024 12:45:48 PM UTC

**end**: 		August 06, 2024 06:18:17 PM UTC

**duration**: 0 days 05:32:28.967625

**tags**:	 

- DUCK



**comments**:	 

- RQT12.L8B1 tripped during rampdown

- Test of an RF dry ramp: RF FGCs triggered the same problem but in B1. Experts accessed PZ45



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

