# FILL 10152

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10152.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_27_11_43_14_F10152.yaml) [:arrow_left:](../10151/index.md) [:arrow_right:](../10153/index.md)

**start**: 	September 27, 2024 11:43:14 AM UTC

**end**: 		September 27, 2024 11:51:26 AM UTC

**duration**: 0 days 00:08:11.804625

**tags**:	 

- MD



**comments**:	 

- 12743RFpowerlimitationsforhigh-intensitybatches




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10152/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10152/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10152/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10152/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10152/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10152/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

