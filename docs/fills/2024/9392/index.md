# FILL 9392

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9392.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_19_15_12_55_F9392.yaml) [:arrow_left:](../9391/index.md) [:arrow_right:](../9393/index.md)

**start**: 	March 19, 2024 03:12:55 PM UTC

**end**: 		March 19, 2024 10:38:13 PM UTC

**duration**: 0 days 07:25:17.104625

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- collimator alignment at FT



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

