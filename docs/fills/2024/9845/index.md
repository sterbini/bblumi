# FILL 9845

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9845.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_01_04_47_18_F9845.yaml) [:arrow_left:](../9844/index.md) [:arrow_right:](../9846/index.md)

**start**: 	July 01, 2024 04:47:18 AM UTC

**end**: 		July 01, 2024 05:21:06 AM UTC

**duration**: 0 days 00:33:48.165625

**tags**:	 

- DUCK



**comments**:	 

- Cycling



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

