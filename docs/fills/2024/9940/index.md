# FILL 9940

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9940.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_27_22_51_03_F9940.yaml) [:arrow_left:](../9939/index.md) [:arrow_right:](../9941/index.md)

**start**: 	July 27, 2024 10:51:03 PM UTC

**end**: 		July 27, 2024 10:53:48 PM UTC

**duration**: 0 days 00:02:44.827125

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

