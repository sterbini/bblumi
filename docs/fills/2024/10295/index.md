# FILL 10295

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10295.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_27_21_43_16_F10295.yaml) [:arrow_left:](../10294/index.md) [:arrow_right:](../10296/index.md)

**start**: 	October 27, 2024 09:43:16 PM UTC

**end**: 		October 28, 2024 06:11:22 AM UTC

**duration**: 0 days 08:28:05.823375

**tags**:	 

- BEAM_COMMISSIONING

- IONS_OPTICS



**comments**:	 

- Collimation checks

- Loss maps and ASD



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

