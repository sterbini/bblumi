# FILL 10148

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10148.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_27_04_14_00_F10148.yaml) [:arrow_left:](../10147/index.md) [:arrow_right:](../10149/index.md)

**start**: 	September 27, 2024 04:14:00 AM UTC

**end**: 		September 27, 2024 07:08:19 AM UTC

**duration**: 0 days 02:54:19.521750

**tags**:	 

- MD



**comments**:	 

- 12663Wirecompensationduringbeta*-leveling



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

