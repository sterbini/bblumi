# FILL 9751

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9751.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_09_05_55_43_F9751.yaml) [:arrow_left:](../9750/index.md) [:arrow_right:](../9752/index.md)

**start**: 	June 09, 2024 05:55:43 AM UTC

**end**: 		June 09, 2024 06:20:46 AM UTC

**duration**: 0 days 00:25:03.381000

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

