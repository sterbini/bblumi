# FILL 9440

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9440.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_29_01_09_11_F9440.yaml) [:arrow_left:](../9439/index.md) [:arrow_right:](../9441/index.md)

**start**: 	March 29, 2024 01:09:11 AM UTC

**end**: 		March 29, 2024 02:36:16 AM UTC

**duration**: 0 days 01:27:05.204000

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

