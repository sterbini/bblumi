# FILL 9718

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9718.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_06_03_58_48_F9718.yaml) [:arrow_left:](../9717/index.md) [:arrow_right:](../9719/index.md)

**start**: 	June 06, 2024 03:58:48 AM UTC

**end**: 		June 06, 2024 08:53:11 AM UTC

**duration**: 0 days 04:54:23.131749925

**tags**:	 

- MD

- OP_DUMP



**comments**:	 

- MD12466 Optics vs CMS solenoid



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

