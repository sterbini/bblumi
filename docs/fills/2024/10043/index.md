# FILL 10043

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10043.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_22_10_36_59_F10043.yaml) [:arrow_left:](../10042/index.md) [:arrow_right:](../10044/index.md)

**start**: 	August 22, 2024 10:36:59 AM UTC

**end**: 		August 22, 2024 10:56:13 AM UTC

**duration**: 0 days 00:19:14.240500

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

