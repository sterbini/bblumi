# FILL 9422

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9422.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_25_00_40_39_F9422.yaml) [:arrow_left:](../9421/index.md) [:arrow_right:](../9423/index.md)

**start**: 	March 25, 2024 12:40:39 AM UTC

**end**: 		March 25, 2024 02:28:46 AM UTC

**duration**: 0 days 01:48:07.512625

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

