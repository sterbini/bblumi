# FILL 9554

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9554.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_23_09_22_56_F9554.yaml) [:arrow_left:](../9553/index.md) [:arrow_right:](../9555/index.md)

**start**: 	April 23, 2024 09:22:56 AM UTC

**end**: 		April 23, 2024 09:56:37 AM UTC

**duration**: 0 days 00:33:40.504375

**tags**:	 

- DUCK



**comments**:	 

- RCBXv3.R5 trip



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

