# FILL 9401

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9401.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_21_16_17_44_F9401.yaml) [:arrow_left:](../9400/index.md) [:arrow_right:](../9402/index.md)

**start**: 	March 21, 2024 04:17:44 PM UTC

**end**: 		March 21, 2024 10:29:35 PM UTC

**duration**: 0 days 06:11:51.112375

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

