# FILL 10180

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10180.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_30_17_32_01_F10180.yaml) [:arrow_left:](../10179/index.md) [:arrow_right:](../10181/index.md)

**start**: 	September 30, 2024 05:32:01 PM UTC

**end**: 		September 30, 2024 11:24:10 PM UTC

**duration**: 0 days 05:52:08.672500

**tags**:	 

- MD



**comments**:	 

- MD11789 PPLP ramp



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

