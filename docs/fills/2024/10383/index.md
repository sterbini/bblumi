# FILL 10383

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10383.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_18_02_10_10_F10383.yaml) [:arrow_left:](../10382/index.md) [:arrow_right:](../10384/index.md)

**start**: 	November 18, 2024 02:10:10 AM UTC

**end**: 		November 18, 2024 09:07:18 AM UTC

**duration**: 0 days 06:57:08.698375

**tags**:	 

- ION_PHYSICS

- OP_DUMP



**comments**:	 

- 66/46 injections

- 60% losses at injection




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10383/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10383/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10383/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10383/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10383/Fill_chromaticity.png)

#### Intensity


##### BBB SB intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/10383/bbb_SB_intensity.png)

##### SB bunch intensity 

![](https://dltwo.web.cern.ch/followup_plots/2024/10383/Fill_adjust_stable_losses_summary.png)

##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10383/Fill_bunch_length.png)

#### Emittance


##### BBB in SB for B1 

![](https://dltwo.web.cern.ch/followup_plots/2024/10383/bbb_SB_B1_emit.png)

##### BBB in SB for B2 

![](https://dltwo.web.cern.ch/followup_plots/2024/10383/bbb_SB_B2_emit.png)

#### Luminosity


##### Overview

![](https://dltwo.web.cern.ch/followup_plots/2024/10383/Fill_luminosity.png)

##### Luminous Regions

![](https://dltwo.web.cern.ch/followup_plots/2024/10383/Fill_luminous_regions.png)

##### BBB luminosity in SB

![](https://dltwo.web.cern.ch/followup_plots/2024/10383/bbb_SB_luminosity.png)

#### Cross-sections


##### ADJUST

![](https://dltwo.web.cern.ch/followup_plots/2024/10383/Xsection_ADJUST_heatmap.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10383/Xsection_ADJUST_mean.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10383/Xsection_ADJUST_bbb_ip15lr.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10383/Xsection_ADJUST_bbb_ip2ho.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10383/Xsection_ADJUST_bbb_lhcbho.png)

##### STABLE BEAM

![](https://dltwo.web.cern.ch/followup_plots/2024/10383/Xsection_STABLE_heatmap_DBLM.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10383/Xsection_SB_mean_DBLM.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10383/Xsection_SB_mean_DBLM_separation.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10383/Xsection_SB_bbb_DBLM_ip15lr.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10383/Xsection_SB_bbb_DBLM_ip2ho.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10383/Xsection_SB_bbb_DBLM_lhcbho.png)

