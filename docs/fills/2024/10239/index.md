# FILL 10239

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10239.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_17_19_11_46_F10239.yaml) [:arrow_left:](../10238/index.md) [:arrow_right:](../10240/index.md)

**start**: 	October 17, 2024 07:11:46 PM UTC

**end**: 		October 17, 2024 07:49:56 PM UTC

**duration**: 0 days 00:38:09.894000

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

