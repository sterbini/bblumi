# FILL 9455

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9455.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_02_20_21_05_F9455.yaml) [:arrow_left:](../9454/index.md) [:arrow_right:](../9456/index.md)

**start**: 	April 02, 2024 08:21:05 PM UTC

**end**: 		April 03, 2024 01:47:03 AM UTC

**duration**: 0 days 05:25:58.531000

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- FASER Test

- Taiwan Earthquake (see https://indico.cern.ch/event/1400243/contributions/5886155)



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

