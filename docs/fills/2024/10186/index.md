# FILL 10186

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10186.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_02_02_15_19_F10186.yaml) [:arrow_left:](../10185/index.md) [:arrow_right:](../10187/index.md)

**start**: 	October 02, 2024 02:15:19 AM UTC

**end**: 		October 02, 2024 02:23:09 AM UTC

**duration**: 0 days 00:07:50.705875

**tags**:	 

- DUCK



**comments**:	 

- nan



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

