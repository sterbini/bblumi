# FILL 9423

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9423.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_25_02_28_46_F9423.yaml) [:arrow_left:](../9422/index.md) [:arrow_right:](../9424/index.md)

**start**: 	March 25, 2024 02:28:46 AM UTC

**end**: 		March 25, 2024 05:36:58 PM UTC

**duration**: 0 days 15:08:11.517250

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

