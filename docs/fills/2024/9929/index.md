# FILL 9929

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9929.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_24_12_36_57_F9929.yaml) [:arrow_left:](../9928/index.md) [:arrow_right:](../9930/index.md)

**start**: 	July 24, 2024 12:36:57 PM UTC

**end**: 		July 24, 2024 12:38:03 PM UTC

**duration**: 0 days 00:01:06.785250

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

