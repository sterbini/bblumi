# FILL 10351

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10351.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_11_11_37_24_F10351.yaml) [:arrow_left:](../10350/index.md) [:arrow_right:](../10352/index.md)

**start**: 	November 11, 2024 11:37:24 AM UTC

**end**: 		November 11, 2024 03:09:08 PM UTC

**duration**: 0 days 03:31:44.429375

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

