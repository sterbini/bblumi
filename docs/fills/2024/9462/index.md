# FILL 9462

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9462.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_04_06_41_59_F9462.yaml) [:arrow_left:](../9461/index.md) [:arrow_right:](../9463/index.md)

**start**: 	April 04, 2024 06:41:59 AM UTC

**end**: 		April 04, 2024 08:03:53 AM UTC

**duration**: 0 days 01:21:54.379500

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

