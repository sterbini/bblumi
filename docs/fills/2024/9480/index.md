# FILL 9480

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9480.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_07_19_50_52_F9480.yaml) [:arrow_left:](../9479/index.md) [:arrow_right:](../9481/index.md)

**start**: 	April 07, 2024 07:50:52 PM UTC

**end**: 		April 07, 2024 07:53:06 PM UTC

**duration**: 0 days 00:02:13.306250

**tags**:	 

- INTENSITY_RAMPUP



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

