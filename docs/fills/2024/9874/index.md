# FILL 9874

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9874.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_08_08_02_55_F9874.yaml) [:arrow_left:](../9873/index.md) [:arrow_right:](../9875/index.md)

**start**: 	July 08, 2024 08:02:55 AM UTC

**end**: 		July 08, 2024 11:54:27 AM UTC

**duration**: 0 days 03:51:32.814750

**tags**:	 

- DUCK



**comments**:	 

- RCBH issue is fixed



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

