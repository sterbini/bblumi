# FILL 9627

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9627.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_14_15_47_51_F9627.yaml) [:arrow_left:](../9626/index.md) [:arrow_right:](../9628/index.md)

**start**: 	May 14, 2024 03:47:51 PM UTC

**end**: 		May 14, 2024 07:12:43 PM UTC

**duration**: 0 days 03:24:52.060125

**tags**:	 

- MD



**comments**:	 

- MD11743 Effect of higher-order contributions to octupole and decapole RDTs



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

