# FILL 10136

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10136.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_22_04_35_24_F10136.yaml) [:arrow_left:](../10135/index.md) [:arrow_right:](../10137/index.md)

**start**: 	September 22, 2024 04:35:24 AM UTC

**end**: 		September 22, 2024 04:48:00 AM UTC

**duration**: 0 days 00:12:36.212125

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

