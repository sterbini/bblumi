# FILL 9362

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9362.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_15_05_16_03_F9362.yaml) [:arrow_left:](../9361/index.md) [:arrow_right:](../9363/index.md)

**start**: 	March 15, 2024 05:16:03 AM UTC

**end**: 		March 15, 2024 06:26:17 AM UTC

**duration**: 0 days 01:10:14.864250

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

