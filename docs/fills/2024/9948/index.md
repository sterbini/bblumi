# FILL 9948

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9948.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_30_16_42_11_F9948.yaml) [:arrow_left:](../9947/index.md) [:arrow_right:](../9949/index.md)

**start**: 	July 30, 2024 04:42:11 PM UTC

**end**: 		July 30, 2024 04:59:01 PM UTC

**duration**: 0 days 00:16:50.017000

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

