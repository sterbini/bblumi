# FILL 9450

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9450.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_01_13_58_05_F9450.yaml) [:arrow_left:](../9449/index.md) [:arrow_right:](../9451/index.md)

**start**: 	April 01, 2024 01:58:05 PM UTC

**end**: 		April 01, 2024 09:29:04 PM UTC

**duration**: 0 days 07:30:59.503875

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- optics at 22cm



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

