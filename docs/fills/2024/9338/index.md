# FILL 9338

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9338.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_10_10_14_48_F9338.yaml) [:arrow_left:](../9337/index.md) [:arrow_right:](../9339/index.md)

**start**: 	March 10, 2024 10:14:48 AM UTC

**end**: 		March 10, 2024 12:16:17 PM UTC

**duration**: 0 days 02:01:28.560500

**tags**:	 

- BEAM_COMMISSIONING

- VDM



**comments**:	 

- non optimised vdm ramp



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

