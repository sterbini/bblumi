# FILL 9328

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9328.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_08_08_00_23_F9328.yaml) [:arrow_left:](../9327/index.md) [:arrow_right:](../9329/index.md)

**start**: 	March 08, 2024 08:00:23 AM UTC

**end**: 		March 08, 2024 12:56:04 PM UTC

**duration**: 0 days 04:55:41.391500

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

