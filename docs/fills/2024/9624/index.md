# FILL 9624

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9624.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_14_04_51_39_F9624.yaml) [:arrow_left:](../9623/index.md) [:arrow_right:](../9625/index.md)

**start**: 	May 14, 2024 04:51:39 AM UTC

**end**: 		May 14, 2024 06:58:59 AM UTC

**duration**: 0 days 02:07:19.992875

**tags**:	 

- MD



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

