# FILL 9345

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9345.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_11_15_15_10_F9345.yaml) [:arrow_left:](../9344/index.md) [:arrow_right:](../9346/index.md)

**start**: 	March 11, 2024 03:15:10 PM UTC

**end**: 		March 11, 2024 06:49:25 PM UTC

**duration**: 0 days 03:34:14.619375

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

