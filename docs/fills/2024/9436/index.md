# FILL 9436

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9436.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_28_06_17_37_F9436.yaml) [:arrow_left:](../9435/index.md) [:arrow_right:](../9437/index.md)

**start**: 	March 28, 2024 06:17:37 AM UTC

**end**: 		March 28, 2024 08:01:41 AM UTC

**duration**: 0 days 01:44:04.387000

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

