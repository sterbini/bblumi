# FILL 9671

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9671.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_26_03_47_08_F9671.yaml) [:arrow_left:](../9670/index.md) [:arrow_right:](../9672/index.md)

**start**: 	May 26, 2024 03:47:08 AM UTC

**end**: 		May 26, 2024 07:29:06 AM UTC

**duration**: 0 days 03:41:58.334375

**tags**:	 

- MD



**comments**:	 

- loss maps




#### Cycle duration

![](https://dltwo.web.cern.ch/followup_plots/2024/9671/Cycle_duration.png)


#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9671/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9671/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9671/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9671/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9671/Fill_chromaticity.png)

#### Intensity


##### Cycle intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/9671/Cycle_intensity.png)

##### Violin BBB intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/9671/Cycle_violin_intensity_bl.png)

#### Bunch Length


![](https://dltwo.web.cern.ch/followup_plots/2024/9671/Cycle_bunchlength.png)

##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9671/Fill_bunch_length.png)

#### Emittance


##### B1 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/9671/Cycle_emit_b1.png)

##### B2 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/9671/Cycle_emit_b2.png)

##### Summary violin plot

![](https://dltwo.web.cern.ch/followup_plots/2024/9671/Cycle_violin_emit.png)

#### Bunch brightness


![](https://dltwo.web.cern.ch/followup_plots/2024/9671/Cycle_brightness.png)

#### Beam properties summary 

![](https://dltwo.web.cern.ch/followup_plots/2024/9671/Cycle_summary.png)

#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

