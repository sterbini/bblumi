# FILL 10078

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10078.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_02_03_56_31_F10078.yaml) [:arrow_left:](../10077/index.md) [:arrow_right:](../10079/index.md)

**start**: 	September 02, 2024 03:56:31 AM UTC

**end**: 		September 02, 2024 07:28:15 PM UTC

**duration**: 0 days 15:31:43.695250

**tags**:	 

- DUCK



**comments**:	 

- cryo recovery after quench in S67 and S78



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

