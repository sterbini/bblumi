# FILL 9993

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9993.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_11_13_14_23_F9993.yaml) [:arrow_left:](../9992/index.md) [:arrow_right:](../9994/index.md)

**start**: 	August 11, 2024 01:14:23 PM UTC

**end**: 		August 11, 2024 03:56:18 PM UTC

**duration**: 0 days 02:41:55.176000

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

