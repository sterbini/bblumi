# FILL 9386

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9386.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_19_05_21_28_F9386.yaml) [:arrow_left:](../9385/index.md) [:arrow_right:](../9387/index.md)

**start**: 	March 19, 2024 05:21:28 AM UTC

**end**: 		March 19, 2024 08:26:24 AM UTC

**duration**: 0 days 03:04:55.788000

**tags**:	 

- BEAM_COMMISSIONING

- VDM



**comments**:	 

- VDM collisions



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

