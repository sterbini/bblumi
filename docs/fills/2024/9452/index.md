# FILL 9452

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9452.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_02_01_21_40_F9452.yaml) [:arrow_left:](../9451/index.md) [:arrow_right:](../9453/index.md)

**start**: 	April 02, 2024 01:21:40 AM UTC

**end**: 		April 02, 2024 05:47:53 AM UTC

**duration**: 0 days 04:26:12.343750

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

