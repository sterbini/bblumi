# FILL 9377

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9377.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_17_23_21_02_F9377.yaml) [:arrow_left:](../9376/index.md) [:arrow_right:](../9378/index.md)

**start**: 	March 17, 2024 11:21:02 PM UTC

**end**: 		March 18, 2024 12:59:37 AM UTC

**duration**: 0 days 01:38:35.085125

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- low-beta/VDM cycles for coupling in the ramp



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

