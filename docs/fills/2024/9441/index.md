# FILL 9441

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9441.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_29_02_36_16_F9441.yaml) [:arrow_left:](../9440/index.md) [:arrow_right:](../9442/index.md)

**start**: 	March 29, 2024 02:36:16 AM UTC

**end**: 		March 29, 2024 05:25:43 AM UTC

**duration**: 0 days 02:49:26.715125

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

