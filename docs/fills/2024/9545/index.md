# FILL 9545

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9545.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_20_09_34_34_F9545.yaml) [:arrow_left:](../9544/index.md) [:arrow_right:](../9546/index.md)

**start**: 	April 20, 2024 09:34:34 AM UTC

**end**: 		April 20, 2024 11:17:48 AM UTC

**duration**: 0 days 01:43:14.304125

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

