# FILL 10062

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10062.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_27_01_38_25_F10062.yaml) [:arrow_left:](../10061/index.md) [:arrow_right:](../10063/index.md)

**start**: 	August 27, 2024 01:38:25 AM UTC

**end**: 		August 28, 2024 02:58:14 PM UTC

**duration**: 1 days 13:19:49.541875

**tags**:	 

- DUCK



**comments**:	 

- Access for following the Point 8 cryo problem



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

