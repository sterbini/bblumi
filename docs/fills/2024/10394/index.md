# FILL 10394

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10394.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_20_15_08_00_F10394.yaml) [:arrow_left:](../10393/index.md) [:arrow_right:](../10395/index.md)

**start**: 	November 20, 2024 03:08:00 PM UTC

**end**: 		November 20, 2024 06:39:31 PM UTC

**duration**: 0 days 03:31:30.969750

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

