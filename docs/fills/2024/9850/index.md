# FILL 9850

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9850.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_02_13_12_45_F9850.yaml) [:arrow_left:](../9849/index.md) [:arrow_right:](../9851/index.md)

**start**: 	July 02, 2024 01:12:45 PM UTC

**end**: 		July 02, 2024 02:43:42 PM UTC

**duration**: 0 days 01:30:57.365000

**tags**:	 

- DUCK



**comments**:	 

- Long recovery due to a QPS and RF issue



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

