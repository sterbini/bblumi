# FILL 9797

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9797.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_19_04_00_45_F9797.yaml) [:arrow_left:](../9796/index.md) [:arrow_right:](../9798/index.md)

**start**: 	June 19, 2024 04:00:45 AM UTC

**end**: 		June 19, 2024 04:03:50 AM UTC

**duration**: 0 days 00:03:05.697875

**tags**:	 

- DUCK



**comments**:	 

- 



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

