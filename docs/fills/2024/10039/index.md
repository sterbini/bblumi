# FILL 10039

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10039.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_21_16_58_05_F10039.yaml) [:arrow_left:](../10038/index.md) [:arrow_right:](../10040/index.md)

**start**: 	August 21, 2024 04:58:05 PM UTC

**end**: 		August 22, 2024 02:08:59 AM UTC

**duration**: 0 days 09:10:53.563875

**tags**:	 

- MD



**comments**:	 

- MD6925 Electron cloud coupled-bunch tune shifts at injection



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

