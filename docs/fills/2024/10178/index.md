# FILL 10178

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10178.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_30_14_47_54_F10178.yaml) [:arrow_left:](../10177/index.md) [:arrow_right:](../10179/index.md)

**start**: 	September 30, 2024 02:47:54 PM UTC

**end**: 		September 30, 2024 03:44:03 PM UTC

**duration**: 0 days 00:56:08.856500

**tags**:	 

- MD



**comments**:	 

- MD11789 and MD13403 ramp tests (PPLP fast ramp and smooth ramp for 2025)



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

