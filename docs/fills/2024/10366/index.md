# FILL 10366

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10366.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_14_09_49_07_F10366.yaml) [:arrow_left:](../10365/index.md) [:arrow_right:](../10367/index.md)

**start**: 	November 14, 2024 09:49:07 AM UTC

**end**: 		November 14, 2024 10:32:22 AM UTC

**duration**: 0 days 00:43:14.832500

**tags**:	 

- MD



**comments**:	 

- MD14364 RF flat bottom optimization for ion debunching

- VRF = 12 MV, less than 1.3 ns after 30 minutes, 8h of beam lifetime, 1.13% debunched beam




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10366/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10366/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10366/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10366/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10366/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10366/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

