# FILL 9952

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9952.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_31_09_33_26_F9952.yaml) [:arrow_left:](../9951/index.md) [:arrow_right:](../9953/index.md)

**start**: 	July 31, 2024 09:33:26 AM UTC

**end**: 		July 31, 2024 09:33:40 AM UTC

**duration**: 0 days 00:00:13.533375

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

