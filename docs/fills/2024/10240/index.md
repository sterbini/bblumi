# FILL 10240

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10240.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_17_19_49_56_F10240.yaml) [:arrow_left:](../10239/index.md) [:arrow_right:](../10241/index.md)

**start**: 	October 17, 2024 07:49:56 PM UTC

**end**: 		October 18, 2024 08:56:20 AM UTC

**duration**: 0 days 13:06:24.808250

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

