# FILL 10284

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10284.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_26_05_30_45_F10284.yaml) [:arrow_left:](../10283/index.md) [:arrow_right:](../10285/index.md)

**start**: 	October 26, 2024 05:30:45 AM UTC

**end**: 		October 26, 2024 06:22:36 AM UTC

**duration**: 0 days 00:51:51.345875

**tags**:	 

- BEAM_COMMISSIONING

- ION_OPTICS



**comments**:	 


#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10284/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10284/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10284/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10284/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10284/Fill_chromaticity.png)

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

