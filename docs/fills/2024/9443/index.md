# FILL 9443

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9443.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_30_16_03_50_F9443.yaml) [:arrow_left:](../9442/index.md) [:arrow_right:](../9444/index.md)

**start**: 	March 30, 2024 04:03:50 PM UTC

**end**: 		March 31, 2024 05:03:02 AM UTC

**duration**: 0 days 12:59:11.499125

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- FASER background test TCL @1.6mm & 1.2mm half-gap



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

