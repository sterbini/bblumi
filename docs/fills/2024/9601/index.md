# FILL 9601

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9601.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_04_21_54_27_F9601.yaml) [:arrow_left:](../9600/index.md) [:arrow_right:](../9602/index.md)

**start**: 	May 04, 2024 09:54:27 PM UTC

**end**: 		May 04, 2024 09:54:58 PM UTC

**duration**: 0 days 00:00:31.034625

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

