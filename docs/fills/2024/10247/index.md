# FILL 10247

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10247.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_19_00_09_28_F10247.yaml) [:arrow_left:](../10246/index.md) [:arrow_right:](../10248/index.md)

**start**: 	October 19, 2024 12:09:28 AM UTC

**end**: 		October 19, 2024 12:31:12 AM UTC

**duration**: 0 days 00:21:43.570125

**tags**:	 

- MD



**comments**:	 

- RF MD12743 RF power limitations for high-intensity batches




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10247/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10247/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10247/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10247/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10247/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10247/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

