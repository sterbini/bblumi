# FILL 9534

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9534.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_17_17_04_04_F9534.yaml) [:arrow_left:](../9533/index.md) [:arrow_right:](../9535/index.md)

**start**: 	April 17, 2024 05:04:04 PM UTC

**end**: 		April 17, 2024 06:07:40 PM UTC

**duration**: 0 days 01:03:35.693500

**tags**:	 

- DUCK



**comments**:	 

- RB.A12 trip



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

