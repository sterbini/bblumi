# FILL 9903

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9903.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_15_16_38_13_F9903.yaml) [:arrow_left:](../9902/index.md) [:arrow_right:](../9904/index.md)

**start**: 	July 15, 2024 04:38:13 PM UTC

**end**: 		July 15, 2024 06:11:04 PM UTC

**duration**: 0 days 01:32:51.740125

**tags**:	 

- DUCK



**comments**:	 

- Ramping up ALICE solenoid



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

