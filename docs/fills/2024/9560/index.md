# FILL 9560

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9560.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_24_08_55_51_F9560.yaml) [:arrow_left:](../9559/index.md) [:arrow_right:](../9561/index.md)

**start**: 	April 24, 2024 08:55:51 AM UTC

**end**: 		April 25, 2024 09:15:34 AM UTC

**duration**: 1 days 00:19:42.902375

**tags**:	 

- DUCK



**comments**:	 

- Water leak issue in P8



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

