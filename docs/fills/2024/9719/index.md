# FILL 9719

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9719.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_06_08_53_11_F9719.yaml) [:arrow_left:](../9718/index.md) [:arrow_right:](../9720/index.md)

**start**: 	June 06, 2024 08:53:11 AM UTC

**end**: 		June 06, 2024 11:56:41 AM UTC

**duration**: 0 days 03:03:30.117125

**tags**:	 

- MD

- OP_DUMP



**comments**:	 

- MD11243, HL-LHC optics cycle (part I) optics preparation



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

