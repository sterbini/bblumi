# FILL 10128

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10128.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_19_05_54_26_F10128.yaml) [:arrow_left:](../10127/index.md) [:arrow_right:](../10129/index.md)

**start**: 	September 19, 2024 05:54:26 AM UTC

**end**: 		September 19, 2024 02:28:42 PM UTC

**duration**: 0 days 08:34:15.834125

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- Commissioning of pp reference run (optics commissioning at injection and flattop)



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

