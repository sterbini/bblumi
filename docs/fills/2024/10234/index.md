# FILL 10234

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10234.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_16_21_36_44_F10234.yaml) [:arrow_left:](../10233/index.md) [:arrow_right:](../10235/index.md)

**start**: 	October 16, 2024 09:36:44 PM UTC

**end**: 		October 17, 2024 10:07:29 AM UTC

**duration**: 0 days 12:30:45.122000

**tags**:	 

- MD



**comments**:	 

- MD9405 Instability Growth Rate at Injection 2024

- chroma to 2, -5, -10, -15,

- used pencil beams (emit=0.6) at 04:35 onwards



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

