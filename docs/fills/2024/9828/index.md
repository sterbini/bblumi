# FILL 9828

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9828.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_26_13_09_19_F9828.yaml) [:arrow_left:](../9827/index.md) [:arrow_right:](../9829/index.md)

**start**: 	June 26, 2024 01:09:19 PM UTC

**end**: 		June 26, 2024 07:40:18 PM UTC

**duration**: 0 days 06:30:58.992250

**tags**:	 

- DUCK



**comments**:	 

- BPM calibration attempts

- issue with SPS magnet

- TI2 transfer line



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

