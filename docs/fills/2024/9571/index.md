# FILL 9571

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9571.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_28_15_12_13_F9571.yaml) [:arrow_left:](../9570/index.md) [:arrow_right:](../9572/index.md)

**start**: 	April 28, 2024 03:12:13 PM UTC

**end**: 		April 29, 2024 06:56:26 AM UTC

**duration**: 0 days 15:44:13.780875

**tags**:	 

- DUCK



**comments**:	 

- magnet quench



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

