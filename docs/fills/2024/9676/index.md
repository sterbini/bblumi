# FILL 9676

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9676.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_27_13_54_33_F9676.yaml) [:arrow_left:](../9675/index.md) [:arrow_right:](../9677/index.md)

**start**: 	May 27, 2024 01:54:33 PM UTC

**end**: 		May 27, 2024 02:19:59 PM UTC

**duration**: 0 days 00:25:25.259875

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

