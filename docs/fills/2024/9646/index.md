# FILL 9646

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9646.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_19_08_09_23_F9646.yaml) [:arrow_left:](../9645/index.md) [:arrow_right:](../9647/index.md)

**start**: 	May 19, 2024 08:09:23 AM UTC

**end**: 		May 19, 2024 09:11:37 AM UTC

**duration**: 0 days 01:02:13.706250

**tags**:	 

- DUCK



**comments**:	 

- Problems with QPS OK on several circuits during preparation for precycle



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

