# FILL 10192

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10192.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_03_05_39_35_F10192.yaml) [:arrow_left:](../10191/index.md) [:arrow_right:](../10193/index.md)

**start**: 	October 03, 2024 05:39:35 AM UTC

**end**: 		October 03, 2024 07:51:10 AM UTC

**duration**: 0 days 02:11:34.471500

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

