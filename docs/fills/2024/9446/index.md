# FILL 9446

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9446.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_31_15_21_32_F9446.yaml) [:arrow_left:](../9445/index.md) [:arrow_right:](../9447/index.md)

**start**: 	March 31, 2024 03:21:32 PM UTC

**end**: 		April 01, 2024 05:08:15 AM UTC

**duration**: 0 days 13:46:43.660750

**tags**:	 

- BEAM_COMMISSIONING

- ION_OPTICS



**comments**:	 

- ion optics at 1m and 50cm



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

