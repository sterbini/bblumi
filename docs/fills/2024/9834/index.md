# FILL 9834

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9834.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_28_13_57_22_F9834.yaml) [:arrow_left:](../9833/index.md) [:arrow_right:](../9835/index.md)

**start**: 	June 28, 2024 01:57:22 PM UTC

**end**: 		June 28, 2024 03:34:16 PM UTC

**duration**: 0 days 01:36:54.500875

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

