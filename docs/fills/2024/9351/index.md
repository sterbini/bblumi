# FILL 9351

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9351.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_13_06_02_57_F9351.yaml) [:arrow_left:](../9350/index.md) [:arrow_right:](../9352/index.md)

**start**: 	March 13, 2024 06:02:57 AM UTC

**end**: 		March 13, 2024 01:09:23 PM UTC

**duration**: 0 days 07:06:25.384625

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- beam splashes



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

