# FILL 10165

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10165.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_28_00_38_54_F10165.yaml) [:arrow_left:](../10164/index.md) [:arrow_right:](../10166/index.md)

**start**: 	September 28, 2024 12:38:54 AM UTC

**end**: 		September 28, 2024 01:15:25 AM UTC

**duration**: 0 days 00:36:31.277000

**tags**:	 

- MD



**comments**:	 

- 6925Electroncloudcoupled-bunchtuneshiftsatinjection



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

