# FILL 9494

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9494.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_10_08_41_40_F9494.yaml) [:arrow_left:](../9493/index.md) [:arrow_right:](../9495/index.md)

**start**: 	April 10, 2024 08:41:40 AM UTC

**end**: 		April 10, 2024 08:45:37 AM UTC

**duration**: 0 days 00:03:57.426375

**tags**:	 

- INTENSITY_RAMPUP



**comments**:	 

- Just after the access



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

