# FILL 10058

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10058.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_26_10_15_58_F10058.yaml) [:arrow_left:](../10057/index.md) [:arrow_right:](../10059/index.md)

**start**: 	August 26, 2024 10:15:58 AM UTC

**end**: 		August 26, 2024 12:37:05 PM UTC

**duration**: 0 days 02:21:07.037375

**tags**:	 

- DUCK



**comments**:	 

- ADT tests on B1 INDIVs



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

