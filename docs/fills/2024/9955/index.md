# FILL 9955

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9955.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_31_10_17_05_F9955.yaml) [:arrow_left:](../9954/index.md) [:arrow_right:](../9956/index.md)

**start**: 	July 31, 2024 10:17:05 AM UTC

**end**: 		July 31, 2024 10:20:19 AM UTC

**duration**: 0 days 00:03:14.283625

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

