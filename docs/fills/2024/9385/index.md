# FILL 9385

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9385.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_18_20_23_14_F9385.yaml) [:arrow_left:](../9384/index.md) [:arrow_right:](../9386/index.md)

**start**: 	March 18, 2024 08:23:14 PM UTC

**end**: 		March 19, 2024 05:21:28 AM UTC

**duration**: 0 days 08:58:13.344375

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

