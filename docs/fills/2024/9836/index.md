# FILL 9836

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9836.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_28_20_07_43_F9836.yaml) [:arrow_left:](../9835/index.md) [:arrow_right:](../9837/index.md)

**start**: 	June 28, 2024 08:07:43 PM UTC

**end**: 		June 28, 2024 08:31:15 PM UTC

**duration**: 0 days 00:23:31.968875

**tags**:	 

- DUCK



**comments**:	 

- Pre-cycle



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

