# FILL 9805

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9805.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_20_04_17_11_F9805.yaml) [:arrow_left:](../9804/index.md) [:arrow_right:](../9806/index.md)

**start**: 	June 20, 2024 04:17:11 AM UTC

**end**: 		June 20, 2024 05:40:27 AM UTC

**duration**: 0 days 01:23:15.725250

**tags**:	 

- DUCK



**comments**:	 

- BPMs miscalibrated (40 MHz)




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9805/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9805/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9805/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9805/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9805/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9805/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

