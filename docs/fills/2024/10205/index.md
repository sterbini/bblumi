# FILL 10205

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10205.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_07_08_51_42_F10205.yaml) [:arrow_left:](../10204/index.md) [:arrow_right:](../10206/index.md)

**start**: 	October 07, 2024 08:51:42 AM UTC

**end**: 		October 07, 2024 09:44:09 AM UTC

**duration**: 0 days 00:52:27.110375

**tags**:	 

- DUCK



**comments**:	 

- ADT issues at SPS



#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10205/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10205/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10205/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10205/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10205/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

