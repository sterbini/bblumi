# FILL 9933

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9933.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_25_22_46_52_F9933.yaml) [:arrow_left:](../9932/index.md) [:arrow_right:](../9934/index.md)

**start**: 	July 25, 2024 10:46:52 PM UTC

**end**: 		July 25, 2024 11:46:46 PM UTC

**duration**: 0 days 00:59:54.370500

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

