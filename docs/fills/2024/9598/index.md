# FILL 9598

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9598.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_04_20_59_56_F9598.yaml) [:arrow_left:](../9597/index.md) [:arrow_right:](../9599/index.md)

**start**: 	May 04, 2024 08:59:56 PM UTC

**end**: 		May 04, 2024 09:13:22 PM UTC

**duration**: 0 days 00:13:26.368625

**tags**:	 

- DUCK



**comments**:	 

- BCCM test




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9598/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9598/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9598/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9598/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9598/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9598/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

