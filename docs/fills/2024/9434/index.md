# FILL 9434

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9434.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_27_19_21_49_F9434.yaml) [:arrow_left:](../9433/index.md) [:arrow_right:](../9435/index.md)

**start**: 	March 27, 2024 07:21:49 PM UTC

**end**: 		March 27, 2024 08:54:22 PM UTC

**duration**: 0 days 01:32:32.876625

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

