# FILL 9396

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9396.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_20_22_06_23_F9396.yaml) [:arrow_left:](../9395/index.md) [:arrow_right:](../9397/index.md)

**start**: 	March 20, 2024 10:06:23 PM UTC

**end**: 		March 20, 2024 10:59:45 PM UTC

**duration**: 0 days 00:53:22.224500

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

