# FILL 10079

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10079.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_02_19_28_15_F10079.yaml) [:arrow_left:](../10078/index.md) [:arrow_right:](../10080/index.md)

**start**: 	September 02, 2024 07:28:15 PM UTC

**end**: 		September 02, 2024 08:40:09 PM UTC

**duration**: 0 days 01:11:54.170500

**tags**:	 

- DUCK



**comments**:	 

- cryo recovery after quench in S67 and S78



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

