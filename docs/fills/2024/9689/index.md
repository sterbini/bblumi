# FILL 9689

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9689.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_30_03_13_45_F9689.yaml) [:arrow_left:](../9688/index.md) [:arrow_right:](../9690/index.md)

**start**: 	May 30, 2024 03:13:45 AM UTC

**end**: 		May 30, 2024 03:41:05 AM UTC

**duration**: 0 days 00:27:20.770750

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

