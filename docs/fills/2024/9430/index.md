# FILL 9430

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9430.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_27_01_20_25_F9430.yaml) [:arrow_left:](../9429/index.md) [:arrow_right:](../9431/index.md)

**start**: 	March 27, 2024 01:20:25 AM UTC

**end**: 		March 27, 2024 04:04:12 AM UTC

**duration**: 0 days 02:43:46.626500

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- ion cycle



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

