# FILL 9957

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9957.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_31_10_22_32_F9957.yaml) [:arrow_left:](../9956/index.md) [:arrow_right:](../9958/index.md)

**start**: 	July 31, 2024 10:22:32 AM UTC

**end**: 		July 31, 2024 10:25:35 AM UTC

**duration**: 0 days 00:03:02.984125

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

