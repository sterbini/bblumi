# FILL 9612

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9612.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_10_13_06_17_F9612.yaml) [:arrow_left:](../9611/index.md) [:arrow_right:](../9613/index.md)

**start**: 	May 10, 2024 01:06:17 PM UTC

**end**: 		May 11, 2024 04:29:04 PM UTC

**duration**: 1 days 03:22:46.803000

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

