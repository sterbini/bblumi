# FILL 9615

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9615.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_12_10_06_54_F9615.yaml) [:arrow_left:](../9614/index.md) [:arrow_right:](../9616/index.md)

**start**: 	May 12, 2024 10:06:54 AM UTC

**end**: 		May 12, 2024 03:58:54 PM UTC

**duration**: 0 days 05:52:00.052625

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

