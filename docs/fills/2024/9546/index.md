# FILL 9546

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9546.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_20_11_17_48_F9546.yaml) [:arrow_left:](../9545/index.md) [:arrow_right:](../9547/index.md)

**start**: 	April 20, 2024 11:17:48 AM UTC

**end**: 		April 20, 2024 11:25:41 AM UTC

**duration**: 0 days 00:07:52.764375

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

