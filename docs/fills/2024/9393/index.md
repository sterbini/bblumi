# FILL 9393

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9393.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_19_22_38_13_F9393.yaml) [:arrow_left:](../9392/index.md) [:arrow_right:](../9394/index.md)

**start**: 	March 19, 2024 10:38:13 PM UTC

**end**: 		March 20, 2024 09:51:33 AM UTC

**duration**: 0 days 11:13:20.933250

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- collimator alignment at FT



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

