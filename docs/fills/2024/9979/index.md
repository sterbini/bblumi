# FILL 9979

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9979.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_06_06_29_49_F9979.yaml) [:arrow_left:](../9978/index.md) [:arrow_right:](../9980/index.md)

**start**: 	August 06, 2024 06:29:49 AM UTC

**end**: 		August 06, 2024 10:54:55 AM UTC

**duration**: 0 days 04:25:06.513000

**tags**:	 

- DUCK



**comments**:	 

- Access for the LHCb magnet PC (broken fan)



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

