# FILL 9584

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9584.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_02_17_17_48_F9584.yaml) [:arrow_left:](../9583/index.md) [:arrow_right:](../9585/index.md)

**start**: 	May 02, 2024 05:17:48 PM UTC

**end**: 		May 02, 2024 05:32:22 PM UTC

**duration**: 0 days 00:14:34.160000

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

