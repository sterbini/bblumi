# FILL 9892

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9892.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_13_01_22_15_F9892.yaml) [:arrow_left:](../9891/index.md) [:arrow_right:](../9893/index.md)

**start**: 	July 13, 2024 01:22:15 AM UTC

**end**: 		July 13, 2024 01:30:30 AM UTC

**duration**: 0 days 00:08:15.254375

**tags**:	 

- DUCK



**comments**:	 

- 



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

