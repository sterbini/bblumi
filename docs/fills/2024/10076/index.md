# FILL 10076

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10076.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_01_13_54_47_F10076.yaml) [:arrow_left:](../10075/index.md) [:arrow_right:](../10077/index.md)

**start**: 	September 01, 2024 01:54:47 PM UTC

**end**: 		September 01, 2024 04:17:58 PM UTC

**duration**: 0 days 02:23:10.967625

**tags**:	 

- DUCK



**comments**:	 

- electrical perturbation in 225kV line



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

