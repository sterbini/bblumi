# FILL 9830

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9830.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_26_23_32_49_F9830.yaml) [:arrow_left:](../9829/index.md) [:arrow_right:](../9831/index.md)

**start**: 	June 26, 2024 11:32:49 PM UTC

**end**: 		June 27, 2024 01:03:23 AM UTC

**duration**: 0 days 01:30:34.326375

**tags**:	 

- DUCK



**comments**:	 

- injection issues

- SPS non availability



#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9830/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9830/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9830/Fill_chromaticity.png)

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

