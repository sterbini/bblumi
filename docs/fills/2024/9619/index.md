# FILL 9619

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9619.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_13_05_52_58_F9619.yaml) [:arrow_left:](../9618/index.md) [:arrow_right:](../9620/index.md)

**start**: 	May 13, 2024 05:52:58 AM UTC

**end**: 		May 13, 2024 07:14:49 AM UTC

**duration**: 0 days 01:21:51.394000

**tags**:	 

- MD



**comments**:	 

- start of MD1

- MD9273 (auto alignment TL coll) and MD11723 (schottkly) parallel on B2 and B1




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9619/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9619/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9619/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9619/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9619/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9619/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

