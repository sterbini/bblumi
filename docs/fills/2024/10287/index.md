# FILL 10287

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10287.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_26_19_41_35_F10287.yaml) [:arrow_left:](../10286/index.md) [:arrow_right:](../10288/index.md)

**start**: 	October 26, 2024 07:41:35 PM UTC

**end**: 		October 27, 2024 01:52:26 AM UTC

**duration**: 0 days 06:10:50.699750

**tags**:	 

- BEAM_COMMISSIONING

- PPREF_OPTICS



**comments**:	 

- AFP alignment and loss maps and ASD



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

