# FILL 9863

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9863.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_05_22_49_40_F9863.yaml) [:arrow_left:](../9862/index.md) [:arrow_right:](../9864/index.md)

**start**: 	July 05, 2024 10:49:40 PM UTC

**end**: 		July 05, 2024 11:18:34 PM UTC

**duration**: 0 days 00:28:54.673875

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

