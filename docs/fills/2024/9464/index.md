# FILL 9464

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9464.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_04_08_25_54_F9464.yaml) [:arrow_left:](../9463/index.md) [:arrow_right:](../9465/index.md)

**start**: 	April 04, 2024 08:25:54 AM UTC

**end**: 		April 04, 2024 11:57:54 AM UTC

**duration**: 0 days 03:31:59.676625

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- Loss maps at injection

-  dBLM phasing



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

