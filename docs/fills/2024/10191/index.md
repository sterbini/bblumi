# FILL 10191

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10191.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_03_01_39_07_F10191.yaml) [:arrow_left:](../10190/index.md) [:arrow_right:](../10192/index.md)

**start**: 	October 03, 2024 01:39:07 AM UTC

**end**: 		October 03, 2024 05:39:35 AM UTC

**duration**: 0 days 04:00:28.412250

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

