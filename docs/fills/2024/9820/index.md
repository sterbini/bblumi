# FILL 9820

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9820.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_24_05_56_22_F9820.yaml) [:arrow_left:](../9819/index.md) [:arrow_right:](../9821/index.md)

**start**: 	June 24, 2024 05:56:22 AM UTC

**end**: 		June 24, 2024 07:57:18 AM UTC

**duration**: 0 days 02:00:55.303500

**tags**:	 

- DUCK



**comments**:	 

- None



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

