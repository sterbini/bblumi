# FILL 10293

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10293.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_27_08_02_36_F10293.yaml) [:arrow_left:](../10292/index.md) [:arrow_right:](../10294/index.md)

**start**: 	October 27, 2024 08:02:36 AM UTC

**end**: 		October 27, 2024 09:20:33 AM UTC

**duration**: 0 days 01:17:56.788250

**tags**:	 

- BEAM_COMMISSIONING

- IONS_OPTICS



**comments**:	 

- BWS checks at FT




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10293/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10293/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10293/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10293/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10293/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10293/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

