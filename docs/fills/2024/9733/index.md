# FILL 9733

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9733.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_08_00_09_08_F9733.yaml) [:arrow_left:](../9732/index.md) [:arrow_right:](../9734/index.md)

**start**: 	June 08, 2024 12:09:08 AM UTC

**end**: 		June 08, 2024 12:37:26 AM UTC

**duration**: 0 days 00:28:18.411500

**tags**:	 

- MD



**comments**:	 

- MD10483 continue




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9733/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9733/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9733/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9733/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9733/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9733/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

