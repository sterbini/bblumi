# FILL 9350

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9350.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_12_22_02_03_F9350.yaml) [:arrow_left:](../9349/index.md) [:arrow_right:](../9351/index.md)

**start**: 	March 12, 2024 10:02:03 PM UTC

**end**: 		March 13, 2024 06:02:57 AM UTC

**duration**: 0 days 08:00:54.056749900

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- optics at injection

- optics at 2m/93cm



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

