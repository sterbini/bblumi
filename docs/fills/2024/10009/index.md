# FILL 10009

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10009.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_16_13_32_00_F10009.yaml) [:arrow_left:](../10008/index.md) [:arrow_right:](../10010/index.md)

**start**: 	August 16, 2024 01:32:00 PM UTC

**end**: 		August 16, 2024 07:27:39 PM UTC

**duration**: 0 days 05:55:38.560000

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

