# FILL 9438

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9438.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_28_16_47_40_F9438.yaml) [:arrow_left:](../9437/index.md) [:arrow_right:](../9439/index.md)

**start**: 	March 28, 2024 04:47:40 PM UTC

**end**: 		March 28, 2024 10:20:09 PM UTC

**duration**: 0 days 05:32:29.831875

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- octupole reversal 120-30cm



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

