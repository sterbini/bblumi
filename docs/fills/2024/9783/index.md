# FILL 9783

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9783.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_16_14_05_12_F9783.yaml) [:arrow_left:](../9782/index.md) [:arrow_right:](../9784/index.md)

**start**: 	June 16, 2024 02:05:12 PM UTC

**end**: 		June 16, 2024 04:10:50 PM UTC

**duration**: 0 days 02:05:38.086250

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- Optics measurements

- Dumped on RQ10.R4B1



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

