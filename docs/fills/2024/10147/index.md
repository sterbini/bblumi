# FILL 10147

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10147.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_27_00_43_22_F10147.yaml) [:arrow_left:](../10146/index.md) [:arrow_right:](../10148/index.md)

**start**: 	September 27, 2024 12:43:22 AM UTC

**end**: 		September 27, 2024 04:14:00 AM UTC

**duration**: 0 days 03:30:37.656250

**tags**:	 

- MD



**comments**:	 

- 12663 Wire compensation during beta*-leveling



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

