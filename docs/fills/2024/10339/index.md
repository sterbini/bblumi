# FILL 10339

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10339.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_08_14_19_02_F10339.yaml) [:arrow_left:](../10338/index.md) [:arrow_right:](../10340/index.md)

**start**: 	November 08, 2024 02:19:02 PM UTC

**end**: 		November 08, 2024 03:26:18 PM UTC

**duration**: 0 days 01:07:16.612125

**tags**:	 

- DUCK



**comments**:	 

- increased BLM thresholds for BLMQI



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

