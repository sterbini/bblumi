# FILL 9340

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9340.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_11_03_57_46_F9340.yaml) [:arrow_left:](../9339/index.md) [:arrow_right:](../9341/index.md)

**start**: 	March 11, 2024 03:57:46 AM UTC

**end**: 		March 11, 2024 03:59:42 AM UTC

**duration**: 0 days 00:01:55.671000

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

