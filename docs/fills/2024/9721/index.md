# FILL 9721

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9721.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_06_17_07_39_F9721.yaml) [:arrow_left:](../9720/index.md) [:arrow_right:](../9722/index.md)

**start**: 	June 06, 2024 05:07:39 PM UTC

**end**: 		June 06, 2024 05:20:40 PM UTC

**duration**: 0 days 00:13:01.361875

**tags**:	 

- MD



**comments**:	 

- MD11243



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

