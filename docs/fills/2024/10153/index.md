# FILL 10153

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10153.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_27_11_51_26_F10153.yaml) [:arrow_left:](../10152/index.md) [:arrow_right:](../10154/index.md)

**start**: 	September 27, 2024 11:51:26 AM UTC

**end**: 		September 27, 2024 12:49:44 PM UTC

**duration**: 0 days 00:58:18.486750

**tags**:	 

- MD



**comments**:	 

- 12743RFpowerlimitationsforhigh-intensitybatches




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10153/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10153/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10153/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10153/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10153/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10153/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

