# FILL 10389

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10389.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_19_15_43_02_F10389.yaml) [:arrow_left:](../10388/index.md) [:arrow_right:](../10390/index.md)

**start**: 	November 19, 2024 03:43:02 PM UTC

**end**: 		November 19, 2024 05:34:20 PM UTC

**duration**: 0 days 01:51:17.864750

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

