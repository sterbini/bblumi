# FILL 9985

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9985.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_08_12_26_07_F9985.yaml) [:arrow_left:](../9984/index.md) [:arrow_right:](../9986/index.md)

**start**: 	August 08, 2024 12:26:07 PM UTC

**end**: 		August 08, 2024 03:14:46 PM UTC

**duration**: 0 days 02:48:39.282375

**tags**:	 

- DUCK



**comments**:	 

- AUG pushed in SG4 building in point 4. Mains tripped with DISCHARGE REQUEST from PC (cooling water)



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

