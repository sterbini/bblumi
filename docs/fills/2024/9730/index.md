# FILL 9730

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9730.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_07_20_04_20_F9730.yaml) [:arrow_left:](../9729/index.md) [:arrow_right:](../9731/index.md)

**start**: 	June 07, 2024 08:04:20 PM UTC

**end**: 		June 07, 2024 08:39:00 PM UTC

**duration**: 0 days 00:34:40.093500

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

