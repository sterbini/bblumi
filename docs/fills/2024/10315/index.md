# FILL 10315

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10315.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_05_01_07_19_F10315.yaml) [:arrow_left:](../10314/index.md) [:arrow_right:](../10316/index.md)

**start**: 	November 05, 2024 01:07:19 AM UTC

**end**: 		November 05, 2024 03:40:23 AM UTC

**duration**: 0 days 02:33:03.886375

**tags**:	 

- ION_PHYSICS

- OP_DUMP



**comments**:	 

- Loss maps




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10315/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10315/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10315/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10315/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10315/Fill_chromaticity.png)

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

