# FILL 10385

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10385.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_18_15_31_41_F10385.yaml) [:arrow_left:](../10384/index.md) [:arrow_right:](../10386/index.md)

**start**: 	November 18, 2024 03:31:41 PM UTC

**end**: 		November 18, 2024 08:38:04 PM UTC

**duration**: 0 days 05:06:22.508375

**tags**:	 

- DUCK



**comments**:	 

- validation fills, loss maps etc after polarity reversal in ALICE



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

