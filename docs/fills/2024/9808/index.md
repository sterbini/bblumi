# FILL 9808

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9808.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_20_14_03_52_F9808.yaml) [:arrow_left:](../9807/index.md) [:arrow_right:](../9809/index.md)

**start**: 	June 20, 2024 02:03:52 PM UTC

**end**: 		June 21, 2024 12:53:45 AM UTC

**duration**: 0 days 10:49:52.574125

**tags**:	 

- INTENSITY_RAMPUP

- OP_DUMP

- WIRES



**comments**:	 

- 1239 bunches step for the intensity ramp up

- Lifetime going in collision is minimum for B1 (5h) but still good

- The BRAND.L2 was "calibrated" to bring the lumi in the right order of magnitude in IP2

- CMS pileup request is 67 (virtal set to 69)Wire ON but B1 lifetime degraded, almost fully recovered by deltaQH_B1= +2e-3 (also hierarchy improved)

- Wires were switched ON/OFF to check the effect on the hierarchy

- End of fill scraping




#### Cycle duration

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Cycle_duration.png)


#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Fill_chromaticity.png)

#### Intensity


##### Cycle intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Cycle_intensity.png)

##### BBB SB intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/bbb_SB_intensity.png)

##### Violin BBB intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Cycle_violin_intensity_bl.png)

##### SB bunch intensity 

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Fill_adjust_stable_losses_summary.png)

#### Bunch Length


![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Cycle_bunchlength.png)

##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Fill_bunch_length.png)

#### Emittance


##### B1 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Cycle_emit_b1.png)

##### B2 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Cycle_emit_b2.png)

##### Summary violin plot

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Cycle_violin_emit.png)

##### BBB in SB for B1 

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/bbb_SB_B1_emit.png)

##### BBB in SB for B2 

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/bbb_SB_B2_emit.png)

#### Bunch brightness


![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Cycle_brightness.png)

#### Beam properties summary 

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Cycle_summary.png)

#### Luminosity


##### Overview

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Fill_luminosity.png)

##### Luminous Regions

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Fill_luminous_regions.png)

##### BBB luminosity in SB

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/bbb_SB_luminosity.png)

#### Cross-sections


##### ADJUST

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Xsection_ADJUST_heatmap.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Xsection_ADJUST_mean.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Xsection_ADJUST_bbb_ip15lr.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Xsection_ADJUST_bbb_ip2ho.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Xsection_ADJUST_bbb_lhcbho.png)

##### STABLE BEAM

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Xsection_STABLE_heatmap_DBLM.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Xsection_SB_mean_DBLM.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Xsection_SB_mean_DBLM_separation.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Xsection_SB_bbb_DBLM_ip15lr.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Xsection_SB_bbb_DBLM_ip2ho.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9808/Xsection_SB_bbb_DBLM_lhcbho.png)

