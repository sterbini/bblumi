# FILL 9854

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9854.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_03_15_26_23_F9854.yaml) [:arrow_left:](../9853/index.md) [:arrow_right:](../9855/index.md)

**start**: 	July 03, 2024 03:26:23 PM UTC

**end**: 		July 03, 2024 03:36:39 PM UTC

**duration**: 0 days 00:10:15.975125

**tags**:	 

- DUCK



**comments**:	 

- Precycle



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

