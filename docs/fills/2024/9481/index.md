# FILL 9481

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9481.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_07_19_53_06_F9481.yaml) [:arrow_left:](../9480/index.md) [:arrow_right:](../9482/index.md)

**start**: 	April 07, 2024 07:53:06 PM UTC

**end**: 		April 08, 2024 12:14:39 AM UTC

**duration**: 0 days 04:21:33.092500

**tags**:	 

- INTENSITY_RAMPUP



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

