# FILL 9343

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9343.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_11_12_33_15_F9343.yaml) [:arrow_left:](../9342/index.md) [:arrow_right:](../9344/index.md)

**start**: 	March 11, 2024 12:33:15 PM UTC

**end**: 		March 11, 2024 01:46:07 PM UTC

**duration**: 0 days 01:12:52.242625

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

