# FILL 9409

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9409.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_23_04_03_59_F9409.yaml) [:arrow_left:](../9408/index.md) [:arrow_right:](../9410/index.md)

**start**: 	March 23, 2024 04:03:59 AM UTC

**end**: 		March 23, 2024 06:43:15 AM UTC

**duration**: 0 days 02:39:15.675000

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- cycle test

- new squeeze adn bunch length



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

