# FILL 10399

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10399.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_21_22_56_33_F10399.yaml) [:arrow_left:](../10398/index.md) [:arrow_right:](../10400/index.md)

**start**: 	November 21, 2024 10:56:33 PM UTC

**end**: 		November 22, 2024 12:07:39 AM UTC

**duration**: 0 days 01:11:05.562500

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

