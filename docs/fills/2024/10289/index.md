# FILL 10289

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10289.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_27_02_58_24_F10289.yaml) [:arrow_left:](../10288/index.md) [:arrow_right:](../10290/index.md)

**start**: 	October 27, 2024 02:58:24 AM UTC

**end**: 		October 27, 2024 04:22:57 AM UTC

**duration**: 0 days 01:24:32.910875

**tags**:	 

- BEAM_COMMISSIONING

- PPREF_OPTICS



**comments**:	 

- Loss maps at qchange

- Issue identified in the introduction of IP_OFFSET in IP5&8 that took place only after TCT alignment in September



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

