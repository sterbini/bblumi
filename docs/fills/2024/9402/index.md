# FILL 9402

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9402.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_21_22_29_35_F9402.yaml) [:arrow_left:](../9401/index.md) [:arrow_right:](../9403/index.md)

**start**: 	March 21, 2024 10:29:35 PM UTC

**end**: 		March 22, 2024 04:50:11 AM UTC

**duration**: 0 days 06:20:35.425500

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- optics 30cm



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

