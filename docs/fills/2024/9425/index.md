# FILL 9425

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9425.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_25_20_40_22_F9425.yaml) [:arrow_left:](../9424/index.md) [:arrow_right:](../9426/index.md)

**start**: 	March 25, 2024 08:40:22 PM UTC

**end**: 		March 26, 2024 05:18:58 PM UTC

**duration**: 0 days 20:38:36.480625

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

