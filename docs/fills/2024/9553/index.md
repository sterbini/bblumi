# FILL 9553

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9553.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_23_03_27_35_F9553.yaml) [:arrow_left:](../9552/index.md) [:arrow_right:](../9554/index.md)

**start**: 	April 23, 2024 03:27:35 AM UTC

**end**: 		April 23, 2024 09:22:56 AM UTC

**duration**: 0 days 05:55:21.619000

**tags**:	 

- DUCK



**comments**:	 

- MQ29L6 quench, 3rd time



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

