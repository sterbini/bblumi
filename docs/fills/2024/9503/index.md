# FILL 9503

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9503.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_11_19_21_40_F9503.yaml) [:arrow_left:](../9502/index.md) [:arrow_right:](../9504/index.md)

**start**: 	April 11, 2024 07:21:40 PM UTC

**end**: 		April 11, 2024 08:02:55 PM UTC

**duration**: 0 days 00:41:15.109375

**tags**:	 

- INTENSITY_RAMPUP



**comments**:	 


#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9503/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9503/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9503/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9503/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9503/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9503/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

