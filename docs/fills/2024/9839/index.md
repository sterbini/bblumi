# FILL 9839

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9839.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_29_04_00_28_F9839.yaml) [:arrow_left:](../9838/index.md) [:arrow_right:](../9840/index.md)

**start**: 	June 29, 2024 04:00:28 AM UTC

**end**: 		June 29, 2024 05:35:20 AM UTC

**duration**: 0 days 01:34:52.276500

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

