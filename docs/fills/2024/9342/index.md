# FILL 9342

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9342.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_11_06_30_57_F9342.yaml) [:arrow_left:](../9341/index.md) [:arrow_right:](../9343/index.md)

**start**: 	March 11, 2024 06:30:57 AM UTC

**end**: 		March 11, 2024 12:33:15 PM UTC

**duration**: 0 days 06:02:17.948375

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

