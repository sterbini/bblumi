# FILL 9776

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9776.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_10_00_59_03_F9776.yaml) [:arrow_left:](../9775/index.md) [:arrow_right:](../9777/index.md)

**start**: 	June 10, 2024 12:59:03 AM UTC

**end**: 		June 14, 2024 08:28:11 PM UTC

**duration**: 4 days 19:29:07.978125

**tags**:	 

- MD

- TS

- OP_DUMP



**comments**:	 

- MD12263 Weak-Strong investigation of Long-Range Beam-beam (second ramp)

- This fill was lasting the full TS1



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

