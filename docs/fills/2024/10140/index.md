# FILL 10140

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10140.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_25_21_10_48_F10140.yaml) [:arrow_left:](../10139/index.md) [:arrow_right:](../10141/index.md)

**start**: 	September 25, 2024 09:10:48 PM UTC

**end**: 		September 26, 2024 03:07:09 AM UTC

**duration**: 0 days 05:56:20.793375

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

