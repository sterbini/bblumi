# FILL 10129

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10129.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_19_14_28_42_F10129.yaml) [:arrow_left:](../10128/index.md) [:arrow_right:](../10130/index.md)

**start**: 	September 19, 2024 02:28:42 PM UTC

**end**: 		September 19, 2024 08:21:43 PM UTC

**duration**: 0 days 05:53:00.699375

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- Commissioning of pp reference run optics (collimation alignment and loss maps)

- At the end of the fill an intervention on ALICE solenoid was performed



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

