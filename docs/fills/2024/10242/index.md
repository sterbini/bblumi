# FILL 10242

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10242.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_18_15_57_47_F10242.yaml) [:arrow_left:](../10241/index.md) [:arrow_right:](../10243/index.md)

**start**: 	October 18, 2024 03:57:47 PM UTC

**end**: 		October 18, 2024 06:08:16 PM UTC

**duration**: 0 days 02:10:28.864500

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

