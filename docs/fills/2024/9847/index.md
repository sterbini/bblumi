# FILL 9847

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9847.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_01_09_54_44_F9847.yaml) [:arrow_left:](../9846/index.md) [:arrow_right:](../9848/index.md)

**start**: 	July 01, 2024 09:54:44 AM UTC

**end**: 		July 01, 2024 12:18:23 PM UTC

**duration**: 0 days 02:23:39.254250

**tags**:	 

- DUCK



**comments**:	 

- 



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

