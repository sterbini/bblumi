# FILL 9383

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9383.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_18_14_41_29_F9383.yaml) [:arrow_left:](../9382/index.md) [:arrow_right:](../9384/index.md)

**start**: 	March 18, 2024 02:41:29 PM UTC

**end**: 		March 18, 2024 07:54:57 PM UTC

**duration**: 0 days 05:13:27.781875

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

