# FILL 10185

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10185.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_01_19_59_14_F10185.yaml) [:arrow_left:](../10184/index.md) [:arrow_right:](../10186/index.md)

**start**: 	October 01, 2024 07:59:14 PM UTC

**end**: 		October 02, 2024 02:15:19 AM UTC

**duration**: 0 days 06:16:04.143875

**tags**:	 

- DUCK



**comments**:	 

- nan



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

