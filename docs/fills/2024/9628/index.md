# FILL 9628

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9628.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_14_19_12_43_F9628.yaml) [:arrow_left:](../9627/index.md) [:arrow_right:](../9629/index.md)

**start**: 	May 14, 2024 07:12:43 PM UTC

**end**: 		May 14, 2024 07:52:08 PM UTC

**duration**: 0 days 00:39:24.557125

**tags**:	 

- MD

- DUCK



**comments**:	 

- Several trips during the pre-cycle



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

