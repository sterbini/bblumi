# FILL 10124

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10124.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_17_13_49_10_F10124.yaml) [:arrow_left:](../10123/index.md) [:arrow_right:](../10125/index.md)

**start**: 	September 17, 2024 01:49:10 PM UTC

**end**: 		September 17, 2024 01:58:35 PM UTC

**duration**: 0 days 00:09:24.671875

**tags**:	 

- DUCK



**comments**:	 

- Precycling



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

