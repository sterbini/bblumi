# FILL 9326

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9326.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_07_20_41_53_F9326.yaml) [:arrow_left:](../9325/index.md) [:arrow_right:](../9327/index.md)

**start**: 	March 07, 2024 08:41:53 PM UTC

**end**: 		March 08, 2024 06:08:54 AM UTC

**duration**: 0 days 09:27:01.331375

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

