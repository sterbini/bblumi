# FILL 9829

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9829.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_26_19_40_18_F9829.yaml) [:arrow_left:](../9828/index.md) [:arrow_right:](../9830/index.md)

**start**: 	June 26, 2024 07:40:18 PM UTC

**end**: 		June 26, 2024 11:32:49 PM UTC

**duration**: 0 days 03:52:30.705375

**tags**:	 

- DUCK



**comments**:	 

- injection issues




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9829/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9829/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9829/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9829/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9829/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9829/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

