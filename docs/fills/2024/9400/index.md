# FILL 9400

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9400.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_21_06_15_53_F9400.yaml) [:arrow_left:](../9399/index.md) [:arrow_right:](../9401/index.md)

**start**: 	March 21, 2024 06:15:53 AM UTC

**end**: 		March 21, 2024 04:17:44 PM UTC

**duration**: 0 days 10:01:50.908875

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- stable beams at 450 GeV



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

