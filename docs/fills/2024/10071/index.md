# FILL 10071

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10071.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_30_05_22_24_F10071.yaml) [:arrow_left:](../10070/index.md) [:arrow_right:](../10072/index.md)

**start**: 	August 30, 2024 05:22:24 AM UTC

**end**: 		August 30, 2024 05:26:52 AM UTC

**duration**: 0 days 00:04:28.702625

**tags**:	 

- DUCK



**comments**:	 

- Precycle



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

