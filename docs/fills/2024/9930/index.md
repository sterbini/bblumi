# FILL 9930

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9930.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_24_12_38_03_F9930.yaml) [:arrow_left:](../9929/index.md) [:arrow_right:](../9931/index.md)

**start**: 	July 24, 2024 12:38:03 PM UTC

**end**: 		July 24, 2024 10:19:50 PM UTC

**duration**: 0 days 09:41:46.212875

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

