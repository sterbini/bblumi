# FILL 9482

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9482.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_08_00_14_39_F9482.yaml) [:arrow_left:](../9481/index.md) [:arrow_right:](../9483/index.md)

**start**: 	April 08, 2024 12:14:39 AM UTC

**end**: 		April 08, 2024 12:40:28 AM UTC

**duration**: 0 days 00:25:49.667875

**tags**:	 

- INTENSITY_RAMPUP



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

