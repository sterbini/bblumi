# FILL 9882

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9882.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_10_05_07_42_F9882.yaml) [:arrow_left:](../9881/index.md) [:arrow_right:](../9883/index.md)

**start**: 	July 10, 2024 05:07:42 AM UTC

**end**: 		July 10, 2024 06:02:25 AM UTC

**duration**: 0 days 00:54:42.924500

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

