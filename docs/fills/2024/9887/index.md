# FILL 9887

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9887.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_11_10_14_51_F9887.yaml) [:arrow_left:](../9886/index.md) [:arrow_right:](../9888/index.md)

**start**: 	July 11, 2024 10:14:51 AM UTC

**end**: 		July 12, 2024 11:03:21 AM UTC

**duration**: 1 days 00:48:30.345750

**tags**:	 

- DUCK



**comments**:	 

- Access in Point 4 for the RF problem

- Problem in SPS with the burnt RF coupler

- LHCb access to check the VELO movement



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

