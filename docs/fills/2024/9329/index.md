# FILL 9329

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9329.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_08_12_56_04_F9329.yaml) [:arrow_left:](../9328/index.md) [:arrow_right:](../9330/index.md)

**start**: 	March 08, 2024 12:56:04 PM UTC

**end**: 		March 08, 2024 10:42:46 PM UTC

**duration**: 0 days 09:46:42.263000

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

