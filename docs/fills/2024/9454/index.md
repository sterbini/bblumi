# FILL 9454

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9454.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_02_14_46_18_F9454.yaml) [:arrow_left:](../9453/index.md) [:arrow_right:](../9455/index.md)

**start**: 	April 02, 2024 02:46:18 PM UTC

**end**: 		April 02, 2024 08:21:05 PM UTC

**duration**: 0 days 05:34:47.090000

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- dBLM and wire scanner test



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

