# FILL 9477

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9477.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_07_02_11_49_F9477.yaml) [:arrow_left:](../9476/index.md) [:arrow_right:](../9478/index.md)

**start**: 	April 07, 2024 02:11:49 AM UTC

**end**: 		April 07, 2024 07:20:15 AM UTC

**duration**: 0 days 05:08:25.714250

**tags**:	 

- INTENSITY_RAMPUP



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

