# FILL 10272

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10272.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_20_19_37_10_F10272.yaml) [:arrow_left:](../10271/index.md) [:arrow_right:](../10273/index.md)

**start**: 	October 20, 2024 07:37:10 PM UTC

**end**: 		October 21, 2024 01:56:45 AM UTC

**duration**: 0 days 06:19:35.392500

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

