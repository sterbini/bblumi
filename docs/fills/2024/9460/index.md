# FILL 9460

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9460.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_03_15_42_57_F9460.yaml) [:arrow_left:](../9459/index.md) [:arrow_right:](../9461/index.md)

**start**: 	April 03, 2024 03:42:57 PM UTC

**end**: 		April 03, 2024 04:01:25 PM UTC

**duration**: 0 days 00:18:28.503375

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

