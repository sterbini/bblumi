# FILL 9785

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9785.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_16_19_45_19_F9785.yaml) [:arrow_left:](../9784/index.md) [:arrow_right:](../9786/index.md)

**start**: 	June 16, 2024 07:45:19 PM UTC

**end**: 		June 16, 2024 07:49:00 PM UTC

**duration**: 0 days 00:03:40.566250

**tags**:	 

- DUCK



**comments**:	 

- Cycling



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

