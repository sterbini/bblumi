# FILL 9384

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9384.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_18_19_54_57_F9384.yaml) [:arrow_left:](../9383/index.md) [:arrow_right:](../9385/index.md)

**start**: 	March 18, 2024 07:54:57 PM UTC

**end**: 		March 18, 2024 08:23:14 PM UTC

**duration**: 0 days 00:28:17.338500

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

