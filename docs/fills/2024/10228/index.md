# FILL 10228

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10228.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_15_18_43_54_F10228.yaml) [:arrow_left:](../10227/index.md) [:arrow_right:](../10229/index.md)

**start**: 	October 15, 2024 06:43:54 PM UTC

**end**: 		October 15, 2024 07:44:59 PM UTC

**duration**: 0 days 01:01:04.347375

**tags**:	 

- DUCK



**comments**:	 

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10228/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10228/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10228/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10228/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10228/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

