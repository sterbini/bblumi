# FILL 9394

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9394.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_20_09_51_33_F9394.yaml) [:arrow_left:](../9393/index.md) [:arrow_right:](../9395/index.md)

**start**: 	March 20, 2024 09:51:33 AM UTC

**end**: 		March 20, 2024 12:26:43 PM UTC

**duration**: 0 days 02:35:09.530250

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

