# FILL 10220

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10220.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_13_02_32_33_F10220.yaml) [:arrow_left:](../10219/index.md) [:arrow_right:](../10221/index.md)

**start**: 	October 13, 2024 02:32:33 AM UTC

**end**: 		October 13, 2024 07:10:17 AM UTC

**duration**: 0 days 04:37:44.308250

**tags**:	 

- DUCK



**comments**:	 

- Access and lost partol issues



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

