# FILL 10274

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10274.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_21_04_37_38_F10274.yaml) [:arrow_left:](../10273/index.md) [:arrow_right:](../10275/index.md)

**start**: 	October 21, 2024 04:37:38 AM UTC

**end**: 		October 24, 2024 12:21:41 PM UTC

**duration**: 3 days 07:44:02.722750

**tags**:	 

- DUCK



**comments**:	 

- TS2

- Deploymnent of BLM Thresholds changes for the 2024 pp reference runIon RF ramp test

- Tested the VdM scan



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

