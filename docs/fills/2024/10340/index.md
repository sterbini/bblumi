# FILL 10340

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10340.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_08_15_26_18_F10340.yaml) [:arrow_left:](../10339/index.md) [:arrow_right:](../10341/index.md)

**start**: 	November 08, 2024 03:26:18 PM UTC

**end**: 		November 08, 2024 04:27:10 PM UTC

**duration**: 0 days 01:00:51.900750

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

