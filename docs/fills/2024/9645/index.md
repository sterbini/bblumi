# FILL 9645

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9645.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_19_03_02_15_F9645.yaml) [:arrow_left:](../9644/index.md) [:arrow_right:](../9646/index.md)

**start**: 	May 19, 2024 03:02:15 AM UTC

**end**: 		May 19, 2024 08:09:23 AM UTC

**duration**: 0 days 05:07:07.900500

**tags**:	 

- VDM



**comments**:	 

- ATLAS magnets trip, power grid (400 kV) glitch, RBA81 trip, rampdown instead of pre-cycle launched



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

