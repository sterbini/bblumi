# FILL 10373

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10373.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_14_18_27_26_F10373.yaml) [:arrow_left:](../10372/index.md) [:arrow_right:](../10374/index.md)

**start**: 	November 14, 2024 06:27:26 PM UTC

**end**: 		November 14, 2024 10:23:24 PM UTC

**duration**: 0 days 03:55:58.396750

**tags**:	 

- MD



**comments**:	 

- MD14343 Schottky MD for ions

- 4 bunches per beam

- coupling changes, emittance blowup, comparison with WS measurements both at injection and during ramp




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10373/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10373/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10373/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10373/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10373/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10373/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

