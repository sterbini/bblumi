# FILL 9368

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9368.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_16_05_40_37_F9368.yaml) [:arrow_left:](../9367/index.md) [:arrow_right:](../9369/index.md)

**start**: 	March 16, 2024 05:40:37 AM UTC

**end**: 		March 16, 2024 07:01:23 AM UTC

**duration**: 0 days 01:20:46.022750

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- cycles for coupling in ther ramp



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

