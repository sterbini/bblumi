# FILL 9998

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9998.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_13_12_20_58_F9998.yaml) [:arrow_left:](../9997/index.md) [:arrow_right:](../9999/index.md)

**start**: 	August 13, 2024 12:20:58 PM UTC

**end**: 		August 13, 2024 12:21:05 PM UTC

**duration**: 0 days 00:00:07.284500

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

