# FILL 9448

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9448.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_01_05_58_47_F9448.yaml) [:arrow_left:](../9447/index.md) [:arrow_right:](../9449/index.md)

**start**: 	April 01, 2024 05:58:47 AM UTC

**end**: 		April 01, 2024 01:40:11 PM UTC

**duration**: 0 days 07:41:24.023875

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- stable beams at injection



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

