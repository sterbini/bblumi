# FILL 10364

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10364.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_14_08_01_58_F10364.yaml) [:arrow_left:](../10363/index.md) [:arrow_right:](../10365/index.md)

**start**: 	November 14, 2024 08:01:58 AM UTC

**end**: 		November 14, 2024 09:07:09 AM UTC

**duration**: 0 days 01:05:10.886375

**tags**:	 

- MD



**comments**:	 

- MD14364 RF flat bottom optimization for ion debunching

- 1.4 ns bunch length after 30 minutes with 8 MV

- lifetime at 5h

- 2% debunched beam

- dumped due to trip of RQT4.L3




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10364/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10364/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10364/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10364/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10364/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10364/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

