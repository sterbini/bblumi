# FILL 9807

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9807.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_20_11_44_24_F9807.yaml) [:arrow_left:](../9806/index.md) [:arrow_right:](../9808/index.md)

**start**: 	June 20, 2024 11:44:24 AM UTC

**end**: 		June 20, 2024 02:03:52 PM UTC

**duration**: 0 days 02:19:28.766250

**tags**:	 

- DUCK



**comments**:	 

- RQT13.L8B2 tripped at injection



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

