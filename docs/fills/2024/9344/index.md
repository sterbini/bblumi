# FILL 9344

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9344.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_11_13_46_07_F9344.yaml) [:arrow_left:](../9343/index.md) [:arrow_right:](../9345/index.md)

**start**: 	March 11, 2024 01:46:07 PM UTC

**end**: 		March 11, 2024 03:15:10 PM UTC

**duration**: 0 days 01:29:03.281000

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

