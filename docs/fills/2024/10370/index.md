# FILL 10370

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10370.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_14_13_44_22_F10370.yaml) [:arrow_left:](../10369/index.md) [:arrow_right:](../10371/index.md)

**start**: 	November 14, 2024 01:44:22 PM UTC

**end**: 		November 14, 2024 01:45:43 PM UTC

**duration**: 0 days 00:01:20.782125

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

