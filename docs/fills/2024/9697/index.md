# FILL 9697

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9697.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_01_22_13_51_F9697.yaml) [:arrow_left:](../9696/index.md) [:arrow_right:](../9698/index.md)

**start**: 	June 01, 2024 10:13:51 PM UTC

**end**: 		June 02, 2024 02:44:03 AM UTC

**duration**: 0 days 04:30:11.880250

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

