# FILL 10007

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10007.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_16_03_32_29_F10007.yaml) [:arrow_left:](../10006/index.md) [:arrow_right:](../10008/index.md)

**start**: 	August 16, 2024 03:32:29 AM UTC

**end**: 		August 16, 2024 01:25:48 PM UTC

**duration**: 0 days 09:53:19.304375

**tags**:	 

- DUCK



**comments**:	 

- fire on point 4 compensator due to water condensation that provoked electrical arc



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

