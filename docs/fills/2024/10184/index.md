# FILL 10184

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10184.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_01_19_42_45_F10184.yaml) [:arrow_left:](../10183/index.md) [:arrow_right:](../10185/index.md)

**start**: 	October 01, 2024 07:42:45 PM UTC

**end**: 		October 01, 2024 07:59:14 PM UTC

**duration**: 0 days 00:16:29.147750

**tags**:	 

- DUCK



**comments**:	 

- nan




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10184/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10184/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10184/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10184/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10184/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10184/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

