# FILL 9884

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9884.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_10_13_32_32_F9884.yaml) [:arrow_left:](../9883/index.md) [:arrow_right:](../9885/index.md)

**start**: 	July 10, 2024 01:32:32 PM UTC

**end**: 		July 10, 2024 11:36:34 PM UTC

**duration**: 0 days 10:04:02.149375

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

