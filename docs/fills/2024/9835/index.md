# FILL 9835

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9835.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_28_15_34_16_F9835.yaml) [:arrow_left:](../9834/index.md) [:arrow_right:](../9836/index.md)

**start**: 	June 28, 2024 03:34:16 PM UTC

**end**: 		June 28, 2024 08:07:43 PM UTC

**duration**: 0 days 04:33:27.147000

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

