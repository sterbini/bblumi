# FILL 10390

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10390.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_19_17_34_20_F10390.yaml) [:arrow_left:](../10389/index.md) [:arrow_right:](../10391/index.md)

**start**: 	November 19, 2024 05:34:20 PM UTC

**end**: 		November 19, 2024 06:03:25 PM UTC

**duration**: 0 days 00:29:05.391625

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

