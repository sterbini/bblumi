# FILL 9875

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9875.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_08_11_54_27_F9875.yaml) [:arrow_left:](../9874/index.md) [:arrow_right:](../9876/index.md)

**start**: 	July 08, 2024 11:54:27 AM UTC

**end**: 		July 08, 2024 12:50:17 PM UTC

**duration**: 0 days 00:55:49.720000

**tags**:	 

- DUCK



**comments**:	 

- alice selonoid is back



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

