# FILL 10177

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10177.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_30_13_01_08_F10177.yaml) [:arrow_left:](../10176/index.md) [:arrow_right:](../10178/index.md)

**start**: 	September 30, 2024 01:01:08 PM UTC

**end**: 		September 30, 2024 02:47:54 PM UTC

**duration**: 0 days 01:46:45.820250

**tags**:	 

- DUCK



**comments**:	 

- preparation for next MD cycle



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

