# FILL 9410

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9410.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_23_06_43_15_F9410.yaml) [:arrow_left:](../9409/index.md) [:arrow_right:](../9411/index.md)

**start**: 	March 23, 2024 06:43:15 AM UTC

**end**: 		March 23, 2024 09:04:19 AM UTC

**duration**: 0 days 02:21:04.101250

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- nominal bunch ramps for BI tests



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

