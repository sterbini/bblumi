# FILL 10227

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10227.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_15_16_51_04_F10227.yaml) [:arrow_left:](../10226/index.md) [:arrow_right:](../10228/index.md)

**start**: 	October 15, 2024 04:51:04 PM UTC

**end**: 		October 15, 2024 06:43:54 PM UTC

**duration**: 0 days 01:52:49.925750

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

