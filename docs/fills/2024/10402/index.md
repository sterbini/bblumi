# FILL 10402

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10402.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_22_16_35_26_F10402.yaml) [:arrow_left:](../10401/index.md) [:arrow_right:](../10403/index.md)

**start**: 	November 22, 2024 04:35:26 PM UTC

**end**: 		November 22, 2024 08:59:56 PM UTC

**duration**: 0 days 04:24:29.715750

**tags**:	 

**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

