# FILL 9532

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9532.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_17_11_59_45_F9532.yaml) [:arrow_left:](../9531/index.md) [:arrow_right:](../9533/index.md)

**start**: 	April 17, 2024 11:59:45 AM UTC

**end**: 		April 17, 2024 12:40:36 PM UTC

**duration**: 0 days 00:40:50.614125

**tags**:	 

- DUCK



**comments**:	 

- Precycle



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

