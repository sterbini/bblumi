# FILL 10212

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10212.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_09_14_54_56_F10212.yaml) [:arrow_left:](../10211/index.md) [:arrow_right:](../10213/index.md)

**start**: 	October 09, 2024 02:54:56 PM UTC

**end**: 		October 09, 2024 03:44:17 PM UTC

**duration**: 0 days 00:49:21.349625

**tags**:	 

- DUCK



**comments**:	 

- RCBCH8.L8B2 tripped during pre-cycle



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

