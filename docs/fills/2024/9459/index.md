# FILL 9459

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9459.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_03_05_44_19_F9459.yaml) [:arrow_left:](../9458/index.md) [:arrow_right:](../9460/index.md)

**start**: 	April 03, 2024 05:44:19 AM UTC

**end**: 		April 03, 2024 03:42:57 PM UTC

**duration**: 0 days 09:58:38.361250

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

