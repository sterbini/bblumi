# FILL 10396

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10396.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_20_19_34_43_F10396.yaml) [:arrow_left:](../10395/index.md) [:arrow_right:](../10397/index.md)

**start**: 	November 20, 2024 07:34:43 PM UTC

**end**: 		November 20, 2024 10:45:06 PM UTC

**duration**: 0 days 03:10:23.611000

**tags**:	 

- DUCK



**comments**:	 

- validation fill for MD with reduced crossing angle



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

