# FILL 10198

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10198.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_04_05_27_04_F10198.yaml) [:arrow_left:](../10197/index.md) [:arrow_right:](../10199/index.md)

**start**: 	October 04, 2024 05:27:04 AM UTC

**end**: 		October 04, 2024 10:47:02 AM UTC

**duration**: 0 days 05:19:58.338375

**tags**:	 

- DUCK



**comments**:	 

- nan



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

