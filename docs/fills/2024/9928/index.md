# FILL 9928

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9928.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_24_06_02_26_F9928.yaml) [:arrow_left:](../9927/index.md) [:arrow_right:](../9929/index.md)

**start**: 	July 24, 2024 06:02:26 AM UTC

**end**: 		July 24, 2024 12:36:57 PM UTC

**duration**: 0 days 06:34:30.966875

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

