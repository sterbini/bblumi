# FILL 10113

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10113.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_14_05_34_21_F10113.yaml) [:arrow_left:](../10112/index.md) [:arrow_right:](../10114/index.md)

**start**: 	September 14, 2024 05:34:21 AM UTC

**end**: 		September 14, 2024 05:53:35 AM UTC

**duration**: 0 days 00:19:14.045500

**tags**:	 

- DUCK



**comments**:	 

- Short issue with the sequencer



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

