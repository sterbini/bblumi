# FILL 9742

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9742.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_09_00_19_17_F9742.yaml) [:arrow_left:](../9741/index.md) [:arrow_right:](../9743/index.md)

**start**: 	June 09, 2024 12:19:17 AM UTC

**end**: 		June 09, 2024 12:43:20 AM UTC

**duration**: 0 days 00:24:02.876625

**tags**:	 

- MD



**comments**:	 

- MD11787




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9742/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9742/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9742/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9742/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9742/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9742/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

