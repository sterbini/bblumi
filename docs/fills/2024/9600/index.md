# FILL 9600

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9600.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_04_21_28_05_F9600.yaml) [:arrow_left:](../9599/index.md) [:arrow_right:](../9601/index.md)

**start**: 	May 04, 2024 09:28:05 PM UTC

**end**: 		May 04, 2024 09:54:27 PM UTC

**duration**: 0 days 00:26:22.074500

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

