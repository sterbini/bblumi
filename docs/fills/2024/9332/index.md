# FILL 9332

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9332.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_09_08_07_52_F9332.yaml) [:arrow_left:](../9331/index.md) [:arrow_right:](../9333/index.md)

**start**: 	March 09, 2024 08:07:52 AM UTC

**end**: 		March 09, 2024 07:31:37 PM UTC

**duration**: 0 days 11:23:45.310125

**tags**:	 

- BEAM_COMMISSIONING

- OPTICS_MEASUREMENTS



**comments**:	 

- optics measuremenents at FT

- second ramp+squeeze to 93/30cm



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

