# FILL 10195

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10195.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_03_09_34_48_F10195.yaml) [:arrow_left:](../10194/index.md) [:arrow_right:](../10196/index.md)

**start**: 	October 03, 2024 09:34:48 AM UTC

**end**: 		October 03, 2024 11:27:36 AM UTC

**duration**: 0 days 01:52:47.817000

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

