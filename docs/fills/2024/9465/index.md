# FILL 9465

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9465.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_04_11_57_54_F9465.yaml) [:arrow_left:](../9464/index.md) [:arrow_right:](../9466/index.md)

**start**: 	April 04, 2024 11:57:54 AM UTC

**end**: 		April 04, 2024 04:53:04 PM UTC

**duration**: 0 days 04:55:10.321375

**tags**:	 

- BEAM_COMMISSIONING

- 12BUNCHES



**comments**:	 

- first injections of 12 bunches and loss maps 



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

