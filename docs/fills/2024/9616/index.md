# FILL 9616

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9616.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_12_15_58_54_F9616.yaml) [:arrow_left:](../9615/index.md) [:arrow_right:](../9617/index.md)

**start**: 	May 12, 2024 03:58:54 PM UTC

**end**: 		May 12, 2024 04:02:41 PM UTC

**duration**: 0 days 00:03:46.889750

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

