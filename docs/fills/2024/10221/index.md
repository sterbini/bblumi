# FILL 10221

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10221.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_13_07_10_17_F10221.yaml) [:arrow_left:](../10220/index.md) [:arrow_right:](../10222/index.md)

**start**: 	October 13, 2024 07:10:17 AM UTC

**end**: 		October 13, 2024 07:41:10 AM UTC

**duration**: 0 days 00:30:52.622750

**tags**:	 

- DUCK



**comments**:	 

- Access and lost partol issues



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

