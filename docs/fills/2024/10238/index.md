# FILL 10238

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10238.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_17_14_21_42_F10238.yaml) [:arrow_left:](../10237/index.md) [:arrow_right:](../10239/index.md)

**start**: 	October 17, 2024 02:21:42 PM UTC

**end**: 		October 17, 2024 07:11:46 PM UTC

**duration**: 0 days 04:50:03.990875

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

