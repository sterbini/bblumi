# FILL 10384

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10384.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_18_09_07_18_F10384.yaml) [:arrow_left:](../10383/index.md) [:arrow_right:](../10385/index.md)

**start**: 	November 18, 2024 09:07:18 AM UTC

**end**: 		November 18, 2024 03:31:41 PM UTC

**duration**: 0 days 06:24:22.682000

**tags**:	 

- DUCK



**comments**:	 

- alice polarity reveral

- dumped due to ADT switched ON and QFB still on driving Q on 3rd order resonance




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10384/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10384/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10384/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10384/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10384/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10384/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

