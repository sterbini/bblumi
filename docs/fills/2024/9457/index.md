# FILL 9457

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9457.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_03_04_05_25_F9457.yaml) [:arrow_left:](../9456/index.md) [:arrow_right:](../9458/index.md)

**start**: 	April 03, 2024 04:05:25 AM UTC

**end**: 		April 03, 2024 05:29:52 AM UTC

**duration**: 0 days 01:24:27.389875

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

