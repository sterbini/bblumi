# FILL 10368

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10368.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_14_10_46_46_F10368.yaml) [:arrow_left:](../10367/index.md) [:arrow_right:](../10369/index.md)

**start**: 	November 14, 2024 10:46:46 AM UTC

**end**: 		November 14, 2024 11:53:47 AM UTC

**duration**: 0 days 01:07:00.785000

**tags**:	 

- MD



**comments**:	 

- MD14364 RF flat bottom optimization for ion debunching

- 14 MV, lifetime 8h, debunching 1%




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10368/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10368/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10368/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10368/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10368/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10368/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

