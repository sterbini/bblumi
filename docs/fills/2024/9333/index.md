# FILL 9333

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9333.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_09_19_31_37_F9333.yaml) [:arrow_left:](../9332/index.md) [:arrow_right:](../9334/index.md)

**start**: 	March 09, 2024 07:31:37 PM UTC

**end**: 		March 10, 2024 02:57:55 AM UTC

**duration**: 0 days 07:26:18.431500

**tags**:	 

- BEAM_COMMISSIONING

- OPTICS_MEASUREMENTS



**comments**:	 

- optics measurements at injection



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

