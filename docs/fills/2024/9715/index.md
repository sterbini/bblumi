# FILL 9715

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9715.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_05_18_08_23_F9715.yaml) [:arrow_left:](../9714/index.md) [:arrow_right:](../9716/index.md)

**start**: 	June 05, 2024 06:08:23 PM UTC

**end**: 		June 05, 2024 06:32:20 PM UTC

**duration**: 0 days 00:23:56.351500

**tags**:	 

- DUCK



**comments**:	 

- BEM interlock issue



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

