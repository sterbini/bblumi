# FILL 10183

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10183.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_01_18_27_14_F10183.yaml) [:arrow_left:](../10182/index.md) [:arrow_right:](../10184/index.md)

**start**: 	October 01, 2024 06:27:14 PM UTC

**end**: 		October 01, 2024 07:42:45 PM UTC

**duration**: 0 days 01:15:31.203500

**tags**:	 

- DUCK



**comments**:	 

- nan




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10183/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10183/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10183/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10183/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10183/Fill_chromaticity.png)

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

