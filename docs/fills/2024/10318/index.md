# FILL 10318

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10318.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_05_11_41_35_F10318.yaml) [:arrow_left:](../10317/index.md) [:arrow_right:](../10319/index.md)

**start**: 	November 05, 2024 11:41:35 AM UTC

**end**: 		November 05, 2024 12:12:00 PM UTC

**duration**: 0 days 00:30:25.779375

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

