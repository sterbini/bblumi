# FILL 10322

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10322.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_06_00_20_26_F10322.yaml) [:arrow_left:](../10321/index.md) [:arrow_right:](../10323/index.md)

**start**: 	November 06, 2024 12:20:26 AM UTC

**end**: 		November 06, 2024 02:08:13 AM UTC

**duration**: 0 days 01:47:46.966625

**tags**:	 

- ION_PHYSICS

- OP_DUMP



**comments**:	 

- More optimizations done



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

