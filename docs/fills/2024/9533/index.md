# FILL 9533

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9533.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_17_12_40_36_F9533.yaml) [:arrow_left:](../9532/index.md) [:arrow_right:](../9534/index.md)

**start**: 	April 17, 2024 12:40:36 PM UTC

**end**: 		April 17, 2024 05:04:04 PM UTC

**duration**: 0 days 04:23:28.480000

**tags**:	 

- DUCK



**comments**:	 

- corrector RCBV.22R6B2 fault



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

