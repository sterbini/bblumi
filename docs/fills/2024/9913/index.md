# FILL 9913

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9913.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_19_05_50_02_F9913.yaml) [:arrow_left:](../9912/index.md) [:arrow_right:](../9914/index.md)

**start**: 	July 19, 2024 05:50:02 AM UTC

**end**: 		July 19, 2024 08:10:46 AM UTC

**duration**: 0 days 02:20:43.938250

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

