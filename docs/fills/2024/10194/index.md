# FILL 10194

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10194.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_03_08_00_32_F10194.yaml) [:arrow_left:](../10193/index.md) [:arrow_right:](../10195/index.md)

**start**: 	October 03, 2024 08:00:32 AM UTC

**end**: 		October 03, 2024 09:34:48 AM UTC

**duration**: 0 days 01:34:16.027250

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

