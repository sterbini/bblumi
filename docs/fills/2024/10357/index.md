# FILL 10357

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10357.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_12_12_41_02_F10357.yaml) [:arrow_left:](../10356/index.md) [:arrow_right:](../10358/index.md)

**start**: 	November 12, 2024 12:41:02 PM UTC

**end**: 		November 12, 2024 01:47:43 PM UTC

**duration**: 0 days 01:06:40.984375

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

