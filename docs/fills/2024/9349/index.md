# FILL 9349

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9349.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_12_12_12_37_F9349.yaml) [:arrow_left:](../9348/index.md) [:arrow_right:](../9350/index.md)

**start**: 	March 12, 2024 12:12:37 PM UTC

**end**: 		March 12, 2024 10:02:03 PM UTC

**duration**: 0 days 09:49:26.407500100

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- ADT setup

- reference orbit



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

