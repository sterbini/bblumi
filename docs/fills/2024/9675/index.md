# FILL 9675

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9675.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_27_06_39_26_F9675.yaml) [:arrow_left:](../9674/index.md) [:arrow_right:](../9676/index.md)

**start**: 	May 27, 2024 06:39:26 AM UTC

**end**: 		May 27, 2024 01:54:33 PM UTC

**duration**: 0 days 07:15:07.719625

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

