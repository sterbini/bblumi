# FILL 9431

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9431.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_27_04_04_12_F9431.yaml) [:arrow_left:](../9430/index.md) [:arrow_right:](../9432/index.md)

**start**: 	March 27, 2024 04:04:12 AM UTC

**end**: 		March 27, 2024 05:35:05 AM UTC

**duration**: 0 days 01:30:53.542375

**tags**:	 

- BEAM_COMMISSIONING

- VDM



**comments**:	 

- vdm cycle



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

