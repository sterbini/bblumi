# FILL 10193

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10193.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_03_07_51_10_F10193.yaml) [:arrow_left:](../10192/index.md) [:arrow_right:](../10194/index.md)

**start**: 	October 03, 2024 07:51:10 AM UTC

**end**: 		October 03, 2024 08:00:32 AM UTC

**duration**: 0 days 00:09:22.226375

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

