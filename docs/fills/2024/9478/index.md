# FILL 9478

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9478.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_07_07_20_15_F9478.yaml) [:arrow_left:](../9477/index.md) [:arrow_right:](../9479/index.md)

**start**: 	April 07, 2024 07:20:15 AM UTC

**end**: 		April 07, 2024 07:43:50 AM UTC

**duration**: 0 days 00:23:35.549250

**tags**:	 

- INTENSITY_RAMPUP



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

