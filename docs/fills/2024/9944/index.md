# FILL 9944

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9944.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_29_12_33_30_F9944.yaml) [:arrow_left:](../9943/index.md) [:arrow_right:](../9945/index.md)

**start**: 	July 29, 2024 12:33:30 PM UTC

**end**: 		July 29, 2024 02:04:46 PM UTC

**duration**: 0 days 01:31:16.167250

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

