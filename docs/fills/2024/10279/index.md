# FILL 10279

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10279.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_24_22_43_04_F10279.yaml) [:arrow_left:](../10278/index.md) [:arrow_right:](../10280/index.md)

**start**: 	October 24, 2024 10:43:04 PM UTC

**end**: 		October 25, 2024 03:54:08 AM UTC

**duration**: 0 days 05:11:03.352250

**tags**:	 

- BEAM_COMMISSIONING

- ION_OPTICS



**comments**:	 

- Ion cycle setup with protons

- Optics measurements



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

