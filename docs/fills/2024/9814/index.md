# FILL 9814

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9814.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_22_17_48_08_F9814.yaml) [:arrow_left:](../9813/index.md) [:arrow_right:](../9815/index.md)

**start**: 	June 22, 2024 05:48:08 PM UTC

**end**: 		June 22, 2024 08:01:08 PM UTC

**duration**: 0 days 02:13:00.556250

**tags**:	 

- DUCK



**comments**:	 

- various issues preventing injections



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

