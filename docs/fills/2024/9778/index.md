# FILL 9778

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9778.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_15_02_13_27_F9778.yaml) [:arrow_left:](../9777/index.md) [:arrow_right:](../9779/index.md)

**start**: 	June 15, 2024 02:13:27 AM UTC

**end**: 		June 15, 2024 11:53:46 AM UTC

**duration**: 0 days 09:40:18.981500

**tags**:	 

- BEAM_COMMISSIONING

- OP_DUMP

- WIRES



**comments**:	 

- Validation with pilot bunches after TS1

- Going to 30 cm and testin the wires at 350 A

- Delays due to SPS problems



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

