# FILL 9746

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9746.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_09_02_15_09_F9746.yaml) [:arrow_left:](../9745/index.md) [:arrow_right:](../9747/index.md)

**start**: 	June 09, 2024 02:15:09 AM UTC

**end**: 		June 09, 2024 02:29:09 AM UTC

**duration**: 0 days 00:13:59.880250

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

