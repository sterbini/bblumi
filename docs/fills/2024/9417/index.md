# FILL 9417

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9417.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_24_02_59_24_F9417.yaml) [:arrow_left:](../9416/index.md) [:arrow_right:](../9418/index.md)

**start**: 	March 24, 2024 02:59:24 AM UTC

**end**: 		March 24, 2024 03:53:50 AM UTC

**duration**: 0 days 00:54:26.529500

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

