# FILL 10347

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10347.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_10_14_49_56_F10347.yaml) [:arrow_left:](../10346/index.md) [:arrow_right:](../10348/index.md)

**start**: 	November 10, 2024 02:49:56 PM UTC

**end**: 		November 10, 2024 05:42:28 PM UTC

**duration**: 0 days 02:52:32.624249925

**tags**:	 

- ION_PHYSICS



**comments**:	 

- 1240 bunches

- 30% dump during injection

- 37% losses at the start of ramp

- dumped due to instability in B2H



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

