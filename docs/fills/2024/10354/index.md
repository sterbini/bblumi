# FILL 10354

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10354.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_11_16_22_52_F10354.yaml) [:arrow_left:](../10353/index.md) [:arrow_right:](../10355/index.md)

**start**: 	November 11, 2024 04:22:52 PM UTC

**end**: 		November 11, 2024 07:16:16 PM UTC

**duration**: 0 days 02:53:23.793000

**tags**:	 

- ION_PHYSICS



**comments**:	 

- 1240 bunches

- injection losses 40%

- losses start of ramp 30%

- Dumped on energy limit of TCTPH.4L1.B1 due to combined issue of settings interpolation and published energy



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

