# FILL 10121

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10121.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_16_15_54_20_F10121.yaml) [:arrow_left:](../10120/index.md) [:arrow_right:](../10122/index.md)

**start**: 	September 16, 2024 03:54:20 PM UTC

**end**: 		September 16, 2024 04:29:05 PM UTC

**duration**: 0 days 00:34:44.453625

**tags**:	 

- DUCK



**comments**:	 

- Precycle



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

