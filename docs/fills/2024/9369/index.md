# FILL 9369

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9369.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_16_07_01_23_F9369.yaml) [:arrow_left:](../9368/index.md) [:arrow_right:](../9370/index.md)

**start**: 	March 16, 2024 07:01:23 AM UTC

**end**: 		March 16, 2024 10:20:34 AM UTC

**duration**: 0 days 03:19:10.270000

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

