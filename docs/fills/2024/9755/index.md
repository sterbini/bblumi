# FILL 9755

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9755.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_09_08_45_52_F9755.yaml) [:arrow_left:](../9754/index.md) [:arrow_right:](../9756/index.md)

**start**: 	June 09, 2024 08:45:52 AM UTC

**end**: 		June 09, 2024 08:49:38 AM UTC

**duration**: 0 days 00:03:46.096750

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

