# FILL 10233

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10233.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_16_11_07_59_F10233.yaml) [:arrow_left:](../10232/index.md) [:arrow_right:](../10234/index.md)

**start**: 	October 16, 2024 11:07:59 AM UTC

**end**: 		October 16, 2024 09:36:44 PM UTC

**duration**: 0 days 10:28:45.009875

**tags**:	 

- MD



**comments**:	 

- preparation of crystal setup and collimation studies

- 24 pilots

- chroma to 10 and octupoles to 0



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

