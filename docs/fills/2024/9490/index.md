# FILL 9490

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9490.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_09_14_05_46_F9490.yaml) [:arrow_left:](../9489/index.md) [:arrow_right:](../9491/index.md)

**start**: 	April 09, 2024 02:05:46 PM UTC

**end**: 		April 09, 2024 06:41:41 PM UTC

**duration**: 0 days 04:35:54.887750

**tags**:	 

- INTENSITY_RAMPUP



**comments**:	 

- None

- empty fills




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9490/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9490/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9490/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9490/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9490/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9490/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

