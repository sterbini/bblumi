# FILL 9757

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9757.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_09_09_55_18_F9757.yaml) [:arrow_left:](../9756/index.md) [:arrow_right:](../9758/index.md)

**start**: 	June 09, 2024 09:55:18 AM UTC

**end**: 		June 09, 2024 09:55:26 AM UTC

**duration**: 0 days 00:00:07.688250

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

