# FILL 10359

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10359.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_12_20_02_00_F10359.yaml) [:arrow_left:](../10358/index.md) [:arrow_right:](../10360/index.md)

**start**: 	November 12, 2024 08:02:00 PM UTC

**end**: 		November 13, 2024 01:50:38 PM UTC

**duration**: 0 days 17:48:38.090250

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

