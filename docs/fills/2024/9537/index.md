# FILL 9537

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9537.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_18_01_09_09_F9537.yaml) [:arrow_left:](../9536/index.md) [:arrow_right:](../9538/index.md)

**start**: 	April 18, 2024 01:09:09 AM UTC

**end**: 		April 18, 2024 12:00:02 PM UTC

**duration**: 0 days 10:50:52.556750

**tags**:	 

- INTENSITY_RAMPUP



**comments**:	 

- 25ns_1791b_1772_1191_1260_144bpi_18inj_3INDIVs

- End of beta leveling at 33 cm

- FBCT jumps by 1 slot

- Issue with filling scheme that impacts beam-beam patterns

- Scan of octupoles and chroma at 30cm

- Dumped due to Velo movement when switching from Stable to Adjust for collimation tests




#### Cycle duration

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Cycle_duration.png)


#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Fill_chromaticity.png)

#### Intensity


##### Cycle intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Cycle_intensity.png)

##### BBB SB intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/bbb_SB_intensity.png)

##### Violin BBB intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Cycle_violin_intensity_bl.png)

##### SB bunch intensity 

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Fill_adjust_stable_losses_summary.png)

#### Bunch Length


![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Cycle_bunchlength.png)

##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Fill_bunch_length.png)

#### Emittance


##### B1 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Cycle_emit_b1.png)

##### B2 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Cycle_emit_b2.png)

##### Summary violin plot

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Cycle_violin_emit.png)

##### BBB in SB for B1 

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/bbb_SB_B1_emit.png)

##### BBB in SB for B2 

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/bbb_SB_B2_emit.png)

#### Bunch brightness


![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Cycle_brightness.png)

#### Beam properties summary 

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Cycle_summary.png)

#### Luminosity


##### Overview

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Fill_luminosity.png)

##### Luminous Regions

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Fill_luminous_regions.png)

##### BBB luminosity in SB

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/bbb_SB_luminosity.png)

#### Cross-sections


##### ADJUST

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Xsection_ADJUST_heatmap.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Xsection_ADJUST_mean.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Xsection_ADJUST_bbb_ip15lr.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Xsection_ADJUST_bbb_ip2ho.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Xsection_ADJUST_bbb_lhcbho.png)

##### STABLE BEAM

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Xsection_STABLE_heatmap_DBLM.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Xsection_SB_mean_DBLM.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Xsection_SB_mean_DBLM_separation.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Xsection_SB_bbb_DBLM_ip15lr.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Xsection_SB_bbb_DBLM_ip2ho.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9537/Xsection_SB_bbb_DBLM_lhcbho.png)

