# FILL 10051

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10051.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_24_19_18_55_F10051.yaml) [:arrow_left:](../10050/index.md) [:arrow_right:](../10052/index.md)

**start**: 	August 24, 2024 07:18:55 PM UTC

**end**: 		August 24, 2024 07:46:02 PM UTC

**duration**: 0 days 00:27:06.463250

**tags**:	 

- DUCK



**comments**:	 

- Precycle



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

