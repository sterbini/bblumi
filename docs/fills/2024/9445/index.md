# FILL 9445

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9445.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_31_09_27_27_F9445.yaml) [:arrow_left:](../9444/index.md) [:arrow_right:](../9446/index.md)

**start**: 	March 31, 2024 09:27:27 AM UTC

**end**: 		March 31, 2024 03:21:32 PM UTC

**duration**: 0 days 05:54:04.275250

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- non-linear optics at 450 GeV



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

