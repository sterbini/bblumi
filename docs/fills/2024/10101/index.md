# FILL 10101

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10101.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_11_00_14_41_F10101.yaml) [:arrow_left:](../10100/index.md) [:arrow_right:](../10102/index.md)

**start**: 	September 11, 2024 12:14:41 AM UTC

**end**: 		September 11, 2024 02:31:21 AM UTC

**duration**: 0 days 02:16:40.003375

**tags**:	 

- PHYSICS

- BLM_DUMP

- TO_STUDY



**comments**:	 

- The injection was long due to a problem with the SPS extraction kicker

- The fill was dumped due to start of ramp losses https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10101.md#2024-09-11-040711

- #TO_STUDY: several bunches stayed at injection for a long time, it can be useful for the emittance blow-up studies




#### Cycle duration

![](https://dltwo.web.cern.ch/followup_plots/2024/10101/Cycle_duration.png)


#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10101/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10101/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10101/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10101/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10101/Fill_chromaticity.png)

#### Intensity


##### Cycle intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/10101/Cycle_intensity.png)

##### Violin BBB intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/10101/Cycle_violin_intensity_bl.png)

#### Bunch Length


![](https://dltwo.web.cern.ch/followup_plots/2024/10101/Cycle_bunchlength.png)

##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10101/Fill_bunch_length.png)

#### Emittance


##### B1 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/10101/Cycle_emit_b1.png)

##### B2 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/10101/Cycle_emit_b2.png)

##### Emittance growth at injection

![](https://dltwo.web.cern.ch/followup_plots/2024/10101/Fill_injection_emit_blowup.png)

##### Summary violin plot

![](https://dltwo.web.cern.ch/followup_plots/2024/10101/Cycle_violin_emit.png)

#### Bunch brightness


![](https://dltwo.web.cern.ch/followup_plots/2024/10101/Cycle_brightness.png)

#### Beam properties summary 

![](https://dltwo.web.cern.ch/followup_plots/2024/10101/Cycle_summary.png)

#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

