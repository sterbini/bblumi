# FILL 9331

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9331.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_09_07_53_18_F9331.yaml) [:arrow_left:](../9330/index.md) [:arrow_right:](../9332/index.md)

**start**: 	March 09, 2024 07:53:18 AM UTC

**end**: 		March 09, 2024 08:07:52 AM UTC

**duration**: 0 days 00:14:34.011000

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

