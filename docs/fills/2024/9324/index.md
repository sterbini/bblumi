# FILL 9324

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9324.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_07_17_16_17_F9324.yaml) [:arrow_left:](../9323/index.md) [:arrow_right:](../9325/index.md)

**start**: 	March 07, 2024 05:16:17 PM UTC

**end**: 		March 07, 2024 06:49:53 PM UTC

**duration**: 0 days 01:33:35.651500

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

