# FILL 9395

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9395.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_20_12_26_43_F9395.yaml) [:arrow_left:](../9394/index.md) [:arrow_right:](../9396/index.md)

**start**: 	March 20, 2024 12:26:43 PM UTC

**end**: 		March 20, 2024 10:06:23 PM UTC

**duration**: 0 days 09:39:39.921125

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

