# FILL 9365

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9365.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_15_12_38_16_F9365.yaml) [:arrow_left:](../9364/index.md) [:arrow_right:](../9366/index.md)

**start**: 	March 15, 2024 12:38:16 PM UTC

**end**: 		March 16, 2024 02:02:37 AM UTC

**duration**: 0 days 13:24:20.652250

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- cycles for coupling in ther ramp



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

