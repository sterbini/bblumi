# FILL 9437

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9437.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_28_08_01_41_F9437.yaml) [:arrow_left:](../9436/index.md) [:arrow_right:](../9438/index.md)

**start**: 	March 28, 2024 08:01:41 AM UTC

**end**: 		March 28, 2024 04:47:40 PM UTC

**duration**: 0 days 08:45:58.581750

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- aperture at 30cm



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

