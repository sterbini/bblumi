# FILL 9748

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9748.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_09_03_30_31_F9748.yaml) [:arrow_left:](../9747/index.md) [:arrow_right:](../9749/index.md)

**start**: 	June 09, 2024 03:30:31 AM UTC

**end**: 		June 09, 2024 03:37:11 AM UTC

**duration**: 0 days 00:06:39.857375

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

