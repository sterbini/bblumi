# FILL 10363

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10363.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_14_05_29_57_F10363.yaml) [:arrow_left:](../10362/index.md) [:arrow_right:](../10364/index.md)

**start**: 	November 14, 2024 05:29:57 AM UTC

**end**: 		November 14, 2024 08:01:58 AM UTC

**duration**: 0 days 02:32:01.864375

**tags**:	 

- MD



**comments**:	 

- MD14364 RF flat bottom optimization for ion debunching

- WS measurements of 1 train, every 10 minutes for 30 minutes

- VRF = 6 MV

- lifetime after 30 minutes 4 hours and 3.5% unbunched beam

- 1.5 ns bunch length after 30 minutes

- dumped and re-injected with VRF = 8 MV but stayed only 10 minutes due to pc trip




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10363/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10363/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10363/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10363/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10363/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10363/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

