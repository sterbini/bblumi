# FILL 9655

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9655.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_21_02_58_00_F9655.yaml) [:arrow_left:](../9654/index.md) [:arrow_right:](../9656/index.md)

**start**: 	May 21, 2024 02:58:00 AM UTC

**end**: 		May 21, 2024 04:03:10 PM UTC

**duration**: 0 days 13:05:09.386125

**tags**:	 

- DUCK



**comments**:	 

- Cryogenics problem.




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9655/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9655/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9655/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9655/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9655/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9655/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

