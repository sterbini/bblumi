# FILL 9538

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9538.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_18_12_00_02_F9538.yaml) [:arrow_left:](../9537/index.md) [:arrow_right:](../9539/index.md)

**start**: 	April 18, 2024 12:00:02 PM UTC

**end**: 		April 18, 2024 03:00:03 PM UTC

**duration**: 0 days 03:00:01.348000

**tags**:	 

- DUCK



**comments**:	 

- No available beam from SPS



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

