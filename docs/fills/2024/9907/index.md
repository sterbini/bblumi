# FILL 9907

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9907.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_17_04_18_01_F9907.yaml) [:arrow_left:](../9906/index.md) [:arrow_right:](../9908/index.md)

**start**: 	July 17, 2024 04:18:01 AM UTC

**end**: 		July 17, 2024 09:35:04 AM UTC

**duration**: 0 days 05:17:02.453000

**tags**:	 

- DUCK



**comments**:	 

- No beam due PS RF intervention

- Several problem on LHC side (RCBH25.R6B2 fault not cleared,lost communication with Point 5)

- ATLAS magnets tripped



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

