# FILL 10181

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10181.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_30_23_24_10_F10181.yaml) [:arrow_left:](../10180/index.md) [:arrow_right:](../10182/index.md)

**start**: 	September 30, 2024 11:24:10 PM UTC

**end**: 		October 01, 2024 02:27:26 PM UTC

**duration**: 0 days 15:03:16.247125

**tags**:	 

- DUCK



**comments**:	 

- nan



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

