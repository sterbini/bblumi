# FILL 9336

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9336.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_10_04_26_13_F9336.yaml) [:arrow_left:](../9335/index.md) [:arrow_right:](../9337/index.md)

**start**: 	March 10, 2024 04:26:13 AM UTC

**end**: 		March 10, 2024 08:08:59 AM UTC

**duration**: 0 days 03:42:45.972750

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

