# FILL 10327

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10327.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_06_04_29_36_F10327.yaml) [:arrow_left:](../10326/index.md) [:arrow_right:](../10328/index.md)

**start**: 	November 06, 2024 04:29:36 AM UTC

**end**: 		November 06, 2024 05:47:14 AM UTC

**duration**: 0 days 01:17:37.933250

**tags**:	 

- ION_PHYSICS



**comments**:	 

- Dump due to BPM interlock



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

