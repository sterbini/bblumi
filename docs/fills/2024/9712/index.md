# FILL 9712

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9712.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_05_14_47_11_F9712.yaml) [:arrow_left:](../9711/index.md) [:arrow_right:](../9713/index.md)

**start**: 	June 05, 2024 02:47:11 PM UTC

**end**: 		June 05, 2024 03:25:11 PM UTC

**duration**: 0 days 00:37:59.723250

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

