# FILL 9669

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9669.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_25_10_40_20_F9669.yaml) [:arrow_left:](../9668/index.md) [:arrow_right:](../9670/index.md)

**start**: 	May 25, 2024 10:40:20 AM UTC

**end**: 		May 25, 2024 11:17:37 AM UTC

**duration**: 0 days 00:37:17.016125

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

