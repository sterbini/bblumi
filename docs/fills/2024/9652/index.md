# FILL 9652

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9652.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_20_00_31_21_F9652.yaml) [:arrow_left:](../9651/index.md) [:arrow_right:](../9653/index.md)

**start**: 	May 20, 2024 12:31:21 AM UTC

**end**: 		May 20, 2024 03:35:58 AM UTC

**duration**: 0 days 03:04:36.236625

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- Access for LHCb

- AGK validation

- Testing injection with 12 bunches and trains

- Dumped on power glitch




#### Cycle duration

![](https://dltwo.web.cern.ch/followup_plots/2024/9652/Cycle_duration.png)


#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9652/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9652/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9652/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9652/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9652/Fill_chromaticity.png)

#### Intensity


##### Cycle intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/9652/Cycle_intensity.png)

##### Violin BBB intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/9652/Cycle_violin_intensity_bl.png)

#### Bunch Length


![](https://dltwo.web.cern.ch/followup_plots/2024/9652/Cycle_bunchlength.png)

##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9652/Fill_bunch_length.png)

#### Emittance


##### B1 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/9652/Cycle_emit_b1.png)

##### B2 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/9652/Cycle_emit_b2.png)

##### Summary violin plot

![](https://dltwo.web.cern.ch/followup_plots/2024/9652/Cycle_violin_emit.png)

#### Bunch brightness


![](https://dltwo.web.cern.ch/followup_plots/2024/9652/Cycle_brightness.png)

#### Beam properties summary 

![](https://dltwo.web.cern.ch/followup_plots/2024/9652/Cycle_summary.png)

#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

