# FILL 9911

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9911.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_18_03_21_41_F9911.yaml) [:arrow_left:](../9910/index.md) [:arrow_right:](../9912/index.md)

**start**: 	July 18, 2024 03:21:41 AM UTC

**end**: 		July 19, 2024 12:03:23 AM UTC

**duration**: 0 days 20:41:42.034000

**tags**:	 

- PHYSICS

- OP_DUMP



**comments**:	 

- 2 h SY-ABT conditioning  at the start of the fill for MKD B2 flashover

- LHCb changed polarity (several settings of the lumi-server changed accordingly)

- B1H blowup for first 72b due to poor orbit

- Losses at 55% at start of ramp

- More than 18 h of B1 at the end of collapse

- Performed b* levelling in steps and re-optimized IP8 along to implement FF after polarity reversal

- Almost 95% of dump to dump losses in IR3 RS12 for B1

- Tested Qp back to 10 to check effect on IR3 losses during blow-up: no effect

- At the end of the fill, target bunch lentgh was also reduced from 1.25 to 1.22 ns (not strong conclusion there)




#### Cycle duration

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Cycle_duration.png)


#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Fill_chromaticity.png)

#### Intensity


##### Cycle intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Cycle_intensity.png)

##### BBB SB intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/bbb_SB_intensity.png)

##### Violin BBB intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Cycle_violin_intensity_bl.png)

##### SB bunch intensity 

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Fill_adjust_stable_losses_summary.png)

#### Bunch Length


![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Cycle_bunchlength.png)

##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Fill_bunch_length.png)

#### Emittance


##### B1 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Cycle_emit_b1.png)

##### B2 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Cycle_emit_b2.png)

##### Emittance growth at injection

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Fill_injection_emit_blowup.png)

##### Summary violin plot

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Cycle_violin_emit.png)

##### BBB in SB for B1 

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/bbb_SB_B1_emit.png)

##### BBB in SB for B2 

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/bbb_SB_B2_emit.png)

#### Bunch brightness


![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Cycle_brightness.png)

#### Beam properties summary 

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Cycle_summary.png)

#### Luminosity


##### Overview

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Fill_luminosity.png)

##### Luminous Regions

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Fill_luminous_regions.png)

##### BBB luminosity in SB

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/bbb_SB_luminosity.png)

#### Cross-sections


##### ADJUST

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Xsection_ADJUST_heatmap.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Xsection_ADJUST_mean.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Xsection_ADJUST_bbb_ip15lr.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Xsection_ADJUST_bbb_ip2ho.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Xsection_ADJUST_bbb_lhcbho.png)

##### STABLE BEAM

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Xsection_STABLE_heatmap_DBLM.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Xsection_SB_mean_DBLM.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Xsection_SB_mean_DBLM_separation.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Xsection_SB_bbb_DBLM_ip15lr.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Xsection_SB_bbb_DBLM_ip2ho.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9911/Xsection_SB_bbb_DBLM_lhcbho.png)

