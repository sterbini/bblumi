# FILL 10176

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10176.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_30_03_07_35_F10176.yaml) [:arrow_left:](../10175/index.md) [:arrow_right:](../10177/index.md)

**start**: 	September 30, 2024 03:07:35 AM UTC

**end**: 		September 30, 2024 01:01:08 PM UTC

**duration**: 0 days 09:53:33.039125

**tags**:	 

- MD



**comments**:	 

- MD9543



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

