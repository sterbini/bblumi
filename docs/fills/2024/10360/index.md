# FILL 10360

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10360.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_13_13_50_38_F10360.yaml) [:arrow_left:](../10359/index.md) [:arrow_right:](../10361/index.md)

**start**: 	November 13, 2024 01:50:38 PM UTC

**end**: 		November 13, 2024 01:50:51 PM UTC

**duration**: 0 days 00:00:12.422250

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

