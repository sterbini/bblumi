# FILL 9999

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9999.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_13_12_21_05_F9999.yaml) [:arrow_left:](../9998/index.md) [:arrow_right:](../10000/index.md)

**start**: 	August 13, 2024 12:21:05 PM UTC

**end**: 		August 13, 2024 12:22:05 PM UTC

**duration**: 0 days 00:00:59.900375

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

