# FILL 10316

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10316.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_05_03_40_23_F10316.yaml) [:arrow_left:](../10315/index.md) [:arrow_right:](../10317/index.md)

**start**: 	November 05, 2024 03:40:23 AM UTC

**end**: 		November 05, 2024 05:57:42 AM UTC

**duration**: 0 days 02:17:19.653750

**tags**:	 

- ION_PHYSICS



**comments**:	 

- B2V unstable

- QPS trigger of sextupoles and octuples in sector 78, power convertor trip




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10316/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10316/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10316/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10316/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10316/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10316/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

