# FILL 10241

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10241.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_18_08_56_20_F10241.yaml) [:arrow_left:](../10240/index.md) [:arrow_right:](../10242/index.md)

**start**: 	October 18, 2024 08:56:20 AM UTC

**end**: 		October 18, 2024 03:57:47 PM UTC

**duration**: 0 days 07:01:26.288750

**tags**:	 

- MD



**comments**:	 

- MD13703 ML asistance crystal channeling optimization



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

