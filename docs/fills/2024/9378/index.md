# FILL 9378

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9378.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_18_00_59_37_F9378.yaml) [:arrow_left:](../9377/index.md) [:arrow_right:](../9379/index.md)

**start**: 	March 18, 2024 12:59:37 AM UTC

**end**: 		March 18, 2024 02:22:19 AM UTC

**duration**: 0 days 01:22:42.390875

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- low-beta/VDM cycles for coupling in the ramp



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

