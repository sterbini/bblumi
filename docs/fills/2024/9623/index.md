# FILL 9623

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9623.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_13_21_50_12_F9623.yaml) [:arrow_left:](../9622/index.md) [:arrow_right:](../9624/index.md)

**start**: 	May 13, 2024 09:50:12 PM UTC

**end**: 		May 14, 2024 04:51:39 AM UTC

**duration**: 0 days 07:01:27.284625

**tags**:	 

- MD



**comments**:	 

- MD11786 Longitudinal threshold of Landau Damping



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

