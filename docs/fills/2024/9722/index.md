# FILL 9722

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9722.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_06_17_20_40_F9722.yaml) [:arrow_left:](../9721/index.md) [:arrow_right:](../9723/index.md)

**start**: 	June 06, 2024 05:20:40 PM UTC

**end**: 		June 06, 2024 08:41:16 PM UTC

**duration**: 0 days 03:20:36.351000

**tags**:	 

- MD

- OP_DUMP



**comments**:	 

- MD11243



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

