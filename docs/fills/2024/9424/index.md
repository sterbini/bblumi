# FILL 9424

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9424.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_25_17_36_58_F9424.yaml) [:arrow_left:](../9423/index.md) [:arrow_right:](../9425/index.md)

**start**: 	March 25, 2024 05:36:58 PM UTC

**end**: 		March 25, 2024 08:40:22 PM UTC

**duration**: 0 days 03:03:24.018000

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

