# FILL 9379

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9379.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_18_02_22_19_F9379.yaml) [:arrow_left:](../9378/index.md) [:arrow_right:](../9380/index.md)

**start**: 	March 18, 2024 02:22:19 AM UTC

**end**: 		March 18, 2024 03:29:03 AM UTC

**duration**: 0 days 01:06:43.736750

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- low-beta/VDM cycles for coupling in the ramp



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

