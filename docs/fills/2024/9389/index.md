# FILL 9389

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9389.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_19_09_53_46_F9389.yaml) [:arrow_left:](../9388/index.md) [:arrow_right:](../9390/index.md)

**start**: 	March 19, 2024 09:53:46 AM UTC

**end**: 		March 19, 2024 11:32:26 AM UTC

**duration**: 0 days 01:38:39.778250

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- collisions at 450 GeV



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

