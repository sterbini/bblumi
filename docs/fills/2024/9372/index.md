# FILL 9372

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9372.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_17_02_59_15_F9372.yaml) [:arrow_left:](../9371/index.md) [:arrow_right:](../9373/index.md)

**start**: 	March 17, 2024 02:59:15 AM UTC

**end**: 		March 17, 2024 04:17:47 AM UTC

**duration**: 0 days 01:18:31.860625

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

