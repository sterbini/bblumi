# FILL 9617

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9617.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_12_16_02_41_F9617.yaml) [:arrow_left:](../9616/index.md) [:arrow_right:](../9618/index.md)

**start**: 	May 12, 2024 04:02:41 PM UTC

**end**: 		May 12, 2024 04:14:47 PM UTC

**duration**: 0 days 00:12:06.134625

**tags**:	 

- DUCK



**comments**:	 

- issue with ADT



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

