# FILL 10270

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10270.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_20_18_42_30_F10270.yaml) [:arrow_left:](../10269/index.md) [:arrow_right:](../10271/index.md)

**start**: 	October 20, 2024 06:42:30 PM UTC

**end**: 		October 20, 2024 07:13:08 PM UTC

**duration**: 0 days 00:30:37.492250

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

