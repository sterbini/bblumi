# FILL 9361

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9361.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_15_03_31_56_F9361.yaml) [:arrow_left:](../9360/index.md) [:arrow_right:](../9362/index.md)

**start**: 	March 15, 2024 03:31:56 AM UTC

**end**: 		March 15, 2024 05:16:03 AM UTC

**duration**: 0 days 01:44:06.319250

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

