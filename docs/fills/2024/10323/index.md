# FILL 10323

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10323.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_06_02_08_13_F10323.yaml) [:arrow_left:](../10322/index.md) [:arrow_right:](../10324/index.md)

**start**: 	November 06, 2024 02:08:13 AM UTC

**end**: 		November 06, 2024 03:04:57 AM UTC

**duration**: 0 days 00:56:44.068000

**tags**:	 

- ION_PHYSICS

- OP_DUMP



**comments**:	 


#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10323/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10323/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10323/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10323/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10323/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10323/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

