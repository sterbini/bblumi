# FILL 9893

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9893.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_13_01_30_30_F9893.yaml) [:arrow_left:](../9892/index.md) [:arrow_right:](../9894/index.md)

**start**: 	July 13, 2024 01:30:30 AM UTC

**end**: 		July 13, 2024 02:59:54 AM UTC

**duration**: 0 days 01:29:24.302125

**tags**:	 

- PHYSICS

- OP_DUMP



**comments**:	 

- The undulators were not ON and to avoid any mismatches the EIC decided to turn them on. Other power converters were already armed so when the timing even was sent all power converter started to play. Not everything was armed so the EiC decided to dump using the switch but just at the same time the beam was dumped by PC interlock.




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9893/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9893/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9893/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9893/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9893/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9893/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

