# FILL 9399

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9399.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_21_03_37_51_F9399.yaml) [:arrow_left:](../9398/index.md) [:arrow_right:](../9400/index.md)

**start**: 	March 21, 2024 03:37:51 AM UTC

**end**: 		March 21, 2024 06:15:53 AM UTC

**duration**: 0 days 02:38:01.815125

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

