# FILL 9414

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9414.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_23_17_15_03_F9414.yaml) [:arrow_left:](../9413/index.md) [:arrow_right:](../9415/index.md)

**start**: 	March 23, 2024 05:15:03 PM UTC

**end**: 		March 23, 2024 07:40:35 PM UTC

**duration**: 0 days 02:25:32.062500

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- ion cycle



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

