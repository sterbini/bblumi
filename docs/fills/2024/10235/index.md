# FILL 10235

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10235.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_17_10_07_29_F10235.yaml) [:arrow_left:](../10234/index.md) [:arrow_right:](../10236/index.md)

**start**: 	October 17, 2024 10:07:29 AM UTC

**end**: 		October 17, 2024 11:17:50 AM UTC

**duration**: 0 days 01:10:20.701250

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

