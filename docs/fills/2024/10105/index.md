# FILL 10105

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10105.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_11_16_35_08_F10105.yaml) [:arrow_left:](../10104/index.md) [:arrow_right:](../10106/index.md)

**start**: 	September 11, 2024 04:35:08 PM UTC

**end**: 		September 12, 2024 01:06:50 PM UTC

**duration**: 0 days 20:31:41.858875

**tags**:	 

- PHYSICS

- WIRES

- OP_DUMP



**comments**:	 

- Good lifetime at the end of collapse (>16 hours)

- Co-Linearity scans: scanned between [0.5, 2] units in IP1 (nominal 1.0) and between [-0.5, 0.5] units in IP5 (nominal 0.0). Did not find a significant improvement, although the optimum for IP5 might be slightly below 0.

- Hiccup due to expiring GitLab tokens




#### Cycle duration

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Cycle_duration.png)


#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Fill_chromaticity.png)

#### Intensity


##### Cycle intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Cycle_intensity.png)

##### BBB SB intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/bbb_SB_intensity.png)

##### Violin BBB intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Cycle_violin_intensity_bl.png)

##### SB bunch intensity 

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Fill_adjust_stable_losses_summary.png)

#### Bunch Length


![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Cycle_bunchlength.png)

##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Fill_bunch_length.png)

#### Emittance


##### B1 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Cycle_emit_b1.png)

##### B2 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Cycle_emit_b2.png)

##### Emittance growth at injection

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Fill_injection_emit_blowup.png)

##### Summary violin plot

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Cycle_violin_emit.png)

##### BBB in SB for B1 

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/bbb_SB_B1_emit.png)

##### BBB in SB for B2 

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/bbb_SB_B2_emit.png)

#### Bunch brightness


![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Cycle_brightness.png)

#### Beam properties summary 

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Cycle_summary.png)

#### Luminosity


##### Overview

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Fill_luminosity.png)

##### Luminous Regions

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Fill_luminous_regions.png)

##### BBB luminosity in SB

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/bbb_SB_luminosity.png)

#### Cross-sections


##### ADJUST

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Xsection_ADJUST_heatmap.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Xsection_ADJUST_mean.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Xsection_ADJUST_bbb_ip15lr.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Xsection_ADJUST_bbb_ip2ho.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Xsection_ADJUST_bbb_lhcbho.png)

##### STABLE BEAM

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Xsection_STABLE_heatmap_DBLM.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Xsection_SB_mean_DBLM.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Xsection_SB_mean_DBLM_separation.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Xsection_SB_bbb_DBLM_ip15lr.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Xsection_SB_bbb_DBLM_ip2ho.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10105/Xsection_SB_bbb_DBLM_lhcbho.png)

