# FILL 9976

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9976.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_05_14_13_54_F9976.yaml) [:arrow_left:](../9975/index.md) [:arrow_right:](../9977/index.md)

**start**: 	August 05, 2024 02:13:54 PM UTC

**end**: 		August 05, 2024 04:33:16 PM UTC

**duration**: 0 days 02:19:21.961250

**tags**:	 

- DUCK



**comments**:	 

- RB.A78 and RQS.R2B2  tripped when starting the precycle: access of the EPC piquet



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

