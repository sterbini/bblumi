# FILL 10252

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10252.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_19_06_17_09_F10252.yaml) [:arrow_left:](../10251/index.md) [:arrow_right:](../10253/index.md)

**start**: 	October 19, 2024 06:17:09 AM UTC

**end**: 		October 19, 2024 11:01:18 AM UTC

**duration**: 0 days 04:44:08.901125

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

