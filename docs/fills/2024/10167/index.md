# FILL 10167

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10167.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_28_01_36_29_F10167.yaml) [:arrow_left:](../10166/index.md) [:arrow_right:](../10168/index.md)

**start**: 	September 28, 2024 01:36:29 AM UTC

**end**: 		September 28, 2024 06:22:50 AM UTC

**duration**: 0 days 04:46:20.595500

**tags**:	 

- MD



**comments**:	 

- 12663 Wire compensation during the beta*-leveling



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

