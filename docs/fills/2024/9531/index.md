# FILL 9531

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9531.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_17_06_48_48_F9531.yaml) [:arrow_left:](../9530/index.md) [:arrow_right:](../9532/index.md)

**start**: 	April 17, 2024 06:48:48 AM UTC

**end**: 		April 17, 2024 11:59:45 AM UTC

**duration**: 0 days 05:10:57.551250

**tags**:	 

- DUCK



**comments**:	 

- Access



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

