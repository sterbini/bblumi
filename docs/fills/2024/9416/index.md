# FILL 9416

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9416.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_24_02_44_41_F9416.yaml) [:arrow_left:](../9415/index.md) [:arrow_right:](../9417/index.md)

**start**: 	March 24, 2024 02:44:41 AM UTC

**end**: 		March 24, 2024 02:59:24 AM UTC

**duration**: 0 days 00:14:42.810250

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

