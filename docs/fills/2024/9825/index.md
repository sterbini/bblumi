# FILL 9825

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9825.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_24_23_11_05_F9825.yaml) [:arrow_left:](../9824/index.md) [:arrow_right:](../9826/index.md)

**start**: 	June 24, 2024 11:11:05 PM UTC

**end**: 		June 25, 2024 11:06:00 AM UTC

**duration**: 0 days 11:54:55.564375

**tags**:	 

- MD



**comments**:	 

- ion beam commissioning



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

