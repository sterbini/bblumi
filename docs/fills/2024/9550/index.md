# FILL 9550

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9550.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_22_16_27_09_F9550.yaml) [:arrow_left:](../9549/index.md) [:arrow_right:](../9551/index.md)

**start**: 	April 22, 2024 04:27:09 PM UTC

**end**: 		April 22, 2024 09:25:34 PM UTC

**duration**: 0 days 04:58:25.242250

**tags**:	 

- DUCK



**comments**:	 

- cryo recovery



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

