# FILL 9975

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9975.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_05_09_56_29_F9975.yaml) [:arrow_left:](../9974/index.md) [:arrow_right:](../9976/index.md)

**start**: 	August 05, 2024 09:56:29 AM UTC

**end**: 		August 05, 2024 02:13:54 PM UTC

**duration**: 0 days 04:17:24.913000

**tags**:	 

- DUCK



**comments**:	 

- Access following the communication problem of the FGCs in Point 7

- SND emulsions changed and access in LHCb (muon chamber issue), ALICE and CV in Point 3



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

