# FILL 10317

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10317.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_05_05_57_42_F10317.yaml) [:arrow_left:](../10316/index.md) [:arrow_right:](../10318/index.md)

**start**: 	November 05, 2024 05:57:42 AM UTC

**end**: 		November 05, 2024 11:41:35 AM UTC

**duration**: 0 days 05:43:52.144250

**tags**:	 

- ION_PHYSICS

- OP_DUMP



**comments**:	 

- Dumped due to access




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10317/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10317/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10317/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10317/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10317/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10317/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

