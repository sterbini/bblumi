# FILL 9908

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9908.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_17_09_35_04_F9908.yaml) [:arrow_left:](../9907/index.md) [:arrow_right:](../9909/index.md)

**start**: 	July 17, 2024 09:35:04 AM UTC

**end**: 		July 17, 2024 10:14:31 AM UTC

**duration**: 0 days 00:39:27.390000

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

