# FILL 10281

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10281.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_25_11_40_35_F10281.yaml) [:arrow_left:](../10280/index.md) [:arrow_right:](../10282/index.md)

**start**: 	October 25, 2024 11:40:35 AM UTC

**end**: 		October 25, 2024 08:15:22 PM UTC

**duration**: 0 days 08:34:47.439750

**tags**:	 

- BEAM_COMMISSIONING

- ION_OPTICS



**comments**:	 

- Afte the VIP visit in IP1Alignment of the TCT

- Testing collisions

- SVC (TCR part) tripped in P4

- Optimization of the BFPP bumps

- Measuring aperture (test) and dumped on ATLAS BCM



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

