# FILL 9752

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9752.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_09_06_20_46_F9752.yaml) [:arrow_left:](../9751/index.md) [:arrow_right:](../9753/index.md)

**start**: 	June 09, 2024 06:20:46 AM UTC

**end**: 		June 09, 2024 07:01:43 AM UTC

**duration**: 0 days 00:40:57.105375

**tags**:	 

- MD



**comments**:	 

- MD9544



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

