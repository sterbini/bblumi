# FILL 9449

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9449.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_01_13_40_11_F9449.yaml) [:arrow_left:](../9448/index.md) [:arrow_right:](../9450/index.md)

**start**: 	April 01, 2024 01:40:11 PM UTC

**end**: 		April 01, 2024 01:58:05 PM UTC

**duration**: 0 days 00:17:53.642125

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

