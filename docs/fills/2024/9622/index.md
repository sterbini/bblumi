# FILL 9622

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9622.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_13_15_32_14_F9622.yaml) [:arrow_left:](../9621/index.md) [:arrow_right:](../9623/index.md)

**start**: 	May 13, 2024 03:32:14 PM UTC

**end**: 		May 13, 2024 09:50:12 PM UTC

**duration**: 0 days 06:17:57.456000

**tags**:	 

- MD



**comments**:	 

- first 1hour MD11603, crabbing at injection

- MD11723, non factorization MD at injection

- WS, BSRT and longitudinal profiles acquired



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

