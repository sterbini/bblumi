# FILL 10224

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10224.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_14_04_47_29_F10224.yaml) [:arrow_left:](../10223/index.md) [:arrow_right:](../10225/index.md)

**start**: 	October 14, 2024 04:47:29 AM UTC

**end**: 		October 14, 2024 03:00:25 PM UTC

**duration**: 0 days 10:12:55.923375

**tags**:	 

- MD



**comments**:	 

- 2 colliding INDIV for FASER/SND background studies

- issue with LINCAC4



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

