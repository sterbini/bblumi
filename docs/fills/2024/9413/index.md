# FILL 9413

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9413.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_23_12_55_49_F9413.yaml) [:arrow_left:](../9412/index.md) [:arrow_right:](../9414/index.md)

**start**: 	March 23, 2024 12:55:49 PM UTC

**end**: 		March 23, 2024 05:15:03 PM UTC

**duration**: 0 days 04:19:14.383375

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- nominal bunch ramps for BI tests



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

