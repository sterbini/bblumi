# FILL 9388

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9388.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_19_08_56_53_F9388.yaml) [:arrow_left:](../9387/index.md) [:arrow_right:](../9389/index.md)

**start**: 	March 19, 2024 08:56:53 AM UTC

**end**: 		March 19, 2024 09:53:46 AM UTC

**duration**: 0 days 00:56:53.417000

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- collisions at 450 GeV



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

