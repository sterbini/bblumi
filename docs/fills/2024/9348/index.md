# FILL 9348

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9348.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_12_12_09_17_F9348.yaml) [:arrow_left:](../9347/index.md) [:arrow_right:](../9349/index.md)

**start**: 	March 12, 2024 12:09:17 PM UTC

**end**: 		March 12, 2024 12:12:37 PM UTC

**duration**: 0 days 00:03:19.571375

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

