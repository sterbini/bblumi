# FILL 9364

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9364.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_15_06_48_53_F9364.yaml) [:arrow_left:](../9363/index.md) [:arrow_right:](../9365/index.md)

**start**: 	March 15, 2024 06:48:53 AM UTC

**end**: 		March 15, 2024 12:38:16 PM UTC

**duration**: 0 days 05:49:23.166000

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

