# FILL 9894

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9894.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_13_02_59_54_F9894.yaml) [:arrow_left:](../9893/index.md) [:arrow_right:](../9895/index.md)

**start**: 	July 13, 2024 02:59:54 AM UTC

**end**: 		July 13, 2024 03:08:01 AM UTC

**duration**: 0 days 00:08:06.390500

**tags**:	 

- DUCK



**comments**:	 

- Cycling



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

