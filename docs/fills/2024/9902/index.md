# FILL 9902

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9902.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_15_12_03_39_F9902.yaml) [:arrow_left:](../9901/index.md) [:arrow_right:](../9903/index.md)

**start**: 	July 15, 2024 12:03:39 PM UTC

**end**: 		July 15, 2024 04:38:13 PM UTC

**duration**: 0 days 04:34:33.576000

**tags**:	 

- DUCK



**comments**:	 

- Precycle after trip of RB.A81




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9902/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9902/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9902/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9902/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9902/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9902/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

