# FILL 10131

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10131.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_20_11_30_50_F10131.yaml) [:arrow_left:](../10130/index.md) [:arrow_right:](../10132/index.md)

**start**: 	September 20, 2024 11:30:50 AM UTC

**end**: 		September 20, 2024 01:41:49 PM UTC

**duration**: 0 days 02:10:59.691625

**tags**:	 

- DUCK



**comments**:	 

- Sector 81 lost



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

