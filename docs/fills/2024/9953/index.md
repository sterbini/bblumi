# FILL 9953

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9953.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_31_09_33_40_F9953.yaml) [:arrow_left:](../9952/index.md) [:arrow_right:](../9954/index.md)

**start**: 	July 31, 2024 09:33:40 AM UTC

**end**: 		July 31, 2024 10:08:49 AM UTC

**duration**: 0 days 00:35:09.571750

**tags**:	 

- DUCK



**comments**:	 

- nfs problems



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

