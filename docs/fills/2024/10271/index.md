# FILL 10271

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10271.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_20_19_13_08_F10271.yaml) [:arrow_left:](../10270/index.md) [:arrow_right:](../10272/index.md)

**start**: 	October 20, 2024 07:13:08 PM UTC

**end**: 		October 20, 2024 07:37:10 PM UTC

**duration**: 0 days 00:24:01.770375

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

