# FILL 9341

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9341.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_11_03_59_42_F9341.yaml) [:arrow_left:](../9340/index.md) [:arrow_right:](../9342/index.md)

**start**: 	March 11, 2024 03:59:42 AM UTC

**end**: 		March 11, 2024 06:30:57 AM UTC

**duration**: 0 days 02:31:14.902125

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

