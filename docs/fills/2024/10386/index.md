# FILL 10386

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10386.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_18_20_38_04_F10386.yaml) [:arrow_left:](../10385/index.md) [:arrow_right:](../10387/index.md)

**start**: 	November 18, 2024 08:38:04 PM UTC

**end**: 		November 18, 2024 10:56:03 PM UTC

**duration**: 0 days 02:17:59.382000

**tags**:	 

- DUCK



**comments**:	 

- asynchronous dump loss maps



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

