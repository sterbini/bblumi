# FILL 10371

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10371.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_14_13_45_43_F10371.yaml) [:arrow_left:](../10370/index.md) [:arrow_right:](../10372/index.md)

**start**: 	November 14, 2024 01:45:43 PM UTC

**end**: 		November 14, 2024 03:32:14 PM UTC

**duration**: 0 days 01:46:31.355500

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

