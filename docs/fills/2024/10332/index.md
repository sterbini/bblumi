# FILL 10332

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10332.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_06_21_20_33_F10332.yaml) [:arrow_left:](../10331/index.md) [:arrow_right:](../10333/index.md)

**start**: 	November 06, 2024 09:20:33 PM UTC

**end**: 		November 07, 2024 12:34:50 AM UTC

**duration**: 0 days 03:14:16.687500

**tags**:	 

- ION_PHYSICS



**comments**:	 

- trip of the orbit correctors



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

