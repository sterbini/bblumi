# FILL 9347

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9347.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_12_02_16_23_F9347.yaml) [:arrow_left:](../9346/index.md) [:arrow_right:](../9348/index.md)

**start**: 	March 12, 2024 02:16:23 AM UTC

**end**: 		March 12, 2024 12:09:17 PM UTC

**duration**: 0 days 09:52:54.562375

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

