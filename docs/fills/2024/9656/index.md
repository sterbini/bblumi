# FILL 9656

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9656.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_21_16_03_10_F9656.yaml) [:arrow_left:](../9655/index.md) [:arrow_right:](../9657/index.md)

**start**: 	May 21, 2024 04:03:10 PM UTC

**end**: 		May 21, 2024 04:37:10 PM UTC

**duration**: 0 days 00:34:00.701250

**tags**:	 

- DUCK



**comments**:	 

- Cryogenics problem.



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

