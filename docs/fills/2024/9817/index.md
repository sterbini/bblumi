# FILL 9817

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9817.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_23_00_27_31_F9817.yaml) [:arrow_left:](../9816/index.md) [:arrow_right:](../9818/index.md)

**start**: 	June 23, 2024 12:27:31 AM UTC

**end**: 		June 23, 2024 09:26:00 AM UTC

**duration**: 0 days 08:58:29.046250

**tags**:	 

- DUCK



**comments**:	 

- SPS RF issues

- cryo conditions



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

