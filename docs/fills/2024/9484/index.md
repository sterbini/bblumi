# FILL 9484

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9484.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_08_06_16_55_F9484.yaml) [:arrow_left:](../9483/index.md) [:arrow_right:](../9485/index.md)

**start**: 	April 08, 2024 06:16:55 AM UTC

**end**: 		April 08, 2024 07:38:03 PM UTC

**duration**: 0 days 13:21:08.136125

**tags**:	 

- INTENSITY_RAMPUP



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

