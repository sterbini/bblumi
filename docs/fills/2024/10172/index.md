# FILL 10172

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10172.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_28_21_01_52_F10172.yaml) [:arrow_left:](../10171/index.md) [:arrow_right:](../10173/index.md)

**start**: 	September 28, 2024 09:01:52 PM UTC

**end**: 		September 29, 2024 05:58:01 AM UTC

**duration**: 0 days 08:56:08.803125

**tags**:	 

- MD



**comments**:	 

- 12723 HL-LHC optics cycle



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

