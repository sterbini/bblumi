# FILL 9486

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9486.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_09_06_31_21_F9486.yaml) [:arrow_left:](../9485/index.md) [:arrow_right:](../9487/index.md)

**start**: 	April 09, 2024 06:31:21 AM UTC

**end**: 		April 09, 2024 06:52:48 AM UTC

**duration**: 0 days 00:21:26.722375

**tags**:	 

- INTENSITY_RAMPUP



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

