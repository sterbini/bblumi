# FILL 9353

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9353.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_13_23_22_39_F9353.yaml) [:arrow_left:](../9352/index.md) [:arrow_right:](../9354/index.md)

**start**: 	March 13, 2024 11:22:39 PM UTC

**end**: 		March 14, 2024 10:30:18 AM UTC

**duration**: 0 days 11:07:39.218000

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- linear optics 2m-1.2m

- non-linear errors in IT1



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

