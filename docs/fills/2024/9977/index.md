# FILL 9977

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9977.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_05_16_33_16_F9977.yaml) [:arrow_left:](../9976/index.md) [:arrow_right:](../9978/index.md)

**start**: 	August 05, 2024 04:33:16 PM UTC

**end**: 		August 05, 2024 04:34:44 PM UTC

**duration**: 0 days 00:01:27.970500

**tags**:	 

- DUCK



**comments**:	 

- Precycle



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

