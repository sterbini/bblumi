# FILL 9461

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9461.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_03_16_01_25_F9461.yaml) [:arrow_left:](../9460/index.md) [:arrow_right:](../9462/index.md)

**start**: 	April 03, 2024 04:01:25 PM UTC

**end**: 		April 04, 2024 06:41:59 AM UTC

**duration**: 0 days 14:40:33.555125

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- XRP alignment



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

