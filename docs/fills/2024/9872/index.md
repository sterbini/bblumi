# FILL 9872

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9872.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_07_15_14_49_F9872.yaml) [:arrow_left:](../9871/index.md) [:arrow_right:](../9873/index.md)

**start**: 	July 07, 2024 03:14:49 PM UTC

**end**: 		July 07, 2024 06:32:26 PM UTC

**duration**: 0 days 03:17:36.886375

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

