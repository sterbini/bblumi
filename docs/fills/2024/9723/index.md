# FILL 9723

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9723.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_06_20_41_16_F9723.yaml) [:arrow_left:](../9722/index.md) [:arrow_right:](../9724/index.md)

**start**: 	June 06, 2024 08:41:16 PM UTC

**end**: 		June 06, 2024 10:42:55 PM UTC

**duration**: 0 days 02:01:38.518625

**tags**:	 

- MD



**comments**:	 

- MD11243, Optics corrections knobs functions in ramp



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

