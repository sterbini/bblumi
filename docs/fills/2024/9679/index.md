# FILL 9679

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9679.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_28_00_39_48_F9679.yaml) [:arrow_left:](../9678/index.md) [:arrow_right:](../9680/index.md)

**start**: 	May 28, 2024 12:39:48 AM UTC

**end**: 		May 28, 2024 05:09:21 AM UTC

**duration**: 0 days 04:29:33.065625

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

