# FILL 9709

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9709.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_05_08_48_54_F9709.yaml) [:arrow_left:](../9708/index.md) [:arrow_right:](../9710/index.md)

**start**: 	June 05, 2024 08:48:54 AM UTC

**end**: 		June 05, 2024 09:25:01 AM UTC

**duration**: 0 days 00:36:07.846000

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

