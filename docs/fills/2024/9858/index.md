# FILL 9858

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9858.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_04_00_22_33_F9858.yaml) [:arrow_left:](../9857/index.md) [:arrow_right:](../9859/index.md)

**start**: 	July 04, 2024 12:22:33 AM UTC

**end**: 		July 04, 2024 05:12:41 AM UTC

**duration**: 0 days 04:50:08.246375

**tags**:	 

- DUCK

- TO_STUDY



**comments**:	 

- Interesting MKI B2 bad shot, dumped for refilling




#### Cycle duration

![](https://dltwo.web.cern.ch/followup_plots/2024/9858/Cycle_duration.png)


#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9858/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9858/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9858/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9858/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9858/Fill_chromaticity.png)

#### Intensity


##### Cycle intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/9858/Cycle_intensity.png)

##### Violin BBB intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/9858/Cycle_violin_intensity_bl.png)

#### Bunch Length


![](https://dltwo.web.cern.ch/followup_plots/2024/9858/Cycle_bunchlength.png)

##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9858/Fill_bunch_length.png)

#### Emittance


##### B1 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/9858/Cycle_emit_b1.png)

##### B2 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/9858/Cycle_emit_b2.png)

##### Emittance growth at injection

![](https://dltwo.web.cern.ch/followup_plots/2024/9858/Fill_injection_emit_blowup.png)

##### Summary violin plot

![](https://dltwo.web.cern.ch/followup_plots/2024/9858/Cycle_violin_emit.png)

#### Bunch brightness


![](https://dltwo.web.cern.ch/followup_plots/2024/9858/Cycle_brightness.png)

#### Beam properties summary 

![](https://dltwo.web.cern.ch/followup_plots/2024/9858/Cycle_summary.png)

#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

