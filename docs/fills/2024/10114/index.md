# FILL 10114

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10114.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_14_05_53_35_F10114.yaml) [:arrow_left:](../10113/index.md) [:arrow_right:](../10115/index.md)

**start**: 	September 14, 2024 05:53:35 AM UTC

**end**: 		September 14, 2024 05:57:20 AM UTC

**duration**: 0 days 00:03:44.625250

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

