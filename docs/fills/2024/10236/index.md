# FILL 10236

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10236.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_17_11_17_50_F10236.yaml) [:arrow_left:](../10235/index.md) [:arrow_right:](../10237/index.md)

**start**: 	October 17, 2024 11:17:50 AM UTC

**end**: 		October 17, 2024 02:09:20 PM UTC

**duration**: 0 days 02:51:29.850250

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

