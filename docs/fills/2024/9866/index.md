# FILL 9866

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9866.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_07_06_15_14_12_F9866.yaml) [:arrow_left:](../9865/index.md) [:arrow_right:](../9867/index.md)

**start**: 	July 06, 2024 03:14:12 PM UTC

**end**: 		July 06, 2024 08:12:04 PM UTC

**duration**: 0 days 04:57:51.865000

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

