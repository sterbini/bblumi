# FILL 9665

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9665.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_24_08_04_01_F9665.yaml) [:arrow_left:](../9664/index.md) [:arrow_right:](../9666/index.md)

**start**: 	May 24, 2024 08:04:01 AM UTC

**end**: 		May 24, 2024 08:51:32 AM UTC

**duration**: 0 days 00:47:30.817125

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

