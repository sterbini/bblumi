# FILL 9407

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9407.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_23_00_31_51_F9407.yaml) [:arrow_left:](../9406/index.md) [:arrow_right:](../9408/index.md)

**start**: 	March 23, 2024 12:31:51 AM UTC

**end**: 		March 23, 2024 02:35:40 AM UTC

**duration**: 0 days 02:03:49.153875

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- new b* levelling 120cm to 30cm



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

