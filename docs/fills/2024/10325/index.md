# FILL 10325

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10325.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_06_03_33_55_F10325.yaml) [:arrow_left:](../10324/index.md) [:arrow_right:](../10326/index.md)

**start**: 	November 06, 2024 03:33:55 AM UTC

**end**: 		November 06, 2024 03:57:15 AM UTC

**duration**: 0 days 00:23:20.258875

**tags**:	 

- DUCK



**comments**:	 

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10325/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10325/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10325/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10325/Fill_chromaticity.png)

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

