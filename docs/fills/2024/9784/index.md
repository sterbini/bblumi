# FILL 9784

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9784.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_16_16_10_50_F9784.yaml) [:arrow_left:](../9783/index.md) [:arrow_right:](../9785/index.md)

**start**: 	June 16, 2024 04:10:50 PM UTC

**end**: 		June 16, 2024 07:45:19 PM UTC

**duration**: 0 days 03:34:28.956250

**tags**:	 

- DUCK



**comments**:	 

- Access for RQ10.R4B1 problem



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

