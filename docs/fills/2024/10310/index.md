# FILL 10310

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10310.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_02_02_28_25_F10310.yaml) [:arrow_left:](../10309/index.md) [:arrow_right:](../10311/index.md)

**start**: 	November 02, 2024 02:28:25 AM UTC

**end**: 		November 03, 2024 09:13:39 AM UTC

**duration**: 1 days 06:45:14.138000

**tags**:	 

- PPREF

- OP_DUMP



**comments**:	 

- B2 bunch length higher than B1

- 18h SB, beams in IP8 head on




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10310/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10310/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10310/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10310/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10310/Fill_chromaticity.png)

#### Intensity


##### BBB SB intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/10310/bbb_SB_intensity.png)

##### SB bunch intensity 

![](https://dltwo.web.cern.ch/followup_plots/2024/10310/Fill_adjust_stable_losses_summary.png)

##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10310/Fill_bunch_length.png)

#### Emittance


##### Emittance growth at injection

![](https://dltwo.web.cern.ch/followup_plots/2024/10310/Fill_injection_emit_blowup.png)

##### BBB in SB for B1 

![](https://dltwo.web.cern.ch/followup_plots/2024/10310/bbb_SB_B1_emit.png)

##### BBB in SB for B2 

![](https://dltwo.web.cern.ch/followup_plots/2024/10310/bbb_SB_B2_emit.png)

#### Luminosity


##### Overview

![](https://dltwo.web.cern.ch/followup_plots/2024/10310/Fill_luminosity.png)

##### Luminous Regions

![](https://dltwo.web.cern.ch/followup_plots/2024/10310/Fill_luminous_regions.png)

##### BBB luminosity in SB

![](https://dltwo.web.cern.ch/followup_plots/2024/10310/bbb_SB_luminosity.png)

#### Cross-sections


##### ADJUST

![](https://dltwo.web.cern.ch/followup_plots/2024/10310/Xsection_ADJUST_heatmap.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10310/Xsection_ADJUST_mean.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10310/Xsection_ADJUST_bbb_ip15lr.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10310/Xsection_ADJUST_bbb_ip2ho.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10310/Xsection_ADJUST_bbb_lhcbho.png)

##### STABLE BEAM

![](https://dltwo.web.cern.ch/followup_plots/2024/10310/Xsection_STABLE_heatmap_DBLM.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10310/Xsection_SB_mean_DBLM.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10310/Xsection_SB_mean_DBLM_separation.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10310/Xsection_SB_bbb_DBLM_ip15lr.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10310/Xsection_SB_bbb_DBLM_ip2ho.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/10310/Xsection_SB_bbb_DBLM_lhcbho.png)

