# FILL 9826

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9826.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_25_11_06_00_F9826.yaml) [:arrow_left:](../9825/index.md) [:arrow_right:](../9827/index.md)

**start**: 	June 25, 2024 11:06:00 AM UTC

**end**: 		June 25, 2024 09:11:24 PM UTC

**duration**: 0 days 10:05:24.335625

**tags**:	 

- MD



**comments**:	 

- RF calibration



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

