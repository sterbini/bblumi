# FILL 10264

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10264.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_20_12_15_20_F10264.yaml) [:arrow_left:](../10263/index.md) [:arrow_right:](../10265/index.md)

**start**: 	October 20, 2024 12:15:20 PM UTC

**end**: 		October 20, 2024 01:39:16 PM UTC

**duration**: 0 days 01:23:56.039125

**tags**:	 

- MD



**comments**:	 

- MD13523 measurements for improvement of intensity dependent correctio




#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/10264/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/10264/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/10264/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10264/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/10264/Fill_chromaticity.png)

#### Intensity


##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/10264/Fill_bunch_length.png)

#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

