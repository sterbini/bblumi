# FILL 10395

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10395.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_11_20_18_39_31_F10395.yaml) [:arrow_left:](../10394/index.md) [:arrow_right:](../10396/index.md)

**start**: 	November 20, 2024 06:39:31 PM UTC

**end**: 		November 20, 2024 07:34:43 PM UTC

**duration**: 0 days 00:55:12.051125

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

