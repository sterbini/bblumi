# FILL 9833

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9833.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_27_20_48_49_F9833.yaml) [:arrow_left:](../9832/index.md) [:arrow_right:](../9834/index.md)

**start**: 	June 27, 2024 08:48:49 PM UTC

**end**: 		June 28, 2024 01:57:22 PM UTC

**duration**: 0 days 17:08:32.348125

**tags**:	 

- DUCK



**comments**:	 

- Long cryo problem



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

