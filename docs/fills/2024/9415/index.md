# FILL 9415

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9415.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_23_19_40_35_F9415.yaml) [:arrow_left:](../9414/index.md) [:arrow_right:](../9416/index.md)

**start**: 	March 23, 2024 07:40:35 PM UTC

**end**: 		March 24, 2024 02:44:41 AM UTC

**duration**: 0 days 07:04:05.973875

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

