# FILL 9699

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9699.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_02_05_14_16_F9699.yaml) [:arrow_left:](../9698/index.md) [:arrow_right:](../9700/index.md)

**start**: 	June 02, 2024 05:14:16 AM UTC

**end**: 		June 02, 2024 09:30:03 AM UTC

**duration**: 0 days 04:15:46.978500

**tags**:	 

- PHYSICS



**comments**:	 

- Energy extraction issue




#### Cycle duration

![](https://dltwo.web.cern.ch/followup_plots/2024/9699/Cycle_duration.png)


#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9699/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9699/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9699/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9699/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9699/Fill_chromaticity.png)

#### Intensity


##### Cycle intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/9699/Cycle_intensity.png)

##### Violin BBB intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/9699/Cycle_violin_intensity_bl.png)

#### Bunch Length


![](https://dltwo.web.cern.ch/followup_plots/2024/9699/Cycle_bunchlength.png)

##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9699/Fill_bunch_length.png)

#### Emittance


##### B1 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/9699/Cycle_emit_b1.png)

##### B2 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/9699/Cycle_emit_b2.png)

##### Emittance growth at injection

![](https://dltwo.web.cern.ch/followup_plots/2024/9699/Fill_injection_emit_blowup.png)

##### Summary violin plot

![](https://dltwo.web.cern.ch/followup_plots/2024/9699/Cycle_violin_emit.png)

#### Bunch brightness


![](https://dltwo.web.cern.ch/followup_plots/2024/9699/Cycle_brightness.png)

#### Beam properties summary 

![](https://dltwo.web.cern.ch/followup_plots/2024/9699/Cycle_summary.png)

#### Luminosity


##### Overview

![](https://dltwo.web.cern.ch/followup_plots/2024/9699/Fill_luminosity.png)

#### Cross-sections


##### ADJUST

![](https://dltwo.web.cern.ch/followup_plots/2024/9699/Xsection_ADJUST_heatmap.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9699/Xsection_ADJUST_mean.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9699/Xsection_ADJUST_bbb_ip15lr.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9699/Xsection_ADJUST_bbb_ip2ho.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9699/Xsection_ADJUST_bbb_lhcbho.png)

##### STABLE BEAM

