# FILL 9335

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9335.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_10_04_25_05_F9335.yaml) [:arrow_left:](../9334/index.md) [:arrow_right:](../9336/index.md)

**start**: 	March 10, 2024 04:25:05 AM UTC

**end**: 		March 10, 2024 04:26:13 AM UTC

**duration**: 0 days 00:01:08.401250

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

