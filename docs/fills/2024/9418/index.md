# FILL 9418

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9418.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_24_03_53_50_F9418.yaml) [:arrow_left:](../9417/index.md) [:arrow_right:](../9419/index.md)

**start**: 	March 24, 2024 03:53:50 AM UTC

**end**: 		March 24, 2024 08:21:17 AM UTC

**duration**: 0 days 04:27:26.474000

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

