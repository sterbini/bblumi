# FILL 10008

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10008.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_16_13_25_48_F10008.yaml) [:arrow_left:](../10007/index.md) [:arrow_right:](../10009/index.md)

**start**: 	August 16, 2024 01:25:48 PM UTC

**end**: 		August 16, 2024 01:32:00 PM UTC

**duration**: 0 days 00:06:12.237375

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

