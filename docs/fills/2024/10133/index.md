# FILL 10133

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10133.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_21_00_34_16_F10133.yaml) [:arrow_left:](../10132/index.md) [:arrow_right:](../10134/index.md)

**start**: 	September 21, 2024 12:34:16 AM UTC

**end**: 		September 21, 2024 01:25:18 AM UTC

**duration**: 0 days 00:51:02.688750

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

