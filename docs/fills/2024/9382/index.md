# FILL 9382

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9382.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_18_14_29_28_F9382.yaml) [:arrow_left:](../9381/index.md) [:arrow_right:](../9383/index.md)

**start**: 	March 18, 2024 02:29:28 PM UTC

**end**: 		March 18, 2024 02:41:29 PM UTC

**duration**: 0 days 00:12:01.407625

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

