# FILL 9660

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9660.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_22_03_44_13_F9660.yaml) [:arrow_left:](../9659/index.md) [:arrow_right:](../9661/index.md)

**start**: 	May 22, 2024 03:44:13 AM UTC

**end**: 		May 22, 2024 06:47:22 AM UTC

**duration**: 0 days 03:03:08.363750

**tags**:	 

- DUCK



**comments**:	 

- Problem with the QPS.



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

