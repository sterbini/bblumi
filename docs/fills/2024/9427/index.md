# FILL 9427

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9427.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_26_17_28_08_F9427.yaml) [:arrow_left:](../9426/index.md) [:arrow_right:](../9428/index.md)

**start**: 	March 26, 2024 05:28:08 PM UTC

**end**: 		March 26, 2024 09:37:33 PM UTC

**duration**: 0 days 04:09:25.431500

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- loss maps at injection



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

