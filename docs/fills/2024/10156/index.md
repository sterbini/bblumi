# FILL 10156

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10156.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_09_27_18_10_08_F10156.yaml) [:arrow_left:](../10155/index.md) [:arrow_right:](../10157/index.md)

**start**: 	September 27, 2024 06:10:08 PM UTC

**end**: 		September 27, 2024 06:12:42 PM UTC

**duration**: 0 days 00:02:34.240625

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

