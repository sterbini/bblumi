# FILL 9673

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9673.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_05_26_11_21_23_F9673.yaml) [:arrow_left:](../9672/index.md) [:arrow_right:](../9674/index.md)

**start**: 	May 26, 2024 11:21:23 AM UTC

**end**: 		May 27, 2024 02:02:00 AM UTC

**duration**: 0 days 14:40:37.635750

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

