# FILL 10003

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10003.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_08_14_16_04_58_F10003.yaml) [:arrow_left:](../10002/index.md) [:arrow_right:](../10004/index.md)

**start**: 	August 14, 2024 04:04:58 PM UTC

**end**: 		August 14, 2024 04:40:55 PM UTC

**duration**: 0 days 00:35:57.258875

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

