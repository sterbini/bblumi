# FILL 9366

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9366.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_16_02_02_37_F9366.yaml) [:arrow_left:](../9365/index.md) [:arrow_right:](../9367/index.md)

**start**: 	March 16, 2024 02:02:37 AM UTC

**end**: 		March 16, 2024 03:53:01 AM UTC

**duration**: 0 days 01:50:24.136500

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

- cycles for coupling in ther ramp



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

