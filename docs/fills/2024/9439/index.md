# FILL 9439

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9439.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_03_28_22_20_09_F9439.yaml) [:arrow_left:](../9438/index.md) [:arrow_right:](../9440/index.md)

**start**: 	March 28, 2024 10:20:09 PM UTC

**end**: 		March 29, 2024 01:09:11 AM UTC

**duration**: 0 days 02:49:01.784750

**tags**:	 

- BEAM_COMMISSIONING



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

