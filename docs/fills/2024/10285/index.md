# FILL 10285

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/10285.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_10_26_06_22_36_F10285.yaml) [:arrow_left:](../10284/index.md) [:arrow_right:](../10286/index.md)

**start**: 	October 26, 2024 06:22:36 AM UTC

**end**: 		October 26, 2024 05:29:00 PM UTC

**duration**: 0 days 11:06:23.388625

**tags**:	 

- BEAM_COMMISSIONING

- ION_OPTICS



**comments**:	 

- Loss maps.



#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

