# FILL 9841

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9841.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_06_29_20_19_22_F9841.yaml) [:arrow_left:](../9840/index.md) [:arrow_right:](../9842/index.md)

**start**: 	June 29, 2024 08:19:22 PM UTC

**end**: 		June 30, 2024 12:32:12 AM UTC

**duration**: 0 days 04:12:49.938125

**tags**:	 

- DUCK



**comments**:	 

#### Intensity


#### Emittance


#### Luminosity


#### Cross-sections


##### ADJUST

##### STABLE BEAM

