# FILL 9474

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9474.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/2024_04_05_22_10_55_F9474.yaml) [:arrow_left:](../9473/index.md) [:arrow_right:](../9475/index.md)

**start**: 	April 05, 2024 10:10:55 PM UTC

**end**: 		April 06, 2024 05:39:22 AM UTC

**duration**: 0 days 07:28:27.031625

**tags**:	 

- INTENSITY_RAMPUP



**comments**:	 

- 4 signle bunches

-  OP dump




#### Cycle duration

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Cycle_duration.png)


#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Fill_chromaticity.png)

#### Intensity


##### Cycle intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Cycle_intensity.png)

##### BBB SB intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/bbb_SB_intensity.png)

##### Violin BBB intensity

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Cycle_violin_intensity_bl.png)

##### SB bunch intensity 

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Fill_adjust_stable_losses_summary.png)

#### Bunch Length


![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Cycle_bunchlength.png)

##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Fill_bunch_length.png)

#### Emittance


##### B1 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Cycle_emit_b1.png)

##### B2 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Cycle_emit_b2.png)

##### Emittance growth at injection

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Fill_injection_emit_blowup.png)

##### Summary violin plot

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Cycle_violin_emit.png)

##### BBB in SB for B1 

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/bbb_SB_B1_emit.png)

##### BBB in SB for B2 

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/bbb_SB_B2_emit.png)

#### Bunch brightness


![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Cycle_brightness.png)

#### Beam properties summary 

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Cycle_summary.png)

#### Luminosity


##### Overview

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Fill_luminosity.png)

##### BBB luminosity in SB

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/bbb_SB_luminosity.png)

#### Cross-sections


##### ADJUST

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Xsection_ADJUST_heatmap.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Xsection_ADJUST_mean.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Xsection_ADJUST_bbb_ip15lr.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Xsection_ADJUST_bbb_ip2ho.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Xsection_ADJUST_bbb_lhcbho.png)

##### STABLE BEAM

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Xsection_STABLE_heatmap_DBLM.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Xsection_SB_mean_DBLM.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Xsection_SB_mean_DBLM_separation.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Xsection_SB_bbb_DBLM_ip15lr.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Xsection_SB_bbb_DBLM_ip2ho.png)

![](https://dltwo.web.cern.ch/followup_plots/2024/9474/Xsection_SB_bbb_DBLM_lhcbho.png)

