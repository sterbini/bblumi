# FILL 9321

 
### Summary


[:notebook:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/elog_follow_up_md/9321.md) [:writing_hand:](https://gitlab.cern.ch/lhclumi/fill-tagger/-/blob/master/weekly_follow_up/) [:arrow_left:](../9320/index.md) [:arrow_right:](../9322/index.md)

**start**: 	1698621371466488525

**end**: 		1698651587768363525

**duration**: 30216301875000


#### Cycle duration

![](https://dltwo.web.cern.ch/followup_plots/2023/9321/Cycle_duration.png)


#### Beta-functions at the IPs

![](https://dltwo.web.cern.ch/followup_plots/2023/9321/Fill_betastar.png)

#### Crossing angles


![](https://dltwo.web.cern.ch/followup_plots/2023/9321/Fill_xing.png)

#### Octupoles


![](https://dltwo.web.cern.ch/followup_plots/2023/9321/Fill_octupoles.png)

#### Trims


##### Tune trims

![](https://dltwo.web.cern.ch/followup_plots/2023/9321/Fill_trims.png)

##### Chromaticity Trims

![](https://dltwo.web.cern.ch/followup_plots/2023/9321/Fill_chromaticity.png)

#### Intensity


##### Cycle intensity

![](https://dltwo.web.cern.ch/followup_plots/2023/9321/Cycle_intensity.png)

##### Violin BBB intensity

![](https://dltwo.web.cern.ch/followup_plots/2023/9321/Cycle_violin_intensity_bl.png)

##### Bunch Length Fill


![](https://dltwo.web.cern.ch/followup_plots/2023/9321/Fill_bunch_length.png)

#### Emittance


##### B1 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2023/9321/Cycle_emit_b1.png)

##### B2 along the cycle

![](https://dltwo.web.cern.ch/followup_plots/2023/9321/Cycle_emit_b2.png)

##### Summary violin plot

![](https://dltwo.web.cern.ch/followup_plots/2023/9321/Cycle_violin_emit.png)

#### Bunch brightness


![](https://dltwo.web.cern.ch/followup_plots/2023/9321/Cycle_brightness.png)

#### Beam properties summary 

![](https://dltwo.web.cern.ch/followup_plots/2023/9321/Cycle_summary.png)

#### Luminosity


##### Overview

![](https://dltwo.web.cern.ch/followup_plots/2023/9321/Fill_luminosity.png)

#### Cross-sections


##### ADJUST

![](https://dltwo.web.cern.ch/followup_plots/2023/9321/Xsection_ADJUST_heatmap.png)

![](https://dltwo.web.cern.ch/followup_plots/2023/9321/Xsection_ADJUST_mean.png)

![](https://dltwo.web.cern.ch/followup_plots/2023/9321/Xsection_ADJUST_bbb_ip15lr.png)

![](https://dltwo.web.cern.ch/followup_plots/2023/9321/Xsection_ADJUST_bbb_ip2ho.png)

![](https://dltwo.web.cern.ch/followup_plots/2023/9321/Xsection_ADJUST_bbb_lhcbho.png)

##### STABLE BEAM

