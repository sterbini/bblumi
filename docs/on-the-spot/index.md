### Technical readings during COVID-19 confinement
I found very interesting the O'Reilly collection offered by CERN:
[https://learning.oreilly.com/home/](https://learning.oreilly.com/home/){target=_blank}.


### A New Path to the Fundamental Theory of Physics?

Stephen Wolfran just released this article on his website, claiming he might have found a theory that would unify all the laws of physics. The article is available [here](https://writings.stephenwolfram.com/2020/04/finally-we-may-have-a-path-to-the-fundamental-theory-of-physics-and-its-beautiful/?fbclid=IwAR0zxVIpJgEn5Fvy2_gsRpRmgiRmCwN1VqqL-dFc948zLB8dwYhb6px-VWU){target=_blank}.

### Accounting for the Higgs

"I’ll let you in on a little secret: 
Even though physicists have produced millions of Higgs bosons at the Large Hadron Collider, they’ve never actually seen one."
More on [https://www.symmetrymagazine.org ](https://www.symmetrymagazine.org/article/accounting-for-the-higgs){target=_blank}

---

### Unexpected Basic Maths Discovery from Neutrinos

Three physicists were studying neutrinos changes while they discover a new relation between eigenvectors and eigenvalues. The article about their story can be found [here](https://www.quantamagazine.org/neutrinos-lead-to-unexpected-discovery-in-basic-math-20191113/?fbclid=IwAR3Xg_1z51e4oLydHwYlk5IUD3Yr_tW-BCEQHRbbGlQeYO2BC07DoyDQ60Q){target=_blank} and the resulting publication can be found [here](https://arxiv.org/pdf/1908.03795.pdf){target=_blank}

---

### A Call for Courage as Physicists Confront Collider Dilemma
Carlo Rubbia, leader of the bold collider experiment that in 1983 discovered the W and Z bosons, 
thinks particle physicists should now smash muons together in an innovative “Higgs factory.”
Read [here](https://www.quantamagazine.org/carlo-rubbia-calls-for-courage-as-physicists-confront-collider-dilemma-20190807/){target=_blank}

---

### Briefing book for 2020 update of European Strategy for Particle Physics
The newly published book distils inputs from Europe’s particle physics community ([link](https://home.cern/news/news/cern/briefing-book-2020-update-european-strategy-particle-physics?utm_source=Bulletin&utm_medium=Email&utm_content=2019-10-03E&utm_campaign=BulletinEmail){target=_blank}).
Have a look to [here](http://cds.cern.ch/record/2691414/files/Briefing_Book_Final.pdf){target=_blank}, in particular to Chapter 10 about Accelerator Science and Technology.

---

### Following the HL-LHC Satellite Meeting about the Wire Compensation

Not in Chicago for the HL-LHC Annual Meeting? You can still participate to the Satellite meeting organized in Fermi Lab on the **17th of October 2019**. From 15.30 to 23.30, the meeting will be accessible remotely from room 6/2-008. Click [here](https://indico.cern.ch/event/844153/overview){target=_blank} for the Indico page of the event. 

Contact the BB&Lumi Team for further information. 

---

### The Compact Linear Collider on Nature

Recent general public [article](https://www.nature.com/articles/s42254-019-0051-5){target=_blank} on Nature from S. Stapnes.

Nature Reviews Physics, volume 1, pages 235–237 (2019) 

---

### ILC news: Not what we hoped...

At [http://www.linearcollider.org](http://www.linearcollider.org/content/decision-international-linear-collider-%E2%80%9Cnot-what-we-had-hoped-progress-nevertheless%E2%80%9D){target=_blank} L. Evans shares with us his perspective.

---