### References

#### Beam-beam effects

From the corresponding [*Zotero* group library](https://www.zotero.org/groups/dl2).

- [**'Beam-beam effects in the LHC'**](https://cds.cern.ch/record/272882), *W. Herr*, 1995.
- [**'The Moeller luminosity factor'**](https://www.osti.gov/biblio/836235), *M. A. Furman*, 2003-08-04.
- [**'Concept of Luminosity'**](https://cas.web.cern.ch/sites/default/files/lectures/trieste-2005/herr-luminosity.pdf), *W. Herr*, 2005.
- [**'Concept of Luminosity'**](https://cds.cern.ch/record/941318), *W. Herr and B Muratori*, 2006.
- [**'Determination of the Absolute Luminosity at the LHC'**](https://cds.cern.ch/record/1308187), *S. M. White*, 2010.
- [**'Concept of Luminosity in particle colliders'**](https://cas.web.cern.ch/sites/default/files/lectures/varna-2010/herr-1-web.pdf), *W. Herr*, 2010.
- [**'Luminosity and luminous region shape for pure Gaussian bunches'**](https://cds.cern.ch/record/1481670), *M Ferro-Luzzi*, 2012.
- [**'Luminosity and Luminous Region Calculations for Different LHC Levelling Scenarios'**](https://indico.cern.ch/event/336296/contributions/1730439/attachments/657609/904100/Luminosity_and_Luminous_Region_Calculations_for_Different_LHC_Levelling_Scenarios.pdf), *G. R. Coombs*, 2014.
- [**'Pile up management at the high-luminosity LHC and introduction to the crab-kissing concept'**](https://link.aps.org/doi/10.1103/PhysRevSTAB.17.111001), *S. Fartoukh*, 2014-11-6.
- [**'Luminosity determination at proton colliders'**](https://www.sciencedirect.com/science/article/pii/S0146641014000878), *P. Grafström and W. Kozanecki*, 2015-03-01.

#### DA studies

From the corresponding [*Zotero* group library](https://www.zotero.org/groups/da_studies).

- [**'Dynamic aperture estimates and phase-space distortions in nonlinear betatron motion'**](https://link.aps.org/doi/10.1103/PhysRevE.53.4067), *E. Todesco and M. Giovannozzi*, 1996-4-1.
- [**'Dynamic aperture extrapolation in the presence of tune modulation'**](https://link.aps.org/doi/10.1103/PhysRevE.57.3432), *M. Giovannozzi, W. Scandale and E. Todesco*, 1998-3-1.
- [**'Progresses in the studies of adiabatic splitting of charged particle beams by crossing nonlinear resonances'**](https://link.aps.org/doi/10.1103/PhysRevSTAB.12.014001), *A. Franchi, S. Gilardoni and M. Giovannozzi*, 2009-1-20.
- [**'Proposed scaling law for intensity evolution in hadron storage rings based on dynamic aperture variation with time'**](https://link.aps.org/doi/10.1103/PhysRevSTAB.15.024001), *M. Giovannozzi*, 2012-2-2.
- [**'Multiparametric response of the LHC Dynamic Aperture in presence of beam-beam effects'**](https://dx.doi.org/10.1088/1742-6596/874/1/012006), *D. Pellegrini et al.*, 2017-07.
- [**'Innovative method to measure the extent of the stable phase-space region of proton synchrotrons'**](https://link.aps.org/doi/10.1103/PhysRevAccelBeams.22.034002), *E. H. Maclean, M. Giovannozzi and R. B. Appleby*, 2019-3-12.

#### Beam-Beam wire compensation

From the corresponding [*Zotero* group library](https://www.zotero.org/groups/bbwc).

- [**'Considerations on compensation of beam-beam effects in the Tevatron with electron beams'**](https://link.aps.org/doi/10.1103/PhysRevSTAB.2.071001), *V. Shiltsev et al.*, 1999-7-28.
- [**'Weak-strong beam-beam simulations for the Large Hadron Collider'**](https://link.aps.org/doi/10.1103/PhysRevSTAB.2.104001), *Y. Papaphilippou and F. Zimmermann*, 1999-10-08.
- [**'Principle of a correction of the long-range beam-beam effect in LHC using electromagnetic lenses'**](https://cds.cern.ch/record/692058), *J.-P. Koutchouk*, 2000.
- [**'Experiments on LHC Long-Range Beam-Beam Compensation and Crossing Schemes at the CERN SPS in 2004'**](https://cds.cern.ch/record/872262), *F. Zimmermann et al.*, 2005.
- [**'DAFNE Lifetime Optimization with Octupoles and Compensating Wires'**](https://cds.cern.ch/record/1093718), *C. Milardi et al.*, 2008.
- [**'Long-range beam-beam experiments in the Relativistic Heavy Ion Collider'**](https://link.aps.org/doi/10.1103/PhysRevSTAB.14.091001), *R. Calaga et al.*, 2011-09-23.
- [**'Simulations and Measurements of Long Range Beam-Beam Effects in the LHC'**](https://indico.cern.ch/event/456856/), 2015-11-30.
- [**'Compensation of the long-range beam-beam interactions as a path towards new configurations for the high luminosity LHC'**](https://link.aps.org/doi/10.1103/PhysRevSTAB.18.121001), *S. Fartoukh et al.*, 2015-12-01.
- [**'Second Workshop on Wire Experiment for Long Range Beam-Beam Compensation'**](https://indico.cern.ch/event/615088/), 2017-03-20.
- [**'BBWC Space Reservation Request'**](https://edms.cern.ch/document/2037987/1.0), *Adriana Rossi*, 2018.
- [**'First results of the compensation of the beam-beam effect with DC wires in the LHC'**](https://cds.cern.ch/record/2693922), *G. Sterbini et al.*, 2019.
- [**'WP2/WP13 HL-LHC Satellite Meeting, Fermilab 2019 - BBWC'**](https://indico.cern.ch/event/844153/), 2019-10-17.
- [**'Numerical optimization of dc wire parameters for mitigation of the long range beam-beam interactions in High Luminosity Large Hadron Collider'**](https://link.aps.org/doi/10.1103/PhysRevAccelBeams.24.074001), *K. Skoufaris et al.*, 2021-07-22.
- [**'WP2/WP13 HL-LHC Satellite Meeting, Uppsala 2022 - BBWC'**](https://indico.cern.ch/event/1168738/), 2022-09-23.
- [**'First Experimental Evidence of a Beam-Beam Long-Range Compensation Using Wires in the Large Hadron Collider'**](http://arxiv.org/abs/2203.08066), *A. Poyet et al.*, 2023-04-19.
- [**'Beam-beam long range compensator mechanical demonstrator'**](https://jacow.org/ipac2023/doi/jacow-ipac2023-thpm015), *Garlasché, M. et al.*, 2023-09-23.

#### Crab cavities

From the corresponding [*Zotero* group library](https://www.zotero.org/groups/crab_cavities).

- [**'Transverse emittance growth due to rf noise in the high-luminosity LHC crab cavities'**](https://journals.aps.org/prab/pdf/10.1103/PhysRevSTAB.18.101001), *P. Baudrenghien and T. Mastoridis*, 2015.
- [**'Successful Crabbing of Proton Beams'**](https://accelconf.web.cern.ch/ipac2021/papers/wexa01.pdf), *Rama Calaga*, 2021.
- [**'Studies of the emittance growth due to noise in the Crab Cavity RF systems'**](https://livrepository.liverpool.ac.uk/3168298/), *Natalia Triantafyllou*, 2023.
- [**'Instability in crab crossing caused by interaction between beam loading on crab cavities and beam-beam force on colliding beams'**](https://journals.aps.org/prab/pdf/10.1103/PhysRevAccelBeams.26.112001), *Kazunori Akai*, 2023.
- [**'Beam-beam interaction study in an electron-ion collider adopting the local crab crossing scheme'**](https://link.aps.org/doi/10.1103/PhysRevAccelBeams.26.101001), *Lei Wang and Jian Cheng Yang*, 2023-10-10.
- [**'Transverse emittance growth due to rf noise in crab cavities: Theory, measurements, cure, and high luminosity LHC estimates'**](https://journals.aps.org/prab/pdf/10.1103/PhysRevAccelBeams.27.051001), *P. Baudrenghien and T. Mastoridis*, 2024.
- [**'Mitigation strategies for the instabilities induced by the fundamental mode of the HL-LHC Crab Cavities'**](https://iopscience.iop.org/article/10.1088/1748-0221/19/05/P05046/pdf), *L. Giacomel et al.*, 2024.
- [**'Impact of beam coupling impedance on crab cavity noise induced emittance growth'**](https://link.aps.org/doi/10.1103/PhysRevAccelBeams.27.071001), *N. Triantafyllou et al.*, 2024-7-8.
