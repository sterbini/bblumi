# Noise source in LHC and HL-LHC

Noise sources in an accelerator are detrimental for the beam performance and induce coherent
and incoherent effects.
A long term scrutiny for the LHC and HL-LHC noise sources has been lauched as a
[WP2 activities](https://hilumilhc.web.cern.ch/wp/wp2-accelerator-physics-performance).

In the following page we try 

1. to define a common framework for the different typology of noise
1. to collect information about the known source of noise and their present understanding
1. normalize the information in a common format (e.g., a table) 

The goal is to serve mainly the HL-LHC WP2 community as suggested in the [WP2 noise action list](https://espace.cern.ch/HiLumi/WP2/_layouts/15/WopiFrame.aspx?sourcedoc=/HiLumi/WP2/Lists/Tasks/Attachments/231/Noise_andEmittanceBlow-up_29082019_uptodate.docx&action=default). 

The electro-magnetic devices in an accelerator produce pre-defined electro-magnetic fields.
The difference between the ideal field and the real one can be considered, in very general term, as a noise.
Issue concerning calibration, reproducibility, slow/fast drifts, jitter, vibration, ground motions,
are different ways to represent the noise in specific frequency windows.

We can develop a generic noise source using RF magnetic thin multipoles and observe their impact on the beam. 

The information we need to perform such a  study is the noise spectrum of the equivalent magnetic field seen by the beam.
This information is in general not directly available, but can be reconstructed by hypotheses or educated guesses.

The latters are the main topic of this web page.
    
!!! Question
    In the "RF" multipoles, is the electric field component neglected?

### Power supplies

In this section we consider the power supplies (PS) of the magnets of the machine.

In fact, not having a viable possibility to monitor the magnetic field along the beam path, we can follow two approaches:

1. From the PS to the beam: characterize, the PS noise spectrum, the transfer function (TF) between the PS noise spectrum and the actual EM field seen by the beam. 
The TF takes into account 
* the trasmission line model of the PS load (in general one PS power a series of magnets) and
* presence of inductive screening current generated by the PS on the magnet and on the vacuum chamber (screening effect).

2. From the beam to the PS: measure the coherent spectrum of the beam and infer the kick distributions in the machine.

The first meaasure require a systematic measurement and modelization campaign.
!!! Question 
	Do we have a clear measurement of PS noise?

This second measure can be done online during the fill but, clearly, cannot isolate the contribution of the single PS's nor their relative phases.


!!! warning
	At wavelenght comparable with the length of the magnet, RF multiples different from the fundamental one can be present.
	In the following we consider only the fundamental multipoles (and their feed-down terms).

By knowing amplitudes and relative phases of the EM noise seen by the beam one can represent it as a series of equivalent RF multipoles.

The power supplies in the machine can be classified following this criteria

* regulation typology: [switching mode](https://en.wikipedia.org/wiki/Switched-mode_power_supply) (SM), [Silicon Controlled Rectifier](https://en.wikipedia.org/wiki/Silicon_controlled_rectifier) (SCR),...
* type of magnet: dipole, quadrupole, solenoid...
* type of cycling: ramping with the beam energy (e.g., main dipoles/quadrupoles), DC (e.g., experimental magnets),...

!!! Question
	Can we classify the LHC PS with respect to the previous topology? Perhaps we can use CALS and a ipynb?

!!! Warning Unanswered Questions
	* Do we understand the beam spectra evolution during the ramp?
	* Do we understand why we see a cluster of 8 kHz in the beam spectrum?
	* ...

**Relevant presentations**

*S. Kostouglou*, [Noise observations in the LHC and projections for HL-LHC](https://indico.cern.ch/event/779650/contributions/3244747/attachments/1770859/2877607/TCC_noise_131218.pdf),
[63rd HL-TCC](https://indico.cern.ch/event/779650/) 

**Bibliography**

We shared a more complete [Mendeley collection](https://www.mendeley.com/community/noise-studies-in-accelerators/documents/)
of papers and presentations on noise in accelerators.



### Transverse feedback

### Electron lens
electron current noise, modulation and corresponding kicks


### Beam screen vibration


### Crab cavities


### Summary 
| Source   |      noise spectrum      |  test |
|----------|:-------------:|------:|
| Power supplies |  left-aligned | test |
| Transverse damper |    centered   |   test |
| Crab cavities | right-aligned |    test |
