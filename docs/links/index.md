### DL2 Meetings
On Indico: <a href="https://indico.cern.ch/category/9172/" target="_blank">https://indico.cern.ch/category/9172/</a>

### LHC Morning Meetings
On Indico: <a href="https://indico.cern.ch/category/6386/" target="_blank">https://indico.cern.ch/category/6386/</a>

### Beam Performance Tracking for LHC
On Indico: <a href="https://bpt.web.cern.ch/lhc/" target="_blank">https://bpt.web.cern.ch/lhc/</a>

### LHC Programme Coordination
Website of the LHC programme coordination <a href="https://lpc.web.cern.ch" target="_blank">here</a>

### BE-ABP-INC Section Meetings 
On Indico: <a href="https://indico.cern.ch/category/13382/" target="_blank">https://indico.cern.ch/category/13382/</a>


<!-- ### Clarification of general MAD-X concepts

On this [page](madx_concepts/madx_concepts.md), we try to describe the different concepts used in MAD-X to describe the beams, the optics, the tracking... In different situations.  -->


### LHC Machine Committee Meeting (LMC)

<a href="https://indico.cern.ch/category/5480/" target="_blank">indico.cern.ch/category/5480/</a>

### LHC Injectors and Experimental Facilities Committee (IEFC)

On Indico: [https://indico.cern.ch/category/4244/](https://indico.cern.ch/category/4244/){target=_blank}

### Facilities Operation Meeting (FOM)

On Indico: [https://indico.cern.ch/category/6175/](https://indico.cern.ch/category/6175/){target=_blank}

### Lumi days

On Indico: 

* <a href="https://indico.cern.ch/event/813285/" target="_blank">LumiDays 2019</a>
* <a href="https://indico.cern.ch/event/162948/" target="_blank">LumiDays 2012</a>
* <a href="https://indico.cern.ch/event/109784/" target="_blank">LumiDays 2011</a>


### ABP Information meeting
On Indico: <a href="https://indico.cern.ch/category/5694/" target="_blank">indico.cern.ch/category/5694/</a>

### BE department web-page

[https://beams.web.cern.ch/](https://beams.web.cern.ch/){target=_blank}

### EPFL and machine learning meeting

On Indico: <a href="https://indico.cern.ch/category/16212/" target="_blank">https://indico.cern.ch/category/16212//</a>


### Mask for LHC and HL-LHC

On this [https://lhcmaskdoc.web.cern.ch](https://lhcmaskdoc.web.cern.ch){target=_blank}, you have the LHC and HL-LHC mask documentation. 

### Noise studies on LHC and HL-LHC
In [this repository](https://noisestudies.web.cern.ch) we collect the table of the possible known noise source for LHC and HL-LHC.

<!-- ### Luminosity calibration
Section on luminosity calibration on the LHC programme coordination website <a href="https://lpc.web.cern.ch/lumicalib.htm" target="_blank">here</a> -->

### Accelerator Schools and MOOCs
<a href="https://cas.web.cern.ch/" target="_blank">CERN Accelerator schools</a> 

<a href="https://www.esi-archamps.eu/juas-presentation/" target="_blank">Joint Universities Accelerator schools</a> 

<a href="https://uspas.fnal.gov/programs/next-program/index.shtml" target="_blank">US Particle Accelerator School</a>

<!-- <a href="https://npap.eu/npas/" target="_blank">Nordic Particle Accelerator Project</a> -->


!!! info "For the CAS Introduction to Accelerator Physics (Budapest, 2016) videos are available on <a href="https://indico.cern.ch/event/532397/timetable/" target="_blank">INDICO</a>."

!!! info "MOOCs on [Particle Accelerators](http://mooc.particle-accelerators.eu){target="_blank"} and the [Theoretical Minimum](https://theoreticalminimum.com/){target="_blank"}."

### ICFA Beam Dynamics Panel and Newsletters
Website of the <a href="http://www.icfa-bd.org/" target="_blank">ICFA</a>

<!-- ### Scripting tools
This is a collections of scripting tools in the BE-CO <a href="https://wikis.cern.ch/display/ST/Scripting+Tools+Home" target="_blank">wiki page</a> (accessible from the GPN and TN) -->

### High Energy Magazines
<a href="https://cerncourier.com" target="_blank">CERN Courier</a>

<a href="https://www.symmetrymagazine.org" target="_blank">Symmetry Magazine</a>


### Jobs Opportunities
<a href="https://cerncourier.com/jobs/" target="_blank">CERN Courier Jobs</a>

<!-- <a href="http://www.esi-archamps.eu/Thematic-Schools/Discover-JUAS/Career-opportunities" target="_blank">ESI and JUAS </a> regularly receive and post information on career opportunities in the field of particle accelerators.
</a> -->

### Beam-beam and luminosity meetings (old site)
On Indico: <a href="https://indico.cern.ch/category/4926/" target="_blank">indico.cern.ch/category/4926/</a>
This was maintained until November 2020.

### Luminosity follow-up meetings (old site)
On Indico: <a href="https://indico.cern.ch/event/834437/" target="_blank">indico.cern.ch/event/834437/</a>
This was maintained until November 2020.

### HSI Section meeting (old site)
On Indico: <a href="https://indico.cern.ch/category/9171/" target="_blank">indico.cern.ch/category/9171/</a>
This was maintained until December 2020.
