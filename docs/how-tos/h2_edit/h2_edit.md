# Editing the website : some guidelines

In this section we report some useful guidelines that one should follow whil editing the website, in order to maintain its consistency. 

??? warning
    In general, sections are following the policy "last on top". Nevertheless, we would like to keep the section "How to edit the website" always on top in order to keep it very visible for the new users of the website. 

### Where should I add my information?

....Well, it depends on what kind of information of course! The main purpose of this website is to propose a collections of instructions/tutorials, called on this site 'How-Tos'. If you want share a way of using a tool, the 'How-Tos' category is made for you! If your goal is to share a general/external tool, or a tip for the already presented utilities, then use the 'Utilities and Tricks' category. Now if you are presenting a *home-made* tool, like a python package you hosted in Git, the way to go is to put the link to the repository in the 'Tools' category, and to provide a tutorial in the 'How-Tos' category. Finally, if you are sharing a general purposed article, the 'On-the-spot' section is the place to be used. 

### Make the *search* more efficient

For a (still) unknown reason, the search tool does not work for high level titles in markdown. For that reason, we are asking you, when creating a new paragraph on an existing page to use the triple hashtags '###' to make your title. That way your subsection will be accessible with the search tool. 

### Links to other pages 

If you create a new page for convenience, the way to make a link to it, is to create a directory in the proper section (let's call it *myDirectory*). The markdown file (let's say *myFile.md*) corresponding to the page you want to create should therefore be located in that directory. When writing on the main page of the section (generally, *index.md*), you can make a link to your page following this example:

```markdown
You can find more information by clicking [here](myDirectory/myFile.md).
```

On the other hand, if you want to add an **external** link to another website or article, the policy of our platform is to force the opening of a new tab. This is typically done by using the mention "target = _blank" after the link. An example: 

```markdown
You can find the best python package by clicking [here](https://pandas.pydata.org/){target=_blank}.
```

This results in a link that will open a new tab if you click on it. 


### Manage *SWAN* and notebooks incorporation in the website

![png](swan_logo_letters.png)

It is quite useful in order to illustrate your How-Tos to add a notebook example, typically using the [SWAN](https://swan.web.cern.ch/){taget=_blank} service at CERN. 

When you finished writing your beautiful notebook example on SWAN, an easy option is to export it as a Markdown file (File>Download As>markdown(.md)). This will download a markdown file that you can then import on the GitLab of the website. 

In addition is a good practice to import directly the notebook file (.ipynb extension) and to create a link to it. By clicking [here](https://cern.ch/swanserver/cgi-bin/go/?projurl=https://gitlab.cern.ch/sterbini/bblumi/raw/master/docs/how-tos/ElogScraping/ElogScraping.ipynb){target=_blank} you will typically access one of the notebook of the site. Here's the link to be used as a template: 

```markdown
By clicking [here](https://cern.ch/swanserver/cgi-bin/go/?projurl=https://gitlab.cern.ch/sterbini/bblumi/raw/master/docs/how-tos/ElogScraping/ElogScraping.ipynb){target=_blank} you will typically access one of the notebook of the site.
```


