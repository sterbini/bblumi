
# Code for Elog scraping
I am using the web site of the elogook to convert the data in a pandas dataframe.


```python
# To get the stack you are using
!which python
```

    /cvmfs/sft.cern.ch/lcg/views/LCG_95apython3/x86_64-centos7-gcc7-opt/bin/python



```python
# To get your path on EOS of your notebook
pwd
```




    '/eos/user/s/sterbini/MD_ANALYSIS/bblumi/docs/how-tos/ElogScraping'




```python
import re
import requests
import pandas as pd
from bs4 import BeautifulSoup

def getSoup(date='20180612', shift=1, elog=60):
    ''' This method will convert a specific shift of the elogbook in BeautifulSoup object
    - date is in yyyymmdd format
    - shift is an integer (1= 'morning', 2='afternoon', 3='night')
    - elog is the elog code (e.g., 60='LHC_OP')
    '''
    address=f'http://elogbook.cern.ch/eLogbook/eLogbook.jsp?lgbk={elog}&date={date}&shift={shift}'
    website_url = requests.get(address).text
    soup = BeautifulSoup(website_url,'lxml') 
    return {'soup':soup,'elog':elog,'date':date,'shift':shift}

def getDictionary(soup):
    ''' This method will convert a BeautifulSoup object from elogbook to a pandas DF
    - soup is the dictionary coming from the getSoup method
    '''
    # Table
    myTable = soup['soup'].find('table',{'id':'events_table'})

    # Table header and column description
    tableHead = myTable.find('thead')
    tableDescription=tableHead.find_all('b')
    fieldNumber=len(tableDescription)
    myFields=[]
    for i in tableDescription:
        myFields.append(i.getText())

    # Table body    
    tableBody = myTable.find('tbody',{'id':'body_events'})

    # From the table body to the events
    myEvents=list(tableBody.children)
    myEvents=myEvents[1::2]
    myEventsShort=[]
    dictionaryList=[]
    for i in myEvents:
        myEvent= list(i.children)
        myEvent=myEvent[1::2]
        myDictionary={}
        for j,z in zip(myEvent,myFields):
            myDictionary[z]=list(j.children)[1].getText()
        myLink=myEvent[1].find_all('a', href=True)
        myDictionary['link']='http://elogbook.cern.ch/eLogbook/'+myLink[0]['href']
        dictionaryList.append(myDictionary)

    myDF=pd.DataFrame(dictionaryList)[myFields+['link']]
    myDF['Time']=myDF['Time'].apply(lambda x:re.sub('[^A-Za-z0-9:./;\-,_]+', '', x))
    myDF['Time']=soup['date'] + ' ' + myDF['Time']
    myDF['Time']=myDF['Time'].apply(lambda x : pd.Timestamp(x))
    myDF=myDF.set_index('Time')
    # this is useful for the change of the day
    if soup['shift']==3:
        aux1= myDF.between_time('21:00','00:00')
        aux2=myDF.between_time('00:00','07:00')
        aux2.index=aux2.index+pd.Timedelta('1d')
        myDF=pd.concat([aux1,aux2])
    myDF.index=myDF.index.tz_localize('CET').tz_convert('UTC')
    myDF.index.name=None
    return myDF
```

# An example


```python
# first I get the soup (this is a bit slow, but is comparable to the loading time of a elog page)
soup=getSoup(date='20180702', shift=3, elog=60)
```


```python
# after I cast it in a pandas DF
aux=getDictionary(soup)
aux
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>#</th>
      <th>PROTONPHY</th>
      <th>Comment</th>
      <th>link</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2018-07-02 21:00:00+00:00</th>
      <td>1</td>
      <td>\n\n\n\nNB\n\n\n\n</td>
      <td>\n\n\n\n\nGuy and Michaela \n\n\n\ncreated by ...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-02 21:52:00+00:00</th>
      <td>2</td>
      <td>\n\n\n\nNB\n\n\n\n</td>
      <td>\n\n\n\n\nStart precycle of EIS and RD1.LR5 \n...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-02 21:52:00+00:00</th>
      <td>3</td>
      <td>\n\n\n\n 1 \n\n\n\n</td>
      <td>\n\n\n\n\nRestore RSS.A56B1, which had tripped...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-02 22:08:00+00:00</th>
      <td>4</td>
      <td>\n\n\n\nSUP\n\n\n\n</td>
      <td>\n\n\n\n\n\n\n\nBEAM MODE &gt; SETUP  \n\n\n\n\n\...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-02 22:15:00+00:00</th>
      <td>5</td>
      <td>\n\n\n\nSUP\n\n\n\n</td>
      <td>\n\n\n\n\nPrecycle of RD1.LR5 complete \n\n\n\...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-02 22:19:00+00:00</th>
      <td>6</td>
      <td>\n\n\n\nSUP\n\n\n\n</td>
      <td>\n\n\n\n\nLHC SEQ: QPS configuration cross che...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-02 22:30:00+00:00</th>
      <td>7</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\n\n\n\nBEAM MODE &gt; INJECTION PROBE BE...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-02 22:31:00+00:00</th>
      <td>8</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nDry dump, XPOC interlock B2, MKD ris...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-02 22:38:00+00:00</th>
      <td>9</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nRD1 replaced. SIS needs to be update...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-02 22:39:00+00:00</th>
      <td>10</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nAnother dry dump to test the XPOC is...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-02 22:39:00+00:00</th>
      <td>11</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\n|*** XPOC error has been reset by us...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-02 22:49:00+00:00</th>
      <td>12</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nRamping the RD1.LR5 to nominal curre...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-02 23:16:00+00:00</th>
      <td>13</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nSent FGC.FAULTS -&gt; FGC_STATE to RD1 ...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-02 23:20:00+00:00</th>
      <td>14</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nRD1 tripped when restetting and goin...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-02 23:25:00+00:00</th>
      <td>15</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nSending a few times the off command ...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-02 23:27:00+00:00</th>
      <td>16</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nLHC SEQ: injection handshake closed;...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-02 23:27:00+00:00</th>
      <td>17</td>
      <td>\n\n\n\nNB\n\n\n\n</td>
      <td>\n\n\n\n\n\n\n\nBEAM MODE &gt; NO BEAM  \n\n\n\n\...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-02 23:45:00+00:00</th>
      <td>18</td>
      <td>\n\n\n\nNB\n\n\n\n</td>
      <td>\n\n\n\n\nFGCM test analysis by Markus Zerlaut...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-02 23:54:00+00:00</th>
      <td>19</td>
      <td>\n\n\n\nNB\n\n\n\n</td>
      <td>\n\n\n\n\nPrecycle of RD1.LR5 complete, and se...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-02 23:55:00+00:00</th>
      <td>20</td>
      <td>\n\n\n\nSUP\n\n\n\n</td>
      <td>\n\n\n\n\n\n\n\nBEAM MODE &gt; SETUP  \n\n\n\n\n\...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-02 23:57:00+00:00</th>
      <td>21</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\n\n\n\nBEAM MODE &gt; INJECTION PROBE BE...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 00:03:00+00:00</th>
      <td>22</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nBad tune signal at injection, many l...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 00:07:00+00:00</th>
      <td>23</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nChroma far off ~20 in V and ~10 in H...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 00:12:00+00:00</th>
      <td>24</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\n\n\n\nBEAM MODE &gt; INJECTION PHYSICS ...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 00:23:00+00:00</th>
      <td>25</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nTL with 12b \n\n\n\n\n\n\n\n\n\n\n\n...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 00:25:00+00:00</th>
      <td>26</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nreset BSRT intensifier on both beams...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 00:29:00+00:00</th>
      <td>27</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nAverage emittance from Wirescanner  ...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 00:29:00+00:00</th>
      <td>28</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nIQC complains about bad scraping, bu...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 00:35:00+00:00</th>
      <td>29</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nCalc all optics task failed. \n\n\n\...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 00:40:00+00:00</th>
      <td>30</td>
      <td>\n\n\n\n 2 \n\n\n\n</td>
      <td>\n\n\n\n\nGlobal Post Mortem Event  Event Time...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>2018-07-03 02:14:00+00:00</th>
      <td>55</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nLHC SEQ: LOAD SPS FREQUENCY FOR PROT...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 02:14:00+00:00</th>
      <td>56</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nARNAUD ANDRE BESSONNAT(ABESSONN) ass...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 02:32:00+00:00</th>
      <td>57</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nEPC piquet called back. The PC is fi...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 02:35:00+00:00</th>
      <td>58</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nprecycle RD1.LR5 \n\n\n\ncreated by ...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 02:37:00+00:00</th>
      <td>59</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nLHC SEQ: injection handshake closed;...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 02:37:00+00:00</th>
      <td>60</td>
      <td>\n\n\n\nSUP\n\n\n\n</td>
      <td>\n\n\n\n\n\n\n\nBEAM MODE &gt; SETUP  \n\n\n\n\n\...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 02:41:00+00:00</th>
      <td>61</td>
      <td>\n\n\n\nSUP\n\n\n\n</td>
      <td>\n\n\n\n\nLHC SEQ: RF LBDS frequency checks do...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 02:43:00+00:00</th>
      <td>62</td>
      <td>\n\n\n\nSUP\n\n\n\n</td>
      <td>\n\n\n\n\nLHC SEQ: resynchronize RF beam contr...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 02:49:00+00:00</th>
      <td>63</td>
      <td>\n\n\n\nSUP\n\n\n\n</td>
      <td>\n\n\n\n\nnew part of RF preparation sequence ...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 03:06:00+00:00</th>
      <td>64</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\n\n\n\nBEAM MODE &gt; INJECTION PROBE BE...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 03:11:00+00:00</th>
      <td>65</td>
      <td>\n\n\n\n 10 \n\n\n\n</td>
      <td>\n\n\n\n\nQPS not OK on  RQ9.L1 RQ4.R1 RQ9.R5 ...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 03:25:00+00:00</th>
      <td>66</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\n\n\n\nBEAM MODE &gt; INJECTION PHYSICS ...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 03:32:00+00:00</th>
      <td>67</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nTL with 12b \n\n\n\n\n\n\n\n\n\n\n\n...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 03:47:00+00:00</th>
      <td>68</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nLosses during injection    05:33:52 ...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 03:48:00+00:00</th>
      <td>69</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\n\n\n\nBEAM MODE &gt; PREPARE RAMP  \n\n...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 03:48:00+00:00</th>
      <td>70</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nLHC BEAM Process Time :The minimum t...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 03:48:00+00:00</th>
      <td>71</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nBSRT Emittance scan \n\n\n\n\n\n\n\n...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 03:49:00+00:00</th>
      <td>72</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\n'LHC Fast BCT v1.0.5' application ha...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 03:49:00+00:00</th>
      <td>73</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nLHC SEQ: INJ PROT COLLIMATORS ARE OU...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 03:51:00+00:00</th>
      <td>74</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\n\n\n\nBEAM MODE &gt; RAMP  \n\n\n\n\n\n...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 03:51:00+00:00</th>
      <td>75</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nLHC SEQ: ramp started \n\n\n\ncreate...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 04:12:00+00:00</th>
      <td>76</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\n\n\n\nBEAM MODE &gt; FLAT TOP  \n\n\n\n...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 04:13:00+00:00</th>
      <td>77</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nLHC SEQ: END OF QCHANGE \n\n\n\ncrea...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 04:14:00+00:00</th>
      <td>78</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\n\n\n\nBEAM MODE &gt; SQUEEZE  \n\n\n\n\...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 04:15:00+00:00</th>
      <td>79</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\n'LHC Beam Quality Monitor' applicati...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 04:16:00+00:00</th>
      <td>80</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nBSRT Emittance scan  B1H blew up dur...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 04:18:00+00:00</th>
      <td>81</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nstrong snapback \n\n\n\n\n\n\n\n\n\n...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 04:45:00+00:00</th>
      <td>82</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nEND OF SQUEEZE SEGMENT: Beta* = 75 m...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 04:56:00+00:00</th>
      <td>83</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\nBSRT Emittance scan \n\n\n\n\n\n\n\n...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
    <tr>
      <th>2018-07-03 04:56:00+00:00</th>
      <td>84</td>
      <td>\n\n\n\nBI\n\n\n\n</td>
      <td>\n\n\n\n\n\n\n\nINFO &gt; Shift Summary  \n\n\n\n...</td>
      <td>http://elogbook.cern.ch/eLogbook/event_viewer....</td>
    </tr>
  </tbody>
</table>
<p>84 rows × 4 columns</p>
</div>




```python
# Memory of usage in MB
aux.memory_usage().sum()/1024/1024
```




    0.003204345703125




```python
# Some naive filtering
aux.loc['2018-07-03 04:56:00+00:00']['link']
```




    2018-07-03 04:56:00+00:00    http://elogbook.cern.ch/eLogbook/event_viewer....
    2018-07-03 04:56:00+00:00    http://elogbook.cern.ch/eLogbook/event_viewer....
    Name: link, dtype: object




```python

```
