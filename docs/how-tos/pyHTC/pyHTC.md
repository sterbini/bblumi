# Submitting to HTCondor using pyHTC

[pyHTC](https://github.com/apoyet/pyHTC){target=_blank} is a python package allowing the user to talk to the cluster system HTCondor in order to submit jobs. Thanks to this tool, you will be able to prepare the submit file, necessary for every submission, and to store all the useful data of your study in a pandas DataFrame. 

### Prerequisites

At the moment, submission from EOS is not supported by HTCondor (see [here](http://batchdocs.web.cern.ch/batchdocs/troubleshooting/eos_submission.html){target=_blank}). Consequently, you should setup your environment in AFS. Once you have your working directory, everything you need can be downloaded from [this folder](https://github.com/apoyet/pyHTC/tree/master/example){target=_blank}. 

Once the folder downloaded, you are ready to go. The only file you will need to open, is the notebook, called "study_submission_example". You can visualize it [here](study_submission_example.md).

### The spirit

The idea is to go through the following steps: 

1. Define your study. This implies to give it a name, an executable, the absolute path of the working directory, as well as the names of the different directory to store input/output/error/log/etc..
2. Then, from the template file you want to run (here, a madx script), you can read what are the masked parameters, in order not to forget any of them :)
3. Define then the parameters of the scan and store everything in a dictionnary. 
4. The 'define_study' will create the combination of all the jobs, and create the input files names. 
5. Unmask the template and create the input files
6. Create the submission file
7. Submit to HTCondor ! :) 
8. Monitor the status of the job
9. Store all the useful information in a nice DataFrame :) 

### Check the outputs!

 You can know check the outputs when the jobs are done. There is not yet a function to re-submit automatically the jobs that failed, but it will come.... :) 

 Section under development... ;) 

