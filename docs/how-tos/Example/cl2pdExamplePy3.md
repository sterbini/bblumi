

```python
# Download the last version on 
# on /eos/user/s/sterbini/MD_ANALYSIS/cl2pdExamplePy3.ipynb
# See the details at https://github.com/sterbini/cl2pd
# download it from  SWAN terminal by
# pip install --user git+https://github.com/sterbini/cl2pd.git

import cl2pd
from cl2pd import importData
pd=importData.pd     #is the pandas package
cals=importData.cals #is the ldb variable of pytimber package
!pwd
```

    /eos/home-s/sterbini/MD_ANALYSIS


# From CALS to pandas DataFrame 
We use the precious package from R. De Maria et al. (**pytimber**, https://github.com/rdemaria/pytimber) to wrap its output in pandas DataFrames.

## cals2pd function
This is the most important function of the importData class.
You can use different timezone, in this example we use Central European Time (local time at CERN).


```python
variables=['LHC.BCTDC.A6R4.B1:BEAM_INTENSITY', 'LHC.BCTDC.A6R4.B2:BEAM_INTENSITY']
startTime = pd.Timestamp('2017-10-01 17:30', tz='CET')
endTime = pd.Timestamp('2017-10-01 17:31', tz='CET')
raw_data = importData.cals2pd(variables,startTime,endTime)
raw_data.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>LHC.BCTDC.A6R4.B1:BEAM_INTENSITY</th>
      <th>LHC.BCTDC.A6R4.B2:BEAM_INTENSITY</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2017-10-01 15:30:00+00:00</th>
      <td>1.470000e+09</td>
      <td>270000000.0</td>
    </tr>
    <tr>
      <th>2017-10-01 15:30:01+00:00</th>
      <td>1.810000e+09</td>
      <td>310000000.0</td>
    </tr>
    <tr>
      <th>2017-10-01 15:30:02+00:00</th>
      <td>1.690000e+09</td>
      <td>-310000000.0</td>
    </tr>
    <tr>
      <th>2017-10-01 15:30:03+00:00</th>
      <td>1.990000e+09</td>
      <td>-720000000.0</td>
    </tr>
    <tr>
      <th>2017-10-01 15:30:04+00:00</th>
      <td>1.270000e+09</td>
      <td>220000000.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
raw_data['2017-10-01 15:30:01+00:00':'2017-10-01 15:30:03+00:00']
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>LHC.BCTDC.A6R4.B1:BEAM_INTENSITY</th>
      <th>LHC.BCTDC.A6R4.B2:BEAM_INTENSITY</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2017-10-01 15:30:01+00:00</th>
      <td>1.810000e+09</td>
      <td>310000000.0</td>
    </tr>
    <tr>
      <th>2017-10-01 15:30:02+00:00</th>
      <td>1.690000e+09</td>
      <td>-310000000.0</td>
    </tr>
    <tr>
      <th>2017-10-01 15:30:03+00:00</th>
      <td>1.990000e+09</td>
      <td>-720000000.0</td>
    </tr>
  </tbody>
</table>
</div>



By default the index timezone is UTC but, even if not encouraged, you can chance the index time zone.


```python
raw_data.index=raw_data.index.tz_convert('CET') # CERN local time
raw_data.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>LHC.BCTDC.A6R4.B1:BEAM_INTENSITY</th>
      <th>LHC.BCTDC.A6R4.B2:BEAM_INTENSITY</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2017-10-01 17:30:00+02:00</th>
      <td>1.470000e+09</td>
      <td>270000000.0</td>
    </tr>
    <tr>
      <th>2017-10-01 17:30:01+02:00</th>
      <td>1.810000e+09</td>
      <td>310000000.0</td>
    </tr>
    <tr>
      <th>2017-10-01 17:30:02+02:00</th>
      <td>1.690000e+09</td>
      <td>-310000000.0</td>
    </tr>
    <tr>
      <th>2017-10-01 17:30:03+02:00</th>
      <td>1.990000e+09</td>
      <td>-720000000.0</td>
    </tr>
    <tr>
      <th>2017-10-01 17:30:04+02:00</th>
      <td>1.270000e+09</td>
      <td>220000000.0</td>
    </tr>
  </tbody>
</table>
</div>



You can split the query in different time windows


```python
raw_data = importData.cals2pd(variables,startTime,endTime,split=4,verbose=True)
```

    Time window: 1
    t1 is UTC localized: 2017-10-01 15:30:00+00:00
    t2 is UTC localized: 2017-10-01 15:30:15+00:00
    No fundamental filter.
    Elaborating variable: LHC.BCTDC.A6R4.B1:BEAM_INTENSITY
    Elaborating variable: LHC.BCTDC.A6R4.B2:BEAM_INTENSITY
    Time window: 2
    t1 is UTC localized: 2017-10-01 15:30:15+00:00
    t2 is UTC localized: 2017-10-01 15:30:30+00:00
    No fundamental filter.
    Elaborating variable: LHC.BCTDC.A6R4.B1:BEAM_INTENSITY
    Elaborating variable: LHC.BCTDC.A6R4.B2:BEAM_INTENSITY
    Time window: 3
    t1 is UTC localized: 2017-10-01 15:30:30+00:00
    t2 is UTC localized: 2017-10-01 15:30:45+00:00
    No fundamental filter.
    Elaborating variable: LHC.BCTDC.A6R4.B1:BEAM_INTENSITY
    Elaborating variable: LHC.BCTDC.A6R4.B2:BEAM_INTENSITY
    Time window: 4
    t1 is UTC localized: 2017-10-01 15:30:45+00:00
    t2 is UTC localized: 2017-10-01 15:31:00+00:00
    No fundamental filter.
    Elaborating variable: LHC.BCTDC.A6R4.B1:BEAM_INTENSITY
    Elaborating variable: LHC.BCTDC.A6R4.B2:BEAM_INTENSITY


You can filter by fundamentals.


```python
t1 = pd.Timestamp('2017-10-01 17:30', tz='CET')
t2 = pd.Timestamp('2017-10-01 17:31', tz='CET')
raw_data = importData.cals2pd(['CPS.TGM:DEST'],t1,t2,fundamental='%TOF%')
raw_data
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>CPS.TGM:DEST</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2017-10-01 15:30:13.900000095+00:00</th>
      <td>NTOF</td>
    </tr>
    <tr>
      <th>2017-10-01 15:30:17.500000+00:00</th>
      <td>NTOF</td>
    </tr>
    <tr>
      <th>2017-10-01 15:30:25.900000095+00:00</th>
      <td>NTOF</td>
    </tr>
    <tr>
      <th>2017-10-01 15:30:27.100000143+00:00</th>
      <td>NTOF</td>
    </tr>
    <tr>
      <th>2017-10-01 15:30:30.700000048+00:00</th>
      <td>NTOF</td>
    </tr>
    <tr>
      <th>2017-10-01 15:30:35.500000+00:00</th>
      <td>NTOF</td>
    </tr>
    <tr>
      <th>2017-10-01 15:30:49.900000095+00:00</th>
      <td>NTOF</td>
    </tr>
    <tr>
      <th>2017-10-01 15:30:53.500000+00:00</th>
      <td>NTOF</td>
    </tr>
  </tbody>
</table>
</div>



## cycleStamp2pd function
This function allow to retrieve information from CALS with a list of cyclestamp. For each element in the list it makes a call to pytimber (so can be slow). It is very useful to make aritmetic with the cyclestamps (e.g., for the CERN Injectors Chain).


```python
startTime=pd.Timestamp('2018-03-27 06:00')
endTime=pd.Timestamp('2018-03-27 06:10')
CPSDF=importData.cals2pd(['CPS.LSA:CYCLE'], startTime, endTime, fundamental='%LHC25%')
importData.cycleStamp2pd(['PSB.LSA:CYCLE'],CPSDF.index[1:]-pd.offsets.Milli(635)).head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>PSB.LSA:CYCLE</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2018-03-27 06:00:36.065000057+00:00</th>
      <td>LHC25_DB_A_PSB</td>
    </tr>
    <tr>
      <th>2018-03-27 06:01:13.265000105+00:00</th>
      <td>LHC25_DB_A_PSB</td>
    </tr>
    <tr>
      <th>2018-03-27 06:01:46.865000010+00:00</th>
      <td>LHC25_DB_A_PSB</td>
    </tr>
    <tr>
      <th>2018-03-27 06:01:50.464999914+00:00</th>
      <td>LHC25_DB_A_PSB</td>
    </tr>
    <tr>
      <th>2018-03-27 06:02:24.065000057+00:00</th>
      <td>LHC25_DB_A_PSB</td>
    </tr>
  </tbody>
</table>
</div>



## LHCFillsByTime function

One can recover the LHC fills between two timestamps.


```python
t1 = pd.Timestamp('2018-04-24 16:57:54.689000+00:00')
t2 = pd.Timestamp('2018-05-24 16:57:54.689000+00:00')
dfFILL=importData.LHCFillsByTime(t1,t2)
dfFILL
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>mode</th>
      <th>startTime</th>
      <th>endTime</th>
      <th>duration</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>6611</th>
      <td>FILL</td>
      <td>2018-04-24 16:57:54.688999891+00:00</td>
      <td>2018-04-25 07:15:18.815999985+00:00</td>
      <td>14:17:24.127000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>SETUP</td>
      <td>2018-04-24 17:16:51.466000080+00:00</td>
      <td>2018-04-24 18:56:27.641000032+00:00</td>
      <td>01:39:36.174999</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>INJPROT</td>
      <td>2018-04-24 18:56:27.641999960+00:00</td>
      <td>2018-04-24 19:13:20.552999973+00:00</td>
      <td>00:16:52.911000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>INJPHYS</td>
      <td>2018-04-24 19:13:20.553999901+00:00</td>
      <td>2018-04-24 19:52:14.631000042+00:00</td>
      <td>00:38:54.077000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>PRERAMP</td>
      <td>2018-04-24 19:52:14.631999969+00:00</td>
      <td>2018-04-24 19:54:30.025000095+00:00</td>
      <td>00:02:15.393000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>RAMP</td>
      <td>2018-04-24 19:54:30.026000023+00:00</td>
      <td>2018-04-24 20:14:44.450000048+00:00</td>
      <td>00:20:14.424000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>FLATTOP</td>
      <td>2018-04-24 20:14:44.450999975+00:00</td>
      <td>2018-04-24 20:17:40.210000038+00:00</td>
      <td>00:02:55.759000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>SQUEEZE</td>
      <td>2018-04-24 20:17:40.210999966+00:00</td>
      <td>2018-04-24 20:28:38.168999910+00:00</td>
      <td>00:10:57.957999</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>ADJUST</td>
      <td>2018-04-24 20:28:38.170000076+00:00</td>
      <td>2018-04-24 20:35:27.440000057+00:00</td>
      <td>00:06:49.269999</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>STABLE</td>
      <td>2018-04-24 20:35:27.440999985+00:00</td>
      <td>2018-04-25 06:00:49.973999977+00:00</td>
      <td>09:25:22.532999</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>ADJUST</td>
      <td>2018-04-25 06:00:49.974999905+00:00</td>
      <td>2018-04-25 07:09:16.572999954+00:00</td>
      <td>01:08:26.598000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>BEAMDUMP</td>
      <td>2018-04-25 07:09:16.573999882+00:00</td>
      <td>2018-04-25 07:10:50.769000053+00:00</td>
      <td>00:01:34.195000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>RAMPDOWN</td>
      <td>2018-04-25 07:10:50.769999981+00:00</td>
      <td>2018-04-25 07:15:18.815999985+00:00</td>
      <td>00:04:28.046000</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>FILL</td>
      <td>2018-04-25 07:15:18.816999912+00:00</td>
      <td>2018-04-25 20:58:43.336999893+00:00</td>
      <td>13:43:24.519999</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>SETUP</td>
      <td>2018-04-25 07:47:43.690000057+00:00</td>
      <td>2018-04-25 08:24:19.234999895+00:00</td>
      <td>00:36:35.544999</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>INJPROT</td>
      <td>2018-04-25 08:24:19.236000061+00:00</td>
      <td>2018-04-25 08:55:44.729000092+00:00</td>
      <td>00:31:25.493000</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>SETUP</td>
      <td>2018-04-25 08:55:44.730000019+00:00</td>
      <td>2018-04-25 09:00:50.864000082+00:00</td>
      <td>00:05:06.134000</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>SETUP</td>
      <td>2018-04-25 09:00:50.865000010+00:00</td>
      <td>2018-04-25 09:06:37.197999954+00:00</td>
      <td>00:05:46.332999</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>INJPROT</td>
      <td>2018-04-25 09:06:37.198999882+00:00</td>
      <td>2018-04-25 10:17:40.709000111+00:00</td>
      <td>01:11:03.510000</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>INJPHYS</td>
      <td>2018-04-25 10:17:40.710000038+00:00</td>
      <td>2018-04-25 11:24:47.825999975+00:00</td>
      <td>01:07:07.115999</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>PRERAMP</td>
      <td>2018-04-25 11:24:47.826999903+00:00</td>
      <td>2018-04-25 11:29:46.378000021+00:00</td>
      <td>00:04:58.551000</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>RAMP</td>
      <td>2018-04-25 11:29:46.378999949+00:00</td>
      <td>2018-04-25 11:50:12.374000072+00:00</td>
      <td>00:20:25.995000</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>FLATTOP</td>
      <td>2018-04-25 11:50:12.375000+00:00</td>
      <td>2018-04-25 11:57:10.351999998+00:00</td>
      <td>00:06:57.976999</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>SQUEEZE</td>
      <td>2018-04-25 11:57:10.352999926+00:00</td>
      <td>2018-04-25 12:08:05.000999928+00:00</td>
      <td>00:10:54.648000</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>ADJUST</td>
      <td>2018-04-25 12:08:05.002000093+00:00</td>
      <td>2018-04-25 12:16:59.278000116+00:00</td>
      <td>00:08:54.276000</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>STABLE</td>
      <td>2018-04-25 12:16:59.279000044+00:00</td>
      <td>2018-04-25 20:53:16.812999964+00:00</td>
      <td>08:36:17.533999</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>BEAMDUMP</td>
      <td>2018-04-25 20:53:16.813999891+00:00</td>
      <td>2018-04-25 20:54:39.635999918+00:00</td>
      <td>00:01:22.822000</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>RAMPDOWN</td>
      <td>2018-04-25 20:54:39.637000084+00:00</td>
      <td>2018-04-25 20:58:43.336999893+00:00</td>
      <td>00:04:03.699999</td>
    </tr>
    <tr>
      <th>6613</th>
      <td>FILL</td>
      <td>2018-04-25 20:58:43.338000059+00:00</td>
      <td>2018-04-25 23:57:48.444999933+00:00</td>
      <td>02:59:05.106999</td>
    </tr>
    <tr>
      <th>6613</th>
      <td>SETUP</td>
      <td>2018-04-25 22:00:32.696000099+00:00</td>
      <td>2018-04-25 22:09:09.723000050+00:00</td>
      <td>00:08:37.026999</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>6712</th>
      <td>SQUEEZE</td>
      <td>2018-05-22 16:23:08.066999911+00:00</td>
      <td>2018-05-22 16:34:02.688999891+00:00</td>
      <td>00:10:54.621999</td>
    </tr>
    <tr>
      <th>6712</th>
      <td>ADJUST</td>
      <td>2018-05-22 16:34:02.690000057+00:00</td>
      <td>2018-05-22 16:41:52.858999968+00:00</td>
      <td>00:07:50.168999</td>
    </tr>
    <tr>
      <th>6712</th>
      <td>STABLE</td>
      <td>2018-05-22 16:41:52.859999895+00:00</td>
      <td>2018-05-23 03:07:48.640000105+00:00</td>
      <td>10:25:55.780000</td>
    </tr>
    <tr>
      <th>6712</th>
      <td>BEAMDUMP</td>
      <td>2018-05-23 03:07:48.641000032+00:00</td>
      <td>2018-05-23 03:08:02.177000046+00:00</td>
      <td>00:00:13.536000</td>
    </tr>
    <tr>
      <th>6712</th>
      <td>RAMPDOWN</td>
      <td>2018-05-23 03:08:02.177999973+00:00</td>
      <td>2018-05-23 03:12:09.263999939+00:00</td>
      <td>00:04:07.085999</td>
    </tr>
    <tr>
      <th>6713</th>
      <td>FILL</td>
      <td>2018-05-23 03:12:09.265000105+00:00</td>
      <td>2018-05-23 04:27:56.147000074+00:00</td>
      <td>01:15:46.881999</td>
    </tr>
    <tr>
      <th>6713</th>
      <td>SETUP</td>
      <td>2018-05-23 03:27:44.332000017+00:00</td>
      <td>2018-05-23 03:47:09.704999924+00:00</td>
      <td>00:19:25.372999</td>
    </tr>
    <tr>
      <th>6713</th>
      <td>INJPROT</td>
      <td>2018-05-23 03:47:09.706000090+00:00</td>
      <td>2018-05-23 03:57:04.144000053+00:00</td>
      <td>00:09:54.437999</td>
    </tr>
    <tr>
      <th>6713</th>
      <td>INJPHYS</td>
      <td>2018-05-23 03:57:04.144999981+00:00</td>
      <td>2018-05-23 04:27:56.147000074+00:00</td>
      <td>00:30:52.002000</td>
    </tr>
    <tr>
      <th>6714</th>
      <td>FILL</td>
      <td>2018-05-23 04:27:56.148000002+00:00</td>
      <td>2018-05-23 19:52:48.005000114+00:00</td>
      <td>15:24:51.857000</td>
    </tr>
    <tr>
      <th>6714</th>
      <td>INJPROT</td>
      <td>2018-05-23 04:28:16.176000118+00:00</td>
      <td>2018-05-23 04:31:35.838000059+00:00</td>
      <td>00:03:19.661999</td>
    </tr>
    <tr>
      <th>6714</th>
      <td>INJPHYS</td>
      <td>2018-05-23 04:31:35.838999987+00:00</td>
      <td>2018-05-23 05:00:19.354000092+00:00</td>
      <td>00:28:43.515000</td>
    </tr>
    <tr>
      <th>6714</th>
      <td>PRERAMP</td>
      <td>2018-05-23 05:00:19.355000019+00:00</td>
      <td>2018-05-23 05:03:33.286999941+00:00</td>
      <td>00:03:13.931999</td>
    </tr>
    <tr>
      <th>6714</th>
      <td>RAMP</td>
      <td>2018-05-23 05:03:33.288000107+00:00</td>
      <td>2018-05-23 05:23:47.737999916+00:00</td>
      <td>00:20:14.449999</td>
    </tr>
    <tr>
      <th>6714</th>
      <td>FLATTOP</td>
      <td>2018-05-23 05:23:47.739000082+00:00</td>
      <td>2018-05-23 05:27:13.437000036+00:00</td>
      <td>00:03:25.697999</td>
    </tr>
    <tr>
      <th>6714</th>
      <td>SQUEEZE</td>
      <td>2018-05-23 05:27:13.437999964+00:00</td>
      <td>2018-05-23 05:38:08.301000118+00:00</td>
      <td>00:10:54.863000</td>
    </tr>
    <tr>
      <th>6714</th>
      <td>ADJUST</td>
      <td>2018-05-23 05:38:08.302000046+00:00</td>
      <td>2018-05-23 05:49:04.497999907+00:00</td>
      <td>00:10:56.195999</td>
    </tr>
    <tr>
      <th>6714</th>
      <td>STABLE</td>
      <td>2018-05-23 05:49:04.499000072+00:00</td>
      <td>2018-05-23 19:48:11.851999998+00:00</td>
      <td>13:59:07.352999</td>
    </tr>
    <tr>
      <th>6714</th>
      <td>BEAMDUMP</td>
      <td>2018-05-23 19:48:11.852999926+00:00</td>
      <td>2018-05-23 19:48:40.487999916+00:00</td>
      <td>00:00:28.634999</td>
    </tr>
    <tr>
      <th>6714</th>
      <td>RAMPDOWN</td>
      <td>2018-05-23 19:48:40.489000082+00:00</td>
      <td>2018-05-23 19:52:48.005000114+00:00</td>
      <td>00:04:07.516000</td>
    </tr>
    <tr>
      <th>6715</th>
      <td>FILL</td>
      <td>2018-05-23 19:52:48.006000042+00:00</td>
      <td>2018-05-23 22:47:48.658999920+00:00</td>
      <td>02:55:00.652999</td>
    </tr>
    <tr>
      <th>6715</th>
      <td>SETUP</td>
      <td>2018-05-23 20:23:27.148999929+00:00</td>
      <td>2018-05-23 21:28:07.967999935+00:00</td>
      <td>01:04:40.819000</td>
    </tr>
    <tr>
      <th>6715</th>
      <td>INJPROT</td>
      <td>2018-05-23 21:28:07.969000101+00:00</td>
      <td>2018-05-23 22:20:01.910000086+00:00</td>
      <td>00:51:53.940999</td>
    </tr>
    <tr>
      <th>6715</th>
      <td>INJPHYS</td>
      <td>2018-05-23 22:20:01.911000013+00:00</td>
      <td>2018-05-23 22:47:48.658999920+00:00</td>
      <td>00:27:46.747999</td>
    </tr>
    <tr>
      <th>6716</th>
      <td>FILL</td>
      <td>2018-05-23 22:47:48.660000086+00:00</td>
      <td>2018-05-24 04:15:31.756000042+00:00</td>
      <td>05:27:43.095999</td>
    </tr>
    <tr>
      <th>6716</th>
      <td>INJPROT</td>
      <td>2018-05-23 22:48:01.979000092+00:00</td>
      <td>2018-05-24 02:11:03.315000057+00:00</td>
      <td>03:23:01.335999</td>
    </tr>
    <tr>
      <th>6716</th>
      <td>NOBEAM</td>
      <td>2018-05-24 02:11:03.315999985+00:00</td>
      <td>2018-05-24 04:15:29.599999905+00:00</td>
      <td>02:04:26.283999</td>
    </tr>
    <tr>
      <th>6716</th>
      <td>CYCLING</td>
      <td>2018-05-24 04:15:29.601000071+00:00</td>
      <td>2018-05-24 04:15:31.756000042+00:00</td>
      <td>00:00:02.154999</td>
    </tr>
    <tr>
      <th>6717</th>
      <td>FILL</td>
      <td>2018-05-24 04:15:31.756999969+00:00</td>
      <td>2018-05-24 18:49:18.076999903+00:00</td>
      <td>14:33:46.319999</td>
    </tr>
    <tr>
      <th>6717</th>
      <td>NOBEAM</td>
      <td>2018-05-24 05:37:40.792000055+00:00</td>
      <td>2018-05-24 18:49:18.076999903+00:00</td>
      <td>13:11:37.284999</td>
    </tr>
  </tbody>
</table>
<p>925 rows × 4 columns</p>
</div>



On can change the time zone (in case the user want to do it...)


```python
def _CETconvertMe(x):
    '''
    Return the tz-aware datetime. In case of error returns x.
    '''
    try:
        return x.tz_convert('CET')
    except:
        return x 

dfFILL['startTime']=dfFILL['startTime'].apply(_CETconvertMe)
dfFILL['endTime']=dfFILL['endTime'].apply(_CETconvertMe)
dfFILL
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>mode</th>
      <th>startTime</th>
      <th>endTime</th>
      <th>duration</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>6611</th>
      <td>FILL</td>
      <td>2018-04-24 18:57:54.688999891+02:00</td>
      <td>2018-04-25 09:15:18.815999985+02:00</td>
      <td>14:17:24.127000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>SETUP</td>
      <td>2018-04-24 19:16:51.466000080+02:00</td>
      <td>2018-04-24 20:56:27.641000032+02:00</td>
      <td>01:39:36.174999</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>INJPROT</td>
      <td>2018-04-24 20:56:27.641999960+02:00</td>
      <td>2018-04-24 21:13:20.552999973+02:00</td>
      <td>00:16:52.911000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>INJPHYS</td>
      <td>2018-04-24 21:13:20.553999901+02:00</td>
      <td>2018-04-24 21:52:14.631000042+02:00</td>
      <td>00:38:54.077000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>PRERAMP</td>
      <td>2018-04-24 21:52:14.631999969+02:00</td>
      <td>2018-04-24 21:54:30.025000095+02:00</td>
      <td>00:02:15.393000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>RAMP</td>
      <td>2018-04-24 21:54:30.026000023+02:00</td>
      <td>2018-04-24 22:14:44.450000048+02:00</td>
      <td>00:20:14.424000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>FLATTOP</td>
      <td>2018-04-24 22:14:44.450999975+02:00</td>
      <td>2018-04-24 22:17:40.210000038+02:00</td>
      <td>00:02:55.759000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>SQUEEZE</td>
      <td>2018-04-24 22:17:40.210999966+02:00</td>
      <td>2018-04-24 22:28:38.168999910+02:00</td>
      <td>00:10:57.957999</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>ADJUST</td>
      <td>2018-04-24 22:28:38.170000076+02:00</td>
      <td>2018-04-24 22:35:27.440000057+02:00</td>
      <td>00:06:49.269999</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>STABLE</td>
      <td>2018-04-24 22:35:27.440999985+02:00</td>
      <td>2018-04-25 08:00:49.973999977+02:00</td>
      <td>09:25:22.532999</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>ADJUST</td>
      <td>2018-04-25 08:00:49.974999905+02:00</td>
      <td>2018-04-25 09:09:16.572999954+02:00</td>
      <td>01:08:26.598000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>BEAMDUMP</td>
      <td>2018-04-25 09:09:16.573999882+02:00</td>
      <td>2018-04-25 09:10:50.769000053+02:00</td>
      <td>00:01:34.195000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>RAMPDOWN</td>
      <td>2018-04-25 09:10:50.769999981+02:00</td>
      <td>2018-04-25 09:15:18.815999985+02:00</td>
      <td>00:04:28.046000</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>FILL</td>
      <td>2018-04-25 09:15:18.816999912+02:00</td>
      <td>2018-04-25 22:58:43.336999893+02:00</td>
      <td>13:43:24.519999</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>SETUP</td>
      <td>2018-04-25 09:47:43.690000057+02:00</td>
      <td>2018-04-25 10:24:19.234999895+02:00</td>
      <td>00:36:35.544999</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>INJPROT</td>
      <td>2018-04-25 10:24:19.236000061+02:00</td>
      <td>2018-04-25 10:55:44.729000092+02:00</td>
      <td>00:31:25.493000</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>SETUP</td>
      <td>2018-04-25 10:55:44.730000019+02:00</td>
      <td>2018-04-25 11:00:50.864000082+02:00</td>
      <td>00:05:06.134000</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>SETUP</td>
      <td>2018-04-25 11:00:50.865000010+02:00</td>
      <td>2018-04-25 11:06:37.197999954+02:00</td>
      <td>00:05:46.332999</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>INJPROT</td>
      <td>2018-04-25 11:06:37.198999882+02:00</td>
      <td>2018-04-25 12:17:40.709000111+02:00</td>
      <td>01:11:03.510000</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>INJPHYS</td>
      <td>2018-04-25 12:17:40.710000038+02:00</td>
      <td>2018-04-25 13:24:47.825999975+02:00</td>
      <td>01:07:07.115999</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>PRERAMP</td>
      <td>2018-04-25 13:24:47.826999903+02:00</td>
      <td>2018-04-25 13:29:46.378000021+02:00</td>
      <td>00:04:58.551000</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>RAMP</td>
      <td>2018-04-25 13:29:46.378999949+02:00</td>
      <td>2018-04-25 13:50:12.374000072+02:00</td>
      <td>00:20:25.995000</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>FLATTOP</td>
      <td>2018-04-25 13:50:12.375000+02:00</td>
      <td>2018-04-25 13:57:10.351999998+02:00</td>
      <td>00:06:57.976999</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>SQUEEZE</td>
      <td>2018-04-25 13:57:10.352999926+02:00</td>
      <td>2018-04-25 14:08:05.000999928+02:00</td>
      <td>00:10:54.648000</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>ADJUST</td>
      <td>2018-04-25 14:08:05.002000093+02:00</td>
      <td>2018-04-25 14:16:59.278000116+02:00</td>
      <td>00:08:54.276000</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>STABLE</td>
      <td>2018-04-25 14:16:59.279000044+02:00</td>
      <td>2018-04-25 22:53:16.812999964+02:00</td>
      <td>08:36:17.533999</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>BEAMDUMP</td>
      <td>2018-04-25 22:53:16.813999891+02:00</td>
      <td>2018-04-25 22:54:39.635999918+02:00</td>
      <td>00:01:22.822000</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>RAMPDOWN</td>
      <td>2018-04-25 22:54:39.637000084+02:00</td>
      <td>2018-04-25 22:58:43.336999893+02:00</td>
      <td>00:04:03.699999</td>
    </tr>
    <tr>
      <th>6613</th>
      <td>FILL</td>
      <td>2018-04-25 22:58:43.338000059+02:00</td>
      <td>2018-04-26 01:57:48.444999933+02:00</td>
      <td>02:59:05.106999</td>
    </tr>
    <tr>
      <th>6613</th>
      <td>SETUP</td>
      <td>2018-04-26 00:00:32.696000099+02:00</td>
      <td>2018-04-26 00:09:09.723000050+02:00</td>
      <td>00:08:37.026999</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>6712</th>
      <td>SQUEEZE</td>
      <td>2018-05-22 18:23:08.066999911+02:00</td>
      <td>2018-05-22 18:34:02.688999891+02:00</td>
      <td>00:10:54.621999</td>
    </tr>
    <tr>
      <th>6712</th>
      <td>ADJUST</td>
      <td>2018-05-22 18:34:02.690000057+02:00</td>
      <td>2018-05-22 18:41:52.858999968+02:00</td>
      <td>00:07:50.168999</td>
    </tr>
    <tr>
      <th>6712</th>
      <td>STABLE</td>
      <td>2018-05-22 18:41:52.859999895+02:00</td>
      <td>2018-05-23 05:07:48.640000105+02:00</td>
      <td>10:25:55.780000</td>
    </tr>
    <tr>
      <th>6712</th>
      <td>BEAMDUMP</td>
      <td>2018-05-23 05:07:48.641000032+02:00</td>
      <td>2018-05-23 05:08:02.177000046+02:00</td>
      <td>00:00:13.536000</td>
    </tr>
    <tr>
      <th>6712</th>
      <td>RAMPDOWN</td>
      <td>2018-05-23 05:08:02.177999973+02:00</td>
      <td>2018-05-23 05:12:09.263999939+02:00</td>
      <td>00:04:07.085999</td>
    </tr>
    <tr>
      <th>6713</th>
      <td>FILL</td>
      <td>2018-05-23 05:12:09.265000105+02:00</td>
      <td>2018-05-23 06:27:56.147000074+02:00</td>
      <td>01:15:46.881999</td>
    </tr>
    <tr>
      <th>6713</th>
      <td>SETUP</td>
      <td>2018-05-23 05:27:44.332000017+02:00</td>
      <td>2018-05-23 05:47:09.704999924+02:00</td>
      <td>00:19:25.372999</td>
    </tr>
    <tr>
      <th>6713</th>
      <td>INJPROT</td>
      <td>2018-05-23 05:47:09.706000090+02:00</td>
      <td>2018-05-23 05:57:04.144000053+02:00</td>
      <td>00:09:54.437999</td>
    </tr>
    <tr>
      <th>6713</th>
      <td>INJPHYS</td>
      <td>2018-05-23 05:57:04.144999981+02:00</td>
      <td>2018-05-23 06:27:56.147000074+02:00</td>
      <td>00:30:52.002000</td>
    </tr>
    <tr>
      <th>6714</th>
      <td>FILL</td>
      <td>2018-05-23 06:27:56.148000002+02:00</td>
      <td>2018-05-23 21:52:48.005000114+02:00</td>
      <td>15:24:51.857000</td>
    </tr>
    <tr>
      <th>6714</th>
      <td>INJPROT</td>
      <td>2018-05-23 06:28:16.176000118+02:00</td>
      <td>2018-05-23 06:31:35.838000059+02:00</td>
      <td>00:03:19.661999</td>
    </tr>
    <tr>
      <th>6714</th>
      <td>INJPHYS</td>
      <td>2018-05-23 06:31:35.838999987+02:00</td>
      <td>2018-05-23 07:00:19.354000092+02:00</td>
      <td>00:28:43.515000</td>
    </tr>
    <tr>
      <th>6714</th>
      <td>PRERAMP</td>
      <td>2018-05-23 07:00:19.355000019+02:00</td>
      <td>2018-05-23 07:03:33.286999941+02:00</td>
      <td>00:03:13.931999</td>
    </tr>
    <tr>
      <th>6714</th>
      <td>RAMP</td>
      <td>2018-05-23 07:03:33.288000107+02:00</td>
      <td>2018-05-23 07:23:47.737999916+02:00</td>
      <td>00:20:14.449999</td>
    </tr>
    <tr>
      <th>6714</th>
      <td>FLATTOP</td>
      <td>2018-05-23 07:23:47.739000082+02:00</td>
      <td>2018-05-23 07:27:13.437000036+02:00</td>
      <td>00:03:25.697999</td>
    </tr>
    <tr>
      <th>6714</th>
      <td>SQUEEZE</td>
      <td>2018-05-23 07:27:13.437999964+02:00</td>
      <td>2018-05-23 07:38:08.301000118+02:00</td>
      <td>00:10:54.863000</td>
    </tr>
    <tr>
      <th>6714</th>
      <td>ADJUST</td>
      <td>2018-05-23 07:38:08.302000046+02:00</td>
      <td>2018-05-23 07:49:04.497999907+02:00</td>
      <td>00:10:56.195999</td>
    </tr>
    <tr>
      <th>6714</th>
      <td>STABLE</td>
      <td>2018-05-23 07:49:04.499000072+02:00</td>
      <td>2018-05-23 21:48:11.851999998+02:00</td>
      <td>13:59:07.352999</td>
    </tr>
    <tr>
      <th>6714</th>
      <td>BEAMDUMP</td>
      <td>2018-05-23 21:48:11.852999926+02:00</td>
      <td>2018-05-23 21:48:40.487999916+02:00</td>
      <td>00:00:28.634999</td>
    </tr>
    <tr>
      <th>6714</th>
      <td>RAMPDOWN</td>
      <td>2018-05-23 21:48:40.489000082+02:00</td>
      <td>2018-05-23 21:52:48.005000114+02:00</td>
      <td>00:04:07.516000</td>
    </tr>
    <tr>
      <th>6715</th>
      <td>FILL</td>
      <td>2018-05-23 21:52:48.006000042+02:00</td>
      <td>2018-05-24 00:47:48.658999920+02:00</td>
      <td>02:55:00.652999</td>
    </tr>
    <tr>
      <th>6715</th>
      <td>SETUP</td>
      <td>2018-05-23 22:23:27.148999929+02:00</td>
      <td>2018-05-23 23:28:07.967999935+02:00</td>
      <td>01:04:40.819000</td>
    </tr>
    <tr>
      <th>6715</th>
      <td>INJPROT</td>
      <td>2018-05-23 23:28:07.969000101+02:00</td>
      <td>2018-05-24 00:20:01.910000086+02:00</td>
      <td>00:51:53.940999</td>
    </tr>
    <tr>
      <th>6715</th>
      <td>INJPHYS</td>
      <td>2018-05-24 00:20:01.911000013+02:00</td>
      <td>2018-05-24 00:47:48.658999920+02:00</td>
      <td>00:27:46.747999</td>
    </tr>
    <tr>
      <th>6716</th>
      <td>FILL</td>
      <td>2018-05-24 00:47:48.660000086+02:00</td>
      <td>2018-05-24 06:15:31.756000042+02:00</td>
      <td>05:27:43.095999</td>
    </tr>
    <tr>
      <th>6716</th>
      <td>INJPROT</td>
      <td>2018-05-24 00:48:01.979000092+02:00</td>
      <td>2018-05-24 04:11:03.315000057+02:00</td>
      <td>03:23:01.335999</td>
    </tr>
    <tr>
      <th>6716</th>
      <td>NOBEAM</td>
      <td>2018-05-24 04:11:03.315999985+02:00</td>
      <td>2018-05-24 06:15:29.599999905+02:00</td>
      <td>02:04:26.283999</td>
    </tr>
    <tr>
      <th>6716</th>
      <td>CYCLING</td>
      <td>2018-05-24 06:15:29.601000071+02:00</td>
      <td>2018-05-24 06:15:31.756000042+02:00</td>
      <td>00:00:02.154999</td>
    </tr>
    <tr>
      <th>6717</th>
      <td>FILL</td>
      <td>2018-05-24 06:15:31.756999969+02:00</td>
      <td>2018-05-24 20:49:18.076999903+02:00</td>
      <td>14:33:46.319999</td>
    </tr>
    <tr>
      <th>6717</th>
      <td>NOBEAM</td>
      <td>2018-05-24 07:37:40.792000055+02:00</td>
      <td>2018-05-24 20:49:18.076999903+02:00</td>
      <td>13:11:37.284999</td>
    </tr>
  </tbody>
</table>
<p>925 rows × 4 columns</p>
</div>



## LHCFillsByNumber function

One can select the LHC fill by fill number.


```python
dfFILL=importData.LHCFillsByNumber(6611)
dfFILL
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>mode</th>
      <th>startTime</th>
      <th>endTime</th>
      <th>duration</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>6611</th>
      <td>FILL</td>
      <td>2018-04-24 16:57:54.688999891+00:00</td>
      <td>2018-04-25 07:15:18.815999985+00:00</td>
      <td>14:17:24.127000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>SETUP</td>
      <td>2018-04-24 17:16:51.466000080+00:00</td>
      <td>2018-04-24 18:56:27.641000032+00:00</td>
      <td>01:39:36.174999</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>INJPROT</td>
      <td>2018-04-24 18:56:27.641999960+00:00</td>
      <td>2018-04-24 19:13:20.552999973+00:00</td>
      <td>00:16:52.911000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>INJPHYS</td>
      <td>2018-04-24 19:13:20.553999901+00:00</td>
      <td>2018-04-24 19:52:14.631000042+00:00</td>
      <td>00:38:54.077000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>PRERAMP</td>
      <td>2018-04-24 19:52:14.631999969+00:00</td>
      <td>2018-04-24 19:54:30.025000095+00:00</td>
      <td>00:02:15.393000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>RAMP</td>
      <td>2018-04-24 19:54:30.026000023+00:00</td>
      <td>2018-04-24 20:14:44.450000048+00:00</td>
      <td>00:20:14.424000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>FLATTOP</td>
      <td>2018-04-24 20:14:44.450999975+00:00</td>
      <td>2018-04-24 20:17:40.210000038+00:00</td>
      <td>00:02:55.759000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>SQUEEZE</td>
      <td>2018-04-24 20:17:40.210999966+00:00</td>
      <td>2018-04-24 20:28:38.168999910+00:00</td>
      <td>00:10:57.957999</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>ADJUST</td>
      <td>2018-04-24 20:28:38.170000076+00:00</td>
      <td>2018-04-24 20:35:27.440000057+00:00</td>
      <td>00:06:49.269999</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>STABLE</td>
      <td>2018-04-24 20:35:27.440999985+00:00</td>
      <td>2018-04-25 06:00:49.973999977+00:00</td>
      <td>09:25:22.532999</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>ADJUST</td>
      <td>2018-04-25 06:00:49.974999905+00:00</td>
      <td>2018-04-25 07:09:16.572999954+00:00</td>
      <td>01:08:26.598000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>BEAMDUMP</td>
      <td>2018-04-25 07:09:16.573999882+00:00</td>
      <td>2018-04-25 07:10:50.769000053+00:00</td>
      <td>00:01:34.195000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>RAMPDOWN</td>
      <td>2018-04-25 07:10:50.769999981+00:00</td>
      <td>2018-04-25 07:15:18.815999985+00:00</td>
      <td>00:04:28.046000</td>
    </tr>
  </tbody>
</table>
</div>




```python
dfFILL=importData.LHCFillsByNumber([6611,6666])
dfFILL
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>mode</th>
      <th>startTime</th>
      <th>endTime</th>
      <th>duration</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>6611</th>
      <td>FILL</td>
      <td>2018-04-24 16:57:54.688999891+00:00</td>
      <td>2018-04-25 07:15:18.815999985+00:00</td>
      <td>14:17:24.127000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>SETUP</td>
      <td>2018-04-24 17:16:51.466000080+00:00</td>
      <td>2018-04-24 18:56:27.641000032+00:00</td>
      <td>01:39:36.174999</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>INJPROT</td>
      <td>2018-04-24 18:56:27.641999960+00:00</td>
      <td>2018-04-24 19:13:20.552999973+00:00</td>
      <td>00:16:52.911000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>INJPHYS</td>
      <td>2018-04-24 19:13:20.553999901+00:00</td>
      <td>2018-04-24 19:52:14.631000042+00:00</td>
      <td>00:38:54.077000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>PRERAMP</td>
      <td>2018-04-24 19:52:14.631999969+00:00</td>
      <td>2018-04-24 19:54:30.025000095+00:00</td>
      <td>00:02:15.393000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>RAMP</td>
      <td>2018-04-24 19:54:30.026000023+00:00</td>
      <td>2018-04-24 20:14:44.450000048+00:00</td>
      <td>00:20:14.424000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>FLATTOP</td>
      <td>2018-04-24 20:14:44.450999975+00:00</td>
      <td>2018-04-24 20:17:40.210000038+00:00</td>
      <td>00:02:55.759000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>SQUEEZE</td>
      <td>2018-04-24 20:17:40.210999966+00:00</td>
      <td>2018-04-24 20:28:38.168999910+00:00</td>
      <td>00:10:57.957999</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>ADJUST</td>
      <td>2018-04-24 20:28:38.170000076+00:00</td>
      <td>2018-04-24 20:35:27.440000057+00:00</td>
      <td>00:06:49.269999</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>STABLE</td>
      <td>2018-04-24 20:35:27.440999985+00:00</td>
      <td>2018-04-25 06:00:49.973999977+00:00</td>
      <td>09:25:22.532999</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>ADJUST</td>
      <td>2018-04-25 06:00:49.974999905+00:00</td>
      <td>2018-04-25 07:09:16.572999954+00:00</td>
      <td>01:08:26.598000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>BEAMDUMP</td>
      <td>2018-04-25 07:09:16.573999882+00:00</td>
      <td>2018-04-25 07:10:50.769000053+00:00</td>
      <td>00:01:34.195000</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>RAMPDOWN</td>
      <td>2018-04-25 07:10:50.769999981+00:00</td>
      <td>2018-04-25 07:15:18.815999985+00:00</td>
      <td>00:04:28.046000</td>
    </tr>
    <tr>
      <th>6666</th>
      <td>FILL</td>
      <td>2018-05-10 19:35:38.635999918+00:00</td>
      <td>2018-05-11 10:29:36.956000090+00:00</td>
      <td>14:53:58.320000</td>
    </tr>
    <tr>
      <th>6666</th>
      <td>INJPROT</td>
      <td>2018-05-10 19:35:51.628000021+00:00</td>
      <td>2018-05-10 19:39:39.914999962+00:00</td>
      <td>00:03:48.286999</td>
    </tr>
    <tr>
      <th>6666</th>
      <td>INJPHYS</td>
      <td>2018-05-10 19:39:39.915999889+00:00</td>
      <td>2018-05-10 20:25:05.993000031+00:00</td>
      <td>00:45:26.077000</td>
    </tr>
    <tr>
      <th>6666</th>
      <td>PRERAMP</td>
      <td>2018-05-10 20:25:05.993999958+00:00</td>
      <td>2018-05-10 20:33:55.484999895+00:00</td>
      <td>00:08:49.490999</td>
    </tr>
    <tr>
      <th>6666</th>
      <td>RAMP</td>
      <td>2018-05-10 20:33:55.486000061+00:00</td>
      <td>2018-05-10 20:54:09.895999908+00:00</td>
      <td>00:20:14.409999</td>
    </tr>
    <tr>
      <th>6666</th>
      <td>FLATTOP</td>
      <td>2018-05-10 20:54:09.897000074+00:00</td>
      <td>2018-05-10 20:56:43.071000099+00:00</td>
      <td>00:02:33.174000</td>
    </tr>
    <tr>
      <th>6666</th>
      <td>SQUEEZE</td>
      <td>2018-05-10 20:56:43.072000027+00:00</td>
      <td>2018-05-10 21:07:37.651000023+00:00</td>
      <td>00:10:54.578999</td>
    </tr>
    <tr>
      <th>6666</th>
      <td>ADJUST</td>
      <td>2018-05-10 21:07:37.651999950+00:00</td>
      <td>2018-05-10 21:13:16.588999987+00:00</td>
      <td>00:05:38.937000</td>
    </tr>
    <tr>
      <th>6666</th>
      <td>STABLE</td>
      <td>2018-05-10 21:13:16.589999914+00:00</td>
      <td>2018-05-11 09:56:28.926000118+00:00</td>
      <td>12:43:12.336000</td>
    </tr>
    <tr>
      <th>6666</th>
      <td>ADJUST</td>
      <td>2018-05-11 09:56:28.927000046+00:00</td>
      <td>2018-05-11 10:22:51.443000078+00:00</td>
      <td>00:26:22.516000</td>
    </tr>
    <tr>
      <th>6666</th>
      <td>BEAMDUMP</td>
      <td>2018-05-11 10:22:51.444000006+00:00</td>
      <td>2018-05-11 10:24:59.108000040+00:00</td>
      <td>00:02:07.664000</td>
    </tr>
    <tr>
      <th>6666</th>
      <td>BEAMDUMP</td>
      <td>2018-05-11 10:24:59.108999968+00:00</td>
      <td>2018-05-11 10:25:15.894000053+00:00</td>
      <td>00:00:16.785000</td>
    </tr>
    <tr>
      <th>6666</th>
      <td>RAMPDOWN</td>
      <td>2018-05-11 10:25:15.894999981+00:00</td>
      <td>2018-05-11 10:29:36.956000090+00:00</td>
      <td>00:04:21.061000</td>
    </tr>
  </tbody>
</table>
</div>



# LHCInstant
To retrieve the status of the LHC (fill and beammode at a specific time).


```python
t1 = pd.Timestamp('2018-05-22 02:10:15', tz='CET')
importData.LHCInstant(t1)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>mode</th>
      <th>startTime</th>
      <th>endTime</th>
      <th>duration</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>6711</th>
      <td>INJPHYS</td>
      <td>2018-05-21 23:23:11.940000057+00:00</td>
      <td>2018-05-22 00:27:06.229000092+00:00</td>
      <td>01:03:54.289000</td>
    </tr>
  </tbody>
</table>
</div>



# LHCCals2pd
To collect datas for specific LHC fills or beam modes.


```python
importData.LHCCals2pd?
```


```python
importData.LHCCals2pd(['%.RQX.%1:I_MEAS'],[6278],['RAMP','FLATTOP'],fill_column=True, beamMode_column=True)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>RPHFC.UL14.RQX.L1:I_MEAS</th>
      <th>RPHFC.UL16.RQX.R1:I_MEAS</th>
      <th>fill</th>
      <th>mode</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2017-10-06 21:25:06.059999943+00:00</th>
      <td>NaN</td>
      <td>407.31</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:06.079999924+00:00</th>
      <td>407.37</td>
      <td>NaN</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:06.559999943+00:00</th>
      <td>NaN</td>
      <td>407.38</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:06.579999924+00:00</th>
      <td>407.45</td>
      <td>NaN</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:10.059999943+00:00</th>
      <td>NaN</td>
      <td>408.06</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:10.079999924+00:00</th>
      <td>408.12</td>
      <td>NaN</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:10.559999943+00:00</th>
      <td>NaN</td>
      <td>408.18</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:10.579999924+00:00</th>
      <td>408.24</td>
      <td>NaN</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:13.059999943+00:00</th>
      <td>NaN</td>
      <td>408.82</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:13.079999924+00:00</th>
      <td>408.89</td>
      <td>NaN</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:13.559999943+00:00</th>
      <td>NaN</td>
      <td>408.96</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:13.579999924+00:00</th>
      <td>409.02</td>
      <td>NaN</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:15.559999943+00:00</th>
      <td>NaN</td>
      <td>409.57</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:15.579999924+00:00</th>
      <td>409.64</td>
      <td>NaN</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:16.059999943+00:00</th>
      <td>NaN</td>
      <td>409.74</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:16.079999924+00:00</th>
      <td>409.81</td>
      <td>NaN</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:17.559999943+00:00</th>
      <td>NaN</td>
      <td>410.26</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:17.579999924+00:00</th>
      <td>410.33</td>
      <td>NaN</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:18.059999943+00:00</th>
      <td>NaN</td>
      <td>410.44</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:18.079999924+00:00</th>
      <td>410.51</td>
      <td>NaN</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:19.559999943+00:00</th>
      <td>NaN</td>
      <td>411.02</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:19.579999924+00:00</th>
      <td>411.09</td>
      <td>NaN</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:20.059999943+00:00</th>
      <td>NaN</td>
      <td>411.22</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:20.079999924+00:00</th>
      <td>411.29</td>
      <td>NaN</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:21.559999943+00:00</th>
      <td>NaN</td>
      <td>411.84</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:21.579999924+00:00</th>
      <td>411.91</td>
      <td>NaN</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:22.059999943+00:00</th>
      <td>NaN</td>
      <td>412.07</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:22.079999924+00:00</th>
      <td>412.14</td>
      <td>NaN</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:23.559999943+00:00</th>
      <td>NaN</td>
      <td>412.75</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:25:23.579999924+00:00</th>
      <td>412.82</td>
      <td>NaN</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>2017-10-06 21:44:58.559999943+00:00</th>
      <td>NaN</td>
      <td>6254.74</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:44:58.579999924+00:00</th>
      <td>6256.49</td>
      <td>NaN</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:44:59.059999943+00:00</th>
      <td>NaN</td>
      <td>6255.41</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:44:59.079999924+00:00</th>
      <td>6257.15</td>
      <td>NaN</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:44:59.559999943+00:00</th>
      <td>NaN</td>
      <td>6256.01</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:44:59.579999924+00:00</th>
      <td>6257.75</td>
      <td>NaN</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:45:00.059999943+00:00</th>
      <td>NaN</td>
      <td>6256.54</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:45:00.079999924+00:00</th>
      <td>6258.28</td>
      <td>NaN</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:45:00.559999943+00:00</th>
      <td>NaN</td>
      <td>6257.00</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:45:00.579999924+00:00</th>
      <td>6258.74</td>
      <td>NaN</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:45:01.059999943+00:00</th>
      <td>NaN</td>
      <td>6257.40</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:45:01.079999924+00:00</th>
      <td>6259.14</td>
      <td>NaN</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:45:01.559999943+00:00</th>
      <td>NaN</td>
      <td>6257.73</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:45:01.579999924+00:00</th>
      <td>6259.46</td>
      <td>NaN</td>
      <td>6278</td>
      <td>RAMP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:46:01.059999943+00:00</th>
      <td>NaN</td>
      <td>6258.37</td>
      <td>6278</td>
      <td>FLATTOP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:46:01.079999924+00:00</th>
      <td>6260.10</td>
      <td>NaN</td>
      <td>6278</td>
      <td>FLATTOP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:46:01.559999943+00:00</th>
      <td>NaN</td>
      <td>6258.37</td>
      <td>6278</td>
      <td>FLATTOP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:46:01.579999924+00:00</th>
      <td>6260.10</td>
      <td>NaN</td>
      <td>6278</td>
      <td>FLATTOP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:47:01.059999943+00:00</th>
      <td>NaN</td>
      <td>6258.37</td>
      <td>6278</td>
      <td>FLATTOP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:47:01.079999924+00:00</th>
      <td>6260.10</td>
      <td>NaN</td>
      <td>6278</td>
      <td>FLATTOP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:47:01.559999943+00:00</th>
      <td>NaN</td>
      <td>6258.37</td>
      <td>6278</td>
      <td>FLATTOP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:47:01.579999924+00:00</th>
      <td>6260.10</td>
      <td>NaN</td>
      <td>6278</td>
      <td>FLATTOP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:48:01.059999943+00:00</th>
      <td>NaN</td>
      <td>6258.37</td>
      <td>6278</td>
      <td>FLATTOP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:48:01.079999924+00:00</th>
      <td>6260.10</td>
      <td>NaN</td>
      <td>6278</td>
      <td>FLATTOP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:48:01.559999943+00:00</th>
      <td>NaN</td>
      <td>6258.37</td>
      <td>6278</td>
      <td>FLATTOP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:48:01.579999924+00:00</th>
      <td>6260.10</td>
      <td>NaN</td>
      <td>6278</td>
      <td>FLATTOP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:49:01.059999943+00:00</th>
      <td>NaN</td>
      <td>6258.37</td>
      <td>6278</td>
      <td>FLATTOP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:49:01.079999924+00:00</th>
      <td>6260.10</td>
      <td>NaN</td>
      <td>6278</td>
      <td>FLATTOP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:49:01.559999943+00:00</th>
      <td>NaN</td>
      <td>6258.37</td>
      <td>6278</td>
      <td>FLATTOP</td>
    </tr>
    <tr>
      <th>2017-10-06 21:49:01.579999924+00:00</th>
      <td>6260.10</td>
      <td>NaN</td>
      <td>6278</td>
      <td>FLATTOP</td>
    </tr>
  </tbody>
</table>
<p>4742 rows × 4 columns</p>
</div>



## massiFile2pd function
One can select load massi files in pandas Dataframes.


```python
ATLAS=importData.massiFile2pd('/eos/user/s/sterbini/MD_ANALYSIS/2017/LHC/MD2201/ATLAS_6195.tgz')
```


```python
ATLAS.tail(3)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>FILL</th>
      <th>Stable Beam Flag</th>
      <th>Experiment</th>
      <th>Bunch</th>
      <th>Luminosity [Hz/ub]</th>
      <th>P2P luminosity error [Hz/ub]</th>
      <th>Specific luminosity [Hz/ub]</th>
      <th>P2P specific luminosity [Hz/ub]</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2017-09-13 18:00:43+00:00</th>
      <td>6195</td>
      <td>0</td>
      <td>ATLAS</td>
      <td>1490</td>
      <td>0.000047</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>2017-09-13 18:01:43+00:00</th>
      <td>6195</td>
      <td>0</td>
      <td>ATLAS</td>
      <td>1490</td>
      <td>0.000047</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>2017-09-13 18:21:26+00:00</th>
      <td>6195</td>
      <td>0</td>
      <td>ATLAS</td>
      <td>1490</td>
      <td>0.000017</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
    </tr>
  </tbody>
</table>
</div>



## calsCSV2pd function

This allows to convert in pandas dataframes the csv data obtained from CALS.


```python
myFile='/eos/project/l/lhc-lumimod/LuminosityFollowUp/2017/rawdata/fill_bunchbybunch_data_csvs/bunchbybunch_lumi_data_fill_5848.csv'
aux=importData.calsCSV2pd(myFile)
aux.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>ATLAS:BUNCH_LUMI_INST</th>
      <th>CMS:BUNCH_LUMI_INST</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2017-06-19 15:42:47.745000+00:00</th>
      <td>[0.045945592, 0.045026667, 0.046864517, 0.0450...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2017-06-19 15:44:48.104000+00:00</th>
      <td>[0.039506007, 0.04899983, 0.041649766, 0.04011...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2017-06-19 15:45:48.287000+00:00</th>
      <td>[0.044709116, 0.04869012, 0.0523649, 0.0431779...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2017-06-19 15:47:48.234000+00:00</th>
      <td>[0.03982115, 0.039514832, 0.042884354, 0.04870...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2017-06-19 15:48:48.298000+00:00</th>
      <td>[0.05175795, 0.04410135, 0.049307834, 0.049614...</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
</div>




```python
# to retrieve directly from CALS. This is for checks and in general is slower
aux=importData.cals2pd(aux.columns,t1=aux.index[0],t2=aux.index[2])
aux
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>ATLAS:BUNCH_LUMI_INST</th>
      <th>CMS:BUNCH_LUMI_INST</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2017-06-19 15:42:47.745000124+00:00</th>
      <td>[0.045945592, 0.045026667, 0.046864517, 0.0450...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2017-06-19 15:44:48.104000092+00:00</th>
      <td>[0.039506007, 0.04899983, 0.041649766, 0.04011...</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2017-06-19 15:45:48.286999941+00:00</th>
      <td>[0.044709116, 0.04869012, 0.0523649, 0.0431779...</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
</div>




```python
myFile='/eos/project/l/lhc-lumimod/LuminosityFollowUp/2017/rawdata/fill_basic_data_csvs/basic_data_fill_5706.csv'
aux=importData.calsCSV2pd(myFile)
aux.head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>LHC.BCTDC.A6R4.B1:BEAM_INTENSITY</th>
      <th>LHC.BCTDC.A6R4.B2:BEAM_INTENSITY</th>
      <th>LHC.BSRA.US45.B1:ABORT_GAP_ENERGY</th>
      <th>LHC.BSRA.US45.B2:ABORT_GAP_ENERGY</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2017-05-25 13:01:13+00:00</th>
      <td>2.410000e+09</td>
      <td>590000000.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2017-05-25 13:01:13.736000+00:00</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>450.0</td>
      <td>450.0</td>
    </tr>
    <tr>
      <th>2017-05-25 13:01:14+00:00</th>
      <td>8.600000e+08</td>
      <td>390000000.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
    <tr>
      <th>2017-05-25 13:01:14.736000+00:00</th>
      <td>NaN</td>
      <td>NaN</td>
      <td>450.0</td>
      <td>450.0</td>
    </tr>
    <tr>
      <th>2017-05-25 13:01:15+00:00</th>
      <td>1.300000e+09</td>
      <td>-150000000.0</td>
      <td>NaN</td>
      <td>NaN</td>
    </tr>
  </tbody>
</table>
</div>



## mat2dict function
Import a mat file in a mat_struct (very similar to a dict) that can be easily browsed.


```python
aux=importData.mat2dict('/eos/user/s/sterbini/MD_ANALYSIS/2016/MD1780_80b/2016.10.26.22.23.42.135.mat')
aux.CPS_BLM
```




    <scipy.io.matlab.mio5_params.mat_struct at 0x7f84505ba128>



## mat2pd function


```python
aux=importData.mat2pd(['CPS_BLM.Acquisition.value.lastLosses'],['/eos/user/s/sterbini/MD_ANALYSIS/2016/MD1780_80b/2016.10.26.22.23.42.135.mat',
                                                   '/eos/user/s/sterbini/MD_ANALYSIS/2016/MD1780_80b/2016.10.26.22.23.06.147.mat'])
aux
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>CPS_BLM.Acquisition.value.lastLosses</th>
      <th>matlabFilePath</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2016-10-26 20:22:27.100000+00:00</th>
      <td>[53, 30, 95, 29, 12, 18, 3, 3, 12, 4, 2, 6, 0,...</td>
      <td>/eos/user/s/sterbini/MD_ANALYSIS/2016/MD1780_8...</td>
    </tr>
    <tr>
      <th>2016-10-26 20:23:03.100000+00:00</th>
      <td>[58, 32, 93, 29, 12, 18, 3, 3, 13, 4, 2, 6, 0,...</td>
      <td>/eos/user/s/sterbini/MD_ANALYSIS/2016/MD1780_8...</td>
    </tr>
  </tbody>
</table>
</div>



## tfs2pd function

With this funtion we can import the the MADX TFS file in a pandas Dataframe.


```python
aux=importData.tfs2pd('/eos/user/s/sterbini/MD_ANALYSIS/2018/LHC MD Optics/collisionAt25cm_180urad/lhcb1_thick.survey')
display(aux.transpose())
aux.iloc[0]['TABLE'].head()
```

    The column SLOT_ID is empty.
    The column ASSEMBLY_ID is empty.



<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>/eos/user/s/sterbini/MD_ANALYSIS/2018/LHC MD Optics/collisionAt25cm_180urad/lhcb1_thick.survey</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>DATE</th>
      <td>22/03/18</td>
    </tr>
    <tr>
      <th>ORIGIN</th>
      <td>5.03.07</td>
    </tr>
    <tr>
      <th>TIME</th>
      <td>22.30.11</td>
    </tr>
    <tr>
      <th>TITLE</th>
      <td>no-title</td>
    </tr>
    <tr>
      <th>TYPE</th>
      <td>SURVEY</td>
    </tr>
    <tr>
      <th>TABLE</th>
      <td>NAME      KEYWORD   ...</td>
    </tr>
  </tbody>
</table>
</div>





<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>NAME</th>
      <th>KEYWORD</th>
      <th>S</th>
      <th>L</th>
      <th>ANGLE</th>
      <th>X</th>
      <th>Y</th>
      <th>Z</th>
      <th>THETA</th>
      <th>PHI</th>
      <th>PSI</th>
      <th>GLOBALTILT</th>
      <th>MECH_SEP</th>
      <th>V_POS</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0.000</th>
      <td>IP1</td>
      <td>MARKER</td>
      <td>0.000</td>
      <td>0.000</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.000</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>3.000</th>
      <td>MBAS2.1R1</td>
      <td>SOLENOID</td>
      <td>3.000</td>
      <td>3.000</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>3.000</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>19.050</th>
      <td>DRIFT_0</td>
      <td>DRIFT</td>
      <td>19.050</td>
      <td>16.050</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>19.050</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>20.850</th>
      <td>TAS.1R1</td>
      <td>RCOLLIMATOR</td>
      <td>20.850</td>
      <td>1.800</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>20.850</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
    </tr>
    <tr>
      <th>21.564</th>
      <td>DRIFT_1</td>
      <td>DRIFT</td>
      <td>21.564</td>
      <td>0.714</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>21.564</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
      <td>0.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
aux=importData.tfs2pd('/eos/user/s/sterbini/MD_ANALYSIS/2018/LHC MD Optics/collisionAt25cm_180urad/lhcb1_thick.twiss')
aux.iloc[0]['TABLE'].head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>NAME</th>
      <th>PARENT</th>
      <th>KEYWORD</th>
      <th>S</th>
      <th>L</th>
      <th>X</th>
      <th>Y</th>
      <th>PX</th>
      <th>PY</th>
      <th>BETX</th>
      <th>BETY</th>
      <th>ALFX</th>
      <th>ALFY</th>
      <th>MUX</th>
      <th>MUY</th>
      <th>DX</th>
      <th>DY</th>
      <th>DPX</th>
      <th>DPY</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0.000</th>
      <td>IP1</td>
      <td>OMK</td>
      <td>MARKER</td>
      <td>0.000</td>
      <td>0.000</td>
      <td>-4.874885e-11</td>
      <td>-1.062519e-12</td>
      <td>-4.666460e-11</td>
      <td>0.00018</td>
      <td>0.250001</td>
      <td>0.250000</td>
      <td>-0.000002</td>
      <td>1.233730e-07</td>
      <td>0.000000</td>
      <td>0.000000</td>
      <td>0.026209</td>
      <td>0.005558</td>
      <td>0.01455</td>
      <td>0.039107</td>
    </tr>
    <tr>
      <th>3.000</th>
      <td>MBAS2.1R1</td>
      <td>MBAS2</td>
      <td>SOLENOID</td>
      <td>3.000</td>
      <td>3.000</td>
      <td>-1.887427e-10</td>
      <td>5.400004e-04</td>
      <td>-4.666460e-11</td>
      <td>0.00018</td>
      <td>36.249874</td>
      <td>36.249965</td>
      <td>-11.999956</td>
      <td>-1.199999e+01</td>
      <td>0.236767</td>
      <td>0.236768</td>
      <td>0.069860</td>
      <td>0.122338</td>
      <td>0.01455</td>
      <td>0.039107</td>
    </tr>
    <tr>
      <th>19.050</th>
      <td>DRIFT_0</td>
      <td>DRIFT</td>
      <td>DRIFT</td>
      <td>19.050</td>
      <td>16.050</td>
      <td>-9.377096e-10</td>
      <td>3.429003e-03</td>
      <td>-4.666460e-11</td>
      <td>0.00018</td>
      <td>1451.854515</td>
      <td>1451.858614</td>
      <td>-76.199710</td>
      <td>-7.619993e+01</td>
      <td>0.247911</td>
      <td>0.247911</td>
      <td>0.303393</td>
      <td>0.747112</td>
      <td>0.01455</td>
      <td>0.039107</td>
    </tr>
    <tr>
      <th>20.850</th>
      <td>TAS.1R1</td>
      <td>TAS</td>
      <td>RCOLLIMATOR</td>
      <td>20.850</td>
      <td>1.800</td>
      <td>-1.021706e-09</td>
      <td>3.753003e-03</td>
      <td>-4.666460e-11</td>
      <td>0.00018</td>
      <td>1739.133423</td>
      <td>1739.138340</td>
      <td>-83.399683</td>
      <td>-8.339992e+01</td>
      <td>0.248091</td>
      <td>0.248092</td>
      <td>0.329583</td>
      <td>0.817180</td>
      <td>0.01455</td>
      <td>0.039107</td>
    </tr>
    <tr>
      <th>21.564</th>
      <td>DRIFT_1</td>
      <td>DRIFT</td>
      <td>DRIFT</td>
      <td>21.564</td>
      <td>0.714</td>
      <td>-1.055024e-09</td>
      <td>3.881523e-03</td>
      <td>-4.666460e-11</td>
      <td>0.00018</td>
      <td>1860.267346</td>
      <td>1860.272608</td>
      <td>-86.255672</td>
      <td>-8.625592e+01</td>
      <td>0.248155</td>
      <td>0.248155</td>
      <td>0.339972</td>
      <td>0.844974</td>
      <td>0.01455</td>
      <td>0.039107</td>
    </tr>
  </tbody>
</table>
</div>




```python
aux=importData.tfs2pd(['/eos/user/s/sterbini/MD_ANALYSIS/2018/LHC MD Optics/collisionAt25cm_180urad/lhcb1_thick.survey','/eos/user/s/sterbini/MD_ANALYSIS/2018/LHC MD Optics/collisionAt25cm_180urad/lhcb1_thick.twiss'])
```

    The column SLOT_ID is empty.
    The column ASSEMBLY_ID is empty.



```python
aux.iloc[0]['TABLE'].index
```




    Float64Index([       0.0,        3.0,      19.05,      20.85,     21.564,
                      21.564,     21.564,      21.62,      21.62,     21.724,
                  ...
                  26637.2482, 26637.2482, 26637.3042, 26637.3042, 26637.3042,
                  26638.0042, 26639.8042, 26655.8832, 26658.8832, 26658.8832],
                 dtype='float64', length=13283)



## From dotdict to a simple pandas Dataframe.
We can transform a dotdict in a pandas Dataframe.


```python
from cl2pd import dotdict
a=dotdict.dotdict()
np=importData.np
for i in {'B1'}:
    a[i]=dotdict.dotdict()
    for j in range(10):
        a[i]['b'+str(j)]=np.random.randn(10)
        
for i in {'B2'}:
    a[i]=dotdict.dotdict()
    for j in range(20):
        a[i]['b'+str(j)]=np.random.randn(10)
```


```python
pd.DataFrame(a)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>B1</th>
      <th>B2</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>b0</th>
      <td>[0.6467321347905923, -1.0370676505843401, -1.5...</td>
      <td>[1.5270813764876534, 1.2100639941492304, -1.11...</td>
    </tr>
    <tr>
      <th>b1</th>
      <td>[0.32969179854420916, 0.7858822252604646, -0.3...</td>
      <td>[1.3064310857233223, -0.3359487143927113, 0.66...</td>
    </tr>
    <tr>
      <th>b10</th>
      <td>NaN</td>
      <td>[-0.6957631261643009, 0.32370222762197626, 2.8...</td>
    </tr>
    <tr>
      <th>b11</th>
      <td>NaN</td>
      <td>[-0.779053908334011, 1.506635143971471, -0.233...</td>
    </tr>
    <tr>
      <th>b12</th>
      <td>NaN</td>
      <td>[2.0691097429781133, -1.528249449203943, 0.477...</td>
    </tr>
    <tr>
      <th>b13</th>
      <td>NaN</td>
      <td>[-0.3705018424449959, -1.1877717848803848, -1....</td>
    </tr>
    <tr>
      <th>b14</th>
      <td>NaN</td>
      <td>[-0.9949066537823384, -0.35825900064144167, -0...</td>
    </tr>
    <tr>
      <th>b15</th>
      <td>NaN</td>
      <td>[0.21650090161488317, -0.5518600501885274, 0.5...</td>
    </tr>
    <tr>
      <th>b16</th>
      <td>NaN</td>
      <td>[-0.6802440968350015, 0.7573210133031285, 0.53...</td>
    </tr>
    <tr>
      <th>b17</th>
      <td>NaN</td>
      <td>[-2.2167781994862197, 0.6851372659010567, -0.0...</td>
    </tr>
    <tr>
      <th>b18</th>
      <td>NaN</td>
      <td>[-1.7360108910276053, 0.3803327796866624, -1.6...</td>
    </tr>
    <tr>
      <th>b19</th>
      <td>NaN</td>
      <td>[0.9476187485046135, 0.640806434726803, 0.5297...</td>
    </tr>
    <tr>
      <th>b2</th>
      <td>[-1.7182497708209206, 0.2865187814502491, -0.7...</td>
      <td>[0.7847226477419496, -0.3710598283057992, -0.0...</td>
    </tr>
    <tr>
      <th>b3</th>
      <td>[-1.469273880140067, 0.4212359180496405, 1.037...</td>
      <td>[0.05188168046413233, 1.4355051492894526, 2.11...</td>
    </tr>
    <tr>
      <th>b4</th>
      <td>[0.007974681615498608, -0.2610696151209144, 0....</td>
      <td>[0.9520874475610571, -1.2910502451899022, -1.7...</td>
    </tr>
    <tr>
      <th>b5</th>
      <td>[0.49349416323479206, 0.8088796123324145, 0.19...</td>
      <td>[-0.44431802009622257, 0.4787313869654191, 0.1...</td>
    </tr>
    <tr>
      <th>b6</th>
      <td>[0.5405258900294131, -0.5084350523854257, 0.14...</td>
      <td>[-0.7898382541838375, 0.11480202852339073, -0....</td>
    </tr>
    <tr>
      <th>b7</th>
      <td>[-0.11305147458494907, 0.03314302262877794, -0...</td>
      <td>[0.896925953242836, -0.14222940386632202, 0.18...</td>
    </tr>
    <tr>
      <th>b8</th>
      <td>[0.9405878186855667, 1.1799075215016352, 1.087...</td>
      <td>[1.3164852160372085, -1.1817663457608947, 0.03...</td>
    </tr>
    <tr>
      <th>b9</th>
      <td>[-0.11927058554763645, -0.0652152524769821, 0....</td>
      <td>[-0.4518129428607217, -0.16586580625802247, -0...</td>
    </tr>
  </tbody>
</table>
</div>



# Concat check


```python
variables=['LHC.BCTDC.A6R4.B1:BEAM_INTENSITY']
startTime = pd.Timestamp('2017-10-01 17:30', tz='CET')
endTime = pd.Timestamp('2017-10-01 17:31', tz='CET')
raw_data1 = importData.cals2pd(variables,startTime,endTime)
variables=['LHC.BCTDC.A6R4.B2:BEAM_INTENSITY']
raw_data2 = importData.cals2pd(variables,startTime,endTime)
pd.merge(raw_data1,raw_data2,left_index=True,right_index=True).head()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>LHC.BCTDC.A6R4.B1:BEAM_INTENSITY</th>
      <th>LHC.BCTDC.A6R4.B2:BEAM_INTENSITY</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>2017-10-01 15:30:00+00:00</th>
      <td>1.470000e+09</td>
      <td>270000000.0</td>
    </tr>
    <tr>
      <th>2017-10-01 15:30:01+00:00</th>
      <td>1.810000e+09</td>
      <td>310000000.0</td>
    </tr>
    <tr>
      <th>2017-10-01 15:30:02+00:00</th>
      <td>1.690000e+09</td>
      <td>-310000000.0</td>
    </tr>
    <tr>
      <th>2017-10-01 15:30:03+00:00</th>
      <td>1.990000e+09</td>
      <td>-720000000.0</td>
    </tr>
    <tr>
      <th>2017-10-01 15:30:04+00:00</th>
      <td>1.270000e+09</td>
      <td>220000000.0</td>
    </tr>
  </tbody>
</table>
</div>




```python
# Interesting checks 
# pd.Timestamp('2018-03-25 02:00', tz='CET')      #this date does not exist, so the function returns an error.
# pd.Timestamp('2015-06-30 23:59:60', tz='UTC')   #this date does exist (leap second) but it is not recognized.
```

# In case of problem with pytimber


```python
if 0:
    import cmmnbuild_dep_manager
    mgr = cmmnbuild_dep_manager.Manager()
    mgr.install('pytimber')
    mgr.resolve()
```


```python
from cl2pd import variablesDF
variablesDF.LHC
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Variable</th>
      <th>Tag</th>
      <th>Type</th>
      <th>On change</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>LHC.BOFSU:OFC_ENERGY</td>
      <td>Energy</td>
      <td>NUMERIC</td>
      <td>True</td>
      <td>Beam energy</td>
    </tr>
    <tr>
      <th>1</th>
      <td>LHC.BCTFR.A6R4.B1:BEAM_INTENSITY</td>
      <td>Intensity</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>B1 intensity</td>
    </tr>
    <tr>
      <th>2</th>
      <td>LHC.BCTFR.A6R4.B2:BEAM_INTENSITY</td>
      <td>Intensity</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>B2 intensity</td>
    </tr>
    <tr>
      <th>3</th>
      <td>LHC.RUNCONFIG:IP1-XING-V-MURAD</td>
      <td>Crossing angle</td>
      <td>NUMERIC</td>
      <td>True</td>
      <td>Crossing angle IP1 V</td>
    </tr>
    <tr>
      <th>4</th>
      <td>LHC.RUNCONFIG:IP2-XING-V-MURAD</td>
      <td>Crossing angle</td>
      <td>NUMERIC</td>
      <td>True</td>
      <td>Crossing angle IP2 V</td>
    </tr>
    <tr>
      <th>5</th>
      <td>LHC.RUNCONFIG:IP5-XING-H-MURAD</td>
      <td>Crossing angle</td>
      <td>NUMERIC</td>
      <td>True</td>
      <td>Crossing angle IP5 H</td>
    </tr>
    <tr>
      <th>6</th>
      <td>LHC.RUNCONFIG:IP8-XING-H-MURAD</td>
      <td>Crossing angle</td>
      <td>NUMERIC</td>
      <td>True</td>
      <td>Crossing angle IP8 H</td>
    </tr>
    <tr>
      <th>7</th>
      <td>LHC.BQM.B1:BUNCH_LENGTH_MEAN</td>
      <td>Bunch length</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Bunch Length B1</td>
    </tr>
    <tr>
      <th>8</th>
      <td>LHC.BQM.B2:BUNCH_LENGTH_MEAN</td>
      <td>Bunch length</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Bunch Length B2</td>
    </tr>
    <tr>
      <th>9</th>
      <td>LHC.BQBBQ.CONTINUOUS.B1:TUNE_V</td>
      <td>Tune</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Tune V B1 BBQ</td>
    </tr>
    <tr>
      <th>10</th>
      <td>LHC.BQBBQ.CONTINUOUS.B2:TUNE_V</td>
      <td>Tune</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Tune V B2 BBQ</td>
    </tr>
    <tr>
      <th>11</th>
      <td>LHC.BQBBQ.CONTINUOUS_HS.B1:TUNE_V</td>
      <td>Tune</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Most sensitive tune B1 V</td>
    </tr>
    <tr>
      <th>12</th>
      <td>LHC.BQBBQ.CONTINUOUS_HS.B2:TUNE_V</td>
      <td>Tune</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Most sensitive tune B2 V</td>
    </tr>
    <tr>
      <th>13</th>
      <td>LHC.BQBBQ.CONTINUOUS.B1:TUNE_H</td>
      <td>Tune</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Tune H B1 BBQ</td>
    </tr>
    <tr>
      <th>14</th>
      <td>LHC.BQBBQ.CONTINUOUS.B2:TUNE_H</td>
      <td>Tune</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Tune H B2 BBQ</td>
    </tr>
    <tr>
      <th>15</th>
      <td>LHC.BQBBQ.CONTINUOUS_HS.B1:TUNE_H</td>
      <td>Tune</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Most sensitive tune B1 H</td>
    </tr>
    <tr>
      <th>16</th>
      <td>LHC.BQBBQ.CONTINUOUS_HS.B2:TUNE_H</td>
      <td>Tune</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Most sensitive tune B2 H</td>
    </tr>
    <tr>
      <th>17</th>
      <td>LHC.BQBBQ.CONTINUOUS.B1:FFT_DATA_H</td>
      <td>Tune FFT</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Tune FFT H B1</td>
    </tr>
    <tr>
      <th>18</th>
      <td>LHC.BQBBQ.CONTINUOUS.B1:FFT_DATA_V</td>
      <td>Tune FFT</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Tune FFT V B1</td>
    </tr>
    <tr>
      <th>19</th>
      <td>LHC.BQBBQ.CONTINUOUS.B2:FFT_DATA_H</td>
      <td>Tune FFT</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Tune FFT H B2</td>
    </tr>
    <tr>
      <th>20</th>
      <td>LHC.BQBBQ.CONTINUOUS.B2:FFT_DATA_V</td>
      <td>Tune FFT</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Tune FFT V B2</td>
    </tr>
    <tr>
      <th>21</th>
      <td>LHC.BCTFR.A6R4.B1:BUNCH_INTENSITY</td>
      <td>Intensity</td>
      <td>VECTOR NUMERIC</td>
      <td>False</td>
      <td>Main FBCT B1</td>
    </tr>
    <tr>
      <th>22</th>
      <td>LHC.BCTFR.A6R4.B2:BUNCH_INTENSITY</td>
      <td>Intensity</td>
      <td>VECTOR NUMERIC</td>
      <td>False</td>
      <td>Main FBCT B2</td>
    </tr>
    <tr>
      <th>23</th>
      <td>LHC.BCTFR.B6R4.B1:BUNCH_INTENSITY</td>
      <td>Intensity</td>
      <td>VECTOR NUMERIC</td>
      <td>False</td>
      <td>Spare FBCT B1</td>
    </tr>
    <tr>
      <th>24</th>
      <td>LHC.BCTFR.A6R4.B2:BUNCH_INTENSITY</td>
      <td>Intensity</td>
      <td>VECTOR NUMERIC</td>
      <td>False</td>
      <td>Spare FBCT B2</td>
    </tr>
    <tr>
      <th>25</th>
      <td>LHC.BCTFR.A6R4.B1:BUNCH_FILL_PATTERN</td>
      <td>Filling Pattern</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Main FBCT B1</td>
    </tr>
    <tr>
      <th>26</th>
      <td>LHC.BCTFR.A6R4.B2:BUNCH_FILL_PATTERN</td>
      <td>Filling Pattern</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Main FBCT B2</td>
    </tr>
    <tr>
      <th>27</th>
      <td>LHC.BCTFR.B6R4.B1:BUNCH_FILL_PATTERN</td>
      <td>Filling Pattern</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Spare FBCT B1</td>
    </tr>
    <tr>
      <th>28</th>
      <td>LHC.BCTFR.A6R4.B2:BUNCH_FILL_PATTERN</td>
      <td>Filling Pattern</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Spare FBCT B2</td>
    </tr>
    <tr>
      <th>29</th>
      <td>CMS:BUNCH_LUMI_INST</td>
      <td>Luminosity</td>
      <td>VECTOR NUMERIC</td>
      <td>False</td>
      <td>CMS bbb luminosity</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>124</th>
      <td>ADTH.SR4.M1.B2:MDSPU_PHASE1</td>
      <td>ADT Phase</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>B2 Hozirontal ADT Phase 1 Mon1 (deg)</td>
    </tr>
    <tr>
      <th>125</th>
      <td>ADTH.SR4.M1.B2:MDSPU_PHASE2</td>
      <td>ADT Phase</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>B2 Hozirontal ADT Phase 2 Mon1 (deg)</td>
    </tr>
    <tr>
      <th>126</th>
      <td>ADTH.SR4.M1.B2:MDSPU_PHASE3</td>
      <td>ADT Phase</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>B2 Hozirontal ADT Phase 3 Mon1 (deg)</td>
    </tr>
    <tr>
      <th>127</th>
      <td>ADTH.SR4.M1.B2:MDSPU_PHASE4</td>
      <td>ADT Phase</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>B2 Hozirontal ADT Phase 4 Mon1 (deg)</td>
    </tr>
    <tr>
      <th>128</th>
      <td>ADTV.SR4.M1.B1:MDSPU_PHASE1</td>
      <td>ADT Phase</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>B2 Vertical ADT Phase 1 Mon1 (deg)</td>
    </tr>
    <tr>
      <th>129</th>
      <td>ADTV.SR4.M1.B1:MDSPU_PHASE2</td>
      <td>ADT Phase</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>B2 Vertical ADT Phase 2 Mon1 (deg)</td>
    </tr>
    <tr>
      <th>130</th>
      <td>ADTV.SR4.M1.B1:MDSPU_PHASE3</td>
      <td>ADT Phase</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>B2 Vertical ADT Phase 3 Mon1 (deg)</td>
    </tr>
    <tr>
      <th>131</th>
      <td>ADTV.SR4.M1.B1:MDSPU_PHASE4</td>
      <td>ADT Phase</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>B2 Vertical ADT Phase 4 Mon1 (deg)</td>
    </tr>
    <tr>
      <th>132</th>
      <td>ADTH.SR4.B1:TRANSVERSEACTIVITYMAX_HB1</td>
      <td>ADT Activity</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>B1 Horizontal ADT Transverse Activity (um)</td>
    </tr>
    <tr>
      <th>133</th>
      <td>ADTV.SR4.B1:TRANSVERSEACTIVITYMAX_VB1</td>
      <td>ADT Activity</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>B1 Vertical ADT Transverse Activity (um)</td>
    </tr>
    <tr>
      <th>134</th>
      <td>ADTH.SR4.B2:TRANSVERSEACTIVITYMAX_HB2</td>
      <td>ADT Activity</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>B2 Horizontal ADT Transverse Activity (um)</td>
    </tr>
    <tr>
      <th>135</th>
      <td>ADTV.SR4.B2:TRANSVERSEACTIVITYMAX_VB2</td>
      <td>ADT Activity</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>B2 Vertical ADT Transverse Activity (um)</td>
    </tr>
    <tr>
      <th>136</th>
      <td>LHC.BOFSU:DEFLECTIONS_H</td>
      <td>Orbit</td>
      <td>VECTORNUMERIC</td>
      <td>False</td>
      <td>COD deflections in horizontal plane (urad)</td>
    </tr>
    <tr>
      <th>137</th>
      <td>LHC.BOFSU:DEFLECTIONS_V</td>
      <td>Orbit</td>
      <td>VECTORNUMERIC</td>
      <td>False</td>
      <td>COD deflections in vertical plane (urad)</td>
    </tr>
    <tr>
      <th>138</th>
      <td>LHC.BPTUH.A4L5.B1:CALIBLINEARPOS</td>
      <td>Orbit</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Upstream Horizontal TCT.A4L5.B1 Calibrated Lin...</td>
    </tr>
    <tr>
      <th>139</th>
      <td>LHC.BPTUV.A4L5.B1:CALIBLINEARPOS</td>
      <td>Orbit</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Upstream Vertical TCT.A4L5.B1 Calibrated Linea...</td>
    </tr>
    <tr>
      <th>140</th>
      <td>LHC.BPTUH.A4R5.B2:CALIBLINEARPOS</td>
      <td>Orbit</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Upstream Horizontal TCT.A4R5.B2 Calibrated Lin...</td>
    </tr>
    <tr>
      <th>141</th>
      <td>LHC.BPTUV.A4R5.B2:CALIBLINEARPOS</td>
      <td>Orbit</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Upstream Vertical TCT.A4R5.B2 Calibrated Linea...</td>
    </tr>
    <tr>
      <th>142</th>
      <td>LHC.BPTDH.A4L5.B1:CALIBLINEARPOS</td>
      <td>Orbit</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Downstream Horizontal TCT.A4L5.B1 Calibrated L...</td>
    </tr>
    <tr>
      <th>143</th>
      <td>LHC.BPTDV.A4L5.B1:CALIBLINEARPOS</td>
      <td>Orbit</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Downstream Vertical TCT.A4L5.B1 Calibrated Lin...</td>
    </tr>
    <tr>
      <th>144</th>
      <td>LHC.BPTDH.A4R5.B2:CALIBLINEARPOS</td>
      <td>Orbit</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Downstream Horizontal TCT.A4R5.B2 Calibrated L...</td>
    </tr>
    <tr>
      <th>145</th>
      <td>LHC.BPTDV.A4R5.B2:CALIBLINEARPOS</td>
      <td>Orbit</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Downstream Vertical TCT.A4R5.B2 Calibrated Lin...</td>
    </tr>
    <tr>
      <th>146</th>
      <td>LHC.BPTUH.A4L1.B1:CALIBLINEARPOS</td>
      <td>Orbit</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Upstream Horizontal TCT.A4L1.B1 Calibrated Lin...</td>
    </tr>
    <tr>
      <th>147</th>
      <td>LHC.BPTUV.A4L1.B1:CALIBLINEARPOS</td>
      <td>Orbit</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Upstream Vertical TCT.A4L1.B1 Calibrated Linea...</td>
    </tr>
    <tr>
      <th>148</th>
      <td>LHC.BPTUH.A4R1.B2:CALIBLINEARPOS</td>
      <td>Orbit</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Upstream Horizontal TCT.A4R1.B2 Calibrated Lin...</td>
    </tr>
    <tr>
      <th>149</th>
      <td>LHC.BPTUV.A4R1.B2:CALIBLINEARPOS</td>
      <td>Orbit</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Upstream Vertical TCT.A4R1.B2 Calibrated Linea...</td>
    </tr>
    <tr>
      <th>150</th>
      <td>LHC.BPTDH.A4L1.B1:CALIBLINEARPOS</td>
      <td>Orbit</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Downstream Horizontal TCT.A4L1.B1 Calibrated L...</td>
    </tr>
    <tr>
      <th>151</th>
      <td>LHC.BPTDV.A4L1.B1:CALIBLINEARPOS</td>
      <td>Orbit</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Downstream Vertical TCT.A4L1.B1 Calibrated Lin...</td>
    </tr>
    <tr>
      <th>152</th>
      <td>LHC.BPTDH.A4R1.B2:CALIBLINEARPOS</td>
      <td>Orbit</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Downstream Horizontal TCT.A4R1.B2 Calibrated L...</td>
    </tr>
    <tr>
      <th>153</th>
      <td>LHC.BPTDV.A4R1.B2:CALIBLINEARPOS</td>
      <td>Orbit</td>
      <td>NUMERIC</td>
      <td>False</td>
      <td>Downstream Vertical TCT.A4R1.B2 Calibrated Lin...</td>
    </tr>
  </tbody>
</table>
<p>154 rows × 5 columns</p>
</div>



# LPC website
A rich source of information is the [LPC website](http://lpc.web.cern.ch/). 
We will show how to import its data in pd dataframes.

### Getting the filling table


```python
def LPCFillTable(year=2018):
    '''
    It return the LPC Fill Table of the specified year.
    Note that the fill table is available only for a limited number of year.
    '''
    import urllib.request, json 
    with urllib.request.urlopen('http://lpc.web.cern.ch/cgi-bin/fillTableReader.py?action=load&year='+str(year)) as url:
        data = json.loads(url.read().decode())
    if len(data['data'])>0: 
        myDF=pd.DataFrame(data['data']).transpose()[['start_sb','length_sb','type','scheme','end','ta','fl','remarks']]
        return myDF.sort_index()
    else:
        return pd.DataFrame()
```


```python
LPCFillTable()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>start_sb</th>
      <th>length_sb</th>
      <th>type</th>
      <th>scheme</th>
      <th>end</th>
      <th>ta</th>
      <th>fl</th>
      <th>remarks</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>6469</th>
      <td>2018-03-22 18:47:32</td>
      <td>0:50:55</td>
      <td>special</td>
      <td>alternating R1 R2 pilot</td>
      <td>operator</td>
      <td>True</td>
      <td>True</td>
      <td>handshake tests</td>
    </tr>
    <tr>
      <th>6570</th>
      <td>2018-04-17 12:54:14</td>
      <td>6:36:36</td>
      <td>rampup</td>
      <td>Single_3b_2_2_2</td>
      <td>operator</td>
      <td>True</td>
      <td>True</td>
      <td>First STABLE BEAMS in 2018. At the start befor...</td>
    </tr>
    <tr>
      <th>6573</th>
      <td>2018-04-18 00:29:29</td>
      <td>6:20:49</td>
      <td>rampup</td>
      <td>Single_3b_2_2_2</td>
      <td>operator</td>
      <td>True</td>
      <td>True</td>
      <td>Second fill with crossing angle leveling and s...</td>
    </tr>
    <tr>
      <th>6574</th>
      <td>2018-04-18 22:28:06</td>
      <td>11:33:43</td>
      <td>rampup</td>
      <td>Single_12b_8_8_8_2018</td>
      <td>operator</td>
      <td>True</td>
      <td>True</td>
      <td>Last fill of first intensity step. At the end ...</td>
    </tr>
    <tr>
      <th>6579</th>
      <td>2018-04-20 00:22:38</td>
      <td>11:58:16</td>
      <td>rampup</td>
      <td>25ns_75b_62_32_62_12bpi_9inj</td>
      <td>RF</td>
      <td>True</td>
      <td>True</td>
      <td>First fill with trains. At the end ß* leveling...</td>
    </tr>
    <tr>
      <th>6583</th>
      <td>2018-04-20 19:48:23</td>
      <td>5:58:46</td>
      <td>rampup</td>
      <td>25ns_75b_62_32_62_12bpi_9inj</td>
      <td>others</td>
      <td>True</td>
      <td>True</td>
      <td>Dump by COD interlock. Lumi was lower since th...</td>
    </tr>
    <tr>
      <th>6584</th>
      <td>2018-04-21 04:04:20</td>
      <td>6:50:59</td>
      <td>rampup</td>
      <td>no_value</td>
      <td>operator</td>
      <td>True</td>
      <td>True</td>
      <td>ß* leveling in SB. Lumi in LHCb moved the same...</td>
    </tr>
    <tr>
      <th>6592</th>
      <td>2018-04-21 23:15:46</td>
      <td>2:54:31</td>
      <td>rampup</td>
      <td>Single_10b_9_2_2_BSRT_Calib_RampUp</td>
      <td>operator</td>
      <td>True</td>
      <td>True</td>
      <td>Emittance between 1.5 and 4.5 µm</td>
    </tr>
    <tr>
      <th>6594</th>
      <td>2018-04-22 09:29:22</td>
      <td>9:14:28</td>
      <td>rampup</td>
      <td>25ns_339b_326_206_266_12bpi_31inj</td>
      <td>operator</td>
      <td>True</td>
      <td>True</td>
      <td>Was stopped due to access request of ATLAS for...</td>
    </tr>
    <tr>
      <th>6595</th>
      <td>2018-04-22 22:14:52</td>
      <td>9:04:39</td>
      <td>rampup</td>
      <td>25ns_339b_326_206_266_12bpi_31inj</td>
      <td>operator</td>
      <td>True</td>
      <td>True</td>
      <td>CMS took some low µ data. After this fill one ...</td>
    </tr>
    <tr>
      <th>6611</th>
      <td>2018-04-24 22:35:27</td>
      <td>9:25:22</td>
      <td>rampup</td>
      <td>25ns_315b_302_237_240_48bpi_11inj</td>
      <td>operator</td>
      <td>True</td>
      <td>True</td>
      <td>Loss spike due to 16L2 reached 40% during ramp...</td>
    </tr>
    <tr>
      <th>6612</th>
      <td>2018-04-25 14:16:59</td>
      <td>8:36:17</td>
      <td>rampup</td>
      <td>25ns_603b_590_524_542_48bpi_17inj</td>
      <td>operator</td>
      <td>True</td>
      <td>True</td>
      <td></td>
    </tr>
    <tr>
      <th>6613</th>
      <td>2018-04-26 01:23:52</td>
      <td>0:29:22</td>
      <td>rampup</td>
      <td>25ns_603b_590_524_542_48bpi_17inj</td>
      <td>others</td>
      <td>True</td>
      <td>True</td>
      <td>BTV interlock. Crate exchanged</td>
    </tr>
    <tr>
      <th>6614</th>
      <td>2018-04-26 03:46:32</td>
      <td>5:24:46</td>
      <td>rampup</td>
      <td>25ns_603b_590_524_542_48bpi_17inj</td>
      <td>pwr converter</td>
      <td>True</td>
      <td>True</td>
      <td>RCBCVS5.R2B1</td>
    </tr>
    <tr>
      <th>6615</th>
      <td>2018-04-26 19:54:06</td>
      <td>5:11:26</td>
      <td>rampup</td>
      <td>25ns_603b_590_526_547_96bpi_13inj</td>
      <td>operator</td>
      <td>True</td>
      <td>True</td>
      <td>operator</td>
    </tr>
    <tr>
      <th>6616</th>
      <td>2018-04-27 04:44:15</td>
      <td>8:10:27</td>
      <td>rampup</td>
      <td>25ns_987b_974_876_912_96bpi_17inj</td>
      <td>operator</td>
      <td>True</td>
      <td>True</td>
      <td>ALICE lower solenoid field (12kA instead of 30kA)</td>
    </tr>
    <tr>
      <th>6617</th>
      <td>2018-04-27 16:40:40</td>
      <td>8:40:53</td>
      <td>rampup</td>
      <td>25ns_987b_974_878_917_144bpi_13inj</td>
      <td>operator</td>
      <td>False</td>
      <td>True</td>
      <td>During first filling trial one bunch was missi...</td>
    </tr>
    <tr>
      <th>6618</th>
      <td>2018-04-28 05:05:13</td>
      <td>4:03:42</td>
      <td>rampup</td>
      <td>25ns_987b_974_878_917_144bpi_13inj</td>
      <td>operator</td>
      <td>True</td>
      <td>True</td>
      <td></td>
    </tr>
    <tr>
      <th>6620</th>
      <td>2018-04-28 21:31:28</td>
      <td>5:12:16</td>
      <td>rampup</td>
      <td>25ns_1227b_1214_1054_1102_144bpi_14inj</td>
      <td>16L2</td>
      <td>True</td>
      <td>True</td>
      <td>...number one... Beam 2 dumped. ADT B1 H was off</td>
    </tr>
    <tr>
      <th>6621</th>
      <td>2018-04-29 05:17:00</td>
      <td>10:19:29</td>
      <td>rampup</td>
      <td>25ns_1227b_1214_1054_1102_144bpi_14inj</td>
      <td>operator</td>
      <td>False</td>
      <td>True</td>
      <td>IP2 IP8 lumi was changing when crossing angle ...</td>
    </tr>
    <tr>
      <th>6624</th>
      <td>2018-04-30 01:52:55</td>
      <td>11:12:05</td>
      <td>rampup</td>
      <td>25ns_1227b_1214_1054_1102_144bpi_14inj</td>
      <td>operator</td>
      <td>False</td>
      <td>True</td>
      <td></td>
    </tr>
    <tr>
      <th>6628</th>
      <td>2018-04-30 21:07:28</td>
      <td>12:01:33</td>
      <td>physics</td>
      <td>25ns_1551b_1538_1404_1467_144bpi_16inj</td>
      <td>operator</td>
      <td>False</td>
      <td>True</td>
      <td>L=1.3e34; first fill with continuous crossing ...</td>
    </tr>
    <tr>
      <th>6629</th>
      <td>2018-05-01 12:07:32</td>
      <td>10:38:32</td>
      <td>physics</td>
      <td>25ns_1887b_1874_1694_1772_144bpi_19inj</td>
      <td>pwr converter</td>
      <td>False</td>
      <td>True</td>
      <td>L=1.6e34; trip of orbit corrector; afterwards ...</td>
    </tr>
    <tr>
      <th>6633</th>
      <td>2018-05-02 06:08:50</td>
      <td>0:47:31</td>
      <td>physics</td>
      <td>25ns_1887b_1874_1694_1772_144bpi_19inj</td>
      <td>RF</td>
      <td>False</td>
      <td>True</td>
      <td>During filling the first time uncaptured beam ...</td>
    </tr>
    <tr>
      <th>6636</th>
      <td>2018-05-02 11:41:44</td>
      <td>2:41:30</td>
      <td>physics</td>
      <td>25ns_1887b_1874_1694_1772_144bpi_19inj</td>
      <td>pwr converter</td>
      <td>False</td>
      <td>False</td>
      <td>Orbit Corrector trip; communication fault.&amp;nbs...</td>
    </tr>
    <tr>
      <th>6638</th>
      <td>2018-05-02 23:30:33</td>
      <td>14:20:52</td>
      <td>physics</td>
      <td>25ns_2175b_2162_1874_1964_144bpi_19inj</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td>Abort gap keeper changed to 32811. RF voltage ...</td>
    </tr>
    <tr>
      <th>6639</th>
      <td>2018-05-03 16:57:34</td>
      <td>11:54:41</td>
      <td>physics</td>
      <td>25ns_2319b_2306_1964_2060_144bpi_22inj</td>
      <td>pwr converter</td>
      <td>False</td>
      <td>False</td>
      <td>Lower currents since settings in Injectors wer...</td>
    </tr>
    <tr>
      <th>6640</th>
      <td>2018-05-04 10:07:21</td>
      <td>9:09:26</td>
      <td>physics</td>
      <td>25ns_2460b_2448_2052_2154_144bpi_19injv2</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td>Lower intenity due to PS RF issue.</td>
    </tr>
    <tr>
      <th>6641</th>
      <td>2018-05-04 21:34:11</td>
      <td>7:06:39</td>
      <td>physics</td>
      <td>25ns_2460b_2448_2052_2154_144bpi_19injv2</td>
      <td>QPS</td>
      <td>False</td>
      <td>False</td>
      <td>trip of RCBXH3.L5 and RCBXH3.L5 triggered by Q...</td>
    </tr>
    <tr>
      <th>6642</th>
      <td>2018-05-05 07:07:09</td>
      <td>3:56:27</td>
      <td>physics</td>
      <td>25ns_2460b_2448_2052_2154_144bpi_19injv2</td>
      <td>others</td>
      <td>False</td>
      <td>False</td>
      <td>BLM dumped the beam (SUE candidarw)</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
      <td>...</td>
    </tr>
    <tr>
      <th>7449</th>
      <td>2018-11-16 18:31:13</td>
      <td>7:23:15</td>
      <td>ions</td>
      <td>100_150ns_648Pb_620_619_52_36bpi_20inj_V2</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td></td>
    </tr>
    <tr>
      <th>7450</th>
      <td>2018-11-17 04:30:21</td>
      <td>7:20:12</td>
      <td>ions</td>
      <td>100_150ns_648Pb_620_619_52_36bpi_20inj_V2</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td>After wards fill 7451 was again lost due to lo...</td>
    </tr>
    <tr>
      <th>7453</th>
      <td>2018-11-18 07:36:17</td>
      <td>11:14:01</td>
      <td>ions</td>
      <td>100_150ns_648Pb_620_619_52_36bpi_20inj_V2</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td>kept long due to Linac-III source and LEIR pro...</td>
    </tr>
    <tr>
      <th>7454</th>
      <td>2018-11-19 00:35:24</td>
      <td>11:21:55</td>
      <td>ions</td>
      <td>100_150ns_648Pb_620_619_52_36bpi_20inj_V2</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td></td>
    </tr>
    <tr>
      <th>7456</th>
      <td>2018-11-19 22:21:10</td>
      <td>6:51:04</td>
      <td>ions</td>
      <td>100_150ns_648Pb_620_619_52_36bpi_20inj_V2</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td></td>
    </tr>
    <tr>
      <th>7457</th>
      <td>2018-11-20 11:03:57</td>
      <td>7:16:12</td>
      <td>ions</td>
      <td>100_150ns_648Pb_620_619_52_36bpi_20inj_V2</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td></td>
    </tr>
    <tr>
      <th>7459</th>
      <td>2018-11-21 00:09:47</td>
      <td>0:09:48</td>
      <td>ions</td>
      <td>75_150ns_460Pb_460_456_304_36bpi_17inj</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td>First ramp up fill with reversed polarity in A...</td>
    </tr>
    <tr>
      <th>7460</th>
      <td>2018-11-21 02:41:29</td>
      <td>4:51:48</td>
      <td>ions</td>
      <td>75_150ns_460Pb_460_456_304_36bpi_17inj</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td>Second ramp up fill with reversed polarity in ...</td>
    </tr>
    <tr>
      <th>7464</th>
      <td>2018-11-22 03:58:13</td>
      <td>6:31:19</td>
      <td>ions</td>
      <td>75_150ns_460Pb_460_456_304_42bpi_13inj</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td>Third ramp up fill.</td>
    </tr>
    <tr>
      <th>7466</th>
      <td>2018-11-22 19:08:59</td>
      <td>8:37:46</td>
      <td>ions</td>
      <td>75_150ns_670Pb_607_630_384_42bpi_18inj</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td>Levelling IP 1&amp;amp;5 at 3.5e27. Reduced fillin...</td>
    </tr>
    <tr>
      <th>7467</th>
      <td>2018-11-23 09:58:59</td>
      <td>7:58:52</td>
      <td>ions</td>
      <td>75_150ns_733Pb_733_702_468_42bpi_20inj</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td>Levelling IP 1&amp;amp;5 at 4e27.</td>
    </tr>
    <tr>
      <th>7468</th>
      <td>2018-11-23 20:43:11</td>
      <td>9:50:12</td>
      <td>ions</td>
      <td>75_150ns_733Pb_733_702_468_42bpi_20inj</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td>Levelling IP 1&amp;amp;5 at 5e27.</td>
    </tr>
    <tr>
      <th>7471</th>
      <td>2018-11-24 14:35:23</td>
      <td>8:07:52</td>
      <td>ions</td>
      <td>75_150ns_733Pb_733_702_468_42bpi_20inj</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td></td>
    </tr>
    <tr>
      <th>7472</th>
      <td>2018-11-25 01:25:01</td>
      <td>7:50:12</td>
      <td>ions</td>
      <td>75_150ns_733Pb_733_702_468_42bpi_20inj</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td></td>
    </tr>
    <tr>
      <th>7473</th>
      <td>2018-11-25 11:49:05</td>
      <td>9:48:03</td>
      <td>ions</td>
      <td>75_150ns_733Pb_733_702_468_42bpi_20inj</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td>Problem with inner detector power supply in AL...</td>
    </tr>
    <tr>
      <th>7474</th>
      <td>2018-11-26 02:13:25</td>
      <td>5:55:26</td>
      <td>ions</td>
      <td>75_150ns_733Pb_733_702_468_42bpi_20inj</td>
      <td>pwr converter</td>
      <td>False</td>
      <td>False</td>
      <td>Power converters losing communication</td>
    </tr>
    <tr>
      <th>7475</th>
      <td>2018-11-26 18:06:32</td>
      <td>0:38:36</td>
      <td>ions</td>
      <td>75_150ns_733Pb_733_702_468_42bpi_20inj</td>
      <td>QPS</td>
      <td>False</td>
      <td>False</td>
      <td>Spurious QPS trip in RB of S12</td>
    </tr>
    <tr>
      <th>7477</th>
      <td>2018-11-26 23:27:29</td>
      <td>8:50:01</td>
      <td>ions</td>
      <td>75_150ns_733Pb_733_702_468_42bpi_20inj</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td></td>
    </tr>
    <tr>
      <th>7480</th>
      <td>2018-11-28 00:10:18</td>
      <td>7:53:18</td>
      <td>ions</td>
      <td>75_150ns_733Pb_733_702_468_42bpi_20inj</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td></td>
    </tr>
    <tr>
      <th>7481</th>
      <td>2018-11-28 11:00:42</td>
      <td>8:23:53</td>
      <td>ions</td>
      <td>75_150ns_733Pb_733_702_468_42bpi_20inj</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td></td>
    </tr>
    <tr>
      <th>7482</th>
      <td>2018-11-28 23:10:05</td>
      <td>0:31:41</td>
      <td>ions</td>
      <td>75_150ns_733Pb_733_702_468_42bpi_20inj</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td>Losses in IR7 with 10Hz oscillations</td>
    </tr>
    <tr>
      <th>7483</th>
      <td>2018-11-29 02:33:11</td>
      <td>13:01:29</td>
      <td>ions</td>
      <td>100_150ns_648Pb_620_619_52_36bpi_20inj_V2</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td>VDM repetition for ALICE in shadow of&amp;nbsp;</td>
    </tr>
    <tr>
      <th>7485</th>
      <td>2018-11-29 21:29:08</td>
      <td>7:51:20</td>
      <td>ions</td>
      <td>75_150ns_733Pb_733_702_468_42bpi_20inj</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td>Slow filling</td>
    </tr>
    <tr>
      <th>7486</th>
      <td>2018-11-30 09:38:54</td>
      <td>7:35:52</td>
      <td>ions</td>
      <td>75_150ns_733Pb_733_702_468_42bpi_20inj</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td></td>
    </tr>
    <tr>
      <th>7487</th>
      <td>2018-11-30 20:04:57</td>
      <td>8:29:44</td>
      <td>ions</td>
      <td>75_150ns_733Pb_733_702_468_42bpi_20inj</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td></td>
    </tr>
    <tr>
      <th>7488</th>
      <td>2018-12-01 07:40:42</td>
      <td>8:05:50</td>
      <td>ions</td>
      <td>75_150ns_733Pb_733_702_468_42bpi_20inj</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td></td>
    </tr>
    <tr>
      <th>7489</th>
      <td>2018-12-01 18:27:18</td>
      <td>0:44:40</td>
      <td>ions</td>
      <td>75_150ns_733Pb_733_702_468_42bpi_20inj</td>
      <td>others</td>
      <td>False</td>
      <td>False</td>
      <td>Vacuum</td>
    </tr>
    <tr>
      <th>7490</th>
      <td>2018-12-01 21:37:48</td>
      <td>8:31:35</td>
      <td>ions</td>
      <td>75_150ns_733Pb_733_702_468_42bpi_20inj</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td></td>
    </tr>
    <tr>
      <th>7491</th>
      <td>2018-12-02 08:48:41</td>
      <td>4:50:14</td>
      <td>ions</td>
      <td>75_150ns_733Pb_733_702_468_42bpi_20inj</td>
      <td>RF</td>
      <td>False</td>
      <td>False</td>
      <td>RF</td>
    </tr>
    <tr>
      <th>7492</th>
      <td>2018-12-02 15:52:49</td>
      <td>1:18:06</td>
      <td>ions</td>
      <td>75_150ns_733Pb_733_702_468_42bpi_20inj</td>
      <td>operator</td>
      <td>False</td>
      <td>False</td>
      <td></td>
    </tr>
  </tbody>
</table>
<p>291 rows × 8 columns</p>
</div>



## Luminosity data


```python
def LPCLumiData(source='http://lpc-afs.web.cern.ch/lpc-afs/LHC/2018/luminosity_data_ions.txt'):
    import requests

    response = requests.get('http://lpc-afs.web.cern.ch/lpc-afs/LHC/2018/luminosity_data_ions.txt')
    data = response.text
    aux=(pd.DataFrame([x.split() for x in data.split('\n')[2:]], columns=['fill','tstart_UNIX_UTC','tstop_UNIX_UTC','ATLAS_lui_inverse_ub',\
                                                                    'CMS_lui_inverse_ub', 'LHCb_lui_inverse_ub', 'ALICE_lui_inverse_ub','ATLAS_plu_Hz_inverse_ub',\
                                                                    'CMS_plu_Hz_inverse_ub', 'LHCb_plu_Hz_inverse_ub', 'ALICE_plu_Hz_inverse_ub']))
    aux=aux.set_index('fill')
    aux.index.name=None
    return aux
```


```python
LPCLumiData(source='http://lpc-afs.web.cern.ch/lpc-afs/LHC/2018/luminosity_data_ions.txt')
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>tstart_UNIX_UTC</th>
      <th>tstop_UNIX_UTC</th>
      <th>ATLAS_lui_inverse_ub</th>
      <th>CMS_lui_inverse_ub</th>
      <th>LHCb_lui_inverse_ub</th>
      <th>ALICE_lui_inverse_ub</th>
      <th>ATLAS_plu_Hz_inverse_ub</th>
      <th>CMS_plu_Hz_inverse_ub</th>
      <th>LHCb_plu_Hz_inverse_ub</th>
      <th>ALICE_plu_Hz_inverse_ub</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>7427</th>
      <td>1541708363</td>
      <td>1541733698</td>
      <td>1</td>
      <td>1</td>
      <td>0</td>
      <td>2</td>
      <td>1.510e-04</td>
      <td>1.720e-04</td>
      <td>3.200e-05</td>
      <td>2.065e-04</td>
    </tr>
    <tr>
      <th>7428</th>
      <td>1541741102</td>
      <td>1541752703</td>
      <td>2</td>
      <td>2</td>
      <td>0</td>
      <td>2</td>
      <td>1.320e-04</td>
      <td>1.290e-04</td>
      <td>2.900e-05</td>
      <td>8.785e-05</td>
    </tr>
    <tr>
      <th>7433</th>
      <td>1541805329</td>
      <td>1541820593</td>
      <td>10</td>
      <td>10</td>
      <td>0</td>
      <td>6</td>
      <td>8.220e-04</td>
      <td>8.150e-04</td>
      <td>2.800e-05</td>
      <td>4.479e-04</td>
    </tr>
    <tr>
      <th>7435</th>
      <td>1541864777</td>
      <td>1541885107</td>
      <td>21</td>
      <td>21</td>
      <td>1</td>
      <td>12</td>
      <td>9.940e-04</td>
      <td>9.870e-04</td>
      <td>3.000e-05</td>
      <td>5.904e-04</td>
    </tr>
    <tr>
      <th>7436</th>
      <td>1541897140</td>
      <td>1541917561</td>
      <td>46</td>
      <td>47</td>
      <td>2</td>
      <td>26</td>
      <td>2.461e-03</td>
      <td>2.445e-03</td>
      <td>8.700e-05</td>
      <td>9.689e-04</td>
    </tr>
    <tr>
      <th>7437</th>
      <td>1541933644</td>
      <td>1541947812</td>
      <td>64</td>
      <td>64</td>
      <td>2</td>
      <td>34</td>
      <td>2.350e-03</td>
      <td>2.360e-03</td>
      <td>7.600e-05</td>
      <td>9.662e-04</td>
    </tr>
    <tr>
      <th>7438</th>
      <td>1541962710</td>
      <td>1541986715</td>
      <td>89</td>
      <td>89</td>
      <td>5</td>
      <td>54</td>
      <td>2.043e-03</td>
      <td>2.016e-03</td>
      <td>1.970e-04</td>
      <td>1.002e-03</td>
    </tr>
    <tr>
      <th>7439</th>
      <td>1542004896</td>
      <td>1542030234</td>
      <td>125</td>
      <td>125</td>
      <td>6</td>
      <td>75</td>
      <td>2.999e-03</td>
      <td>2.960e-03</td>
      <td>1.220e-04</td>
      <td>1.044e-03</td>
    </tr>
    <tr>
      <th>7440</th>
      <td>1542049336</td>
      <td>1542077104</td>
      <td>163</td>
      <td>162</td>
      <td>9</td>
      <td>96</td>
      <td>3.075e-03</td>
      <td>3.775e-03</td>
      <td>1.660e-04</td>
      <td>1.172e-03</td>
    </tr>
    <tr>
      <th>7441</th>
      <td>1542090535</td>
      <td>1542117430</td>
      <td>179</td>
      <td>169</td>
      <td>13</td>
      <td>114</td>
      <td>1.443e-03</td>
      <td>1.459e-03</td>
      <td>2.010e-04</td>
      <td>9.124e-04</td>
    </tr>
    <tr>
      <th>7442</th>
      <td>1542135376</td>
      <td>1542149279</td>
      <td>185</td>
      <td>181</td>
      <td>15</td>
      <td>126</td>
      <td>2.068e-03</td>
      <td>2.059e-03</td>
      <td>2.710e-04</td>
      <td>9.953e-04</td>
    </tr>
    <tr>
      <th>7443</th>
      <td>1542212137</td>
      <td>1542233763</td>
      <td>192</td>
      <td>195</td>
      <td>19</td>
      <td>144</td>
      <td>1.843e-03</td>
      <td>1.845e-03</td>
      <td>2.390e-04</td>
      <td>9.750e-04</td>
    </tr>
    <tr>
      <th>7444</th>
      <td>1542247020</td>
      <td>1542273588</td>
      <td>235</td>
      <td>238</td>
      <td>20</td>
      <td>166</td>
      <td>2.223e-03</td>
      <td>2.231e-03</td>
      <td>1.000e-04</td>
      <td>1.021e-03</td>
    </tr>
    <tr>
      <th>7446</th>
      <td>1542309239</td>
      <td>1542335749</td>
      <td>279</td>
      <td>283</td>
      <td>22</td>
      <td>189</td>
      <td>2.249e-03</td>
      <td>2.231e-03</td>
      <td>9.300e-05</td>
      <td>1.012e-03</td>
    </tr>
    <tr>
      <th>7447</th>
      <td>1542344205</td>
      <td>1542344424</td>
      <td>279</td>
      <td>283</td>
      <td>22</td>
      <td>189</td>
      <td>0.000e+00</td>
      <td>1.888e-03</td>
      <td>1.600e-04</td>
      <td>1.018e-03</td>
    </tr>
    <tr>
      <th>7448</th>
      <td>1542353526</td>
      <td>1542380442</td>
      <td>323</td>
      <td>327</td>
      <td>23</td>
      <td>211</td>
      <td>2.248e-03</td>
      <td>2.660e-03</td>
      <td>1.030e-04</td>
      <td>1.030e-03</td>
    </tr>
    <tr>
      <th>7449</th>
      <td>1542389473</td>
      <td>1542416067</td>
      <td>369</td>
      <td>373</td>
      <td>25</td>
      <td>235</td>
      <td>2.261e-03</td>
      <td>2.274e-03</td>
      <td>1.030e-04</td>
      <td>1.050e-03</td>
    </tr>
    <tr>
      <th>7450</th>
      <td>1542425421</td>
      <td>1542451833</td>
      <td>412</td>
      <td>416</td>
      <td>26</td>
      <td>257</td>
      <td>2.238e-03</td>
      <td>2.231e-03</td>
      <td>1.010e-04</td>
      <td>1.044e-03</td>
    </tr>
    <tr>
      <th>7453</th>
      <td>1542522977</td>
      <td>1542568209</td>
      <td>467</td>
      <td>472</td>
      <td>28</td>
      <td>287</td>
      <td>2.228e-03</td>
      <td>2.274e-03</td>
      <td>1.190e-04</td>
      <td>1.058e-03</td>
    </tr>
    <tr>
      <th>7454</th>
      <td>1542584123</td>
      <td>1542642529</td>
      <td>513</td>
      <td>517</td>
      <td>30</td>
      <td>315</td>
      <td>2.231e-03</td>
      <td>2.231e-03</td>
      <td>8.900e-05</td>
      <td>1.121e-02</td>
    </tr>
    <tr>
      <th>7456</th>
      <td>1542662470</td>
      <td>1542687133</td>
      <td>555</td>
      <td>559</td>
      <td>32</td>
      <td>340</td>
      <td>2.235e-03</td>
      <td>2.231e-03</td>
      <td>1.020e-04</td>
      <td>1.075e-03</td>
    </tr>
    <tr>
      <th>7457</th>
      <td>1542708236</td>
      <td>1542734526</td>
      <td>598</td>
      <td>602</td>
      <td>33</td>
      <td>362</td>
      <td>2.239e-03</td>
      <td>2.231e-03</td>
      <td>1.000e-04</td>
      <td>1.040e-03</td>
    </tr>
    <tr>
      <th>7459</th>
      <td>1542755386</td>
      <td>1542755976</td>
      <td>599</td>
      <td>604</td>
      <td>33</td>
      <td>363</td>
      <td>2.720e-03</td>
      <td>2.746e-03</td>
      <td>5.670e-04</td>
      <td>1.016e-03</td>
    </tr>
    <tr>
      <th>7460</th>
      <td>1542764489</td>
      <td>1542781997</td>
      <td>633</td>
      <td>638</td>
      <td>39</td>
      <td>379</td>
      <td>2.751e-03</td>
      <td>2.703e-03</td>
      <td>5.700e-04</td>
      <td>1.041e-03</td>
    </tr>
    <tr>
      <th>7464</th>
      <td>1542855492</td>
      <td>1542878972</td>
      <td>671</td>
      <td>677</td>
      <td>46</td>
      <td>399</td>
      <td>2.713e-03</td>
      <td>2.703e-03</td>
      <td>5.560e-04</td>
      <td>1.220e-03</td>
    </tr>
    <tr>
      <th>7466</th>
      <td>1542910139</td>
      <td>1542941206</td>
      <td>727</td>
      <td>734</td>
      <td>55</td>
      <td>429</td>
      <td>3.522e-03</td>
      <td>3.518e-03</td>
      <td>5.240e-04</td>
      <td>1.087e-03</td>
    </tr>
    <tr>
      <th>7467</th>
      <td>1542963538</td>
      <td>1542992270</td>
      <td>791</td>
      <td>799</td>
      <td>67</td>
      <td>458</td>
      <td>4.030e-03</td>
      <td>4.033e-03</td>
      <td>1.205e-03</td>
      <td>1.073e-03</td>
    </tr>
    <tr>
      <th>7468</th>
      <td>1543002191</td>
      <td>1543037603</td>
      <td>866</td>
      <td>875</td>
      <td>79</td>
      <td>492</td>
      <td>5.142e-03</td>
      <td>5.148e-03</td>
      <td>1.014e-03</td>
      <td>1.071e-03</td>
    </tr>
    <tr>
      <th>7471</th>
      <td>1543066522</td>
      <td>1543095796</td>
      <td>932</td>
      <td>942</td>
      <td>91</td>
      <td>521</td>
      <td>4.995e-03</td>
      <td>4.976e-03</td>
      <td>9.950e-04</td>
      <td>1.102e-03</td>
    </tr>
    <tr>
      <th>7472</th>
      <td>1543105500</td>
      <td>1543133713</td>
      <td>998</td>
      <td>1009</td>
      <td>102</td>
      <td>549</td>
      <td>5.007e-03</td>
      <td>4.976e-03</td>
      <td>1.025e-03</td>
      <td>1.132e-03</td>
    </tr>
    <tr>
      <th>7473</th>
      <td>1543142944</td>
      <td>1543178229</td>
      <td>1074</td>
      <td>1085</td>
      <td>115</td>
      <td>588</td>
      <td>6.125e-03</td>
      <td>6.092e-03</td>
      <td>1.057e-03</td>
      <td>1.290e-03</td>
    </tr>
    <tr>
      <th>7474</th>
      <td>1543194804</td>
      <td>1543216131</td>
      <td>1133</td>
      <td>1144</td>
      <td>125</td>
      <td>609</td>
      <td>5.917e-03</td>
      <td>5.834e-03</td>
      <td>1.019e-03</td>
      <td>1.070e-03</td>
    </tr>
    <tr>
      <th>7475</th>
      <td>1543251991</td>
      <td>1543254308</td>
      <td>1143</td>
      <td>1147</td>
      <td>127</td>
      <td>611</td>
      <td>6.107e-03</td>
      <td>5.577e-03</td>
      <td>1.020e-03</td>
      <td>1.065e-03</td>
    </tr>
    <tr>
      <th>7477</th>
      <td>1543271249</td>
      <td>1543303184</td>
      <td>1210</td>
      <td>1214</td>
      <td>139</td>
      <td>642</td>
      <td>5.648e-03</td>
      <td>5.663e-03</td>
      <td>1.000e-03</td>
      <td>1.067e-03</td>
    </tr>
    <tr>
      <th>7480</th>
      <td>1543360218</td>
      <td>1543388616</td>
      <td>1278</td>
      <td>1285</td>
      <td>150</td>
      <td>671</td>
      <td>5.986e-03</td>
      <td>5.963e-03</td>
      <td>1.098e-03</td>
      <td>1.084e-03</td>
    </tr>
    <tr>
      <th>7481</th>
      <td>1543399242</td>
      <td>1543429605</td>
      <td>1351</td>
      <td>1360</td>
      <td>163</td>
      <td>701</td>
      <td>5.969e-03</td>
      <td>6.006e-03</td>
      <td>1.037e-03</td>
      <td>1.093e-03</td>
    </tr>
    <tr>
      <th>7482</th>
      <td>1543443005</td>
      <td>1543444906</td>
      <td>1360</td>
      <td>1368</td>
      <td>165</td>
      <td>702</td>
      <td>5.721e-03</td>
      <td>5.577e-03</td>
      <td>1.025e-03</td>
      <td>1.072e-03</td>
    </tr>
    <tr>
      <th>7483</th>
      <td>1543455191</td>
      <td>1543502081</td>
      <td>1409</td>
      <td>1415</td>
      <td>167</td>
      <td>738</td>
      <td>4.059e-03</td>
      <td>3.947e-03</td>
      <td>2.760e-04</td>
      <td>1.059e-03</td>
    </tr>
    <tr>
      <th>7485</th>
      <td>1543523347</td>
      <td>1543551628</td>
      <td>1466</td>
      <td>1471</td>
      <td>177</td>
      <td>766</td>
      <td>4.610e-03</td>
      <td>4.547e-03</td>
      <td>9.350e-04</td>
      <td>1.066e-03</td>
    </tr>
    <tr>
      <th>7486</th>
      <td>1543567133</td>
      <td>1543594487</td>
      <td>1522</td>
      <td>1527</td>
      <td>187</td>
      <td>794</td>
      <td>4.836e-03</td>
      <td>4.633e-03</td>
      <td>9.500e-04</td>
      <td>1.295e-03</td>
    </tr>
    <tr>
      <th>7487</th>
      <td>1543604697</td>
      <td>1543635280</td>
      <td>1588</td>
      <td>1594</td>
      <td>198</td>
      <td>825</td>
      <td>5.600e-03</td>
      <td>5.620e-03</td>
      <td>1.065e-03</td>
      <td>1.064e-03</td>
    </tr>
    <tr>
      <th>7488</th>
      <td>1543646442</td>
      <td>1543675593</td>
      <td>1655</td>
      <td>1660</td>
      <td>210</td>
      <td>854</td>
      <td>5.836e-03</td>
      <td>5.749e-03</td>
      <td>1.017e-03</td>
      <td>1.074e-03</td>
    </tr>
    <tr>
      <th>7489</th>
      <td>1543685238</td>
      <td>1543687919</td>
      <td>1668</td>
      <td>1673</td>
      <td>212</td>
      <td>856</td>
      <td>5.787e-03</td>
      <td>5.792e-03</td>
      <td>9.990e-04</td>
      <td>1.059e-03</td>
    </tr>
    <tr>
      <th>7490</th>
      <td>1543696668</td>
      <td>1543727362</td>
      <td>1736</td>
      <td>1741</td>
      <td>224</td>
      <td>887</td>
      <td>5.887e-03</td>
      <td>5.834e-03</td>
      <td>1.006e-03</td>
      <td>1.085e-03</td>
    </tr>
    <tr>
      <th>7491</th>
      <td>1543736920</td>
      <td>1543754334</td>
      <td>1788</td>
      <td>1794</td>
      <td>234</td>
      <td>904</td>
      <td>5.798e-03</td>
      <td>5.749e-03</td>
      <td>1.002e-03</td>
      <td>1.075e-03</td>
    </tr>
    <tr>
      <th>7492</th>
      <td>1543762369</td>
      <td>1543767055</td>
      <td>1797</td>
      <td>1802</td>
      <td>235</td>
      <td>907</td>
      <td>2.343e-03</td>
      <td>2.231e-03</td>
      <td>2.470e-04</td>
      <td>7.774e-04</td>
    </tr>
    <tr>
      <th>NaN</th>
      <td>None</td>
      <td>None</td>
      <td>None</td>
      <td>None</td>
      <td>None</td>
      <td>None</td>
      <td>None</td>
      <td>None</td>
      <td>None</td>
      <td>None</td>
    </tr>
  </tbody>
</table>
</div>



# The instability table


```python
def LHCInstabilityTable():
    import requests
    response = requests.get('https://lhcinstability.web.cern.ch/lhcinstability/FillSummaries/2018/index.html')
    data = response.text
    aux=[x.split('</a>') for x in data.split('<a href="')]
    aux=aux[1:]
    fill=[int(x[0].split('/')[0]) for x in aux]
    comment=[x[1].split('<!--PLACEHOLDER-->')[0] for x in aux]
    aux=pd.DataFrame({'fill': fill, 'Comment':comment})
    aux=aux.set_index('fill')
    aux.index.name=None
    return aux
```


```python
LHCInstabilityTable()
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>Comment</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>6484</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6492</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6493</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6494</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6495</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6496</th>
      <td>\n - Instabilities of probe bunches during the...</td>
    </tr>
    <tr>
      <th>6497</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6498</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6499</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6500</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6501</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6502</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6503</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6504</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6505</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6506</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6507</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6508</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6509</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6510</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6511</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6512</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6513</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6514</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6516</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6517</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6518</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6519</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6520</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>6521</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>...</th>
      <td>...</td>
    </tr>
    <tr>
      <th>7462</th>
      <td>- Loss maps at injection and flat-top with 20...</td>
    </tr>
    <tr>
      <th>7463</th>
      <td>- No instability detected. Validation cycle w...</td>
    </tr>
    <tr>
      <th>7464</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>7465</th>
      <td>- No coherent instability detected. Dumped by...</td>
    </tr>
    <tr>
      <th>7466</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>7467</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>7468</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>7469</th>
      <td>- No beam (only probes).&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>7470</th>
      <td>- RF resynchronization first. Then beam was r...</td>
    </tr>
    <tr>
      <th>7471</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>7472</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>7473</th>
      <td>- No instability detected. Activity in V for ...</td>
    </tr>
    <tr>
      <th>7474</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>7475</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>7477</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>7478</th>
      <td>- Crystal collimation MD.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>7479</th>
      <td>- Crystal collimation MD.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>7480</th>
      <td>- No instability detected.&lt;br&gt;\n</td>
    </tr>
    <tr>
      <th>7481</th>
      <td>- no emittance blow-up. Vertical activity at ...</td>
    </tr>
    <tr>
      <th>7482</th>
      <td>- beams dumped by 10 Hz oscillations 30 minut...</td>
    </tr>
    <tr>
      <th>7483</th>
      <td>- no emittance blow-up. Vertical activity at ...</td>
    </tr>
    <tr>
      <th>7485</th>
      <td>- no emittance blow-up. Vertical activity at ...</td>
    </tr>
    <tr>
      <th>7486</th>
      <td>- no emittance blow-up. Vertical activity at ...</td>
    </tr>
    <tr>
      <th>7487</th>
      <td>- no emittance blow-up. Vertical activity at ...</td>
    </tr>
    <tr>
      <th>7488</th>
      <td>- no emittance blow-up. Vertical activity at ...</td>
    </tr>
    <tr>
      <th>7489</th>
      <td>- no emittance blow-up. Vertical activity at ...</td>
    </tr>
    <tr>
      <th>7490</th>
      <td>- no emittance blow-up. Vertical activity at ...</td>
    </tr>
    <tr>
      <th>7491</th>
      <td>- no emittance blow-up. Vertical activity at ...</td>
    </tr>
    <tr>
      <th>7492</th>
      <td>- no emittance blow-up. Vertical activity at ...</td>
    </tr>
    <tr>
      <th>7493</th>
      <td>- optics MD. &lt;br&gt;\n</td>
    </tr>
  </tbody>
</table>
<p>893 rows × 1 columns</p>
</div>



# Building a fill table


```python
import numpy as np
listDF=[]
myFills=np.arange(6666,6670)
for i in np.unique(myFills):
    aux=importData.LHCFillsByNumber(i)
    myDF=aux[aux['mode']=='FILL'].copy()
    del myDF['mode']
    myDF['modes DF']=[aux[~(aux['mode']=='FILL')]]
    aux=myDF.iloc[0]['modes DF'].groupby('mode').duration.sum()
    aux=pd.DataFrame(aux).transpose()
    aux.columns.name=None
    aux['fill']=i
    aux=aux.set_index('fill')
    aux.index.name=None
    #aux1=importData.LHCCals2pd(['LHC.STATS:B%_NUMBER_BUNCHES'],fillList=[i])
    listDF.append(pd.concat([aux,myDF], axis=1))
fillsDF=pd.concat(listDF,sort=True)
```

This is the minimal fill DF contatain the specified fill and the relative duration modes.


```python
fillsDF
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>ADJUST</th>
      <th>BEAMDUMP</th>
      <th>FLATTOP</th>
      <th>INJPHYS</th>
      <th>INJPROT</th>
      <th>PRERAMP</th>
      <th>RAMP</th>
      <th>RAMPDOWN</th>
      <th>SETUP</th>
      <th>SQUEEZE</th>
      <th>STABLE</th>
      <th>duration</th>
      <th>endTime</th>
      <th>modes DF</th>
      <th>startTime</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>6666</th>
      <td>00:32:01.453000</td>
      <td>00:02:24.449000</td>
      <td>00:02:33.174000</td>
      <td>00:45:26.077000</td>
      <td>00:03:48.286999</td>
      <td>00:08:49.490999</td>
      <td>00:20:14.409999</td>
      <td>00:04:21.061000</td>
      <td>NaT</td>
      <td>00:10:54.578999</td>
      <td>12:43:12.336000</td>
      <td>14:53:58.320000</td>
      <td>2018-05-11 10:29:36.956000090+00:00</td>
      <td>mode                           start...</td>
      <td>2018-05-10 19:35:38.635999918+00:00</td>
    </tr>
    <tr>
      <th>6667</th>
      <td>NaT</td>
      <td>NaT</td>
      <td>NaT</td>
      <td>00:43:23.439999</td>
      <td>00:10:15.817999</td>
      <td>NaT</td>
      <td>NaT</td>
      <td>NaT</td>
      <td>00:07:09.145999</td>
      <td>NaT</td>
      <td>NaT</td>
      <td>01:35:39.635999</td>
      <td>2018-05-11 12:05:16.592999935+00:00</td>
      <td>mode                           startT...</td>
      <td>2018-05-11 10:29:36.957000017+00:00</td>
    </tr>
    <tr>
      <th>6668</th>
      <td>NaT</td>
      <td>NaT</td>
      <td>NaT</td>
      <td>00:10:28.993000</td>
      <td>00:04:30.044999</td>
      <td>NaT</td>
      <td>NaT</td>
      <td>NaT</td>
      <td>NaT</td>
      <td>NaT</td>
      <td>NaT</td>
      <td>00:15:09.246999</td>
      <td>2018-05-11 12:20:25.841000080+00:00</td>
      <td>mode                           startT...</td>
      <td>2018-05-11 12:05:16.594000101+00:00</td>
    </tr>
    <tr>
      <th>6669</th>
      <td>NaT</td>
      <td>NaT</td>
      <td>NaT</td>
      <td>00:10:24.634999</td>
      <td>00:15:45.363000</td>
      <td>NaT</td>
      <td>NaT</td>
      <td>NaT</td>
      <td>NaT</td>
      <td>NaT</td>
      <td>NaT</td>
      <td>00:29:17.625000</td>
      <td>2018-05-11 12:49:43.467000008+00:00</td>
      <td>mode                           startT...</td>
      <td>2018-05-11 12:20:25.842000008+00:00</td>
    </tr>
  </tbody>
</table>
</div>




```python
fillsDF[(fillsDF['STABLE']>pd.Timedelta('10h'))]
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>ADJUST</th>
      <th>BEAMDUMP</th>
      <th>FLATTOP</th>
      <th>INJPHYS</th>
      <th>INJPROT</th>
      <th>PRERAMP</th>
      <th>RAMP</th>
      <th>RAMPDOWN</th>
      <th>SETUP</th>
      <th>SQUEEZE</th>
      <th>STABLE</th>
      <th>duration</th>
      <th>endTime</th>
      <th>modes DF</th>
      <th>startTime</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>6666</th>
      <td>00:32:01.453000</td>
      <td>00:02:24.449000</td>
      <td>00:02:33.174000</td>
      <td>00:45:26.077000</td>
      <td>00:03:48.286999</td>
      <td>00:08:49.490999</td>
      <td>00:20:14.409999</td>
      <td>00:04:21.061000</td>
      <td>NaT</td>
      <td>00:10:54.578999</td>
      <td>12:43:12.336000</td>
      <td>14:53:58.320000</td>
      <td>2018-05-11 10:29:36.956000090+00:00</td>
      <td>mode                           start...</td>
      <td>2018-05-10 19:35:38.635999918+00:00</td>
    </tr>
  </tbody>
</table>
</div>




```python
importData.LHCFillsAggregation(['LHC.BCTFR.A6R4.B%:BEAM_INTENSITY','LHC.BQM.B%:NO_BUNCHES','LHC.BQM.B1:NO_BUNCHES'],6666, ['INJPHYS'], functionList = np.max, mapInsteadAgg = False)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th></th>
      <th>LHC.BCTFR.A6R4.B1:BEAM_INTENSITY</th>
      <th>LHC.BCTFR.A6R4.B2:BEAM_INTENSITY</th>
      <th>LHC.BQM.B1:NO_BUNCHES</th>
      <th>LHC.BQM.B2:NO_BUNCHES</th>
      <th>startTime</th>
      <th>endTime</th>
      <th>duration</th>
    </tr>
    <tr>
      <th>fill</th>
      <th>mode</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>6666</th>
      <th>INJPHYS</th>
      <td>2.833537e+14</td>
      <td>2.856917e+14</td>
      <td>2556.0</td>
      <td>2556.0</td>
      <td>2018-05-10 19:39:39.915999889+00:00</td>
      <td>2018-05-10 20:25:05.993000031+00:00</td>
      <td>00:45:26.077000</td>
    </tr>
  </tbody>
</table>
</div>




```python
importData.LHCFillsAggregation(['LHC.BQM.B%:NO_BUNCHES'], range(6666, 6670),functionList = np.max, mapInsteadAgg = False)
```




<div>
<style scoped>
    .dataframe tbody tr th:only-of-type {
        vertical-align: middle;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }

    .dataframe thead th {
        text-align: right;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th></th>
      <th>LHC.BQM.B1:NO_BUNCHES</th>
      <th>LHC.BQM.B2:NO_BUNCHES</th>
      <th>startTime</th>
      <th>endTime</th>
      <th>duration</th>
    </tr>
    <tr>
      <th>fill</th>
      <th>mode</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>6666</th>
      <th>FILL</th>
      <td>2556.0</td>
      <td>2556.0</td>
      <td>2018-05-10 19:35:38.635999918+00:00</td>
      <td>2018-05-11 10:29:36.956000090+00:00</td>
      <td>14:53:58.320000</td>
    </tr>
    <tr>
      <th>6667</th>
      <th>FILL</th>
      <td>2.0</td>
      <td>2.0</td>
      <td>2018-05-11 10:29:36.957000017+00:00</td>
      <td>2018-05-11 12:05:16.592999935+00:00</td>
      <td>01:35:39.635999</td>
    </tr>
    <tr>
      <th>6668</th>
      <th>FILL</th>
      <td>3.0</td>
      <td>2.0</td>
      <td>2018-05-11 12:05:16.594000101+00:00</td>
      <td>2018-05-11 12:20:25.841000080+00:00</td>
      <td>00:15:09.246999</td>
    </tr>
    <tr>
      <th>6669</th>
      <th>FILL</th>
      <td>12.0</td>
      <td>1.0</td>
      <td>2018-05-11 12:20:25.842000008+00:00</td>
      <td>2018-05-11 12:49:43.467000008+00:00</td>
      <td>00:29:17.625000</td>
    </tr>
  </tbody>
</table>
</div>




```python
cals.search('%NUMBER_%')
```




    ['LHC.BSRA.US45.B1/XPocAcquisition#aGXpocNumberAcq',
     'LHC.BSRA.US45.B2/XPocAcquisition#aGXpocNumberAcq',
     'LHC.BSRT.5L4.B2:GATENUMBEROFPULSES',
     'LHC.BSRT.5R4.B1:GATENUMBEROFPULSES',
     'LHC.STATS:B1_NUMBER_BUNCHES',
     'LHC.STATS:B2_NUMBER_BUNCHES',
     'LHC.STATS:NUMBER_COLLISIONS_IP1_5',
     'LHC.STATS:NUMBER_COLLISIONS_IP2',
     'LHC.STATS:NUMBER_COLLISIONS_IP8',
     'LHC.STATS:NUMBER_OF_INJECTIONS',
     'LMFGLO2_STATUS_FLUSHING_NUMBER.POSST',
     'LMFGLO3_STATUS_FLUSHING_NUMBER.POSST',
     'LP.NSRCGEN:NUMBEROFSPARKS',
     'SIMA.10L3.3RM10S:REV_NUMBER_OBSOLETE',
     'SIMA.10L7.7LM08S:REV_NUMBER_OBSOLETE',
     'SIMA.10R3.3RM20S:REV_NUMBER_OBSOLETE',
     'SIMA.10R7.7RM10S:REV_NUMBER_OBSOLETE',
     'SIMA.11L3.3RM09S:REV_NUMBER_OBSOLETE',
     'SIMA.11L7.7LM09S:REV_NUMBER_OBSOLETE',
     'SIMA.11R3.3RM21S:REV_NUMBER_OBSOLETE',
     'SIMA.11R7.7RM11S:REV_NUMBER_OBSOLETE',
     'SIMA.12L3.3RM08S:REV_NUMBER_OBSOLETE',
     'SIMA.12L7.7LM10S:REV_NUMBER_OBSOLETE',
     'SIMA.12R3.3RM22S:REV_NUMBER_OBSOLETE',
     'SIMA.12R7.7RM12S:REV_NUMBER_OBSOLETE',
     'SIMA.13L3.3RM07S:REV_NUMBER_OBSOLETE',
     'SIMA.13L7.7LM11S:REV_NUMBER_OBSOLETE',
     'SIMA.13R3.3RM23S:REV_NUMBER_OBSOLETE',
     'SIMA.13R7.7RM13S:REV_NUMBER_OBSOLETE',
     'SIMA.14L3.3RM06S:REV_NUMBER_OBSOLETE',
     'SIMA.14L7.7LM12S:REV_NUMBER_OBSOLETE',
     'SIMA.14R3.3RM24S:REV_NUMBER_OBSOLETE',
     'SIMA.14R7.7RM14S:REV_NUMBER_OBSOLETE',
     'SIMA.15L3.3RM05S:REV_NUMBER_OBSOLETE',
     'SIMA.15L7.7LM13S:REV_NUMBER_OBSOLETE',
     'SIMA.15R3.3RM25S:REV_NUMBER_OBSOLETE',
     'SIMA.15R7.7RM15S:REV_NUMBER_OBSOLETE',
     'SIMA.16L3.3RM04S:REV_NUMBER_OBSOLETE',
     'SIMA.16L7.7LM14S:REV_NUMBER_OBSOLETE',
     'SIMA.16R3.3RM26S:REV_NUMBER_OBSOLETE',
     'SIMA.16R7.7RM16S:REV_NUMBER_OBSOLETE',
     'SIMA.17L3.3RM03S:REV_NUMBER_OBSOLETE',
     'SIMA.17L7.7LM15S:REV_NUMBER_OBSOLETE',
     'SIMA.17R3.3RM27S:REV_NUMBER_OBSOLETE',
     'SIMA.17R7.7RM17S:REV_NUMBER_OBSOLETE',
     'SIMA.18L7.7LM16S:REV_NUMBER_OBSOLETE',
     'SIMA.18R3.3RM28S:REV_NUMBER_OBSOLETE',
     'SIMA.18R7.7RM18S:REV_NUMBER_OBSOLETE',
     'SIMA.19L7.7LM17S:REV_NUMBER_OBSOLETE',
     'SIMA.19R3.3RM29S:REV_NUMBER_OBSOLETE',
     'SIMA.19R7.7RM19S:REV_NUMBER_OBSOLETE',
     'SIMA.20L7.7LM18S:REV_NUMBER_OBSOLETE',
     'SIMA.20R3.3RM30S:REV_NUMBER_OBSOLETE',
     'SIMA.20R7.7RM20S:REV_NUMBER_OBSOLETE',
     'SIMA.21R7.7RM21S:REV_NUMBER_OBSOLETE',
     'SIMA.4L3.3RM17S:REV_NUMBER_OBSOLETE',
     'SIMA.4L7.7LM01S:REV_NUMBER_OBSOLETE',
     'SIMA.4R7.7RM03S:REV_NUMBER_OBSOLETE',
     'SIMA.5L3.3RM16S:REV_NUMBER_OBSOLETE',
     'SIMA.6L3.3RM15S:REV_NUMBER_OBSOLETE',
     'SIMA.8L3.3RM12S:REV_NUMBER_OBSOLETE',
     'SIMA.8L7.7LM06S:REV_NUMBER_OBSOLETE',
     'SIMA.8R3.3RM18S:REV_NUMBER_OBSOLETE',
     'SIMA.8R7.7RM08S:REV_NUMBER_OBSOLETE',
     'SIMA.9L3.3RM11S:REV_NUMBER_OBSOLETE',
     'SIMA.9L7.7LM07S:REV_NUMBER_OBSOLETE',
     'SIMA.9R3.3RM19S:REV_NUMBER_OBSOLETE',
     'SIMA.9R7.7RM09S:REV_NUMBER_OBSOLETE',
     'SIMA.RE38.3RM31S:REV_NUMBER_OBSOLETE',
     'SIMA.RE38.3RM32S:REV_NUMBER_OBSOLETE',
     'SIMA.RE72.7LM19S:REV_NUMBER_OBSOLETE',
     'SIMA.RE72.7LM20S:REV_NUMBER_OBSOLETE',
     'SIMA.RE78.7RM22S:REV_NUMBER_OBSOLETE',
     'SIMA.RE78.7RM23S:REV_NUMBER_OBSOLETE',
     'SIMA.RR73.7LM05S:REV_NUMBER_OBSOLETE',
     'SIMA.RR77.7RM07S:REV_NUMBER_OBSOLETE',
     'SIMA.UJ33.3RM13S:REV_NUMBER_OBSOLETE',
     'SIMA.UJ33.3RM14S:REV_NUMBER_OBSOLETE',
     'SIMA.UJ76.7RM01S:REV_NUMBER_OBSOLETE',
     'SPS.BWS.41677.V_ROT.APP.IN:BU_NUMBERS',
     'SPS.BWS.41677.V_ROT.APP.OUT:BU_NUMBERS',
     'SPS.BWS.51995.H_ROT.APP.IN:BU_NUMBERS',
     'SPS.BWS.51995.H_ROT.APP.OUT:BU_NUMBERS',
     'SPSQC:LHC_NUMBER_OF_EXECUTED_BATCHES',
     'SPSQC:NUMBER_OF_BUNCHES_FLAT_TOP',
     'SPSQC:NUMBER_OF_INJECTIONS']


