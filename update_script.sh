#!/bin/bash
# You can update your crontab with the following command:
# crontab -e
# and add the following line to update the tagger every 15 minutes:
# */15 * * * *  /home/lumimod/fill-tagger/bblumi/update_script.sh
# set variable
export PATH_FILL_TAGGER=~/fill-tagger
source $PATH_FILL_TAGGER/miniconda/bin/activate
# kinit with keytab
kinit -f -r 5d -kt ~/$(whoami).keytab $(whoami)
cd  $PATH_FILL_TAGGER/bblumi
python python_script.py
git pull
git add  $PATH_FILL_TAGGER/bblumi/docs/dl2
git add  $PATH_FILL_TAGGER/bblumi/docs/fills
git commit -am 'Automatic update from update_script.sh'
git push
